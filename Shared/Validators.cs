using System;
using System.ComponentModel.DataAnnotations;

namespace FusionPOS
{
	public class NumberAttribute: ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			int num;
			return int.TryParse(value as string, out num);
		}
	}
	
	public class IpAddressAttribute: ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			return false; // XXX
		}
	}
	
	public class NetmaskAttribute: ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			return false; // XXX
		}
	}
	
	public class NipAttribute: ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			return false; // XXX
		}	
	}
	
	public class RegonAttribute: ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			return false; // XXX
		}
	}
}

