using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Gtk;
using FusionPOS.Widgets;
using FusionPOS.Keymaps;

namespace FusionPOS
{
	public class Keyboard: Gtk.EventBox
	{
		public Keyboard(Gtk.Window window)
		{
			Gtk.Button button;
			Gtk.ToggleButton toggle;
			
			m_Keyboard = new Widgets.Table(5, 12, true);
			m_Window = window;
			
			Keymap keymap = new Keymap();
			keymap.LoadFromFile("/Configuration/Keymaps/Default.xml");
			
			foreach (KeyEntry k in keymap.Keys) {
				Widgets.Button key = AttachButton(k.Value, k.Column, k.Column + 1, k.Row, k.Row + 1);
				key.Clicked += RegularKeyClicked;
			}
			
			/* Backspace */
			button = AttachButton("Bksp", 10U, 11U, 0U, 1U);
			button.Clicked += delegate(object sender, EventArgs e) {
				EmitIfPossible(Gdk.Key.BackSpace);
			};	
		
			/* Spacebar */
			button = AttachButton(new KeyValue(Gdk.Key.space), 1U, 8U, 4U, 5U);
			button.Clicked += RegularKeyClicked;
			
			/* Alt */
			toggle = AttachToggle("Alt", 8U, 9U, 4U, 5U);
			toggle.Toggled += AltToggled;
		
			/* Shift */
			toggle = AttachToggle("Shift", 0U, 1U, 3U, 4U);
			toggle.Toggled += ShiftToggled;
			
			/* Enter */
			button = AttachButton("Enter", 10U, 11U, 1U, 3U);
			button.Clicked += delegate(object sender, EventArgs e) {
				EmitIfPossible(Gdk.Key.Return);
			};
			
			/* Left arrow */
			button = AttachImageButton("", "arrow-left-3", 9U, 10U, 4U, 5U);
			button.Clicked += (sender, e) => {
				EmitIfPossible(Gdk.Key.Left);
			};
			
			/* Right arrow */
			button = AttachImageButton("", "arrow-right-3", 10U, 11U, 4U, 5U);
			button.Clicked += (sender, e) => {
				EmitIfPossible(Gdk.Key.Right);
			};	
			
			/* Hide button */
			button = AttachImageButton("Ukryj", "arrow-down-double-3", 11U, 12U, 4U, 5U);
			button.Clicked += delegate(object sender, EventArgs e) {
				Hide();
			};
			
			Add(m_Keyboard);
			RenderKeyLabels(false, false);
		}
		
		private Widgets.Button AttachButton(string label, uint left, uint right, uint top, uint bottom)
		{
			Widgets.Button button = new Widgets.Button(label);
			button.CanFocus = false;
			button.CanDefault = false;
			button.SetSizeRequest(0, 0);
			m_Keyboard.Attach(button, left, right, top, bottom);
			return button;	
		}
		
		private Gtk.Button AttachImageButton(string label, string image, uint left, uint right, 
		    uint top, uint bottom)
		{
			Gtk.Button button = Widgets.ImageButtonFactory.ImageButton(label, image);
			button.CanFocus = false;
			button.CanDefault = false;
			button.SetSizeRequest(0, 0);
			m_Keyboard.Attach(button, left, right, top, bottom);
			return button;
		}
		
		private Gtk.ToggleButton AttachToggle(string label, uint left, uint right, uint top, uint bottom)
		{
			Gtk.ToggleButton button = new Gtk.ToggleButton(label);
			button.CanFocus = false;
			button.CanDefault = false;
			button.SetSizeRequest(0, 0);
			m_Keyboard.Attach(button, left, right, top, bottom);
			return button;
		}
		
		private Widgets.Button AttachButton(KeyValue val, uint left, uint right, uint top, uint bottom)
		{
			Widgets.Button button = new Widgets.Button();
			button.Data["KeyValue"] = val;
			button.CanFocus = false;
			button.CanDefault = false;
			button.SetSizeRequest(0, 0);
			m_Keyboard.Attach(button, left, right, top, bottom);
			return button;
		}
		
		private void RenderKeyLabels(bool shift, bool alt)
		{
			foreach (Gtk.Widget widget in m_Keyboard.Children) {
				if (!(widget is Widgets.Button))
					continue;
				
				Widgets.Button b = widget as Widgets.Button;
				
				if (b.Data.ContainsKey("KeyValue"))
					b.Label = ((KeyValue)b.Data["KeyValue"]).ToString(shift, alt);
			}
		}
		
		private void EmitIfPossible(Gdk.Key key)
		{
			if (m_Window.Focus != null)
				m_Window.Focus.EmitKey(key);
		}
	
		private void RegularKeyClicked(object s, EventArgs args)
		{
			Gtk.Button key = (Gtk.Button)s;
			EmitIfPossible(((KeyValue)key.Data["KeyValue"]).ToKey(m_ShiftOn, m_AltOn));
		}
		
		private void SpaceClicked(object s, EventArgs args)
		{

		}
		
		private void ShiftToggled(object s, EventArgs args)
		{
			m_ShiftOn = (s as Gtk.ToggleButton).Active;
			RenderKeyLabels(m_ShiftOn, m_AltOn);
		}
		
		private void AltToggled(object s, EventArgs args)
		{
			m_AltOn = (s as Gtk.ToggleButton).Active;
			RenderKeyLabels(m_ShiftOn, m_AltOn);
		}
		
		private Gtk.Window m_Window;
		private Widgets.Table m_Keyboard;
		private bool m_ShiftOn;
		private bool m_AltOn;
	}
}

