using System;
using System.Collections.Generic;
using System.ServiceModel;
using FusionPOS.Domain;

namespace FusionPOS.Services
{
	[ServiceContract(SessionMode = SessionMode.Allowed)]
	public interface IDeviceControlService
	{
		[OperationContract]
		void LockSales();
		[OperationContract]
		void UnlockSales();
		[OperationContract]
		void ReloadConfiguration();
		[OperationContract]
		void SendMessage(string message);
		[OperationContract]
		User GetLoggedIn();
		[OperationContract]
		IList<User> GetUsersAtWork();
		[OperationContract]
		IList<Order> GetActiveOrders();
	}
}
