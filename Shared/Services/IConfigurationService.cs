using System;
using System.ServiceModel;
using FusionPOS.Config;

namespace FusionPOS.Services
{
	[ServiceContract(SessionMode = SessionMode.Allowed)]
	public interface IConfigurationService
	{
		[OperationContract]
		string ReadSection(string name);
	}
}

