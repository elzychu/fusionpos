using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.ServiceModel;

namespace FusionPOS.Services
{
	[DataContract]
	public class ServiceInfo
	{
		[DataMember]
		public string ContractName;
		[DataMember]
		public string Uri;
	}
	
	[ServiceContract(SessionMode = SessionMode.Allowed)]
	public interface ISystemInfoService
	{
		[OperationContract]
		bool CheckStatus();
		[OperationContract]
		DateTime GetBootupTime();
		[OperationContract]
		string GetSystemVersion();
		[OperationContract]
		string GetSoftwareVersion();
		[OperationContract]
		[ServiceKnownType(typeof(ServiceInfo))]
		List<ServiceInfo> DiscoveryServices();
	}
}

