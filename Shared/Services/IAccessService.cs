using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace FusionPOS.Services
{
	[ServiceContract(SessionMode = SessionMode.Allowed)]
	public interface IAccessService
	{
		[OperationContract]
		bool IsOnline();
		[OperationContract]
		bool IsDeviceOnline(string hostname);
		[OperationContract]
		string SystemVersion();
		[OperationContract]
		List<ServiceInfo> GetServices(string hostname);
	}
}

