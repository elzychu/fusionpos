using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using FusionPOS.Domain;

namespace FusionPOS.Services
{
	public class ServerException: Exception
	{ 
		public ServerException(string msg): base(msg) 
		{ }
	}
	public class OrderException: ServerException
	{ 
		public OrderException(string msg): base(msg)
		{ }
	}
	
	public class SettlementException: ServerException
	{ 
		public SettlementException(string msg): base(msg)
		{ }
	}
	
	public class NoLastSettlementException: SettlementException
	{ 
		public NoLastSettlementException(string msg): base(msg)
		{ }
	}
	
	public class DatabaseDamaged: ServerException
	{
		public DatabaseDamaged(string msg): base(msg)
		{ }
	}
	
	[ServiceContract(SessionMode = SessionMode.Allowed)]
	public interface ITransactionService
	{
		[OperationContract]
		void MakeOrder(TransactionDTO order, bool print);
		[OperationContract]
		void UpdateOrder(TransactionDTO order, bool print);
		[OperationContract]
		void FinishOrder(TransactionDTO order, bool receipt);
		[OperationContract]
		void CancelOrder(TransactionDTO order);
		[OperationContract]
		void DoPayment(decimal value, long userId);
		[OperationContract]
		void NewSettlement();
		[OperationContract]
		void SaveWaiterSettlement(decimal value, long userId);
		[OperationContract]
		Settlement GetCurrentSettlement();
		[OperationContract]
		void CloseSettlement(long userId);
		[OperationContract]
		void PrintInvoice(Kitchen kitchen, OutgoingInvoiceDTO invoice);
	}
}

