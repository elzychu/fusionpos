using System;
using System.ServiceModel;

namespace FusionPOS.Services
{
	public class LifecycleException: ServerException
	{
		public LifecycleException(string msg): base(msg) 
		{ }
	}
	
	[ServiceContract(SessionMode = SessionMode.Allowed)]
	public interface ILifecycleService
	{
		[OperationContract]
		void Login(string hostname, string lifecycle);
		[OperationContract]
		void Logout(string hostname);
		[OperationContract]
		void Ping();
	}
}

