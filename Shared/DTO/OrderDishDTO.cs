using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NHibernate;
using FusionPOS.Domain;

namespace FusionPOS.Services
{
	[DataContract(IsReference = true)] [Serializable]
	public class OrderDishDTO
	{
		public OrderDishDTO ()
		{
		}
		
		public void Pack(OrderDish od)
		{
			this.Id = od.Id;
			this.OrderId = od.Order != null ? od.Order.Id : 0;
			this.DishId = od.Dish != null  ? od.Dish.Id : 0;
			this.ParentId = od.Parent != null ? od.Parent.Id : 0;
			this.Amount = od.Amount;
			this.DirectionId = od.Direction != null ? od.Direction.Id : 0;
			this.Modifications = new List<OrderDishModificationDTO>();
			
			if(od.Modifications != null)
				od.Modifications.Each(i => {
					var odm = new OrderDishModificationDTO();
					odm.Pack(i);
					this.Modifications.Add(odm);
				});
		}
		
		public OrderDish Unpack(ISession ses)
		{
			OrderDish result = new OrderDish();
			result.Id = this.Id;
			result.Order = ses.Get<Order>(this.OrderId);
			result.Dish = ses.Get<Dish>(this.DishId);
			//result.Dish.Kitchen = ses.Load("Kitchen", result.Dish.Kitchen.Id != 0 ? result.Dish.Kitchen.Id : 1) as Kitchen;
			result.Parent = ses.Get<OrderDish>(this.ParentId);
			result.Amount = this.Amount;
			result.Direction = ses.Get<Kitchen>(this.DirectionId);
			result.Modifications = new List<OrderDishModification>();
			
			this.Modifications.Each(i => result.Modifications.Add (i.Unpack(ses)));
			
			return result;
		}
		
		[DataMember] public long Id { get; set; }
		[DataMember] public long OrderId { get; set; }
		[DataMember] public long DishId { get; set; }
		[DataMember] public long ParentId { get; set; }
		[DataMember] public decimal Amount { get; set; }
		[DataMember] public long DirectionId { get; set; }
		[DataMember] public IList<OrderDishModificationDTO> Modifications { get; set; }
	}
}

