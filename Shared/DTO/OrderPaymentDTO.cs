using System;
using System.Runtime.Serialization;
using NHibernate;
using FusionPOS.Domain;

namespace FusionPOS.Services
{
	[DataContract(IsReference = true)] [Serializable]
	public class OrderPaymentDTO
	{
		public OrderPaymentDTO ()
		{
		}
		
		public void Pack(OrderPayment op)
		{
			this.Id = op.Id;
			this.OrderId = op.Order != null ? op.Order.Id : 0;
			this.Method = op.Method;
			this.Amount = op.Amount;
		}
		
		public OrderPayment Unpack(ISession ses)
		{
			OrderPayment result = new OrderPayment();
			result.Id = this.Id;
			result.Order = ses.Get<Order>(this.OrderId);
			result.Method = this.Method;
			result.Amount = this.Amount;
			
			return result;
		}
		
		[DataMember] public long Id { get; set; }
		[DataMember] public long OrderId { get; set; }
		[DataMember] public PaymentMethod Method { get; set; }
		[DataMember] public decimal Amount { get; set; }
	}
}

