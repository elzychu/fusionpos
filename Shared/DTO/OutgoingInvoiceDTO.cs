using System;
using FusionPOS.Domain;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using System.Runtime.Serialization;

namespace FusionPOS.Services
{
	[DataContract(IsReference = true)] [Serializable]
	public class OutgoingInvoiceDTO
	{
		public OutgoingInvoiceDTO ()
		{
		}
		
		public void Pack(OutgoingInvoice invoice)
		{
			this.Id = invoice.Id;
			this.Number = invoice.Number;
			this.IssueTime = invoice.IssueTime;
			this.SellTime = invoice.SellTime;
			this.BuyerName = invoice.BuyerName;
			this.BuyerAddress = invoice.BuyerAddress;
			this.BuyerNip = invoice.BuyerNip;
			this.PaymentMethod = invoice.PaymentMethod;
			this.BankName = invoice.BankName;
			this.BankAccount = invoice.BankAccount;
			this.PaymentTime = invoice.PaymentTime;
			this.ComponentsId = new List<long>();
			invoice.Components.Each(p => this.ComponentsId.Add(p.Id));
		}
		
		public OutgoingInvoice Unpack(ISession ses)
		{
			OutgoingInvoice invoice = new OutgoingInvoice();
			invoice.Id = this.Id;
			invoice.Number = this.Number;
			invoice.IssueTime = this.IssueTime;
			invoice.SellTime = this.SellTime;
			invoice.BuyerName = this.BuyerName;
			invoice.BuyerAddress = this.BuyerAddress;
			invoice.BuyerNip = this.BuyerNip;
			invoice.PaymentMethod = this.PaymentMethod;
			invoice.BankName = this.BankName;
			invoice.BankAccount = this.BankAccount;
			invoice.PaymentTime = this.PaymentTime;
			invoice.Components = new List<OutgoingInvoiceComponent>();
			this.ComponentsId.Each(p => invoice.Components.Add(
			    ses.Get<OutgoingInvoiceComponent>(p)));
			return invoice;
		}
		
		[DataMember] public long Id { get; set; }
		[DataMember] public string Number { get; set; }
		[DataMember] public DateTime IssueTime { get; set; }
		[DataMember] public DateTime SellTime { get; set; }
		[DataMember] public string BuyerName { get; set; }
		[DataMember] public string BuyerAddress { get; set; }
		[DataMember] public string BuyerNip { get; set; }
		[DataMember] public PaymentMethod PaymentMethod { get; set; }
		[DataMember] public string BankName { get; set; }
		[DataMember] public string BankAccount { get; set; }
		[DataMember] public DateTime PaymentTime { get; set; }
		[DataMember] public IList<long> ComponentsId { get; set; }
	}
}

