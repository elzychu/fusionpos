using System;
using System.Runtime.Serialization;
using FusionPOS.Domain;
using NHibernate;

namespace FusionPOS.Services
{
	[DataContract(IsReference = true)] [Serializable]
	public class DestinationDTO
	{
		public DestinationDTO ()
		{
		}
		
		public void Pack(Destination destination)
		{
			this.Id = destination.Id;
			this.OrderId = destination.Order.Id;
			this.Description = destination.Description;
			this.Type = destination.GetType().Name;
			
			if (destination is DestinationDelivery) {
				this.Address = (destination as DestinationDelivery).Address;
				this.DeliveryTime = (destination as DestinationDelivery).DeliveryTime;
				//this.SupplierId = (destination as DestinationDelivery).Supplier.Id;
			}
			
			if (destination is DestinationOnSite)
				this.TableId = (destination as DestinationOnSite).Table.Id;
		}
		
		public Destination Unpack(ISession ses)
		{
			Destination result = new DestinationNone();
			
			if (this.Type == typeof(DestinationDelivery).Name) {
				DestinationDelivery delivery = new DestinationDelivery();
				delivery.Address = this.Address;
				delivery.DeliveryTime = this.DeliveryTime;
				//delivery.Supplier = ses.Get<User>(this.SupplierId);
				result = delivery;
			}
			
			if (this.Type == typeof(DestinationOnSite).Name) {
				DestinationOnSite onsite = new DestinationOnSite();
				onsite.Table = ses.Get<Table>(this.TableId);
				result = onsite;
			}
			
			result.Description = this.Description;
			result.Order = ses.Get<Order>(this.OrderId);
			result.Id = this.Id;
			return result;
		}
		
		[DataMember] public long Id { get; set; }
		[DataMember] public long OrderId { get; set; }
		[DataMember] public string Description { get; set; }
		[DataMember] public long TableId { get; set; }
		[DataMember] public string Address { get; set; }
		[DataMember] public DateTime DeliveryTime { get; set; }
		[DataMember] public long SupplierId { get; set; }
		[DataMember] public string Type {get; set;}
	}
}

