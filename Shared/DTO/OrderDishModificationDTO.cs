using System;
using System.Runtime.Serialization;
using NHibernate;
using FusionPOS.Domain;

namespace FusionPOS.Services
{
	[DataContract(IsReference = true)] [Serializable]
	public class OrderDishModificationDTO
	{
		public OrderDishModificationDTO ()
		{
		}
		
		public void Pack(OrderDishModification odm)
		{
			this.Id = odm.Id;
			this.TargetId = odm.Target != null ? odm.Target.Id : 0;
			this.IngredientId = odm.Ingredient != null ? odm.Ingredient.Id : 0;
			this.Amount = odm.Amount;
			this.PriceChange = odm.PriceChange;
		}
		
		public OrderDishModification Unpack(ISession ses)
		{
			OrderDishModification result = new OrderDishModification();
			result.Id = this.Id;
			result.Target = ses.Get<OrderDish>(this.TargetId);
			result.Ingredient = ses.Get<RecipeIngredient>(this.IngredientId);
			result.Amount = this.Amount;
			result.PriceChange = this.PriceChange;
			return result;
		}
		
		[DataMember] public long Id { get; set; }
		[DataMember] public long TargetId { get; set; }
		[DataMember] public long IngredientId { get; set; }
		[DataMember] public int Amount { get; set; }
		[DataMember] public decimal PriceChange { get; set; }
	}
}

