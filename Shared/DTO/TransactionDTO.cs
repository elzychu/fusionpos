using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using NHibernate;
using FusionPOS.Domain;

namespace FusionPOS.Services
{
	[DataContract(IsReference = true)] [Serializable]
	public class TransactionDTO
	{
		public TransactionDTO ()
		{ }
		
		public void Pack(Transaction transaction)
		{
			this.Id = transaction.Id;
			this.TotalValue = transaction.TotalValue;
			this.WaiterId = transaction.Waiter != null ? transaction.Waiter.Id : 0;
			this.Time = transaction.Time;
			this.ClientId = transaction.Client != null ? transaction.Client.Id : 0;
			this.Type = transaction.GetType().Name;
			
			if(transaction is Order)
			{
				Order order = transaction as Order;
				if(order.Destination != null) {
					this.Destination = new DestinationDTO();
					this.Destination.Pack(order.Destination);
				}
				this.Status = order.Status;
				this.Description = order.Description;
				this.RebateAmount = order.RebateAmount;
				this.RebatePercent = order.RebatePercent;
				this.InvoiceId = order.Invoice != null ? order.Invoice.Id : 0;
				this.Entries = new List<OrderDishDTO>();
				this.Payments = new List<OrderPaymentDTO>();
				
				if(order.Entries != null)
					order.Entries.Each(i => {
						var od = new OrderDishDTO();
						od.Pack(i);
						this.Entries.Add(od);
					});
				
				if(order.Payments != null)
					order.Payments.Each(i => {
						var op = new OrderPaymentDTO();
						op.Pack(i);
						this.Payments.Add(op);
					});
			}
		}
		
		public Transaction Unpack(ISession ses)
		{
			Transaction tr = null;
			
			if(this.Type == typeof(Order).Name)
			{
				Order order = new Order();
				order.Destination = this.Destination.Unpack(ses);
				order.Status = this.Status;
				order.Description = this.Description;
				order.RebateAmount = this.RebateAmount;
				order.RebatePercent = this.RebatePercent;
				order.Invoice = ses.Get<OutgoingInvoice>(this.InvoiceId);
				order.Entries = new List<OrderDish>();
				this.Entries.Each(i => order.Entries.Add(i.Unpack(ses)));
				order.Payments = new List<OrderPayment>();
				this.Payments.Each(i => order.Payments.Add(i.Unpack(ses)));
				
				tr = order;
			}
			
			if (tr == null)
				tr = new Payment();
			
			tr.Id = this.Id;
			tr.TotalValue = this.TotalValue;
			tr.Waiter = ses.Get<User>(this.WaiterId);
			tr.Time = this.Time;
			tr.Client = ses.Get<Client>(this.ClientId);
			return tr;
		}
		
		[DataMember] public long Id { get; set; }
		[DataMember] public decimal TotalValue { get; set; }
		[DataMember] public long WaiterId { get; set; }
		[DataMember] public DateTime Time { get; set; }
		[DataMember] public long ClientId { get; set; }
		[DataMember] public DestinationDTO Destination { get; set; }
		[DataMember] public OrderStatus Status { get; set; }
		[DataMember] public string Description { get; set; }
		[DataMember] public decimal RebateAmount { get; set; }
		[DataMember] public decimal RebatePercent { get; set; }
		[DataMember] public long InvoiceId { get; set; }
		[DataMember] public IList<OrderDishDTO> Entries { get; set; }
		[DataMember] public IList<OrderPaymentDTO> Payments { get; set; }
		[DataMember] public string Type { get; set; }
	}
}