using System;
using FusionPOS.Translations;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public class LoginPad: Widgets.VBox
	{
		public LoginPad(): base(false, 10)
		{
			Widgets.Table table = new Widgets.Table(4, 3, true);
			Widgets.Button[] keys = new Widgets.Button[12];
			Widgets.Label introduction = new Widgets.Label("login.enter_pin_label".Tr(), FontSettings.BiggerFont);
			m_PIN = new Widgets.TextBox();
			m_PIN.StyleContext.AddClass("BiggerFont");
			table.RowSpacing = 10;
			table.ColumnSpacing = 10;
			
			for (uint i = 0; i < 9; i++) {
				keys[i] = new Widgets.Button((i+1).ToString(), FontSettings.BiggerFont, 120, 80);
				keys[i].Clicked += (o, sender) => m_PIN.Text += (o as Widgets.Button).Label;
				
				table.Attach(keys[i],
				   i % 3,
				   i % 3 + 1,
				   i / 3 ,
				   i / 3 + 1);
				
			}
			
			keys[09] = new Widgets.Button("0", FontSettings.BiggerFont, 120, 80);
			keys[09].Clicked += (sender, o) => m_PIN.Text += "0";
			table.Attach(keys[09], 0, 1, 3, 4);
			
			keys[10] = new Widgets.Button("<", FontSettings.BiggerFont, 120, 80);
			keys[10].Clicked += (sender, o) => m_PIN.Text = m_PIN.Text.Substring(1);
			table.Attach(keys[10], 1, 2, 3, 4);
			
			keys[11] = new Widgets.Button("OK", FontSettings.BiggerFont, 120, 80);
			keys[11].Clicked += (sender, o) => LoginClicked(this, o);
			
			table.Attach(keys[11], 2, 3, 3, 4);
			PackFill(introduction);
			PackFill(m_PIN);
			PackExpand(table);
			ShowAll();
		}
		
		public string Value
		{
			get {
				return m_PIN.Text;
			}
		}
		
		public event EventHandler LoginClicked;
		
		private Widgets.TextBox m_PIN;
	}
}

