using System;
using System.Collections.Generic;
using System.Linq;
using FusionPOS;

namespace FusionPOS.Widgets
{
	public class BrickTable: Widgets.Table
	{
		public BrickTable(uint width, uint height): base(height, width, true)
		{
			m_Bricks = new List<Gtk.Widget>();
			m_ScrollBack = new Gtk.Button(new Gtk.Image(Gtk.Stock.GoBack, Gtk.IconSize.Button));
			m_ScrollBack.Sensitive = false;
			m_ScrollBack.Clicked += (o, args) => ScrollBack();
			m_ScrollForward = new Gtk.Button(new Gtk.Image(Gtk.Stock.GoForward, Gtk.IconSize.Button));
			m_ScrollForward.Sensitive = false;
			m_ScrollForward.Clicked += (o, args) => ScrollForward();
			m_CurrentPage = 0;
			m_PageCapacity = width * (height - 1);
		}
		
		public void AddBrick(Gtk.Widget brick)
		{
			m_Bricks.Add(brick);
			Rebuild();
		}
		
		public void AddBrickList(IList<Gtk.Widget> bricks)
		{
			bricks.Each(w => m_Bricks.Add(w));
			Rebuild();
		}
		
		public void RemoveBrick(Gtk.Widget brick)
		{
			m_Bricks.Remove(brick);
			Rebuild();
		}
		
		public void Rebuild()
		{
			/* Remove old bricks */
			Children.Each(w => Remove(w));
			
			/* Put new bricks */
			uint i = 0;
			m_Bricks.Skip((int)(m_CurrentPage * m_PageCapacity)).Take((int)m_PageCapacity).Each(w => {
				Attach(w,
				    i % NColumns, i % NColumns + 1,
				    i / NRows, i / NRows + 1);
				i++;
			});
			
			/* Put scroll buttons */
			Attach(m_ScrollBack, NColumns - 2, NColumns -1, NRows - 1, NRows);
			Attach(m_ScrollForward, NColumns - 1, NColumns, NRows - 1, NRows);

			/* Adjust scroll buttons */
			m_ScrollBack.Sensitive = m_CurrentPage != 0;
			m_ScrollForward.Sensitive = m_CurrentPage == m_Bricks.Count / m_PageCapacity;
		}
		
		public void ClearBricks()
		{
			m_Bricks.Clear();
			Rebuild();
		}
		
		public void ScrollBack()
		{
			CurrentPage--;
		}
		
		public void ScrollForward()
		{
			CurrentPage++;
		}
		
		public uint CurrentPage
		{
			get {
				return m_CurrentPage;
			}
			
			set {
				m_CurrentPage = value;
				Rebuild();
			}
		}
		
		private uint m_PageCapacity;
		private uint m_CurrentPage;
		private Gtk.Button m_ScrollBack;
		private Gtk.Button m_ScrollForward;
		private IList<Gtk.Widget> m_Bricks;
	}
}

