using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using FusionPOS.Domain;

namespace FusionPOS
{
	public static class StringExtensions
	{
		public static string FirstUpper(this string s)
		{
			if (string.IsNullOrEmpty(s))
				return string.Empty;
			
			return char.ToUpper(s[0]) + s.Substring(1);
		}
		
		public static string FirstLower(this string s)
		{
			if (string.IsNullOrEmpty(s))
				return string.Empty;
			
			return char.ToLower(s[0]) + s.Substring(1);
		}

		public static string CamelCaseToDashedCase(this string s)
		{
			s = s.FirstLower();
			return Regex.Replace(s, @"[a-z][A-Z]+", 
			    m => string.Format("{0}-{1}", m.Value[0], char.ToLower(m.Value[1])));
		}
		
		public static string DashedCaseToCamelCase(this string s)
		{
			s = s.FirstUpper();
			return Regex.Replace(s, @"[a-z]-[a-z]",
			    m => string.Format("{0}{1}", m.Value[0], char.ToUpper(m.Value[1])));
		}
		
		public static string Multiply(this string s, int count)
		{
			string result = "";
			for(int i=0; i<count; ++i)
				result += s;
			return result;
		}
		
		public static string Multiply(this char s, int count)
		{
			string result = "";
			for(int i=0; i<count; ++i)
				result += s;
			return result;
		}
	}
	
	public static class DecimalExtensions
	{
		public static string FormatPrice(this decimal self)
		{
			return self.ToString("0.00");
		}
	}
	
	public static class ListExtensions
	{
		public static void RemoveDuplicates<TSource, TKey>(this IList<TSource> source, Func<TSource, TKey> keySelector)
		{
			HashSet<TKey> knownKeys = new HashSet<TKey>();
			foreach (TSource element in new List<TSource>(source)) {
				if (!knownKeys.Add(keySelector(element)))
					source.Remove(element);
			}
		}
	}
	
	public static class DictionaryExtensions
	{
		public static void AddOverwrite<K, V>(this IDictionary<K, V> self, KeyValuePair<K, V> val)
		{
			if (self.ContainsKey(val.Key))
				self.Remove(val.Key);
			
			self.Add(val);
		}
		
		public static void AddOverwrite<K, V>(this IDictionary<K, V> self, K key, V val)
		{
			self.AddOverwrite(new KeyValuePair<K, V>(key, val));
		}
	}
	
	public static class EachExtension  
	{  
		public static void Each<T>(this IEnumerable<T> enumberable, Action<T> action)
		{  
			foreach (var item in enumberable)  
				action(item);  
		}  
	}
	
	public static class ClassifyExtension
	{
		public static IEnumerable<IEnumerable<TSource>> Classify<TSource, TKey>(this IList<TSource> source, Func<TSource, TKey> keySelector)
		{
			List<List<TSource>> result = new List<List<TSource>>();
			foreach(TSource element in source)
			{				
				TKey k = keySelector(element);
				bool flag=false;
				foreach(List<TSource> list in result)
				{
						if( keySelector(list.First()).Equals(k))
						{
							list.Add(element);
							flag=true;
							break;
						}
				}
				
				if(!flag)
					result.Add(new List<TSource>() {element});	   
			}
			
			return result;
		}
	}
	
	public static class EmitEventExtension
	{
		[StructLayout(LayoutKind.Sequential)] 
		public struct EventKeyStruct 
		{ 
			public Gdk.EventType type; 
			public IntPtr window; 
			public SByte send_event; 	
			public uint time; 
			public uint state; 
			public uint keyval; 
			public uint length; 
			public string str; 
			public ushort hardware_keycode; 
			public byte group; 
			public uint is_modifier;     
		} 
		
		public static void EmitKey(this Gtk.Widget widget, Gdk.Key key)
		{
			uint keyval = (uint)key; 
			Gdk.Window window = widget.GdkWindow;
			Gdk.KeymapKey[] keymap=Gdk.Keymap.Default.GetEntriesForKeyval(keyval);
			EventKeyStruct native = new EventKeyStruct(); 
			
			native.type = Gdk.EventType.KeyPress;
			native.window = window.Handle; 
			native.send_event = 1; 
			native.state = 0;
			native.keyval = keyval; 
			native.length = 0; 
			native.str = null; 
			
			if (keymap.Length > 0) {
				native.hardware_keycode = (ushort)keymap[0].Keycode;
                                native.group = (byte)keymap[0].Group; 
			}
			     
			IntPtr ptr = GLib.Marshaller.StructureToPtrAlloc(native); 
			try { 
			        Gdk.EventKey evnt = new Gdk.EventKey(ptr); 
			        Gdk.EventHelper.Put(evnt); 
			} finally { 
			        GLib.Marshaller.Free(ptr); 
			} 
		}
	}
	
	public static class StyleContextExtensions
	{
		public static void ParseClasses(this Gtk.StyleContext context, string spec)
		{
			spec.Split(',').Each(i => context.AddClass(i));
		}
	}
	
	public class Utils
	{
		public delegate void Delegate();
		
		public static object ReadProperty(object obj, string property)
		{
			object current = obj;
			PropertyInfo prop;
			
			foreach (string i in property.Split('.')) {
				prop = current.GetType().GetProperty(i);
				current = prop.GetValue(current, null);
			}
			
			return current;
		}
		
		public static IEnumerable<T> IterateHourly<T>(DateTime start, DateTime end, int hours, Func<DateTime, DateTime, T> selector)
		{
			DateTime iterator = start;
			
			while (iterator < end) {
				yield return selector(iterator, iterator.AddHours(hours));
				iterator = iterator.AddHours(hours);
			}
		}

		public static void AsyncInvoke<T>(Func<T> async, Action<T> result)
		{
			AsyncCallback callback = delegate(IAsyncResult ar) {
				result((ar.AsyncState as Func<T>).EndInvoke(ar));
			};
			
			async.BeginInvoke(callback, async);
		}
	}
}

