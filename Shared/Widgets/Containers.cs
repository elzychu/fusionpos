using System;

namespace FusionPOS.Widgets
{
	public class VBox: Gtk.VBox
	{
		public VBox(): base()
		{
			Spacing = 2;
		}
		
		public VBox(bool homogenous, int spacing): base(homogenous, spacing)
		{
		}
		
		public void PackExpand(Gtk.Widget child)
		{
			base.PackStart(child, true, true, 0);
		}
		
		public void PackFill(Gtk.Widget child)
		{
			base.PackStart(child, false, true, 0);
		}
	}
	
	public class HBox: Gtk.HBox
	{
		public HBox(): base()
		{
			Spacing = 2;
		}
		
		public HBox(bool homogenous, int spacing): base(homogenous, spacing)
		{
		}

		public void PackExpand(Gtk.Widget child)
		{
			base.PackStart(child, true, true, 0);
		}
		
		public void PackFill(Gtk.Widget child)
		{
			base.PackStart(child, false, true, 0);
		}
	}
	
	public class Table: Gtk.Table
	{
		public Table(uint rows, uint cols, bool homogenous): base(rows, cols, homogenous)
		{
			RowSpacing = 2;
			ColumnSpacing = 2;
		}
		
		public void AttachFill(Gtk.Widget widget, uint left, uint right, uint top, uint bottom)
		{
			Attach(widget, left, right, top, bottom, 
			    Gtk.AttachOptions.Fill,
			    Gtk.AttachOptions.Fill, 
			    0, 0);
		}
		
		public void AttachExpand(Gtk.Widget widget, uint left, uint right, uint top, uint bottom, bool x, bool y)
		{
			Attach(widget, left, right, top, bottom, 
			    Gtk.AttachOptions.Fill | (x ? Gtk.AttachOptions.Expand : 0),
			    Gtk.AttachOptions.Fill | (y ? Gtk.AttachOptions.Expand : 0), 
			    0, 0);			
		}
	}
	
	public class FormTable: Table
	{
		public FormTable(uint rows, int leftwidth = 320): base(rows, 2, false)
		{
			LeftWidth = leftwidth;
		}
		
		public void PackElement(Gtk.Widget left, Gtk.Widget right, uint row, bool expand = false)
		{
			Gtk.Alignment align = new Gtk.Alignment(1.0f, 0.5f, 0.0f, 0.0f);
			align.SetSizeRequest(LeftWidth, -1);
			align.Add(left);
			align.MarginRight = 20;
			AttachFill(align, 0, 1, row, row + 1);
			AttachExpand(right, 1, 2, row, row + 1, true, expand);
		}
		
		public int LeftWidth
		{
			get; set;
		}
	}
	
	public class Frame: Gtk.Frame
	{
		/*protected override bool OnDrawn(Cairo.Context cr)
		{
			StyleContext.RenderBackground(cr, 0, 0, Allocation.Width, Allocation.Height);
			return base.OnDrawn(cr);
		}*/
	}
	
	public class ButtonsBar: Widgets.HBox
	{
		public ButtonsBar(): base(false, 0)
		{
			m_LeftAlign = new Gtk.Alignment(0, 0, 0, 1);
			m_RightAlign = new Gtk.Alignment(1, 0, 0, 1);
			m_Left = new Widgets.HBox(true, 1);
			m_Right = new Widgets.HBox(true, 1);
			
			m_LeftAlign.Add(m_Left);
			m_RightAlign.Add(m_Right);
			PackExpand(m_LeftAlign);
			PackFill(m_RightAlign);
			
			SetSizeRequest(1020, 80);
		}
		
		public void AddLeftButton(string label, EventHandler handler)
		{
			Widgets.Button button = new Widgets.Button(label);
			button.Clicked += handler;
			button.SetWrap(10);
			button.Show();
			m_Left.PackFill(button);
		}
		
		public void AddLeftImageButton(string label, string imageName, EventHandler handler)
		{
			Gtk.Button button = Widgets.ImageButtonFactory.ImageButton(label, imageName);
			button.Clicked += handler;
			button.Show();
			m_Left.PackFill(button);
		}
		
		public void AddRightImageButton(string label, string imageName, EventHandler handler)
		{
			Gtk.Button button = Widgets.ImageButtonFactory.ImageButton(label, imageName);
			button.Clicked += handler;
			button.Show();
			m_Right.PackFill(button);
		}
		
		public void AddLeftButton(string stockId, Gtk.IconSize iconSize, EventHandler handler)
		{
			Widgets.Button button = new Widgets.Button(stockId, iconSize);
			button.Clicked += handler;
			button.Show();
			m_Left.PackFill(button);	
		}
		
		public void AddLeftButton(Gtk.Widget button)
		{
			m_Left.PackFill(button);
			button.Show();
		}
		
		public void AddRightButton(string label, EventHandler handler)
		{
			Widgets.Button button = new Widgets.Button(label);
			button.Clicked += handler;
			button.SetWrap(10);
			m_Right.PackFill(button);
		}
		
		public void AddRightButton(Gtk.Widget button)
		{
			m_Right.PackFill(button);
			button.Show();
		}
		
		public void AddRightToggle(string label, EventHandler handler)
		{
			Gtk.ToggleButton button = new Gtk.ToggleButton();
			button.Label = label;
			button.Clicked += handler;
			button.SetWrap(10);
			m_Right.PackStartSimple(button);
		}
		
		public void AddRightImageToggle(string label, string name, EventHandler handler)
		{
			Gtk.ToggleButton button = Widgets.ImageButtonFactory.ImageToggleButton(label, name);
			button.Clicked += handler;
			m_Right.PackStartSimple(button);
		}
		
		private Gtk.Alignment m_LeftAlign;
		private Gtk.Alignment m_RightAlign;
		private Widgets.HBox m_Left;
		private Widgets.HBox m_Right;
	}

}

