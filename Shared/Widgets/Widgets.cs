using System;
using Gtk;
using Pango;
using FusionPOS;
using System.IO;
using System.Reflection;

namespace FusionPOS.Widgets
{
	public static class ImageButtonFactory
	{
		public static Gtk.Button ImageButton(string name, string imageName)
		{
			Widgets.VBox vbox = new Widgets.VBox();
			vbox.PackExpand(ImageFromResource(imageName));
			
			if (name != "") {
				Gtk.Label label = new Gtk.Label(name);
				label.Wrap = true;
				vbox.PackStart(label, false, false, 2);
			}
			
			return new Gtk.Button(vbox);
		}
		
		public static Gtk.ToggleButton ImageToggleButton(string name, string imageName)
		{
			Widgets.VBox vbox = new Widgets.VBox();
			vbox.PackExpand(ImageFromResource(imageName));
			
			if (name != "") {
				Gtk.Label label = new Gtk.Label(name);
				label.Wrap = true;
				vbox.PackStart(label, false, false, 2);
			}
			
			Gtk.ToggleButton button = new Gtk.ToggleButton();
			button.Add(vbox);
			return button;
		}
		
		public static Gtk.Image ImageFromResource(string name)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			Stream imageStream = assembly.GetManifestResourceStream("Shared.Icons." + name + ".png");
			return new Gtk.Image(imageStream);
		}
	}
	
	public static class Extensions
	{
		public static void Resize(this Gtk.Widget w, int width, int height)
		{
			w.SizeAllocated += delegate(object o, SizeAllocatedArgs args) {
				int x = width < 0 ? args.Allocation.Width : width;
				int y = height < 0 ? args.Allocation.Height : height;
				w.SizeAllocate(new Gdk.Rectangle(0, 0, x, y));
			};
		}

		public static void SetWrap(this Gtk.Button w, int width)
		{
			Gtk.Label label = (Gtk.Label)w.Child;
			label.Justify = Justification.Center;
			label.Wrap = true; 
			label.LineWrap = true;
			label.LineWrapMode = Pango.WrapMode.Word;
			label.WidthChars = width;
		}
		
		public static void InverseColors(this Gtk.Widget w)
		{
			w.Name = "Inverted";
			//w.ModifyBg(StateType.Normal, new Gdk.Color(0, 0, 0));
			//w.ModifyFg(StateType.Normal, new Gdk.Color(255, 255, 255));
		}
		
		public static void ApplyWhiteTextColor(this Gtk.Container w)
		{
			foreach (Gtk.Widget i in w.Children)
				i.ModifyFg(StateType.Normal, new Gdk.Color(255, 255, 255));
		}
		
		public static Gtk.TreeIter AppendValuesRef(this Gtk.TreeStore store, Gtk.TreeIter? iter, params object[] args)
		{
			if (iter.HasValue)
				return store.AppendValues(iter.Value, args);
			
			return store.AppendValues(args);
		}
	
		public static void PackStartSimple(this Gtk.Box box, Gtk.Widget child)
		{
			box.PackStart(child, false, true, 0);
		}
	}
	
	public class ComboBox: Gtk.ComboBox
	{
		public ComboBox(String [] list): base(list)
		{ }
		
		public string RetrieveActive()
		{
			//xxx
			return "";
		}
		
		public void SetActive(string value)
		{
			//xxx
		}
	}
	
	public delegate void ValueSetterInt(int value);
	public delegate void ValueSetterFloat(decimal value);
	
	public class ValuePicker: TitleBox
	{
		public ValuePicker(decimal startValue, bool canDouble, Delegate setter): this(startValue, canDouble, setter, "Wpisz wartość") 
		{}
		
		public ValuePicker(decimal startValue, bool canDouble, Delegate setter, String text): base()
		{
			m_TitleBar.Text = text;
			m_Setter = setter;
			m_TextBox = new TextBox();
			m_TextBox.Text = startValue.ToString();
			m_TextBox.IsEditable = false;
			
			Button[] buttons = new Button[10];
			
			for (int i=1; i<10; i++)
			{
				buttons[i] = new Button(i.ToString());
				buttons[i].Clicked += delegate(object o, EventArgs x) 
				{
					if (m_TextBox.Text != "0")
						m_TextBox.Text += ((Button)o).Label;
					else
						m_TextBox.Text = ((Button)o).Label;
				};
			}
			
			Button button_0 = new Button("0");
			button_0.Clicked += delegate(object sender, EventArgs e) {
				if (m_TextBox.Text != "0")
					m_TextBox.Text += "0";
			};			
			
			Button button_c = new Button("c");
			button_c.Clicked += delegate(object sender, EventArgs e) {
				m_MemoryValue = 0;
				m_TextBox.Text = "0";
			};
			
			Button button_undo = new Button("<");
			button_undo.Clicked += delegate(object sender, EventArgs e) {
				if(m_TextBox.Text.Length > 1)
					m_TextBox.Text = m_TextBox.Text.Substring(0, m_TextBox.Text.Length-1);
				else
					m_TextBox.Text = "0";
			};
			
			Widgets.Table table = new Widgets.Table(4, 3, true);
			
			table.Attach(m_TextBox, 0, 3, 0, 1); 
			
			for(uint i=0, j=1;j<4;)
			{
				table.Attach(buttons[i+3*(j-1) + 1], i, i+1, j, j+1);
				
				++i;
				if(i==3)
				{
					j++;
					i=0;
				}
			}
			
			table.Attach (button_undo, 0, 1, 4, 5);
			table.Attach (button_0, 1, 2, 4, 5);
			table.Attach (button_c, 2, 3, 4, 5);
			
			Button button_ok = new Button("OK");
			
			button_ok.Clicked += delegate(object sender, EventArgs e) {
				
				if(canDouble)
					((ValueSetterFloat)m_Setter)(decimal.Parse(m_TextBox.Text));
				else
					((ValueSetterInt)m_Setter)(int.Parse(m_TextBox.Text));
				Hide();
			};
			
			Button button_cancel = new Button("Cancel");
			
			button_cancel.Clicked += delegate(object sender, EventArgs e) {
				Hide();
			};
			
			if (canDouble)
			{
				Button button_dot = new Button(",");
				button_dot.Clicked += delegate(object sender, EventArgs e) {
					m_TextBox.Text += ",";
				};
				table.Attach(button_dot, 1, 2, 5, 6);
			}
			
			table.Attach(button_ok, 0, 1, 5, 6);
			
			table.Attach(button_cancel, (uint)(canDouble?2:1), 3, 5, 6);
			table.SetSizeRequest(240, 400);
			
			m_Placement.Add(table);
			ShowAll();
		}
		
		public static decimal GetFloatValue(string text)
		{
			ValuePicker picker = new ValuePicker(0, true, new ValueSetterFloat(FloatSetter), text);
			
			while(!m_Returned)
				Gtk.Main.Iteration();
			m_Returned = false;
			return m_ResultFloat;
		}
		
		public static int GetIntValue(string text)
		{
			ValuePicker picker = new ValuePicker(0, false, new ValueSetterInt(IntSetter), text);
			
			while(!m_Returned)
				Gtk.Main.Iteration();
			m_Returned = false;
			return m_ResultInt;
		}
		
		static void IntSetter(int value)
		{
			m_ResultInt = value;
			m_Returned = true;
		}
		
		static void FloatSetter(decimal value)
		{
			m_ResultFloat = value;
			m_Returned = true;
		}
		
		static private bool m_Returned;
		static private decimal m_ResultFloat;
		static private int m_ResultInt;
		
		int m_MemoryValue;
		TextBox m_TextBox;
		Delegate m_Setter;
	}
	
	public class FloatEntry: Gtk.Entry
	{
		public FloatEntry(int precision): this(precision, 0, decimal.MinValue, decimal.MaxValue)
		{ }
		
		public FloatEntry(int precision, decimal startValue): this(precision, startValue, decimal.MinValue, decimal.MaxValue)
		{ }
		
		public FloatEntry(int precision, decimal startValue, decimal min, decimal max): base()
		{
			SetSizeRequest(60,-1);
			if(startValue>max || startValue<min)
				startValue = min;
			
			Text = startValue.ToString();
			m_Min = min;
			m_Max = max;
			m_Filtr = ".";
			for(int i=0;i<precision;++i)
				m_Filtr += "#";
			this.CanFocus = false;
		}
		
		public decimal GetDecimalValue()
		{
			try {
				return decimal.Parse(Text);
			} catch (ArgumentNullException ex) {
				return 0m;
			} catch (FormatException ex) {
				return 0m;
			}
		}
		
		public int GetIntValue()
		{
			int returnVal;
			int.TryParse(Text, out returnVal);
			
			return returnVal;
		}
		
		protected override bool OnButtonPressEvent(Gdk.EventButton evt)
		{
			ValuePicker vPicker = new ValuePicker(GetDecimalValue(), true, new ValueSetterFloat(SetValue));
			return base.OnButtonPressEvent(evt);
		}
		
		private void SetValue(decimal v)
		{
			if(v<=m_Max  && v>=m_Min)
				Text = v.ToString(m_Filtr);
			else
				MessageBox.Popup("FusionPOS", "Wartość z poza zakresu");
		}
		private string m_Filtr;
		private decimal m_Min;
		private decimal m_Max;
	}
	
	public class ValueEntry: Gtk.Entry
	{
		public ValueEntry(): this(0, int.MinValue, int.MaxValue)
		{ }
		
		public ValueEntry(int startValue): this(startValue, int.MinValue, int.MaxValue)
		{ }
		
		public ValueEntry(int startValue, int min, int max, string format = null): base()
		{
			WidthChars = 5;
			
			if(format != "")
				m_Format = format;
			else
				m_Format = null;
			
			if(startValue>max || startValue<min)
				startValue = min;
			
			Text = startValue.ToString();
			m_Min = min;
			m_Max = max;
			this.CanFocus = false;
		}
		
		public decimal GetFloatValue()
		{
			decimal returnVal;
			try {
				decimal.TryParse(Text, out returnVal);
			} catch (System.FormatException e)
			{
				return 0;	
			}
			return returnVal;
		}
		
		public int GetIntValue()
		{
			int returnVal;
			try {
				int.TryParse(Text, out returnVal);
			} catch (System.FormatException e)
			{
				return 0;	
			}
			return returnVal;
		}
		
		protected override bool OnButtonPressEvent(Gdk.EventButton evt)
		{
			try {
				ValuePicker vPicker = new ValuePicker(Int32.Parse(Text), false, new ValueSetterInt(SetValue));
			}
			catch (System.FormatException e)
			{
				ValuePicker vPicker = new ValuePicker(0, false, new ValueSetterInt(SetValue));
			}
			return base.OnButtonPressEvent(evt);
		}
		
		
		public void SetValue(int v)
		{
			SetValue ((decimal)v);	
		}
		
		public void SetValue(decimal v)
		{
			if(v<=m_Max && v>=m_Min)
			{
				Text = (m_Format == null) ? v.ToString() :
					v.ToString(m_Format);
			}
			else
				MessageBox.Popup("FusionPOS", "Wartość z poza zakresu");
		}
		
		private string m_Format;
		private int m_Min;
		private int m_Max;
	}
	
	public delegate void DateSetter(DateTime date);
	
	public class DatePicker: TitleBox
	{
		public DatePicker(DateSetter setter): this(DateTime.Now, setter)
		{ }
		
		public DatePicker(DateTime startValue, DateSetter setter): base()
		{
			m_TitleBar.Text = "Wprowadź datę";
			Gtk.Calendar calendar = new Gtk.Calendar();

			calendar.Date = startValue;
			
			Widgets.Button buttonOk = new Widgets.Button("Ok");
			buttonOk.Clicked += delegate(object sender, EventArgs e) {
				setter(calendar.Date);
				Hide();
			};
			
			Widgets.Button buttonCancel = new Widgets.Button("Cancel");
			buttonCancel.Clicked += delegate(object sender, EventArgs e) {
				Hide();
			};
			
			VBox mainBox = new VBox();
			HBox buttonBox = new HBox();
			
			mainBox.PackStartSimple(calendar);
			buttonBox.PackStartSimple(buttonOk);
			buttonBox.PackStartSimple(buttonCancel);
			
			mainBox.PackStartSimple(buttonBox);
			m_Placement.Add (mainBox);
			ShowAll();
		}
	}
	
	public class DateEntry: Gtk.Entry
	{
		public DateEntry(): this(DateTime.Now)
		{ }
		
		public DateEntry(DateTime startValue): base()
		{
			m_Date = startValue;
			if (startValue!=null)
				Text = startValue.ToString("dd/MM/yyyy");
			
			this.CanFocus = false;
		}
		
		protected override bool OnButtonPressEvent(Gdk.EventButton evt)
		{
			DatePicker datePicker = new DatePicker(m_Date, SetDate);
			return base.OnButtonPressEvent(evt);
		}
		
		public void SetDate(DateTime date)
		{
			m_Date = date;
			Text = date.ToString("dd/MM/yyyy");
		}
		
		public DateTime GetValue()
		{
			return m_Date;	
		}
		
		private DateTime m_Date;
	}

	public class Button: Gtk.Button
	{
		public Button(): base()
		{
			m_LongPressActive = false;
			FocusOnClick = false;
		}
	
		public Button(string text): this()
		{
			Label = text;
			SetSizeRequest(0, 0);
			(Child as Gtk.Label).Ellipsize = EllipsizeMode.Middle;
		}
		
		public Button(string text, string b, bool sw): this()
		{
			if(sw) {
				Label = text;
				StyleContext.ParseClasses(b);
				SetSizeRequest(0, 0);
				(Child as Gtk.Label).Ellipsize = EllipsizeMode.Middle;
			}
			else  {
				Assembly _assembly;
				Stream _imageStream;
				StreamReader _textStreamReader;

				_assembly = Assembly.GetExecutingAssembly();
				_imageStream = _assembly.GetManifestResourceStream("Shared.Icons." + b + ".png");
				
				Image = new Gtk.Image(_imageStream);
			}
		}
		
		public Button(string text, int w, int h): this(text)
		{
			SetSizeRequest(Math.Max(w, h), h);
		}
		
		public Button(string text, string style, int w, int h): this(text, style, true)
		{
			SetSizeRequest(Math.Max(w, h), h);
		}
		
		public Button(string stock_id, IconSize size): base(new Gtk.Image(stock_id, size))
		{	
			
		}
		
		public Button(string stock_id, IconSize size, int w, int h): this(stock_id, size)
		{
			SetSizeRequest(Math.Max(w, h), h);
		}
		
		public event EventHandler LongClicked;
		
		protected override bool OnButtonPressEvent(Gdk.EventButton evnt)
		{
			m_LongPressActive = true;
			GLib.Timeout.Add(500, delegate() {
				if (m_LongPressActive == true && LongClicked != null)
					LongClicked(this, new EventArgs());

				m_LongPressActive = false;
				return false;
			});

			return base.OnButtonPressEvent(evnt);
		}
		
		protected override bool OnButtonReleaseEvent(Gdk.EventButton evnt)
		{
			m_LongPressActive = false;
			return base.OnButtonReleaseEvent(evnt);
		}

		private bool m_LongPressActive;
	}
	
	public class ToggleButton: Gtk.ToggleButton
	{
		public ToggleButton(string label): base(label)
		{
		}
	}
	
	public class RadioButton: Gtk.RadioButton
	{
		public RadioButton(Gtk.RadioButton parent, string label): base(parent, label)
		{
			DrawIndicator = false;
		}
	}
	
	public class Label: Gtk.Label
	{
		public Label(): this("")
		{
		}
		
		public Label(string text): base()
		{
			Text = text;
			SetSizeRequest(0, 0);
			Ellipsize = EllipsizeMode.Middle;
			Wrap = true;
		}

		public Label(string text, string style): base()
		{
			Text = text;
			SetSizeRequest(0, 0);
			StyleContext.ParseClasses(style);
			Ellipsize = EllipsizeMode.Middle;
			Wrap = true;
		}
		
		public Label(string text, int w, int h): this(text)
		{
			SetSizeRequest(w, h);
			Ellipsize = EllipsizeMode.Middle;
		}
	}
	
	public class DynamicLabel: Label
	{
		public DynamicLabel(Func<string> renderer): base()
		{
			m_Renderer = renderer;	
		}
		
		public DynamicLabel(string style, Func<string> renderer): base(string.Empty,  style)
		{
			m_Renderer = renderer;
		}
		
		public void Refresh()
		{
			Markup = m_Renderer();	
		}
		
		public static void RefreshAll(Gtk.Container start)
		{
			start.Children.Each(obj => {
				if (obj is Gtk.Container)
					DynamicLabel.RefreshAll(obj as Gtk.Container);
				
				if (obj is DynamicLabel)
					(obj as DynamicLabel).Refresh();
			});
		}
		
		private Func<string> m_Renderer;
	}
	
	public class WindowLabel: Gtk.EventBox
	{		
		public WindowLabel(): this("")
		{
		}
		
		public WindowLabel(string text): base()
		{
			Add(new Widgets.Label(text));
		}		

		public WindowLabel(string text, string style): base()
		{
			StyleContext.ParseClasses(style);
			Add(new Widgets.Label(text, style));
		}
		
		public WindowLabel(string text, string style, int w, int h): this(text, style)
		{
			SetSizeRequest(w, h);
		}
		
		public WindowLabel(string text, Justification j): this(text)
		{
			(Child as Label).Justify = j;
		}
		
		
		public String Text
		{
			get { return ((Gtk.Label)Child).Text; }
			set { ((Gtk.Label)Child).Markup = value; }
		}
	}
	
	public class TextView: Gtk.ScrolledWindow
	{
		class InnerTextView: Gtk.TextView 
		{
				protected override bool OnFocusInEvent(Gdk.EventFocus evnt)
				{
					BaseWindow.getInstance().ShowKeyboard();
					return base.OnFocusInEvent(evnt);
				}
		
				protected override bool OnFocusOutEvent(Gdk.EventFocus evnt)
				{
					BaseWindow.getInstance().HideKeyboard();
					return base.OnFocusOutEvent(evnt);
				}
		}
		
		public TextView(): base()
		{
			m_TextView = new Widgets.TextView.InnerTextView();
			this.Add(m_TextView);
		}
		
		public string Text
		{
			get { return m_TextView.Buffer.Text; }
			set { m_TextView.Buffer.Text = value; }
		}
		
		Widgets.TextView.InnerTextView m_TextView;
	}
		
	public class TextBox: Gtk.Entry
	{
		public TextBox(): base()
		{
		}
		
		public TextBox(string value): base(value)
		{
			
		}
		
		public string GetText() {
			return Text;	
		}
		
		public TextBox(string value, bool visibility): this(value)
		{
			Visibility = visibility;
		}
		
		protected override bool OnFocusInEvent(Gdk.EventFocus evnt)
		{
			BaseWindow.getInstance().ShowKeyboard();
			return base.OnFocusInEvent(evnt);
		}
		
		protected override bool OnFocusOutEvent(Gdk.EventFocus evnt)
		{
			BaseWindow.getInstance().HideKeyboard();
			return base.OnFocusOutEvent(evnt);
		}
	}
	
	public class SimpleTreeViewColumn: Gtk.TreeViewColumn
	{
		public SimpleTreeViewColumn(string label, string field): base()
		{
			CellRenderer = new Gtk.CellRendererText();
			PackStart(CellRenderer, false);
			SetCellDataFunc(CellRenderer, RenderCell);
			Title = label;
			m_FieldName = field;
		}
		
		public Gtk.CellRendererText CellRenderer
		{
			get; set;
		}
		
		private void RenderCell(Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.TreeModel model, Gtk.TreeIter iter)
		{
			object obj = model.GetValue(iter, 0);
			(cell as Gtk.CellRendererText).Text = Utils.ReadProperty(obj, m_FieldName).ToString();
		}
		
		private string m_FieldName;
	}
	
	public class ModelTreeViewColumn: Gtk.TreeViewColumn
	{
		public ModelTreeViewColumn(string label, int column): base()
		{
			CellRenderer = new Gtk.CellRendererText();
			PackStart(CellRenderer, false);
			SetCellDataFunc(CellRenderer, RenderCell);
			Title = label;
			m_Column = column;
		}
		
		public Gtk.CellRendererText CellRenderer
		{
			get; set;
		}
		
		private void RenderCell(Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.TreeModel model, Gtk.TreeIter iter)
		{
			object obj = model.GetValue(iter, m_Column);
			(cell as Gtk.CellRendererText).Text = obj.ToString();
		}
		
		private int m_Column;
	}	
	
	public class TreeViewColumn: Gtk.TreeViewColumn
	{
		public delegate string RenderColumnDelegate(object model);
		
		public TreeViewColumn(string label, RenderColumnDelegate func): base()
		{
			CellRenderer = new Gtk.CellRendererText();
			PackStart(CellRenderer, false);
			SetCellDataFunc(CellRenderer, RenderCell);
			Title = label;
			m_Renderer = func;
		}
		
		public Gtk.CellRendererText CellRenderer
		{
			get; set;
		}
		
		private void RenderCell(Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.TreeModel model, Gtk.TreeIter iter)
		{
			object obj = model.GetValue(iter, 0);
			(cell as Gtk.CellRendererText).Text = m_Renderer(obj);
			(cell as Gtk.CellRendererText).WrapMode = Pango.WrapMode.Word;
		}
		
		private RenderColumnDelegate m_Renderer;
	}

	public class HourEntry: Widgets.HBox
	{
		public HourEntry()
		{
			Gtk.Label minutes = new Gtk.Label("Minuty");
			Gtk.Label hours = new Gtk.Label("Godziny");
			
			m_Minutes = new ValueEntry(12, 0, 59, "##");
			m_Hours = new ValueEntry(00, 0, 23, "##");
			
			Gtk.HBox vbox1 = new Gtk.HBox();
			vbox1.PackStart(m_DecHours, false, false, 1);
			vbox1.PackStart(m_Hours, true, false, 1);
			vbox1.PackStart(m_IncHours, false, false, 1);
			
			Gtk.HBox vbox2 = new Gtk.HBox();
			vbox2.PackStart(m_DecMinutes, false, false, 1);
			vbox2.PackStart(m_Minutes, true, false, 1);
			vbox2.PackStart(m_IncMinutes, false, false, 1);
			
			PackStart(hours, false, false, 1);
			PackStart(vbox1, false, false, 1);
			
			PackStart(minutes, false, false, 1);
			PackStart(vbox2, false, false, 1);
			
			m_Minutes.Text = "0";
			m_Hours.Text = "0";
		}
		
		public void SetHour(uint hour, uint minute)
		{
			MinutesVal = minute;
			HoursVal = hour;
			m_Minutes.Text = minute.ToString();
			m_Hours.Text = hour.ToString();
		}
		
		private void IncHoursClick(object sender, EventArgs args)
		{
			HoursVal++;
			if(HoursVal>24)
				HoursVal=0;
			m_Hours.Text = HoursVal.ToString();
		}
		
		private void DecHoursClick(object sender, EventArgs args)
		{
			HoursVal--;
			if(HoursVal>24)
				HoursVal=12;
			m_Hours.Text = HoursVal.ToString();
		}
		
		private void IncMinutesClick(object sender, EventArgs args)
		{
			MinutesVal+=15;
			if(MinutesVal>60)
				MinutesVal=0;
			m_Minutes.Text = MinutesVal.ToString();
		}
		
		private void DecMinutesClick(object sender, EventArgs args)
		{
			MinutesVal-=15;
			if(MinutesVal>60)
				MinutesVal=60;
			m_Minutes.Text = MinutesVal.ToString();
			
		}
		
		public uint MinutesVal {get; set;}
		public uint HoursVal {get; set;}
		private Gtk.Entry m_Minutes;
		private Gtk.Entry m_Hours;
		
		private Gtk.Button m_IncMinutes;
		private Gtk.Button m_DecMinutes;
		private Gtk.Button m_IncHours;
		private Gtk.Button m_DecHours;
	}
}

