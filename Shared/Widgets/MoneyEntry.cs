using System;
using Gtk;

namespace FusionPOS.Widgets
{
	public class MoneyEntry: Gtk.Table
	{
		public MoneyEntry(): base(5, 3, false)
		{
			Widgets.Button[] keys = new Widgets.Button[12];
			Widgets.TextBox entry = new Widgets.TextBox();
			
			RowSpacing = 10;
			ColumnSpacing = 10;
			Attach(entry, 0, 3, 1, 2);
			
			for (uint i = 0; i < 9; i++) {
				keys[i] = new Widgets.Button((i+1).ToString(), FontSettings.BiggerFont, true);
				keys[i].Clicked += delegate(object o, EventArgs x) {};
				
				Attach(keys[i],
				   i % 3,
				   i % 3 + 1,
				   i / 3 + 2,
				   i / 3 + 3);
				
			}
			
			keys[09] = new Widgets.Button("0", FontSettings.BiggerFont, true);
			keys[09].Clicked += delegate(object o, EventArgs x) {};
			Attach(keys[09], 0, 1, 5, 6);
			
			keys[10] = new Widgets.Button("<", FontSettings.BiggerFont, true);
			keys[10].Clicked += delegate(object o, EventArgs x) {};
			Attach(keys[10], 1, 2, 5, 6);
			
			keys[11] = new Widgets.Button("OK", FontSettings.BiggerFont, true);
			keys[11].Clicked += delegate(object o, EventArgs x) {};
			
			Attach(keys[11], 2, 3, 5, 6);

			ShowAll();
		}
	}
}

