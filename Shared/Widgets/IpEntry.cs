using System;
using System.Net;
using Gtk;
using Widgets = FusionPOS.Widgets;
using FusionPOS;

namespace FusionPOS.Widgets
{
	public class IpEntry: Widgets.HBox
	{
		protected Widgets.ValueEntry m_OctetA;
		protected Widgets.ValueEntry m_OctetB;
		protected Widgets.ValueEntry m_OctetC;
		protected Widgets.ValueEntry m_OctetD;
		
		public IpEntry(): this("0.0.0.0")
		{
		}
		
		public IpEntry(string address): this(IPAddress.Parse(address))
		{
		}
		
		public IpEntry(IPAddress address): base(false, 1)
		{
			byte A = address.GetAddressBytes()[0];
			byte B = address.GetAddressBytes()[1];
			byte C = address.GetAddressBytes()[2];
			byte D = address.GetAddressBytes()[3];

			m_OctetA = new Widgets.ValueEntry((int)A, 0, 255);
			m_OctetB = new Widgets.ValueEntry((int)B, 0, 255);
			m_OctetC = new Widgets.ValueEntry((int)C, 0, 255);
			m_OctetD = new Widgets.ValueEntry((int)D, 0, 255);

			PackExpand(m_OctetA);
			PackFill(new Widgets.Label("."));
			PackExpand(m_OctetB);
			PackFill(new Widgets.Label("."));
			PackExpand(m_OctetC);
			PackFill(new Widgets.Label("."));
			PackExpand(m_OctetD);
		}
		
		public IPAddress GetIpAddr()
		{
			byte [] addr = new byte[4];
			
			addr[0] = (byte)(m_OctetA.GetIntValue());
			addr[1] = (byte)(m_OctetB.GetIntValue());
			addr[2] = (byte)(m_OctetC.GetIntValue());
			addr[3] = (byte)(m_OctetD.GetIntValue());
			
			return new IPAddress(addr);
		}
	}
}

