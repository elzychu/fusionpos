using System;

namespace Shared
{
	public class MonetaryDescription 
	{
		private static String [] jednosci = {"", " jeden", " dwa", " trzy", " cztery", " pięć", " sześć", " siedem", " osiem", " dziewięć"};
		private static String [] nascie = {"dziesięć", " jedenaście", " dwanaście", " trzynaście", " czternaście", " pietnaście", " szesnaście", " siedemnaście", " osiemnaście", " dziewiętnaście"};
		private static String [] dziesiatki ={"", " dziesięć", " dwadzieścia", " trzydzieści", " czterdzieści", " pięćdziesiąt", " sześćdziesiąt", " siedemdziesiąt", " osiemdziesiąt", " dziewięćdziesiąt"};
		private static String [] setki = {"", " sto", " dwieście", " trzysta", " czterysta", " pięćset", " sześćset", " siedemset", " osiemset", " dziewięćset"};
		private static String [] x = {"", " tyś.", " mln.", " mld.", " bln.", " bld."};
	
		private static string GetString(int v)
		{
			String slownie = " ";
			int liczba = v;
			int koncowka;
			int rzad = 0;
			int j = 0;
			int minus = 0;
			
			
			if (liczba<0)
			{
				minus=1;
				liczba=-liczba;
			}
			
			if (liczba==0) slownie="zero";
			
				while (liczba>0)
			        {
			        koncowka=(liczba%10);
			        liczba/=10;
			        if ((j==0)&&(liczba%100!=0 || koncowka !=0)) slownie = x[rzad] + slownie;
			        if ((j==0)&&(liczba%10!=1)) slownie = jednosci[koncowka] + slownie;
			        if ((j==0)&&(liczba%10==1))
			                {
			                slownie = nascie[koncowka] + slownie;
			                liczba/=10;
			                j+=2;
			                continue;
			                }
			        if (j==1) slownie = dziesiatki[koncowka] + slownie;
			        if (j==2)
			                {
			                slownie = setki[koncowka] + slownie;
			                j=-1;
			                rzad++;
			                }
			        j++;
			        }
			
			if (minus==1) slownie = "minus" + slownie;
			
			return slownie;   	
		}
		
		public static string TranslateValue(decimal val) 
		{
			int total = (int)Math.Truncate(val);
			int rest = (int)((val - Math.Truncate(val))*100);
			return GetString(total) + "zł i" + GetString(rest) + "groszy";
		}
	}
}

