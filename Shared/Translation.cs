using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using FusionPOS.Logging;

namespace FusionPOS.Translations
{
	public static class TranslatorExtensions
	{
		public static string Tr(this string self)
		{
			return Translator.CurrentTranslator.GetString(self);
		}
	}
	
	public class Translator
	{
		public Translator(string module)
		{
			m_Instance = this;
			m_ModuleName = module;
			m_Metadata = new Dictionary<string, string>();
			m_Items = new Dictionary<string, string>();
		}
		
		public static Translator CurrentTranslator
		{
			get {
				return m_Instance;	
			}
		}
		
		public void LoadLanguage(TranslationLanguage lang)
		{
			LoadLanguage(lang.Filename);	
		}
		
		public void LoadLanguage(string filename)
		{
			XmlNode root;
			XmlNode module;
			m_Document = new XmlDocument();
			m_Document.Load(filename);
			root = m_Document.DocumentElement;
			
			/* Load translation metadata */
			m_Metadata.Clear();
			foreach (XmlNode n in root.SelectNodes("metadata/property"))
				m_Metadata.Add(n.Attributes["name"].Value, n.InnerText);
					
			/* Look up module */
			module = root.SelectSingleNode(String.Format(
			    "modules/module[@name='{0}']",
			    m_ModuleName));
			
			if (module == null)
				throw new KeyNotFoundException("Cannot found specified module");
			
			/* Load translation units */
			m_Items.Clear();
			foreach (XmlNode n in module.SelectNodes("item"))
				m_Items.Add(n.Attributes["id"].Value, n.InnerText);
		}
		
		public string GetString(string id)
		{
			if (!m_Items.ContainsKey(id))
				return "!TRMISSING!";

			return m_Items[id];
		}
		
		public string Language
		{
			get; private set;
		}
		
		private string m_ModuleName;
		private IDictionary<string, string> m_Metadata;
		private IDictionary<string, string> m_Items;
		private XmlDocument m_Document;
		private static Translator m_Instance;
	}
	
	public class TranslationLanguage
	{
		public string Filename { get; set; }
		public IDictionary<string, string> Metadata { get; set; }
	}
	
	public class TranslationContext
	{
		public TranslationContext()
		{
			AvailableLanguages = new List<TranslationLanguage>();
		}
		
		public void AddTranslationsDirectory(string path)
		{
			XmlDocument doc = new XmlDocument();
			DirectoryInfo dir = new DirectoryInfo(path);
			
			foreach (FileInfo i in dir.GetFiles("*.xml")) {
				doc.Load(i.OpenRead());
				XmlNode root = doc.DocumentElement;
				
				if (root.Name != "translation")
					continue;
				
				TranslationLanguage lang = new TranslationLanguage();
				lang.Filename = i.FullName;
				lang.Metadata = new Dictionary<string, string>();
				AvailableLanguages.Add(lang);
				
				/* Load translation metadata */
				foreach (XmlNode n in root.SelectNodes("metadata/property"))
					lang.Metadata.Add(n.Attributes["name"].Value, n.InnerText);
				
				if (Logger != null){
					Logger.Debug(String.Format(
					    "Loaded language: {0} ({1})", 
					    lang.Metadata["language"], 
					    lang.Metadata["code"]));
				}
			}
		}
	
		public TranslationLanguage GetLanguage(string code)
		{
			return AvailableLanguages.First(i => i.Metadata["code"] == code);
		}
		
		public bool HasLanguage(string code)
		{
			return AvailableLanguages.Count(i => i.Metadata["code"] == code) > 0;
		}
		
		public IList<TranslationLanguage> AvailableLanguages
		{
			get; private set;
		}
		
		public Logger Logger
		{
			get; set;
		}
	}
}

