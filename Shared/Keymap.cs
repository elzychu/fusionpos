using System;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;

namespace FusionPOS.Keymaps
{
	public struct KeyValue
	{
		public Gdk.Key Key;
		public Gdk.Key ShiftKey;
		public Gdk.Key AltKey;
		public Gdk.Key ShiftAltKey;
		
		public KeyValue(string key)
		{
			Key = (Gdk.Key)Gdk.Keyval.FromName(key);
			ShiftKey = (Gdk.Key)Gdk.Keyval.FromName(key.ToUpper());
			AltKey = 0;
			ShiftAltKey = 0;
		}
		
		public KeyValue(string key, string alt)
		{
			Key = (Gdk.Key)Gdk.Keyval.FromName(key);
			ShiftKey = (Gdk.Key)Gdk.Keyval.FromName(key.ToUpper());
			AltKey = (Gdk.Key)Gdk.Keyval.FromName(alt);
			ShiftAltKey = (Gdk.Key)Gdk.Keyval.FromName(alt.ToUpper());
		}
		
		public KeyValue(Gdk.Key key, Gdk.Key alt)
		{
			Key = key;
			ShiftKey = (Gdk.Key)Gdk.Keyval.ToUpper((uint)key);
			AltKey = alt;
			ShiftAltKey = (Gdk.Key)Gdk.Keyval.ToUpper((uint)alt);
		}
		
		public KeyValue(Gdk.Key key)
		{
			Key = key;
			ShiftKey = key;
			AltKey = 0;
			ShiftAltKey = 0;
		}
		
		public Gdk.Key ToKey(bool shift, bool alt)
		{
			if (alt) return shift ? ShiftAltKey : AltKey;
			else return shift ? ShiftKey : Key;
		}
		
		public string ToString(bool shift, bool alt)
		{
			Gdk.Key key; 
			if (alt) key = shift ? ShiftAltKey : AltKey;
			else key = shift ? ShiftKey : Key;
			
			return char.ConvertFromUtf32((int)Gdk.Keyval.ToUnicode((uint)key));
		}
	}
	
	public struct KeyEntry
	{
		public KeyValue Value;
		public uint Row;
		public uint Column;
	}
	
	public class Keymap
	{
		public Keymap()
		{
			Keys = new List<KeyEntry>();
		}
		
		public void LoadFromFile(string filename)
		{
			XmlDocument doc = new XmlDocument();
			
			doc.Load(filename);
			
			foreach (XmlNode n in doc.DocumentElement.SelectNodes("keys/key")) {
				Gdk.Key key = (Gdk.Key)Gdk.Keyval.FromName(n.Attributes["value"].Value);
				Gdk.Key alt = n.Attributes["alt"] != null ? (Gdk.Key)Gdk.Keyval.FromName(n.Attributes["alt"].Value) : 0;
				KeyEntry entry = new KeyEntry();
				entry.Value = new KeyValue(key, alt);
				entry.Row = uint.Parse(n.Attributes["row"].Value);
				entry.Column = uint.Parse(n.Attributes["column"].Value);
				Keys.Add(entry);
			}
		}
		
		public string Language
		{
			get; private set;
		}
		
		public IList<KeyEntry> Keys
		{
			get; private set;
		}
	}
}

