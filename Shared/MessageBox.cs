using System;
using Gtk;
using Gdk;
using FusionPOS.Widgets;
using System.Threading;

namespace FusionPOS
{
	public class MessageBox: TitleBox
	{
		public Widgets.Label Message;
		public ResultType Result;
		
		public enum ResultType
		{
			NoResult, OK, Cancel
		};
		
		public MessageBox(bool cancel): base()
		{
			Message = new Widgets.Label();
			m_Placement.Add(Message);
			
			AddButton("OK", OkClicked);
			if(cancel)
				AddButton("Anuluj", CancelClicked);
		}
		
		private void OkClicked(object s, EventArgs args)
		{
			Result = ResultType.OK;
			Hide();
		}
		
		private void CancelClicked(object s, EventArgs args)
		{
			Result = ResultType.Cancel;
			Hide();
		}
		
		public static ResultType Popup(string title, string message)
		{
			return Popup(title, message, true);
		}
		
		public static ResultType Popup(string title, string message, bool cancel)
		{
			MessageBox m = new MessageBox(cancel);
			m.m_TitleBar.Text = title;
			m.Message.Text = message;
			m.ShowAll();
			while (m.Result == ResultType.NoResult)
				Gtk.Main.Iteration();

			return m.Result;
		}
		
	}
}

