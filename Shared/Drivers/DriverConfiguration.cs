using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;

namespace FusionPOS.Drivers
{
	public class DriverConfiguration
	{
		public SerialPort Port { get; set; }
		public Stream Stream { get; set; }
		public IDictionary<string, string> Configuration { get; set; }
	}
}

