using System;
using System.Collections.Generic;
using FusionPOS.Domain;

namespace FusionPOS.Drivers
{
	public enum ReceiptLineType
	{
		TransactionNumber = 0,
		Points,
		PointsCount,
		RegistationNumber,
		Surname,
		Card,
		CardNumber,
		ValidThru,
		Cashier,
		CashierName,
		Advance,
		Currency,
		ConversionRate,
		OrderNumber,
		EmployeeNumber,
		EmployeeName,
		AccountBeforeTransaction,
		Granted,
		Used,
		AccountAfterTransaction,
		RegularClient,
		Voucher,
		VoucherValue,
		VoucherPayment,
		Empty
	}
	
	public class FiscalPrinterError: Exception
	{
		public FiscalPrinterError(string message, string command, int code): base(message)
		{
			ErrorCommand = command;
			ErrorCode = code;
		}
		
		public int ErrorCode { get; private set; }
		public string ErrorCommand { get; private set; }
	}
	
	public interface IFiscalPrinterDriver
	{
		string Footer { get; set; }
		string CashierName { get; set; }
		string CashDeskNumber { get; set; }
		string ModelName { get; }
		
		void Configure(DriverConfiguration configuration);
		void Initialize();
		bool IsAlive();
		void DisplayTextOperator(string text);
		void DisplayText(string text, int line);
		void DisplayDateTime();
		void Break();
		void PrintDailyReport();
		void PrintReportByDates(DateTime start, DateTime end);
		IFiscalReceipt BeginTransaction();
	}
	
	public interface IFiscalReceipt: IDisposable
	{
		void Commit(string systemNumber);
		void Abort();
		void Add(string name, decimal amount, char ptu, decimal price, decimal total);
		void AddExtraLine(ReceiptLineType type, string value);
		void AddPayment(PaymentMethod method, decimal amount);
		void ApplyRebate(decimal amount, decimal percent, string name);
	}
}

