using System;
using System.Drawing;

namespace FusionPOS.Drivers
{
	public enum LinePrinterFont
	{
		FontA, FontB
	};
	
	public class LinePrinterImage
	{
		public LinePrinterImage(string filename)
		{
			m_Bitmap = new Bitmap(filename);
		}
		
		public int Width
		{
			get { return m_Bitmap.Width; }
		}
		
		public int Height
		{
			get { return m_Bitmap.Height; }
		}
		
		public bool GetXY(int x, int y)
		{
			if (y >= m_Bitmap.Height - 1 || x >= m_Bitmap.Width - 1)
				return false;
			
			return m_Bitmap.GetPixel(x, y).GetBrightness() < 0.5f;
		}
		
		private Bitmap m_Bitmap;
	}

	public interface ILinePrinterDriver
	{
		void Initialize(DriverConfiguration configuration);
		void SendText(string text);
		void SendImage(LinePrinterImage image);
		void SetCharacterImage(byte code, LinePrinterImage image);
		void SetFont(LinePrinterFont font);
		void SetUnderline(bool underline);
		void SetBold(bool bold);
		void SetItalic(bool italic);
		void SetDoubleHeight(bool height);
		void SetDoubleWidth(bool width);
		void PaperFeed();
	}
}

