using System;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using FusionPOS.Drivers;
using sharpPDF;
using sharpPDF.Fonts;
using sharpPDF.Tables;
using sharpPDF.Elements;
using sharpPDF.Enumerators;
using FusionPOS.Domain;
using Shared;
using System.Text;
using FusionPOS.Config;

namespace FusionPOS
{
	public class VatInvoice: IReport
	{
		public VatInvoice (OutgoingInvoice invoice)
		{
			m_Invoice = invoice;
		}
		
		public void GeneratePDF(Stream stream)
		{
			pdfDocument doc = new pdfDocument("Faktura VAT", "FusionPOS");
			pdfPage page = doc.addPage(sharpPDF.Enumerators.predefinedPageSize.csA4Page);
			
			pdfColor clBlack = new pdfColor(0,0,0);
			pdfColor clWhite = new pdfColor(255, 255, 255);
			pdfColor clRed = new pdfColor(190, 10, 10);
			
			pdfAbstractFont font1 = doc.getFontReference(predefinedFont.csHelvetica);
			pdfAbstractFont font2 = doc.getFontReference(predefinedFont.csHelveticaBold);
			
			pdfTableStyle tbStyle = new pdfTableStyle(doc.getFontReference(predefinedFont.csHelveticaBold),
			            						 15, clBlack, clWhite);
			
			pdfTableStyle hdStyle = new pdfTableStyle(doc.getFontReference(predefinedFont.csHelveticaBold),
			             15, clBlack, clRed);
			
			//stamp
			page.addElement(new rectangleElement(50, page.height - 50, 215, page.height - 120, clBlack, clWhite));
			
			PdfTable.AddTextOnCenterDown("pieczec firmy", font2, page, doc, 8, 50, 215, page.height - 120);
			
			//vat invoice label
			page.addElement(new rectangleElement(217, page.height - 50, 378, page.height - 90, clBlack, clRed));
			PdfTable.AddTextOnCenterMiddle("Faktura Vat", font2, page, doc, 15, 217, page.height-50, 378, 
			                               page.height - 90);
			//date
			page.addElement(new rectangleElement(217, page.height - 92, 378, page.height - 120, clBlack, clWhite));
			PdfTable.AddLinesToCenterOfArea(font1, 8, doc, page, 217, page.height - 92, 378, page.height - 120,
			                                DateTime.Now.ToString("dd-mm-rrrr"), "Data wystawienia");
			
			//number
			page.addElement(new rectangleElement(380, page.height - 50, 545, page.height - 70, clBlack, clWhite));
			PdfTable.AddTextOnCenterMiddle("Nr 1/2012", font1, page, doc, 10, 380, page.height - 55, 545, 
			                               page.height - 70);
			
			//Type
			PdfTable.AddTextOnCenterMiddle("ORYGINAL", font2, page, doc, 12, 380, page.height - 78, 545, 
			                               page.height - 90);
			
			//Date of transaction
			page.addElement(new rectangleElement(380, page.height - 92, 545, page.height - 120, clBlack, clWhite));
			PdfTable.AddLinesToCenterOfArea(font1, 8, doc, page, 380, page.height - 92, 545, page.height - 120,
			                                m_Order.Time.ToString("dd-mm-rrrr"), "Data sprzedazy");
			
			int offset = 122;
			
			pdfTable sellerData = new pdfTable(doc, 1, clBlack, 3, tbStyle); 
			
			sellerData.tableHeader.addColumn(246, predefinedAlignment.csLeft);
			sellerData.coordX = 50;
			sellerData.coordY = page.height - offset;
			pdfTableRow row = sellerData.createRow();
			row[0].addParagraph("Sprzedawca: ", 10, predefinedAlignment.csLeft);
			row[0].addParagraph("Adres: ", 10, predefinedAlignment.csLeft);
			row[0].addParagraph("Nip: ", 10, predefinedAlignment.csLeft);
			sellerData.addRow(row);
			page.addTable(sellerData);
			
			pdfTable buyerData = new pdfTable(doc, 1, clBlack, 3, tbStyle); 
			buyerData.tableHeader.addColumn(246, predefinedAlignment.csLeft);
			buyerData.coordX = 299;
			buyerData.coordY = page.height - offset;
			row = buyerData.createRow();
			row[0].addParagraph(String.Format("Nabywca: "), 10, predefinedAlignment.csLeft);
			row[0].addParagraph("Adres: ", 10, predefinedAlignment.csLeft);
			row[0].addParagraph("Nip: ", 10, predefinedAlignment.csLeft);
			buyerData.addRow(row);
			page.addTable(buyerData);
			
			
			offset += 2 + (PdfTable.GetTableHeight(sellerData)>PdfTable.GetTableHeight(buyerData) ?
			               PdfTable.GetTableHeight(sellerData) : PdfTable.GetTableHeight(buyerData));
			
			pdfTable payment = new pdfTable(doc, 1, clBlack, 5, tbStyle); 
			payment.tableHeader.addColumn(495, predefinedAlignment.csLeft);
			payment.coordX = 50;
			payment.coordY = page.height - offset;
			row = payment.createRow();
			
			//string method = "";
			/* XXX
			switch (m_Order.PaymentMethod)
			{
			case PaymentMethod.Card:
				method = "Przelew bankowy";
			break;
			case PaymentMethod.Cash:
				method = "Gotówka";
			break;
			case PaymentMethod.Credit:
				method = "Gotówka";
			break;
			}
			row[0].addParagraph(String.Format("Forma platności: {0} Termin platności: {1}", method, DateTime.Now.ToString("dd-mm-rrrr")), 10, predefinedAlignment.csLeft);
			*/
			payment.addRow(row);
			page.addTable(payment);
			
			offset += 2 + PdfTable.GetTableHeight(payment);
			
			PdfTable ordersData = new PdfTable(doc, page, 50, page.height - offset);
			ordersData.AddHeader(new KeyValuePair<string, int>("Lp.", 25),
			                new KeyValuePair<string, int>("Nazwa", 145), 
			                new KeyValuePair<string, int>("PKWiU", 44), 
			                new KeyValuePair<string, int>("Ilosc", 35), 
			                new KeyValuePair<string, int>("Jm", 26), 
			                new KeyValuePair<string, int>("Cena netto", 44),
			                new KeyValuePair<string, int>("Wartosc netto", 44),
			                new KeyValuePair<string, int>("Stawka vat", 44), 
			                new KeyValuePair<string, int>("Kwota vat", 44),
			                new KeyValuePair<string, int>("Wartosc brutto", 44));
			int idx=0;
			foreach(OrderDish dish in m_Order.Entries)
			{
				idx++;
				
				string name=dish.Dish.Name;
				if (dish.Modifications.Count > 0) {
					name += " z ";
					name += string.Join(", ", dish.Modifications.Select(i => i.Ingredient.Component.Name));
				}
				
				ordersData.AddRow(
					new KeyValuePair<string, predefinedAlignment>(idx.ToString(), predefinedAlignment.csCenter),
					new KeyValuePair<string, predefinedAlignment>(name, predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>("", predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.Amount.ToString(), predefinedAlignment.csRight),
					new KeyValuePair<string, predefinedAlignment>("szt.", predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.ValueNet.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.ValueWithoutRebateNet.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.Dish.Tax.PercentValue.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.TaxValue.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.ValueGross.ToString(), predefinedAlignment.csLeft)
					);	
			}
			
			ordersData.DoTable();
			
			offset += 2 + ordersData.GetHeight();
			
			pdfTable labelTable = new pdfTable(doc, 1, clBlack, 3, hdStyle); 
			labelTable.tableHeader.addColumn(70, predefinedAlignment.csLeft);
			labelTable.coordX = 299;
			labelTable.coordY = page.height - offset;
			row = labelTable.createRow();
			row[0].addText("RAZEM: ");
			row[1].addText(m_Order.ValueNet.ToString());
			row[2].addText("-");
			row[3].addText(m_Order.TaxValue.ToString());
			row[3].addText(m_Order.TotalValue.ToString());
			row.rowStyle = hdStyle;
			labelTable.addRow(row);

			row = labelTable.createRow();
			row[0].addText("W tym: ");
			labelTable.addRow(row);
			row.rowStyle = hdStyle;
			page.addTable(labelTable);
			
			
			pdfTable sumTable = new pdfTable(doc, 1, clBlack, 3, tbStyle);			
			sumTable.tableHeader.addColumn(44, predefinedAlignment.csRight);
			sumTable.tableHeader.addColumn(44, predefinedAlignment.csRight);
			sumTable.tableHeader.addColumn(44, predefinedAlignment.csRight);
			sumTable.tableHeader.addColumn(44, predefinedAlignment.csRight);
			sumTable.coordX = 369;
			sumTable.coordY = page.height - offset;
			
			row = sumTable.createRow();
			row[0].addText(" ");
			sumTable.addRow(row);
			
			row = sumTable.createRow();
			row[0].addText(" ");
			sumTable.addRow(row);
			
			page.addTable(sumTable);
			
			offset += 10 + PdfTable.GetTableHeight(sumTable);
			
			page.addElement(new rectangleElement(50, page.height - offset, 200, page.height -offset - 30, clBlack,
			                                     clWhite));
			PdfTable.AddTextOnLeftMiddle(String.Format("Razem do zaplaty: {0}", m_Order.TotalValue.ToString()), 
			                             font1, page, doc, 8, 50, 
			                             page.height - offset, page.height -offset - 25);
		//	PdfTable.AddTextOnLeftMiddle(String.Format("Zaplacono: {0}", m_Order.CashGiven.ToString()), font1, page,
		//	                             doc, 8, 210, page.height - offset, page.height -offset - 25);
		//	PdfTable.AddTextOnLeftMiddle(String.Format("Do zaplaty:  {0}",(m_Order.TotalValue-m_Order.CashGiven).ToString()),
		// XXX	                                           font1, page, doc, 8, 350, page.height - offset, page.height -offset - 25);
			
			offset+=40;
			page.addElement(new rectangleElement(50, page.height - offset, 545, page.height -offset - 30, 
			                                     clBlack, clWhite));
			PdfTable.AddTextOnLeftMiddle("Slownie: ", font1, page,
			                             doc, 8, 50, page.height - offset, page.height -offset - 25);
			
			offset+=40;
			page.addElement(new rectangleElement(100, page.height - offset, 270, page.height -offset - 70, 
			                                     clBlack, clWhite));
			PdfTable.AddLinesToBottomOfArea(font1, 8, doc, page, 100, 270, page.height -offset - 70, 
			                                "Imie, nazwisko oraz podpis osoby", "upowaznionej do odebrania dokumentu");
			page.addElement(new rectangleElement(325, page.height - offset, 495, page.height -offset - 70, 
			                                     clBlack, clWhite));
			PdfTable.AddLinesToBottomOfArea(font1, 8, doc, page, 325, 495, page.height -offset - 70, 
			                                "Imie, nazwisko oraz podpis osoby", "upowaznionej do wystawienia dokumentu");
			
			doc.createPDF(stream);
		}
		
		public void GenerateHTML(StringWriter writer)
		{
			ConfigContext Configuration = new ConfigContext();
			Configuration.ReadFile("/Configuration/Server.xml");
			InvoiceHTML invoicehtml = new InvoiceHTML();
			invoicehtml.Invoice = m_Invoice;
			invoicehtml.Configuration = Configuration;
			writer.Write(invoicehtml.TransformText());
		}
		
		public static List<string> GetLines(string s, int length, int linesCount)
		{
			List<string> list = new List<string>();
			for(int i=0; linesCount<0 ? i*length <= s.Length:
			    i<linesCount; ++i)
			{
				string line;
				if(i*length <= s.Length)
					line = (i*length + length < s.Length) ? 
					s.Substring(i*length, length):
					s.Substring(i*length, s.Length-(i*length));
				else
					line = " ".Multiply(length);
				
				
				list.Add(String.Format("{0, -"+ length.ToString() +"}", line));
			}
			
			return list;
		}
		
		public static int GetLinesCount(string s, int length)
		{
			return s.Length / length;	
		}
		
		public void PrintText(ILinePrinterDriver drv)
		{
			//drv.SendText(output);
			if(m_Invoice == null)
				return;
				
			drv.SetFont(LinePrinterFont.FontA);
			Encoding encoding = Encoding.GetEncoding("ascii");
			char ud = (char)179;
			char lr = (char)196;
			char ul = (char)192;
			char urd = (char)195;
			char rdl = (char)194;
			char lur = (char)193;
			char urdl = (char)197;
						
			string header = "Sprzedawca:\n    ";
			drv.SendText(header);
			//sprzedawca
			header = "\nData wystawienia:        ";
			drv.SendText(header);
			drv.SetBold(true);
			header = m_Invoice.IssueTime.ToString("dd-MM-yyyy");
			drv.SendText(header);
			drv.SetBold(false);
			header = "\nData sprzedaży:          ";
			drv.SendText(header);
			drv.SetBold(true);
			header = m_Invoice.SellTime.ToString("dd-MM-yyyy") + "\n\n";
			drv.SendText(header);
			drv.SetBold(false);
			drv.SetDoubleHeight(true);
			header = " ".Multiply((48 - (16 + m_Invoice.Number.Length))/2) + "Faktura Vat nr. ";
			drv.SendText(header);
			//drv.SetBold(true);
			header = m_Invoice.Number;
			drv.SendText(header);
			drv.SetDoubleHeight(false);
			//drv.SetBold(false);
			header = "\n\nNabywca: ";
			drv.SendText(header);
			drv.SetBold(true);
			header = m_Invoice.BuyerName;
			drv.SendText(header);
			drv.SetBold(false);
			header = "\nAdres:   ";
			drv.SendText(header);
			drv.SetBold(true);
			header = m_Invoice.BuyerAddress;
			drv.SendText(header);
			drv.SetBold(false);
			header = "\n";
			drv.SendText(header);
			
			drv.SetFont(LinePrinterFont.FontB);
			string table = String.Format(
				"{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}\n", 
				lr.Multiply(2), rdl, lr.Multiply(20), rdl,
				lr.Multiply(5), rdl, lr.Multiply(3), rdl,
				lr.Multiply(3), rdl, lr.Multiply(8), rdl, 
				lr.Multiply(8), rdl, lr.Multiply(8)
			);
			
			table += "lp|        nazwa       |ilosc|j m|sta|  cena  | podatek|  cena  \n";
			table += "  |                    |prod |   |vat|  netto | vat    | brutto \n";  
			drv.SendText(table);
			List<string>  name = new List<string>();
			
			int i=0;
			foreach(OutgoingInvoiceComponent component in m_Invoice.Components)
			{
				i++;
				
				table = String.Format(
					"{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}\n", 
					lr.Multiply(2), urdl, lr.Multiply(20), urdl,
					lr.Multiply(5), urdl, lr.Multiply(3), urdl,
					lr.Multiply(3), urdl, lr.Multiply(8), urdl, 
					lr.Multiply(8), urdl, lr.Multiply(8)
				);
				
				name = GetLines(component.Name, 20, -1);
				
				table += String.Format("{0, -2}{1}", component.IdOnInvoice.ToString(), ud);
				table += String.Format("{0, 20}{1}", name[0], ud);
				table += String.Format("{0,  5}{1}", component.Amount.ToString("F02"), ud);
				table += String.Format("SZT{0}", ud);
				table += String.Format("{0, 2}%{1}", component.VatPercent.ToString("F0"), ud);
				table += String.Format("{0, 8}{1}", (component.Amount*component
				            .NetUnitPrice).ToString("F02"), ud);
				table += String.Format("{0, 8}{1}", (component.Amount*component
				            .NetUnitPrice*component.VatPercent).ToString("F02"), ud);
				table += String.Format("{0, 8}\n", (component.Amount*component
				            .NetUnitPrice + (component.Amount*component.NetUnitPrice
				            * component.VatPercent)).ToString("F02"));
				for(int j=1; j<name.Count(); ++j)
				{
					table += String.Format(
						"{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}\n", 
						" ".Multiply(2), ud, name[j], ud,
						" ".Multiply(5), ud, " ".Multiply(3), ud,
						" ".Multiply(3), ud, " ".Multiply(8), ud, 
						" ".Multiply(8), ud, " ".Multiply(8)
					);
				}
				drv.SendText(table);
			}
			
			table = String.Format(
					"{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}\n", 
					lr.Multiply(2), lur, lr.Multiply(20), urdl,
					lr.Multiply(5), lur, lr.Multiply(3), lur,
					lr.Multiply(3), urdl, lr.Multiply(8), urdl, 
					lr.Multiply(8), urdl, lr.Multiply(8)
			);
			
			decimal totalNetto = m_Invoice.Components.Sum(p => (p.Amount * p.NetUnitPrice));
			decimal totalTax = m_Invoice.Components.Sum(p => p.Amount*p.NetUnitPrice*p.VatPercent);
			decimal totalBrutto = m_Invoice.Components.Sum(p => p.Amount*p.NetUnitPrice + 
				(p.Amount*p.NetUnitPrice * p.VatPercent));
			table+=String.Format(" ".Multiply(23)+ud+"       Razem "+
			        ud+"{0, 8}"+ud+"{1, 8}"+ud+"{2, 8}\n", totalNetto.ToString("F02"),
			        totalTax.ToString("F02"), totalBrutto.ToString("F02"));
			
			table += String.Format(
				"{0}{1}{2}{3}{4}{5}{6}{7}{8}\n", 
				" ".Multiply(23), urd, lr.Multiply(13), urdl, 
				lr.Multiply(8), lur, lr.Multiply(8), lur,
				lr.Multiply(8)
			);
			
			table+=String.Format(" ".Multiply(23)+ud+"   Do zapłaty"+ud+"{0, 24}zl\n",
			                     totalBrutto.ToString("F02"));
			
			table += String.Format(
				"{0}{1}{2}{3}{4}\n", 
				" ".Multiply(23), ul, lr.Multiply(13), lur,
				lr.Multiply(26)
			);
			drv.SendText(table);
			
			drv.SetFont(LinePrinterFont.FontA);
			string footer = "\n\n";
			footer += "Słownie do zapłaty:" + MonetaryDescription.TranslateValue(Math.Round(totalBrutto, 2));
			footer += "\n\n\n\n\n\n";
			footer += "  _________________          _________________\n";
			footer += "   imie i nazwisko           imie i nazwisko\n";
			footer += "   oraz podpis osoby         oraz podpis osoby\n";
			footer += "   upowaznionej do           upowaznionej do\n";
			footer += "   odebrania faktury         wystawienia faktury\n\n\n\n\n\n\n\n\n";
			
			drv.SendText(footer);
		}
		
		public OutgoingInvoice m_Invoice;
		public ConfigNode Configuration;
		public Order m_Order;
	}
}
