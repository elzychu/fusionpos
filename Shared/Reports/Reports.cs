using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using sharpPDF;
using sharpPDF.Collections;
using sharpPDF.Enumerators;
using sharpPDF.Fonts;
using sharpPDF.Tables;

namespace FusionPOS
{
	public class PdfTable
	{
		public PdfTable(pdfDocument doc, pdfPage page, int x, int y)
		{
			m_Doc = doc;
			m_Page = page;
			m_X = x;
			m_Y = y;
			
			pdfColor clBlack = new pdfColor(0,0,0);
			pdfColor clWhite = new pdfColor(255, 255, 255);
			pdfColor clRed = new pdfColor(190, 10, 10);
			
			m_HdStyle = new pdfTableStyle(doc.getFontReference(predefinedFont.csHelveticaBold),
			             15, clBlack, clRed);
			m_TbStyle = new pdfTableStyle(doc.getFontReference(predefinedFont.csHelveticaBold),
			             8, clBlack, clWhite);
			
			m_Table = new pdfTable(doc, 1, clBlack, 3, m_HdStyle);
			m_Table.coordX = x;
			m_Table.coordY = y;
		}
		
		
		public PdfTable(pdfDocument doc, pdfPage page, int x, int y, pdfTableStyle hdStyle, pdfTableStyle tbStyle)
		{
			m_Doc = doc;
			m_Page = page;
			m_X = x;
			m_Y = y;
			pdfTableStyle m_HdStyle = hdStyle;
			pdfTableStyle m_TbStyle = tbStyle;
			pdfColor clBlack = new pdfColor(0,0,0);
			pdfColor clWhite = new pdfColor(255, 255, 255);
			pdfColor clRed = new pdfColor(190, 10, 10);
			
			m_HdStyle = hdStyle;
			m_TbStyle = tbStyle;
			
			m_Table = new pdfTable(doc, 1, clBlack, 3, hdStyle, tbStyle);
			m_Table.coordX = x;
			m_Table.coordY = y;
		}
		
		public void AddHeader(params KeyValuePair<string, int> [] headers)
		{
			foreach(KeyValuePair<string, int> col in headers)
				m_Table.tableHeader.addColumn(col.Value, predefinedAlignment.csCenter);
			
			pdfTableRow head = m_Table.createRow();
			
			int index=0;
			foreach(KeyValuePair<string, int> col in headers)
			{
				head[index].addParagraph(col.Key, 10, predefinedAlignment.csCenter);
				index++;
			}
			
			head.rowStyle = m_HdStyle;
			m_Table.addRow(head);
		}
		
		public void AddRow(params KeyValuePair<string, predefinedAlignment> [] cells)
		{
			pdfTableRow row = m_Table.createRow();
			row.rowStyle = m_TbStyle;
			
			int index=0;
			
			if(cells.Length <= 0)
				return;
			
			foreach(KeyValuePair<string, predefinedAlignment> col in cells)
			{
				row[index].addParagraph(col.Key, 10, col.Value);
				index++;
			}
			
			row.rowStyle = m_TbStyle;
			m_Table.addRow(row);
		}
		
		public void DoTable()
		{
			m_Page.addTable(m_Table);	
		}
		
		public int GetHeight()
		{
				return GetTableHeight(m_Table);
		}
		
		public static int GetTableHeight(pdfTable table)
		{
			int result = 0;
			foreach(pdfTableRow row in table)
					result += row.rowHeight;
			
			return result;
		}
		
		public static void AddTextToArea(string text,predefinedAlignment align, predefinedVerticalAlignment valign, pdfAbstractFont font, 
		                                int size, pdfDocument doc, pdfPage page, int x1, int y1, int x2, int y2)
		{
			int posX=0;
			int posY=0;
			
			if(align.Equals(predefinedAlignment.csCenter))
				posX = (x1 + (x2-x1)/2) - (font.getWordWidth(text, size)/2);
			else if (align.Equals(predefinedAlignment.csLeft))
				posX = x1 + 6;
			else if (align.Equals(predefinedAlignment.csRight))
				posX = x2 -6;
			
			if(valign.Equals(predefinedVerticalAlignment.csMiddle))
				posY = y2 + ((y1-y2)/2) - size; 
			else if (valign.Equals(predefinedVerticalAlignment.csTop))
				posY = y1 + 2;
			else if (valign.Equals(predefinedVerticalAlignment.csBottom))
				posY = y2 -2;

			
			pdfColor clBlack = new pdfColor(0,0,0);
			//page.addParagraph(text, posX, posY, font, size, 10, 100, clBlack);
			page.addText(text, posX, posY+10, font, size, clBlack);
		}
		
		public static void AddLinesToCenterOfArea(pdfAbstractFont font, int size, pdfDocument doc, pdfPage page, 
		                                          int x1, int y1, int x2, int y2, params string [] lines)
		{
			pdfColor clBlack = new pdfColor(0,0,0);
			int idx=0;
			foreach(string line in lines)
			{
				int posX = (x1 + (x2-x1)/2) - (font.getWordWidth(line, size)/2);
				int posY = y2 + ((y1-y2)/2) + idx*size;
				page.addText(line, posX, posY, font, size, clBlack);
				posY+=size;
				idx--;
			}
		}
		
		public static void AddLinesToBottomOfArea(pdfAbstractFont font, int size, pdfDocument doc, pdfPage page, 
		                                          int x1, int x2, int y2, params string [] lines)
		{
			pdfColor clBlack = new pdfColor(0,0,0);
			int idx=lines.Length;
			
			int posY = y2 - 5 + idx*size;
			foreach(string line in lines)
			{
				int posX = (x1 + (x2-x1)/2) - (font.getWordWidth(line, size)/2);
				page.addText(line, posX, posY, font, size, clBlack);
				posY-=size;
				idx--;
			}
		}
		
		public static void AddTextOnLeftDown(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x1, int y2)
		{
			AddTextToArea(text, predefinedAlignment.csLeft, predefinedVerticalAlignment.csBottom, font, size,
			              doc, page, x1, 0, 0, y2);
		}
		
		public static void AddTextOnRightDown(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x2, int y2)
		{
			AddTextToArea(text, predefinedAlignment.csRight, predefinedVerticalAlignment.csBottom, font, size,
			              doc, page, 0, 0, x2, y2);				
		}
		
		public static void AddTextOnCenterDown(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x1, int x2, int y2)
		{
			AddTextToArea(text, predefinedAlignment.csCenter, predefinedVerticalAlignment.csBottom, font, size,
			              doc, page, x1, 0, x2, y2);				
		}

		public static void AddTextOnLeftMiddle(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x1, int y1, int y2)
		{
			AddTextToArea(text, predefinedAlignment.csLeft, predefinedVerticalAlignment.csBottom, font, size,
			              doc, page, x1, y1, 0, y2);
		}
		
		public static void AddTextOnRightMiddle(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x2, int y1, int y2)
		{
			AddTextToArea(text, predefinedAlignment.csRight, predefinedVerticalAlignment.csMiddle, font, size,
			              doc, page, 0, y1, x2, y2);				
		}
		
		public static void AddTextOnCenterMiddle(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x1, int y1, int x2, int y2)
		{
			AddTextToArea(text, predefinedAlignment.csCenter, predefinedVerticalAlignment.csMiddle, font, size,
			              doc, page, x1, y1, x2, y2);				
		}
		
		public static void AddTextOnLeftTop(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x1, int y1)
		{
			AddTextToArea(text, predefinedAlignment.csLeft, predefinedVerticalAlignment.csTop, font, size,
			              doc, page, x1, y1, 0, 0);
		}
		
		public static void AddTextOnRightTop(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x2, int y1)
		{
			AddTextToArea(text, predefinedAlignment.csRight, predefinedVerticalAlignment.csTop, font, size,
			              doc, page, 0, y1, x2, 0);				
		}
		
		public static void AddTextOnCenterTop(string text, pdfAbstractFont font, pdfPage page, pdfDocument doc,
		                                int size, int x1, int y1, int x2, int y2)
		{
			AddTextToArea(text, predefinedAlignment.csCenter, predefinedVerticalAlignment.csTop, font, size,
			              doc, page, x1, y1, x2, y2);				
		}
		
		private pdfDocument m_Doc;
		private pdfPage m_Page;
		private int m_X;
		private int m_Y;
		private pdfTableStyle m_HdStyle;
		private pdfTableStyle m_TbStyle;
		private pdfTable m_Table;
	}
}

