using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using FusionPOS.Drivers;
using sharpPDF;
using sharpPDF.Collections;
using sharpPDF.Elements;
using sharpPDF.Enumerators;
using sharpPDF.Fonts;
using sharpPDF.Tables;
using FusionPOS.Domain;
using Shared;
using NHibernate;
using NHibernate.Linq;

namespace FusionPOS
{
	public enum ReportType { Cargo, Financial, Stocktaking, Invoice }
	
	public class FinancialReport: IReport
	{
		public struct ItemCount {
			public string Name;
			public string JM;
			public decimal Count;
			public decimal CountForPiece;
			public decimal VatRate;
			public decimal VatCount;
			public decimal NettoPrice;
			public decimal BruttoPrice;
		}
		
		public FinancialReport(Settlement settlement, ISession session)
		{
			m_Settlement = settlement;
			m_Session = session;
		}
		
		public FinancialReport(DateTime form, DateTime to, ISession session)
		{
			m_From = form;
			m_To = to;
			m_Session = session;
		}
		
		public FinancialReport(DateTime form, DateTime to, User user, ISession session)
		{
			m_From = form;
			m_To = to;
			m_User = user;
			m_Session = session;
		}
		
		private List<ItemCount> GetFilteredEntitesFromOrder(Order order)
		{
			List<ItemCount> list = new List<ItemCount>();
			
			foreach(OrderDish entity in order.Entries)
			{
				if((entity.Dish.Categories.Any(p => CategoriesFilter.Any(r => r.Equals(p))) ||
				    !CategoriesFilter.Any ()) ||
				   (entity.Dish.Groups.Any ( p => GroupsFilter.Any(r => r.Equals(p))) ||
				 !GroupsFilter.Any()) )
				{					
					if(list.Any(p => (p.Name.Equals(entity.Dish.Name)) && (p.VatRate == entity.Dish.Tax.PercentValue)))
					{
						ItemCount d = list.First(p => (p.Name == entity.Dish.Name) 
						                        && (p.VatRate == entity.Dish.Tax.PercentValue));
						d.Count += entity.Amount;
						d.NettoPrice += entity.Dish.FinalValueNet * entity.Amount;
						d.VatCount += entity.Dish.TaxValue * entity.Amount;
						d.BruttoPrice += entity.Dish.FinalValueGross * entity.Amount;
					}
					else
					{
						Dish d = entity.Dish;
						ItemCount c = new ItemCount();
						c.Count = entity.Amount;
						c.Name = d.Name;
						c.JM = "sztuka";
						c.NettoPrice = d.FinalValueNet * entity.Amount;
						c.CountForPiece = d.FinalValueNet;
						c.VatCount = entity.Amount * d.TaxValue;
						c.VatRate = d.Tax.PercentValue;
						c.BruttoPrice = d.FinalValueGross * entity.Amount;
						
						list.Add(c);
					}
					
					foreach(OrderDishModification mod in entity.Modifications)
					{
						if(!(mod.Ingredient.Component is Dish))
							continue;
						Dish curr = mod.Ingredient.Component as Dish;
						
						if(list.Any(p => (p.Name.Equals(curr.Name)) && (p.VatRate == curr.Tax.PercentValue)))
						{
							ItemCount d = list.First(p => (p.Name == curr.Name) 
							                        && (p.VatRate == curr.Tax.PercentValue));
							d.Count += mod.Amount;
							d.NettoPrice += curr.FinalValueNet * mod.Amount;
							d.VatCount += curr.TaxValue * mod.Amount;
							d.BruttoPrice += curr.FinalValueGross * mod.Amount;	
						}
						else
						{
							ItemCount c = new ItemCount();
							c.Count = mod.Amount;
							c.Name = curr.Name;
							c.JM = "sztuka";
							c.NettoPrice = curr.FinalValueNet * mod.Amount;
							c.CountForPiece = curr.FinalValueNet;
							c.VatCount = mod.Amount * curr.TaxValue;
							c.VatRate = curr.Tax.PercentValue;
							c.BruttoPrice = curr.FinalValueGross * mod.Amount;
							
							list.Add(c);
						}
					}
				}	
			}
			
			return list;
		}
		
		private IList<ItemCount> Filter(IList<Order> orders)
		{
			List<ItemCount> list = new List<ItemCount>();

			orders.Each(p => Merge(list, GetFilteredEntitesFromOrder(p)));
			
			return list;
		}
		
		private void Merge(List<ItemCount> list1, List<ItemCount> list2)
		{
			if(list1.Count == 0)
			{
				foreach(ItemCount c in list2)
					list1.Add(c);
				return;
			}
			
			foreach(ItemCount c in list2)
			{
				if(list1.Any(p => p.Name.Equals(c.Name) && p.VatRate == c.VatRate))
				{
					ItemCount ic = list1.First(p => p.Name.Equals(c.Name) 
					                           && p.VatRate == c.VatRate);
					ic.Count += c.Count;
					ic.NettoPrice += c.NettoPrice;
					ic.VatCount += c.VatCount;
					ic.BruttoPrice += c.BruttoPrice;
				}
				else
				{
					list1.Add(c);	
				}
			}
		}
		
		public void GenerateHTML(StringWriter output) 
		{
			IList<Order> orders;
			
			if(CategoriesFilter == null)
				CategoriesFilter = new List<DishCategory>();
			if(GroupsFilter == null)
				GroupsFilter = new List<DishGroup>();
			
			if(m_Settlement != null)
			{
				m_From = m_Settlement.From;
				m_To = m_Settlement.To;
			}
			
			if(m_User != null)
			{
				orders = m_Session.Query<Order>()
					.Where(p => p.Time >= m_From)
					.Where(p => p.Time <= m_To)
					.Where(p => p.Waiter.Equals(m_User)).ToList();
			}
			else
			{
				orders = m_Session.Query<Order>()
					.Where(p => p.Time >= m_From)
					.Where(p => p.Time <= m_To).ToList();
			}
			
			ReportsHTML report = new ReportsHTML();
			
			List<String> groups = new List<String>();
			foreach(DishGroup gr in GroupsFilter)
				groups.Add (gr.Name);
			List<String> categories = new List<String>();
			foreach(DishCategory ic in CategoriesFilter)
				categories.Add(ic.Name);
			List<String> ingredients = new List<String>();
			report.CategoriesFilter = categories;
			report.GroupsFilter = groups;
			report.From = m_From;
			report.To = m_To;
			report.Type_ = ReportType.Financial;
			report.Dishes = Filter(orders);
			report.IngredientsFilter = ingredients;
			report.Ingredients = new List<CargoReport.IngredientCount>();
			string buff = report.TransformText();
			output.Write(buff);
		}
		
		public void GeneratePDF(Stream output)
		{
			
		}
		
		public void PrintText(ILinePrinterDriver drv)
		{
			
		}
		
		private User m_User;
		private DateTime m_From;
		private DateTime m_To;
		private Settlement m_Settlement;
		private ISession m_Session;
		public List<DishGroup> GroupsFilter;
		public List<DishCategory> CategoriesFilter;
	}
	
	public class CargoReport: IReport
	{
		public struct IngredientCount {
			public string Ingredient;
			public decimal Count;
			public string Unit;
			public decimal PriceForPiece;
		}
		
		public CargoReport(DateTime from_, DateTime to, ISession session)
		{
			m_From = from_;
			m_To = to;
			m_Session = session;
			CategoriesFilter = new List<IngredientCategory>();
			GroupsFilter = new List<IngredientGroup>();
			IngredientsFilter = new List<Ingredient>();
		}
		
		private void Merge(List<IngredientCount> list1, List<IngredientCount> list2)
		{
			if(list1.Count == 0)
			{
				foreach(IngredientCount c in list2)
					list1.Add(c);
				return;
			}
			
			foreach(IngredientCount c in list2)
			{
				if(list1.Any(p => p.Ingredient.Equals(c.Ingredient)))
				{
					IngredientCount ic = list1.First(p => p.Ingredient.Equals(c.Ingredient));
					ic.Count += c.Count;
				}
				else
				{
					list1.Add(c);	
				}
			}
		}
		
		private bool CheckIngredient(Ingredient ingredient)
		{
			if(IngredientsFilter.Any())
				if(!IngredientsFilter.Any(p => p.Name.Equals(ingredient.Name)))
					return false;
			
			
			if(CategoriesFilter.Any())
				if(!CategoriesFilter.Any(p =>p.Name.Equals(ingredient.Category)))
						return false;

			
			if(GroupsFilter.Any())
				if(!GroupsFilter.Any (p => !ingredient.Groups.Contains(p)))
					return false;
			
			return true;
		}
		
		private List<IngredientCount> GetIngredientsFromDishOrder(OrderDish dishorder)
		{
			List<IngredientCount> list = new List<IngredientCount>();

			foreach(RecipeIngredient i in dishorder.Dish.Recipe.Ingredients)
			{
				if(i.Component is OrderDish)
					Merge(list, GetIngredientsFromDishOrder(i.Component as OrderDish));
				else if(i.Component is Ingredient)
					if(CheckIngredient(i.Component as Ingredient))
					{
						Ingredient t = i.Component as Ingredient;
						IngredientCount ic = new IngredientCount();
						ic.Ingredient = t.Name;
						ic.Count = i.Amount;
						ic.Unit = t.Unit.ToString();
						ic.PriceForPiece = t.Price;
						list.Add(ic);
					}
			}
			
			foreach(OrderDishModification mod in dishorder.Modifications){
				if(mod.Ingredient.Component is OrderDish)
					Merge(list, GetIngredientsFromDishOrder(mod.Ingredient.Component as OrderDish));
				else if(mod.Ingredient.Component is Ingredient)
					if(CheckIngredient(mod.Ingredient.Component as Ingredient))
					{
						Ingredient t = mod.Ingredient.Component as Ingredient;
						IngredientCount ic = new IngredientCount();
						ic.Ingredient = t.Name;
						ic.Count = mod.Ingredient.Amount;
						ic.Unit = t.Unit.ToString();
						ic.PriceForPiece = t.Price;
						list.Add(ic);
					}
			}
			
			return list;
			
		}
		
		
		protected List<IngredientCount> GetCount(IList<Order> orders)
		{
			List<IngredientCount> list = new List<IngredientCount>();
			
			foreach(Order order in orders) {
				order.Entries.Each(p => Merge(list, GetIngredientsFromDishOrder(p)));
			}
			
			return list;	
		}
		
		public virtual void GenerateHTML(StringWriter stream)
		{
			IList<Order> orders = m_Session.Query<Order>()
				.Where(c => c.Time <= m_To)
				.Where(c => c.Time >= m_From).ToList();
			if(orders==null)
				orders = new List<Order>();
			//IList<IngredientCount> count = GetCount(orders);
			
			string name = orders[0].Entries[0].Dish.Name;
			ReportsHTML report = new ReportsHTML();
			report.Ingredients = GetCount(orders);
			List<String> groups = new List<String>();
			foreach(IngredientGroup gr in GroupsFilter)
				groups.Add (gr.Name);
			List<String> categories = new List<String>();
			foreach(IngredientCategory ic in CategoriesFilter)
				categories.Add(ic.Name);
			List<String> ingredients = new List<String>();
			foreach(Ingredient i in IngredientsFilter)
				ingredients.Add(i.Name);
			report.Type_ = ReportType.Cargo;
			report.CategoriesFilter = categories;
			report.GroupsFilter = groups;
			report.IngredientsFilter = ingredients;
			report.From = m_From;
			report.To = m_To;
			report.Dishes = new List<FinancialReport.ItemCount>();
			string buff = report.TransformText();
			stream.Write(buff);
		}
		
		public void GeneratePDF(Stream stream)
		{
			
		}
		
		public void PrintText(ILinePrinterDriver drv)
		{
			
		}
		
		protected DateTime m_From;
		protected DateTime m_To;
		protected ISession m_Session;
		public IList<Ingredient> IngredientsFilter;
		public IList<IngredientGroup> GroupsFilter;
		public IList<IngredientCategory> CategoriesFilter;
	}
	
	public class Stocktaking: CargoReport
	{
		public Stocktaking(DateTime form, DateTime to, ISession ses): base(form, to, ses)
		{ }
		public override void GenerateHTML(StringWriter stream)
		{
			IList<Order> orders = m_Session.Query<Order>()
				.Where(c => c.Time <= m_To)
				.Where(c => c.Time >= m_From).ToList();
			if(orders==null)
				orders = new List<Order>();
			ReportsHTML report = new ReportsHTML();
			report.Ingredients = GetCount(orders);
			
			List<String> groups = new List<String>();
			foreach(IngredientGroup gr in GroupsFilter)
				groups.Add (gr.Name);
			List<String> categories = new List<String>();
			foreach(IngredientCategory ic in CategoriesFilter)
				categories.Add(ic.Name);
			List<String> ingredients = new List<String>();
			foreach(Ingredient i in IngredientsFilter)
				ingredients.Add(i.Name);
			
			report.Type_ = ReportType.Stocktaking;
			report.CategoriesFilter = categories;
			report.GroupsFilter = groups;
			report.IngredientsFilter = ingredients;
			report.From = m_From;
			report.To = m_To;
			report.Dishes = new List<FinancialReport.ItemCount>();
			string buff = report.TransformText();
			stream.Write(buff);
		}
	}
	
	public class VatInvoiceReport: IReport
	{
		public struct VatReport {
			public string Number;
			public string Deliver;
			public decimal Vat;
			public decimal Netto;
			public decimal Brutto;
		}
		
		public VatInvoiceReport(DateTime from_, DateTime to, ISession session)
		{
			m_From = from_;
			m_To = to;
			m_Session = session;
			if(m_SellerName == null)
				m_SellerName = "";
		}
		
		public VatInvoiceReport(DateTime from_, DateTime to, ISession session, String sellerName): this(from_, to, session)
		{ 
			m_SellerName = sellerName;
		}
		
		public List<VatReport> GetReport(IList<IncomingInvoice> invoices)
		{
			List<VatReport> reports = new List<VatReport>();
			foreach(IncomingInvoice invoice in invoices)
			{
				VatReport rep = new VatReport();
				
				rep.Number = invoice.Number;
				rep.Deliver = invoice.SellerName;
				rep.Vat = invoice.Components.Sum(i => i.TaxValue);
				rep.Brutto = invoice.Components.Sum(i => i.GrossValue);
				rep.Netto = invoice.Components.Sum(i => i.NetValue);
				rep.Deliver = invoice.SellerName;
				
				reports.Add(rep);
			}
			
			return reports;
		}
		
		public void GenerateHTML(StringWriter stream)
		{
			IQueryable<IncomingInvoice> query = m_Session.Query<IncomingInvoice>()
				.Where(p => p.SellTime <= m_To && p.SellTime >= m_From);
			
			if(m_SellerName != "")
				query = query.Where(p => p.SellerName == m_SellerName);
			
			IList<IncomingInvoice> invoices = query.ToList();
			
			ReportsHTML report = new ReportsHTML();
			report.Type_ = ReportType.Invoice;
			report.From = m_From;
			report.To = m_To;
			report.Invoices = GetReport(invoices);
			stream.Write(report.TransformText());			
		}
		
		public void GeneratePDF(Stream stream)
		{
			
		}
		
		public void PrintText(ILinePrinterDriver drv)
		{
		}
		
		private string m_SellerName;
		private DateTime m_From;
		private DateTime m_To;
		private ISession m_Session;
	}
	
	/*public class ReportMask
	{
		public ReportMask(ISession session, List<DishCategory> categories, 
		                  List<User> waiters, string mask)
		{
			if (mask == "")
				m_Mask = "*";
			else
				m_Mask = mask;
			m_Session = session;
			m_Categories = categories;
			m_Waiters = waiters;
		}
		
		public ReportMask(ISession session, string mask)
			:this(session, new List<DishCategory>(), new List<User>(), mask)
		{ }
		
		public ReportMask(ISession session, List<DishCategory> categories, string mask)
			:this(session, categories, new List<User>(), mask)
		{ }
		
		public ReportMask(ISession session, List<User> waiters, string mask)
			:this(session, new List<DishCategory>(), waiters, mask)
		{ }
		
		
		
		public string ToString(string type)
		{
			switch (type)
			{
			case "Categories":
				string line = "";
				foreach(DishCategory category in m_Categories)
					line += category.Name + ", ";
				return line;
			break;
			case "Mask":
				return m_Mask;	
			break;
			}
			
			return "";	
		}
		
		public IList<OrderDish> GetOrders(DateTime form, DateTime to)
		{ 
			IList<Order> orders;
			
			orders = m_Session.Query<Order>()
				.Where(c => c.Time <= to)
				.Where(c => c.Time >= form)
				.Where(c => (m_Waiters.Any() ? m_Waiters.Contains(c.Waiter) : true))
				.Where(c => (m_Categories.Any() ? c.Entries.Any(e => e.Dish.Categories.Intersect(m_Categories).Any()) : true))
				.Where(c => true) as IList<Order>;
			
			List<OrderDish> dishes = new List<OrderDish>();
			foreach(Order order in orders)
			{
				foreach(OrderDish dish in order.Entries)
				{
					if(dish.Dish.Name.Contains(m_Mask) || m_Mask == "*")
						dishes.Add(dish);
				}
			}
			
			return dishes;
		}
		
		private string m_Mask;
		private List<DishCategory> m_Categories;
		private ISession m_Session;
		private List<User> m_Waiters;
	}
	
	public class DailyReport: IReport
	{
		public DailyReport (ReportMask mask)
		{
			m_Mask = mask;
		}
		
		public void GeneratePDF(Stream stream)
		{
			pdfDocument doc = new pdfDocument("Daily report", "FusionPOS");
			pdfPage page = doc.addPage(842, 595);
			
			pdfColor clBlack = new pdfColor(0,0,0);
			pdfColor clWhite = new pdfColor(255, 255, 255);
			pdfColor clRed = new pdfColor(190, 10, 10);
			
			pdfAbstractFont font1 = doc.getFontReference(predefinedFont.csHelvetica);
			pdfAbstractFont font2 = doc.getFontReference(predefinedFont.csHelveticaBold);
			
			pdfTableStyle tbStyle = new pdfTableStyle(doc.getFontReference(predefinedFont.csHelveticaBold),
			            						 15, clBlack, clWhite);
			
			pdfTableStyle hdStyle = new pdfTableStyle(doc.getFontReference(predefinedFont.csHelveticaBold),
			             15, clBlack, clRed);
			
			pdfTable table = new pdfTable(doc);
			
			table.tableHeader.addColumn(742);
			table.coordX = 50;
			table.coordY = page.width - 50;
			
			pdfTableRow row = table.createRow();
			row.rowStyle = hdStyle;
			row[0].addParagraph(String.Format("Raport sprzedazy towarow za okres od {0} do {1}", 
			               m_From.ToString("dd-mm-yyyy"), m_To.ToString("dd-mm-yyyy")),
			                    font1, 15, 10, 742, 15, predefinedAlignment.csCenter);
			row[0].addParagraph(String.Format("Data wydruku {0}", DateTime.Now.ToString("dd-mm-yyyy")), 
			                    font1, 15, 10, 742, 15, predefinedAlignment.csCenter);
			
			table.addRow(row);
			int offset = PdfTable.GetTableHeight(table) + 2;
			
			pdfTable categories = new pdfTable(doc);
			categories.coordX = 50;
			categories.coordY = offset;
			
			categories.tableHeader.addColumn(742);
			row = categories.createRow();
			row[0].addParagraph(String.Format ("Wybrane kategorie: {0}", m_Mask.ToString("Categories")), 
			                    font1, 15, 10, 742, 15, predefinedAlignment.csLeft);
			row[0].addParagraph(String.Format ("Maska: {0}", m_Mask.ToString ("Mask")), 
			                    font1, 15, 10, 742, 15, predefinedAlignment.csLeft);
			
			
			categories.addRow(row);
			
			offset += PdfTable.GetTableHeight(categories) + 2;
			
			PdfTable ordersData = new PdfTable(doc, page, 50, page.height - offset);
			ordersData.AddHeader(new KeyValuePair<string, int>("Lp.", 25),
			                new KeyValuePair<string, int>("Nazwa", 145), 
			                new KeyValuePair<string, int>("PKWiU", 44), 
			                new KeyValuePair<string, int>("Ilosc", 35), 
			                new KeyValuePair<string, int>("Jm", 26), 
			                new KeyValuePair<string, int>("Cena netto", 44),
			                new KeyValuePair<string, int>("Wartosc netto", 44),
			                new KeyValuePair<string, int>("Stawka vat", 44), 
			                new KeyValuePair<string, int>("Kwota vat", 44),
			                new KeyValuePair<string, int>("Wartosc brutto", 44));
			int idx=0;
			foreach(OrderDish dish in m_Mask.GetOrders(m_From, m_To))
			{
				idx++;
				
				string name=dish.Dish.Name;
				if (dish.Modifications.Count > 0) {
					name += " z ";
					name += string.Join(", ", dish.Modifications.Select(i => i.Ingredient.Ingredient.Name));
				}
				
				ordersData.AddRow(
					new KeyValuePair<string, predefinedAlignment>(idx.ToString(), predefinedAlignment.csCenter),
					new KeyValuePair<string, predefinedAlignment>(name, predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>("", predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.Amount.ToString(), predefinedAlignment.csRight),
					new KeyValuePair<string, predefinedAlignment>("szt.", predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.ValueWithoutTax.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.ValueWithoutRebate.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.Dish.Tax.PercentValue.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.TaxValue.ToString(), predefinedAlignment.csLeft),
					new KeyValuePair<string, predefinedAlignment>(dish.Value.ToString(), predefinedAlignment.csLeft)
					);	
			}
			
			ordersData.DoTable();
			
			offset += 2 + ordersData.GetHeight();
			
			doc.createPDF(stream);
		}
		
		public void GenerateHTML(StringWriter writer)
		{
			
		}
		
		private DateTime m_From;
		private DateTime m_To;
		private ReportMask m_Mask;
	}*/
}

