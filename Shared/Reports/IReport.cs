using System;
using System.IO;
using System.Collections.Generic;
using FusionPOS.Drivers;

namespace FusionPOS
{
	public interface IReport
	{
		void GenerateHTML(StringWriter output);
		void GeneratePDF(Stream output);
		void PrintText(ILinePrinterDriver drv);
	}
}