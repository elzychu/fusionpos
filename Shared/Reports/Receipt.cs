using System;
using FusionPOS.Drivers;
using FusionPOS.Config;
using FusionPOS;
using FusionPOS.Domain;

namespace Shared
{
	public class Receipt
	{
		public Receipt (Order order)
		{
			m_Order = order;
		}
		
		private void WriteLeftAlign(string text1, string text2, ILinePrinterDriver drv)
		{
			int lg1 = text1.Length;
			int lg2 = text2.Length;
			drv.SendText(String.Format("{0}{1}{2}\n", text1, " ".Multiply(48-(lg1+lg2)),
				text2));
		}
		
		public void Print(ILinePrinterDriver drv)
		{
			char lr = (char)196;
			drv.SetBold(false);
			drv.SetFont(LinePrinterFont.FontA);
			ConfigContext context = new ConfigContext();
			context.ReadFile("/Configuration/Server.xml");
			string restaurantName = context.Sections["company"].Properties["name"];
			string NIP = context.Sections["company"].Properties["nip"];
			string street = context.Sections["company"].Properties["street"];
			string city = context.Sections["company"].Properties["city"];
			string zip = context.Sections["company"].Properties["zip"];
			int length = restaurantName.Length;
			
			drv.SendText(String.Format("{0}{1}\n{2}{3}\n{4} {5}\n", 
				" ".Multiply((int)Math.Round(((48-length)/2m))), restaurantName,
			        " ".Multiply((int)Math.Round(((48-street.Length)/2m))), street,
			        " ".Multiply((int)Math.Round(((48-(zip.Length + city.Length+1))/2m))),zip, city));			
			
			drv.SendText(String.Format("NIP: {0}\n", NIP));
			drv.SendText(String.Format("Data wydruku: {0}\n", DateTime.Now.ToString("dd-MM-yyyy")));
			drv.SendText(lr.Multiply(48) + "\n");
			
			foreach(OrderDish ordi in m_Order.Entries)
			{
				WriteLeftAlign(String.Format("{0} {1} x {2}", ordi.Dish.Name, ordi.Amount, 
					ordi.ValueForPieceGross), ordi.ValueGross.ToString("0.##"), drv); 
			}
			
			drv.SendText(lr.Multiply(48) + "\n");
			drv.SetBold(true);
			drv.SetDoubleHeight(true);
			drv.SetDoubleWidth(true);
			drv.SendText(String.Format("{0}{1}{2}\n", "Razem", " ".Multiply(41 - m_Order.ValueGross.ToString("0.##").Length),
				m_Order.ValueGross.ToString("0.##")));
			//WriteLeftAlign("Razem", m_Order.ValueGross.ToString("0.##"), drv);
			drv.SetDoubleHeight(false);
			drv.SetDoubleWidth(false);
			drv.SetBold(false);
			drv.SendText(lr.Multiply(48) + "\n");
			WriteLeftAlign(m_Order.Waiter.Name, m_Order.Time.ToString("hh:mm"), drv);
			drv.SendText(lr.Multiply(48) + "\n");
			foreach(OrderPayment payment in m_Order.Payments)
			{
				WriteLeftAlign(payment.Method.ToString(), payment.Amount.ToString("0.##"), drv);
			}
			drv.SendText(lr.Multiply(48) + "\n");
			drv.SendText("\n".Multiply(10));
			
		}
			        
		private Order m_Order;
	}
}

