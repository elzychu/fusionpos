using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using FusionPOS.Drivers;
using FusionPOS.Domain;
using FusionPOS;

namespace Shared
{
	public class OrderSummary: IReport
	{
		public OrderSummary (IList<OrderDish> dishes, bool orderStatus)
		{
			m_Dishes = dishes;	
		}
		
		//XXX
		public void GenerateHTML(StringWriter output) { }
		//XXX
		public void GeneratePDF(Stream output) { }
		
		public void PrintText(ILinePrinterDriver drv) 
		{
			if(m_Dishes.Count < 0 )
				return;
			
			char lr = (char)196;
			drv.SendText(String.Format("Id zamówienia: {0}\nStatus: {1}", m_Dishes[0].Order.Id, m_OrderStatus ? 
			    "nowe zamówienie\n" : "aktualizacja zamówienia\n"));
			drv.SendText("Kelner: " + m_Dishes[0].Order.Waiter.Name + "\n");
			drv.SendText("Data i czas wydruku: ");
			drv.SetBold(true);
			drv.SendText(DateTime.Now.ToString("dd-MM-yyyy hh:mm") + "\n\n\n");
			drv.SendText("Przeznaczenie: ");
			if(m_Dishes[0].Order.Destination is DestinationDelivery)
				drv.SendText(String.Format("dostawa na adres {0}\n", 
				    (m_Dishes[0].Order.Destination as DestinationDelivery).Address));
			else if (m_Dishes[0].Order.Destination is DestinationOnSite)
				drv.SendText(String.Format("stolik {0}\n", 
				    (m_Dishes[0].Order.Destination as DestinationOnSite).Table.Name));
			else
				drv.SendText("Na wynos\n");
			
			
			drv.SetBold(false);
			foreach(OrderDish dish in m_Dishes)
			{
				drv.SendText(String.Format("{0}".Multiply(48) + "\n", lr));
				drv.SetBold(true);
				drv.SendText(dish.Amount.ToString() + "x ");
				drv.SendText(dish.Dish.Name);
				drv.SetBold(false);
				foreach(OrderDishModification mod in dish.Modifications)
				{
					drv.SendText("\n");
					drv.SendText(" ".Multiply(8) + (mod.Amount > 0 ? "+" : "") +
					             mod.Amount.ToString() + " " );
					
					drv.SendText(mod.Ingredient.Component.Name);
				}
				drv.SendText("\n");
			}
			drv.SendText(String.Format("{0}".Multiply(48) + "\n", lr));
			drv.SendText("\n\n");
			drv.SetBold(true);
			drv.SendText("Uwagi: \n");
			drv.SetBold(false);
			drv.SendText(m_Dishes[0].Order.Description == "" ? "brak" : 
				m_Dishes[0].Order.Description);
			drv.SendText("\n".Multiply(10));
		}
		
		private IList<OrderDish> m_Dishes;
		private bool m_OrderStatus;
	}
}

