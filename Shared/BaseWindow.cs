using System;
using System.Collections;
using System.Collections.Generic;
using Gtk;
using FusionPOS;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public class BaseWindow: Gtk.Window
	{
		public BaseWindow(): base(WindowType.Toplevel)
		{
			m_Instance = this;
			m_VBox = new Widgets.VBox();
			m_FormPlacement = new EventBox();
			m_FormStack = new Stack<BaseForm>();
			m_Keyboard = new Keyboard(this);
			m_Keyboard.SetSizeRequest(-1, 240);
			m_VBox.PackEnd(m_Keyboard, false, false, 0);
			
			Add(m_VBox);
			ShowAll();
			
			SetDefaultSize(1024, 768);
			//Fullscreen();
		}
		
		public static BaseWindow getInstance()
		{
			return m_Instance;
		}
		
		public void AddForm(BaseForm form)
		{
			ShowForm(form);
			m_FormStack.Push(form);
		}
		
		public void GoBack(object s, EventArgs args)
		{
			m_FormStack.Pop();
			ShowForm(m_FormStack.Peek());
		}
		
		public void ShowKeyboard()
		{
			m_Keyboard.ShowAll();	
		}
		
		public void HideKeyboard()
		{
			m_Keyboard.Hide();
		}
		
		public new void ShowAll()
		{
			base.ShowAll();
			m_Keyboard.Hide();
		}
		
		protected Widgets.VBox VBox
		{
			get { return m_VBox; }
		}
		
		protected Gtk.Widget FormPlacement
		{
			get { return m_FormPlacement; }
		}
		
		private void ShowForm(BaseForm form)
		{
			if (m_CurrentForm != null) {
				m_CurrentForm.OnLeaving();
				m_FormPlacement.Remove(m_CurrentForm);
			}
			
			m_CurrentForm = form;
			m_FormPlacement.Add(m_CurrentForm);
			m_CurrentForm.OnShow();
			HideKeyboard();
		}
		
		private Widgets.VBox m_VBox;
		private Gtk.EventBox m_FormPlacement;
		private Keyboard m_Keyboard;
		protected BaseForm m_CurrentForm;
		protected Stack<BaseForm> m_FormStack;
		private static BaseWindow m_Instance;
	}
}

