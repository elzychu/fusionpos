using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FusionPOS
{
	/*
	struct KeyValue
	{
		public Gdk.Key Key;
		public Gdk.Key ShiftKey;
		public Gdk.Key AltKey;
		public Gdk.Key ShiftAltKey;
		
		public KeyValue(string key)
		{
			Key = (Gdk.Key)Gdk.Keyval.FromName(key);
			ShiftKey = (Gdk.Key)Gdk.Keyval.FromName(key.ToUpper());
			AltKey = 0;
			ShiftAltKey = 0;
		}
		
		public KeyValue(string key, string alt)
		{
			Key = (Gdk.Key)Gdk.Keyval.FromName(key);
			ShiftKey = (Gdk.Key)Gdk.Keyval.FromName(key.ToUpper());
			AltKey = (Gdk.Key)Gdk.Keyval.FromName(alt);
			ShiftAltKey = (Gdk.Key)Gdk.Keyval.FromName(alt.ToUpper());
		}
		
		public KeyValue(Gdk.Key key, Gdk.Key alt)
		{
			Key = key;
			ShiftKey = (Gdk.Key)Gdk.Keyval.ToUpper((uint)key);
			AltKey = alt;
			ShiftAltKey = (Gdk.Key)Gdk.Keyval.ToUpper((uint)alt);
		}
		
		public KeyValue(Gdk.Key key)
		{
			Key = key;
			ShiftKey = key;
			AltKey = 0;
			ShiftAltKey = 0;
		}
		
		public Gdk.Key ToKey(bool shift, bool alt)
		{
			if (alt) return shift ? ShiftAltKey : AltKey;
			else return shift ? ShiftKey : Key;
		}
		
		public string ToString(bool shift, bool alt)
		{
			Gdk.Key key; 
			if (alt) key = shift ? ShiftAltKey : AltKey;
			else key = shift ? ShiftKey : Key;
			
			return char.ConvertFromUtf32((int)Gdk.Keyval.ToUnicode((uint)key));
		}
	}
	*/
	/*
	struct KeyboardKey
	{
		public KeyValue Value;
		public uint X;
		public uint Y;
		
		public KeyboardKey(KeyValue val, uint row, uint column)
		{
			Value = val;
			X = column;
			Y = row;
		}
		
		public static IList<KeyboardKey> StandardKeys = new ReadOnlyCollection<KeyboardKey>(new[] {
			new KeyboardKey(new KeyValue(Gdk.Key.Key_1, Gdk.Key.exclam), 0U, 0U),
			new KeyboardKey(new KeyValue("2"), 0U, 1U),
			new KeyboardKey(new KeyValue("3"), 0U, 2U),
			new KeyboardKey(new KeyValue("4"), 0U, 3U),
			new KeyboardKey(new KeyValue("5"), 0U, 4U),
			new KeyboardKey(new KeyValue("6"), 0U, 5U),
			new KeyboardKey(new KeyValue("7"), 0U, 6U),
			new KeyboardKey(new KeyValue("8"), 0U, 7U),
			new KeyboardKey(new KeyValue("9"), 0U, 8U),
			new KeyboardKey(new KeyValue("0"), 0U, 9U),
			new KeyboardKey(new KeyValue("q"), 1U, 0U),
			new KeyboardKey(new KeyValue("w"), 1U, 1U),
			new KeyboardKey(new KeyValue(Gdk.Key.e, Gdk.Key.eogonek), 1U, 2U),
			new KeyboardKey(new KeyValue("r"), 1U, 3U),     
			new KeyboardKey(new KeyValue("t"), 1U, 4U),
			new KeyboardKey(new KeyValue("y"), 1U, 5U),
			new KeyboardKey(new KeyValue("u"), 1U, 6U),
			new KeyboardKey(new KeyValue("i"), 1U, 7U),
			new KeyboardKey(new KeyValue(Gdk.Key.o, Gdk.Key.oacute), 1U, 8U),
			new KeyboardKey(new KeyValue("p"), 1U, 9U),
			new KeyboardKey(new KeyValue(Gdk.Key.a, Gdk.Key.aogonek), 2U, 0U),
			new KeyboardKey(new KeyValue("s"), 2U, 1U),
			new KeyboardKey(new KeyValue("d"), 2U, 2U),
			new KeyboardKey(new KeyValue("f"), 2U, 3U),
			new KeyboardKey(new KeyValue("g"), 2U, 4U),
			new KeyboardKey(new KeyValue("h"), 2U, 5U),
			new KeyboardKey(new KeyValue("j"), 2U, 6U),
			new KeyboardKey(new KeyValue("k"), 2U, 7U),
			new KeyboardKey(new KeyValue(Gdk.Key.l, Gdk.Key.lcedilla), 2U, 8U),
			new KeyboardKey(new KeyValue(Gdk.Key.semicolon), 2U, 9U),
			new KeyboardKey(new KeyValue(Gdk.Key.z, Gdk.Key.zabovedot), 3U, 1U),
			new KeyboardKey(new KeyValue(Gdk.Key.x, Gdk.Key.zacute), 3U, 2U),
			new KeyboardKey(new KeyValue("c"), 3U, 3U),
			new KeyboardKey(new KeyValue("v"), 3U, 4U),
			new KeyboardKey(new KeyValue("b"), 3U, 5U),
			new KeyboardKey(new KeyValue("n"), 3U, 6U),
			new KeyboardKey(new KeyValue("m"), 3U, 7U),
			new KeyboardKey(new KeyValue(Gdk.Key.comma), 3U, 8U),
			new KeyboardKey(new KeyValue(Gdk.Key.period), 3U, 9U),
		});
	}*/
}

