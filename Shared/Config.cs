using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using FusionPOS.Logging;

namespace FusionPOS.Config
{
	public class ConfigNode
	{
		public string Name;
		public IDictionary<string, string> Properties;
		public IDictionary<string, ConfigNode> Children;
		
		public void ReadToObject(object o)
		{
			Type t = o.GetType();
			foreach (var i in Properties) {
				PropertyInfo prop = t.GetProperty(i.Key.DashedCaseToCamelCase());
				prop.SetValue(o, i.Value, null);
			}
		}
		
		public void WriteFromObject(object o)
		{
			Type t = o.GetType();
			foreach(var i in Properties) {
				PropertyInfo prop = t.GetProperty(i.Key.DashedCaseToCamelCase());
				Properties[i.Key] = prop.GetValue(o, null) as string;
			}
		}
	}
	
	public class ConfigContext
	{
		public ConfigContext()
		{
			Sections = new Dictionary<string, ConfigNode>();
		}
		
		public void ReadFile(string filename)
		{
			XmlDocument doc = new XmlDocument();
			ConfigNode root = new ConfigNode();
			
			if (Logger != null)
				Logger.Info("Reading configuration file {0}", filename);
			
			doc.Load(filename);
			root = ParseNode(doc.DocumentElement);
			root.Children.Each(i => Sections.AddOverwrite(i));
		}
		
		public void WriteFile(string filename, string[] sections)
		{
			XmlDocument doc = new XmlDocument();
			doc.AppendChild(doc.CreateElement("configuration"));

			foreach (string i in sections) {
				XmlElement section = doc.CreateElement("section");
				section.SetAttribute("name", Sections[i].Name);
				WriteNode(Sections[i], section, doc);
				doc.DocumentElement.AppendChild(section);
			}
				
			doc.Save(filename);
		}
		
		public IDictionary<string, ConfigNode> Sections
		{
			get; set;
		}
		
		public Logger Logger
		{
			get; set;
		}
		
		private ConfigNode ParseNode(XmlNode node)
		{
			ConfigNode result = new ConfigNode();
			
			result.Name = (node.Attributes["name"] != null ? node.Attributes["name"].Value : "");
			result.Children = new Dictionary<string, ConfigNode>();
			result.Properties = new Dictionary<string, string>();
			
			if (Logger != null)
				Logger.Debug("Found section '{0}'", result.Name);
			
			foreach (XmlNode i in node.SelectNodes("section")) 
				result.Children.Add(i.Attributes["name"].Value,
				    ParseNode(i));

			foreach (XmlNode i in node.SelectNodes("property")) {
				string name = i.Attributes["name"] != null 
				    ? i.Attributes["name"].Value 
				    : string.Format("<anon_{0:x8}>", i.GetHashCode());
				
				if (Logger != null)
					Logger.Debug("Found property '{0}' = '{1}'", name, i.InnerText);
				
				result.Properties.Add(name, i.InnerText);
			}
			
			return result;
		}
		
		private void WriteNode(ConfigNode node, XmlNode xml, XmlDocument doc)
		{
			foreach (var i in node.Children) {
				XmlElement section = doc.CreateElement("section");
				section.SetAttribute("name", i.Key);
				WriteNode(i.Value, section, doc);
				xml.AppendChild(section);
			}
			
			foreach (var i in node.Properties) {
				XmlElement prop = doc.CreateElement("property");
				prop.SetAttribute("name", i.Key);
				prop.InnerText = i.Value;
				xml.AppendChild(prop);
			}
		}
	}
}
