using System;
using System.Collections.Generic;
using System.Threading;
using System.Net.Sockets;
using System.IO;
using Mono.Unix;

namespace FusionPOS
{
	public delegate void DeviceAttached(string path);
	public delegate void DeviceDetached(string path);
	
	public class DevdWatcher
	{
		public DevdWatcher(string pipe)
		{
			m_Pipe = new Socket(AddressFamily.Unix, SocketType.Stream, ProtocolType.IP);
			m_Pipe.Connect(new UnixEndPoint(pipe));
			m_Reader = new StreamReader(new NetworkStream(m_Pipe));
		}
		
		public void Start()
		{
			m_Worker = new Thread(this.Worker);
			m_Worker.Start();	
		}
		
		private void Worker()
		{
			while (m_Pipe.Connected) {
				IDictionary<string, string> eventData;
				string line = m_Reader.ReadLine();
				
				if (line[0] != '!')
					continue;

				/* Parse received event */
				line = line.Remove(0, 1);
				eventData = new Dictionary<string, string>();
				foreach (string tuple in line.Split(' ')) {
					string[] item = tuple.Split('=');
					if (item.Length == 2)
						eventData.Add(item[0], item[1]);
				}

				if (eventData["type"] == "CREATE") {
					if (Attached != null)
						Attached("/dev/" + eventData["cdev"]);
				}
				
				if (eventData["type"] == "DESTROY") {
					if (Detached != null)
						Detached("/dev/" + eventData["cdev"]);
				}
			}
		}
		
		public event DeviceAttached Attached;
		public event DeviceDetached Detached;
		
		private Thread m_Worker;
		private Socket m_Pipe;
		private StreamReader m_Reader;            
	}
}

