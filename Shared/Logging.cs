using System;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace FusionPOS.Logging
{
	public enum LogPriority
	{
		Debug, Info, Standard, Warning, Error, Critical	
	}
	
	public struct LogFilter
	{
		public LogPriority[] Priorities;
		public string[] Modules;
	}

	public class LoggerContext
	{	
		public LoggerContext()
		{
			m_OutputStreams = new Dictionary<TextWriter, LogFilter>();
		}
		
		public void AddOutputStream(TextWriter stream)
		{
			AddOutputStream(stream, new LogFilter {
				Priorities = null,
				Modules = null
			});
		}
		
		public void AddOutputStream(TextWriter stream, LogFilter filter)
		{
			m_OutputStreams.Add(stream, filter);
		}
		
		public void Log(string module, LogPriority priority, string message)
		{
			string formatted = String.Format(
			    "{0:r} {1,12} {2,12}: {3}",
			    DateTime.Now,
			    module,
			    priority.ToString(),
			    message);
			
			foreach (KeyValuePair<TextWriter, LogFilter> i in m_OutputStreams) {
				if (i.Value.Modules != null && !i.Value.Modules.Contains(module))
					continue;
				
				if (i.Value.Priorities != null && !i.Value.Priorities.Contains(priority))
					continue;
				
				i.Key.WriteLine(formatted);
			}
		}
		
		private IDictionary<TextWriter, LogFilter> m_OutputStreams;
	}
	
	public class Logger
	{
		public Logger(LoggerContext context, string module)
		{
			m_Context = context;
			m_Module = module;
		}
		
		public void Debug(string msg, params object[] args)
		{
			m_Context.Log(m_Module, LogPriority.Debug, string.Format(msg, args));
		}
		
		public void Info(string msg, params object[] args)
		{
			m_Context.Log(m_Module, LogPriority.Info, string.Format(msg, args));
		}
		
		public void Standard(string msg, params object[] args)
		{
			m_Context.Log(m_Module, LogPriority.Standard, string.Format(msg, args));
		}
		
		public void Warning(string msg, params object[] args)
		{
			m_Context.Log(m_Module, LogPriority.Warning, string.Format(msg, args));
		}
		
		public void Error(string msg, params object[] args)
		{
			m_Context.Log(m_Module, LogPriority.Error, string.Format(msg, args));
		}
		
		public void Critical(string msg, params object[] args)
		{
			m_Context.Log(m_Module, LogPriority.Critical, string.Format(msg, args));
		}
			
		private LoggerContext m_Context;
		private string m_Module;
	}
}

