using System;
using Gtk;
using FusionPOS;

namespace FusionPOS
{
	public class BaseForm: Gtk.EventBox
	{
		public BaseForm()
		{
		}
		
		public virtual void Load(params object[] args)
		{
		}
		
		public virtual void OnLeaving()
		{
		}
		
		public virtual void OnShow()
		{
		}
	}
}

