using System;
using Gtk;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public class TitleBox: Gtk.Window
	{
		public TitleBox(): base(WindowType.Popup)
		{
			Gtk.Alignment align = new Gtk.Alignment(1, 0, 0, 1);
			Widgets.VBox vbox = new Widgets.VBox();
			m_ButtonsBox = new Gtk.HBox(true, 0);
			m_ButtonsBox.BorderWidth = 10;
			m_Placement = new EventBox();
			m_Placement.BorderWidth = 20;
			m_TitleBar = new Widgets.WindowLabel("", "Inverted");
			m_TitleBar.SetSizeRequest(-1, 40);
			
			align.Add(m_ButtonsBox);
			vbox.PackStartSimple(m_TitleBar);
			vbox.PackStartSimple(m_Placement);
			vbox.PackStartSimple(align);
			
			StyleContext.AddClass("frame");
			StyleContext.AddClass("TitleBox");
			Add(vbox);
			Modal = true;
			BorderWidth = (uint)StyleContext.GetMargin(StateFlags.Normal).Top;
			TransientFor = BaseWindow.getInstance();
			WindowPosition = WindowPosition.Center;
			//SetSizeRequest(500, 250);
		}
		
		public void AddButton(string label, EventHandler clicked)
		{
			Widgets.Button button = new Widgets.Button(label, 100, 80);
			button.Clicked += clicked;
			m_ButtonsBox.PackStart(button, true, true,
			    m_ButtonsBox.Children.Length == 0 ? 10U : 0U);
		}
		
		public void AddImageButton(string label, string image, EventHandler clicked)
		{
			Gtk.Button button = Widgets.ImageButtonFactory.ImageButton(label, image);
			button.Clicked += clicked;
			m_ButtonsBox.PackStart(button, true, true,
			    m_ButtonsBox.Children.Length == 0 ? 10U : 0U);
		}
		
		/*protected override bool OnDrawn(Cairo.Context cr)
		{
			base.OnDrawn(cr);
			StyleContext.RenderFrame(cr, 0, 0, AllocatedWidth, AllocatedHeight);
			(cr as IDisposable).Dispose();
			return true;
		}*/
		
		protected Widgets.WindowLabel m_TitleBar;
		protected Gtk.EventBox m_Placement;
		protected Gtk.HBox m_ButtonsBox;
	}
}

