using System;
using System.Runtime.InteropServices;

namespace FusionPOS
{
	public class SystemName
	{
		public static string Uname()
		{
			IntPtr buf;
	
			try {
				buf = Marshal.AllocHGlobal(1024);
				uname(buf);
				return Marshal.PtrToStringAnsi(buf);
			} catch { } finally {
				if (buf != IntPtr.Zero)
					Marshal.FreeHGlobal(buf);
			}
	
			return "Unknown";			
		}
	
		[DllImport("libc")]
		private static extern int uname(IntPtr buf);
	}
}
