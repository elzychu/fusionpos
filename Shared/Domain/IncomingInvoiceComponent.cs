using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using FusionPOS;

namespace FusionPOS.Domain
{
	public class IncomingInvoiceComponent
	{
		public virtual long Id { get; set; }
		public virtual long IdOnInvoice { get; set; }
		public virtual IncomingInvoice Invoice { get; set; }
		public virtual Ingredient Ingredient { get; set; }
		public virtual decimal Amount { get; set; }
		public virtual decimal NetUnitPrice { get; set; }
		public virtual decimal VatPercent { get; set; }
		public virtual UnitOfMeasure UnitOfMeasure { get; set; } 
		public virtual string Pkwiu { get; set; }
		
		public virtual decimal TaxValue {
			get {
				return Amount * NetUnitPrice * VatPercent; 	
			}
		}
		
		public virtual decimal NetValue {
			get {
				return Amount * NetUnitPrice;
			}
		}
		
		public virtual decimal GrossValue {
			get {
				return NetValue*VatPercent + NetValue;	
			}
		}
		
		public IncomingInvoiceComponent()
		{
		}
	}
}

