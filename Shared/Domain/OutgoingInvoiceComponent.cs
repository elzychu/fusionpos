using System;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace FusionPOS.Domain
{
	[Serializable]
	public class OutgoingInvoiceComponent: INamedDomainObject
	{
		[DataMember] public virtual long Id { get; set; }
		[DataMember] public virtual long IdOnInvoice { get; set; }
		[DataMember] public virtual string Name { get; set; }
		[DataMember] public virtual decimal Amount { get; set; }
		[DataMember] public virtual decimal NetUnitPrice { get; set; }
		[DataMember] public virtual decimal VatPercent { get; set; }
		[DataMember] public virtual UnitOfMeasure UnitOfMeasure { get; set; } 
		[DataMember] public virtual string Pkwiu { get; set; }
		
		public OutgoingInvoiceComponent()
		{
		}
		
		public virtual decimal ValueNet
		{
			get { return NetUnitPrice * Amount; }
		}
		
		public virtual decimal ValueGross
		{
			get { return ValueNet * (1 + (VatPercent /100)); }
		}
		
		public virtual decimal TaxValue
		{
			get { return ValueNet * (VatPercent / 100); }
		}
	}
}

