using System;
using System.ComponentModel.DataAnnotations;

namespace FusionPOS.Domain
{
	public class Device: INamedDomainObject
	{
		public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		[Required]
		public virtual string IpAddress { get; set; }
		[Required]
		public virtual string Key { get; set; }
		[Required]
		public virtual bool Enabled { get; set; }

		public Device()
		{
		}
	}
}

