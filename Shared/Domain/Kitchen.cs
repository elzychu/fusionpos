using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	[DataContract(IsReference = true)] [Serializable]
	public class Kitchen
	{
		[DataMember] public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		[Required]
		public virtual string PrinterPort { get; set; }
		public virtual string PrinterConfiguration { get; set; }
		public virtual bool MainPrinter { get; set; }
		
		public Kitchen()
		{
		}
	}
}

