using System;

namespace FusionPOS.Domain
{
	public interface IDishComponent
	{
		long Id { get; set; }
		string Name { get; set; }
		decimal Price { get; set; }
	}
}

