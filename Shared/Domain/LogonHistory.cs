using System;

namespace FusionPOS.Domain
{
	public enum LogonAction
	{
		Logon, Logout
	}
	
	public enum LogonDomain
	{
		POS, Manager
	}
	
	public class LogonHistory: IDomainObject
	{
		public virtual long Id { get; set; }
		public virtual User User { get; set; }
		public virtual Device Device { get; set; }
		public virtual DateTime Timestamp { get; set; }
		public virtual LogonAction Action { get; set; }
		public virtual LogonDomain Domain { get; set; }
		
		public LogonHistory()
		{
		}
	}
}

