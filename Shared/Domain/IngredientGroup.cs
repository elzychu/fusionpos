using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	[DataContract(IsReference = true)] [Serializable]
	public class IngredientGroup: INamedDomainObject, IDeletableDomainObject
	{
		[DataMember] public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		[ScriptIgnore]
		public virtual IList<Ingredient> Ingredients { get; set; }
		public virtual bool Deleted { get; set; }
		public IngredientGroup()
		{
		}
	}
}

