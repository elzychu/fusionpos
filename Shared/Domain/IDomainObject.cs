using System;

namespace FusionPOS.Domain
{
	public interface IDomainObject
	{
		long Id { get; set; }
	}
	
	public interface INamedDomainObject: IDomainObject
	{
		string Name { get; set; }
	}
	
	public interface IDeletableDomainObject: IDomainObject
	{
		bool Deleted { get; set; }	
	}
}

