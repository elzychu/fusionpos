using System;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	public abstract class Destination
	{
		public virtual long Id { get; set; }
		[ScriptIgnore] public virtual Order Order { get; set; }
		public virtual string Description { get; set; }
		
		public Destination()
		{
		}
		
		public abstract string Describe { get; }
	}
	
	public class DestinationNone: Destination
	{
		public override string Describe
		{
			get {
				return "Odbiór osobisty";
			}
		}
	}
	public class DestinationOnSite: Destination
	{
		public virtual Table Table { get; set; }
		
		public override string Describe
		{
			get {
				if (Table != null)
					return String.Format("Stolik:\n#{0} ({1})", Table.Number, Table.Parent.Name);	
			
				return "Brak informacji";
			}
		}
	}
	public class DestinationDelivery: Destination
	{
		public virtual string Address { get; set; }
		public virtual DateTime DeliveryTime { get; set; }
		public virtual User Supplier { get; set; }
		
		public override string Describe
		{
			get {
				if(Address != null)
					return String.Format("Dostawa na:\n{0}", Address);
			
				return "Brak informacji";
			}
		}
	}
}

