using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace FusionPOS.Domain
{
	public class DishCategory: INamedDomainObject, IDeletableDomainObject
	{
		public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		public virtual string Color { get; set; }
		[ScriptIgnore]
		public virtual DishCategory Parent { get; set; }
		[ScriptIgnore]
		public virtual IList<DishCategory> Children { get; set; }
		[ScriptIgnore]
		public virtual IList<Dish> Dishes { get; set; }
		public virtual bool Deleted { get; set; }
		public const long RootCategory = 1L;

		public DishCategory ()
		{
		}
	}
}

