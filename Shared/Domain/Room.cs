using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace FusionPOS.Domain
{
	public class Room
	{
		public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		[Required] [Number]
		public virtual uint Width { get; set; }
		[Required] [Number]
		public virtual uint Height { get; set; }
		[ScriptIgnore]
		public virtual IList<Table> Tables { get; set; }
		[ScriptIgnore]
		public virtual IList<Wall> Walls  { get; set; }
		
		public Room ()
		{
		}
	}
}

