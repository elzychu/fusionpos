using System;
using System.ComponentModel.DataAnnotations;
using FusionPOS;

namespace FusionPOS.Domain
{
	public class Tax
	{
		public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		[Required]
		public virtual char Code { get; set; }
		[Required] [Number]
		public virtual decimal PercentValue { get; set; }
		
		public Tax()
		{
		}
		
		public virtual decimal Value
		{
			get { return PercentValue / 100; }
		}
	}
}

