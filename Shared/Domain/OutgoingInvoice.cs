using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	[DataContract(IsReference = true)] [Serializable]
	public class OutgoingInvoice: IDomainObject
	{
		[DataMember] public virtual long Id { get; set; }
		[Required]
		[DataMember] public virtual string Number { get; set; }
		[Required]
		[DataMember] public virtual DateTime IssueTime { get; set; }
		[Required]
		[DataMember] public virtual DateTime SellTime { get; set; }
		[Required]
		[DataMember] public virtual string BuyerName { get; set; }
		[Required]
		[DataMember] public virtual string BuyerAddress { get; set; }
		[Required] [Nip]
		[DataMember] public virtual string BuyerNip { get; set; }
		[Required]
		[DataMember] public virtual PaymentMethod PaymentMethod { get; set; }
		[DataMember] public virtual string BankName { get; set; }
		[DataMember] public virtual string BankAccount { get; set; }
		[DataMember] public virtual DateTime PaymentTime { get; set; }
		[ScriptIgnore]
		[DataMember] public virtual IList<OutgoingInvoiceComponent> Components { get; set; }

		public OutgoingInvoice()
		{ }
		
		public virtual decimal TotalValueNet
		{
			get {
				if (Components == null)
					return 0;
				
				return Components.Sum(i => i.ValueNet);
			}
		}
		
		public virtual decimal TotalValueGross
		{
			get {
				if (Components == null)
					return 0;
				
				return Components.Sum(i => i.ValueGross);
			}			
		}
		
		public virtual decimal TotalValueTax
		{
			get {
				if (Components == null)
					return 0;
				
				return Components.Sum(i => i.TaxValue);
			}			
		}
	}
}

