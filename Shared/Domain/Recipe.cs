using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	[DataContract(IsReference = true)] [Serializable]
	public class Recipe
	{
		[DataMember] public virtual long Id { get; set; }
		public virtual string Protips { get; set; }
		public virtual Dish Dish { get; set; }
		public virtual IList<RecipeIngredient> Ingredients { get; set; }
		
		public Recipe ()
		{
		}
	}
}

