using System;

namespace FusionPOS.Domain
{
	public class DishGroupMapping
	{
		public virtual Dish Dish { get; set; }
		public virtual DishGroup DishGroup { get; set; }
		
		public DishGroupMapping()
		{
		}
		
		public override bool Equals(object obj)
		{
			DishGroupMapping t = obj as DishGroupMapping;
			
			if (t == null)
				return false;
			
			if (Dish.Id == t.Dish.Id && DishGroup.Name == t.DishGroup.Name)
				return true;
			
			return false;
		}
		
		public override int GetHashCode()
		{
			return Dish.GetHashCode() ^ DishGroup.GetHashCode();
		}
	}
}

