using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	[DataContract(IsReference = true)] [Serializable]
	public class DishGroup: INamedDomainObject, IDeletableDomainObject
	{
		[DataMember] public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		public virtual IList<Dish> Dishes { get; set; }
		public virtual bool Deleted { get; set; }
		public DishGroup()
		{
		}
		
		public virtual string Describe()
		{
			return string.Join(", ", Dishes.Select(i => i.Name));
		}
	}
}

