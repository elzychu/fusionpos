using System;
using System.Web.Script.Serialization;

namespace FusionPOS.Domain
{
	public enum PaymentMethod
	{
		Cash,
		Card, 
		Check, 
		Credit, 
		Voucher, 
		Points
	}
	
	public class OrderPayment
	{
		public virtual long Id { get; set; }
		[ScriptIgnore] public virtual Order Order { get; set; }
		public virtual PaymentMethod Method { get; set; }
		public virtual decimal Amount { get; set; }
		
		public OrderPayment()
		{
		}
	}
}

