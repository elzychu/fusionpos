using System;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	public class OrderDishModification
	{
		public virtual long Id { get; set; }
		[ScriptIgnore] public virtual OrderDish Target { get; set; }
		public virtual RecipeIngredient Ingredient { get; set; }
		public virtual int Amount { get; set; }
		public virtual decimal PriceChange { get; set; }
		
		public OrderDishModification()
		{
		}
	}
}
