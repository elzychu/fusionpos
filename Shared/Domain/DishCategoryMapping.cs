using System;

namespace FusionPOS.Domain
{
	public class DishCategoryMapping
	{
		public virtual Dish Dish { get; set; }
		public virtual DishCategory DishCategory { get; set; }
		
		public DishCategoryMapping()
		{
		}
		
		public override bool Equals (object obj)
		{
			DishCategoryMapping t = obj as DishCategoryMapping;
			
			if (t == null)
				return false;
			
			if (Dish.Id == t.Dish.Id && DishCategory.Id == t.DishCategory.Id)
				return true;
			
			return false;
		}
		
		public override int GetHashCode()
		{
			return Dish.GetHashCode() ^ DishCategory.GetHashCode();
		}
	}
}

