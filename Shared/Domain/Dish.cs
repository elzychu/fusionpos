using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	[DataContract(IsReference = true)] [Serializable]
	public class Dish: IDishComponent, INamedDomainObject, IDeletableDomainObject
	{
		[DataMember] public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		public virtual int PositionNumber { get; set; }
		[ScriptIgnore]
		public virtual Recipe Recipe { get; set; }
		[Required]
		public virtual decimal Price1 { get; set; }
		public virtual decimal Price2 { get; set; }
		public virtual decimal RebatePercent { get; set; }
		public virtual decimal RebateAmount { get; set; }
		public virtual long Points { get; set; }
		[Required]
		public virtual Tax Tax { get; set; }
		public virtual string Description { get; set; }
		public virtual string Color { get; set; }
		public virtual bool IsPizza { get; set; }
		public virtual bool IsDrink { get; set; }
		public virtual bool Deleted { get; set; }
		[Required]
		public virtual Kitchen Kitchen { get; set; }
		[ScriptIgnore]
		public virtual IList<DishCategory> Categories { get; set; }
		[ScriptIgnore]
		public virtual IList<DishGroup> Groups { get; set; }
		
		public virtual decimal Price
		{
			get { return Price1; }
			set { Price1 = value; }
		}
		
		public Dish()
		{
		}
		
		public virtual decimal FinalValueNet
		{
			get {
				decimal val = (Price1 - (RebatePercent / 100) * Price1) - RebateAmount;
				return Math.Max(val, 0);
			}
		}
		
		public virtual decimal FinalValueGross
		{
			get {
				return FinalValueNet + TaxValue;
			}
		}
		
		public virtual decimal FinalValueWithoutRebateNet
		{
			get {
				return Price1;
			}
		}
		
		public virtual decimal FinalValueWithoutRebateGross
		{
			get {
				if(Tax == null)
					return 0m;
				
				return Price1 * (1 + Tax.Value);
			}
		}
		
		public virtual decimal TaxValue
		{
			get {
				if (Tax == null)
					return 0m;
				
				return FinalValueNet * Tax.Value;
			}
		}
	}
}

