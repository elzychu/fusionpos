using System;

namespace FusionPOS.Domain
{
	public class SessionItem
	{
		public virtual string Key { get; set; }
		public virtual byte[] Value { get; set; }
	}
}

