using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace FusionPOS.Domain
{
	public enum UnitOfMeasure
	{
		PIECE, GRAM, KILOGRAM, LITER, SET	
	}
	
	[DataContract(IsReference = true)] [Serializable]
	public class Ingredient: IDishComponent, INamedDomainObject, IDeletableDomainObject
	{
		[DataMember] public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		public virtual decimal Quantity { get; set; }
		public virtual decimal MinimumQuantity { get; set; }
		[Required]
		public virtual UnitOfMeasure Unit { get; set; }
		[ScriptIgnore]
		public virtual IngredientCategory Category { get; set; }
		public virtual IList<IngredientGroup> Groups { get; set; }
		public virtual decimal Price { get; set; }
		public virtual bool Deleted { get; set; }
		
		public Ingredient()
		{
		}
	}
}

