using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	public class OrderDish
	{
		public virtual long Id { get; set; }
		[ScriptIgnore]	public virtual Order Order { get; set; }
		public virtual Dish Dish { get; set; }
		[ScriptIgnore] public virtual OrderDish Parent { get; set; }
		public virtual decimal Amount { get; set; }
		public virtual Kitchen Direction { get; set; }
		public virtual IList<OrderDishModification> Modifications { get; set; }
		
		public virtual decimal ValueNet
		{
			get {
				return Math.Round(Amount * ValueForPieceNet, 2);
			}
		}
		
		public virtual decimal ValueGross
		{
			get {
				return Math.Round(Amount * ValueForPieceGross, 2);
			}
		}
		
		public virtual decimal ValueWithoutRebateNet
		{
			get {
				return Math.Round(Amount * ValueForPieceWithoutRebateNet, 2);	
			}
		}
		
		public virtual decimal ValueWithoutRebateGross
		{
			get {
				return Math.Round(Amount * ValueForPieceWithoutRebateGross, 2);
			}
		}
		
		public virtual decimal TaxValue
		{
			get {
				return Math.Round(ValueGross - ValueNet, 2);
			}
		}
		
		public virtual decimal ValueForPieceWithoutRebateNet
		{
			get {
				decimal mod = Modifications.Sum(i => i.PriceChange);
				return Math.Round(Dish.FinalValueWithoutRebateNet + mod, 2);
			}
		}

		public virtual decimal ValueForPieceWithoutRebateGross
		{
			get {
				return ValueForPieceWithoutRebateNet * (1 + Dish.Tax.Value);
			}
		}
		
		public virtual decimal ValueForPieceNet
		{
			get {
				decimal mod = Modifications.Sum(i => i.PriceChange);
				return Math.Round(Dish.FinalValueNet + mod, 2);
			}	
		}
		
		public virtual decimal ValueForPieceGross
		{
			get {
				return Math.Round(ValueForPieceNet * (1 + Dish.Tax.Value), 2);
			}
		}
		
		public OrderDish()
		{
		}
	}
}

