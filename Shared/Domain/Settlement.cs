using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	public enum SettlementStatus
	{
		Opened, Closed
	};
	
	[DataContract(IsReference = true)] [Serializable]
	public class UserBalance 
	{
		[DataMember] public virtual long Id { get; set; }	
		[DataMember] public virtual User Owner { get; set; }
		[DataMember] public virtual decimal Balance { get; set; }
		[DataMember] public virtual Settlement Settlement { get; set; }
		[DataMember] public virtual DateTime Time { get; set; }
		[DataMember] public virtual SettlementStatus Status { get; set; }
	}
	
	[DataContract(IsReference = true)] [Serializable]
	public class Settlement
	{
		[DataMember] public virtual long Id { get; set; }
		[DataMember] public virtual DateTime From { get; set; }
		[DataMember] public virtual DateTime To { get; set; }
		[DataMember] public virtual decimal Balance { get; set; }
		public virtual User Owner { get; set; }
		public virtual IList<UserBalance> UserBalances { get; set; }
		[DataMember] public virtual SettlementStatus Status { get; set; }
	}
}

