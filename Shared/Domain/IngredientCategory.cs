using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;

namespace FusionPOS.Domain
{
	public class IngredientCategory: INamedDomainObject, IDeletableDomainObject
	{
		public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		[ScriptIgnore]
		public virtual IngredientCategory Parent { get; set; }
		[ScriptIgnore]
		public virtual IList<IngredientCategory> Children { get; set; }
		[ScriptIgnore]
		public virtual IList<Ingredient> Ingredients { get; set; }
		public virtual bool Deleted { get; set; }
		public const long RootCategory = 1L;
		
		public IngredientCategory()
		{
		}
	}
}

