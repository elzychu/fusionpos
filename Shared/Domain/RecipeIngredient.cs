using System;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	public abstract class RecipeIngredient
	{
		public virtual long Id { get; set; }
		public virtual Recipe Recipe { get; set; }
		public virtual decimal Amount { get; set; }
		public virtual bool Optional { get; set; }
		public abstract IDishComponent Component { get; }
		public abstract string Name { get; }
		public RecipeIngredient()
		{
		}
	}

	public class RecipeIngredientIngredient: RecipeIngredient
	{
		public virtual Ingredient Ingredient { get; set; }
		
		public override string Name
		{
			get {
				return Ingredient.Name;
			}
		}
		
		public override IDishComponent Component
		{
			get { return Ingredient; }
		}
	}

	public class RecipeIngredientDish: RecipeIngredient
	{
		public virtual Dish Dish { get; set; }
		
		public override string Name {
			get {
				return Dish.Name;	
			}
		}
		
		public override IDishComponent Component
		{
			get { return Dish; }
		}
	}

	public class RecipeIngredientIngredientGroup: RecipeIngredient
	{
		public virtual IngredientGroup IngredientGroup { get; set; }
		
		public override string Name
		{
			get {
				return IngredientGroup.Name;
			}
		}
		
		public override IDishComponent Component
		{
			get { return null; }
		}
	}
	
	public class RecipeIngredientDishGroup: RecipeIngredient
	{
		public virtual DishGroup DishGroup { get; set; }
		
		public override string Name
		{
			get {
				return DishGroup.Name;
			}
		}
		
		public override IDishComponent Component
		{
			get { return null; }
		}
	}
}

