using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using FusionPOS;

namespace FusionPOS.Domain
{
	public enum UserType
	{
		Waiter, Operator, Admin	
	}
	
	[DataContract(IsReference = true)] [Serializable]
	public class User: INamedDomainObject, IDeletableDomainObject
	{
		public virtual long Id { get; set; }
		[Required] [Number]
		public virtual string Pin { get; set; }
		[Required]
		public virtual string Name { get; set; }
		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }
		public virtual string Password { get; set; }
		[Required]
		public virtual UserType Type { get; set; }
		[ScriptIgnore]
		public virtual IList<Transaction> Transactions { get; set; }
		public virtual bool Deleted { get; set; }
		public User()
		{
		}
	}
}

