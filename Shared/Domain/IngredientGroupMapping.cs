using System;

namespace FusionPOS.Domain
{
	public class IngredientGroupMapping
	{
		public virtual Ingredient Ingredient { get; set; }
		public virtual IngredientGroup IngredientGroup { get; set; }
		
		public IngredientGroupMapping()
		{
		}
		
		public override bool Equals (object obj)
		{
			IngredientGroupMapping t = obj as IngredientGroupMapping;
			
			if (t == null)
				return false;
			
			if (Ingredient.Id == t.Ingredient.Id && IngredientGroup.Name == t.IngredientGroup.Name)
				return true;
			
			return false;
		}
		
		public override int GetHashCode()
		{
			return Ingredient.GetHashCode() ^ IngredientGroup.GetHashCode();
		}
	}
}

