using System;

namespace FusionPOS.Domain
{
	public class Currency: INamedDomainObject
	{
		public virtual long Id { get; set; }
		public virtual string Name { get; set; }
		public virtual string LongName { get; set; }
		public virtual bool MainCurrency { get; set; }
		public virtual decimal Ratio { get; set; }
		
		public Currency()
		{
		}
		
		public override string ToString()
		{
			return Name;
		}
	}
}

