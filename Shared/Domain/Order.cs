using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	public enum OrderStatus
	{
		Waiting, Delivery, Finalized, Completed, Canceled
	}
	
	abstract public class Transaction
	{
		public virtual long Id { get; set; }
		public virtual decimal TotalValue { get; set; }
		public virtual User Waiter { get; set; }
		public virtual DateTime Time { get; set; }
		public virtual Client Client { get; set; }
	}
	
	public class Payment: Transaction
	{
		
	}

	public class Order: Transaction
	{
		public virtual Destination Destination { get; set; }
		public virtual OrderStatus Status { get; set; }
		public virtual string Description { get; set; }
		public virtual decimal RebateAmount { get; set; }
		public virtual decimal RebatePercent { get; set; }
		[ScriptIgnore] public virtual OutgoingInvoice Invoice { get; set; }
		[ScriptIgnore] public virtual IList<OrderDish> Entries { get; set; }
		public virtual IList<OrderPayment> Payments { get; set; }
		
		public virtual decimal ValueGross
		{
			get {
				if (Entries == null)
					return 0;
				
				return Math.Round(Entries.Sum(e => e.ValueGross), 2);
			}
		}
		
		public virtual decimal ValueNet
		{
			get {
				if (Entries == null)
					return 0;
				
				return Math.Round(Entries.Sum(e => e.ValueNet), 2);
			}
		}
		
		public virtual decimal TaxValue
		{
			get {
				if (Entries == null)
					return 0;
				
				return Math.Round(Entries.Sum(e => e.TaxValue), 2);
			}
		}
		
		public virtual decimal FinalValue
		{
			get {
				return ValueGross - (ValueGross * (RebatePercent/100)) - RebateAmount;
			}
		}
		
		public virtual string Describe()
		{
			return String.Join(", ", Entries.Select(e => e.Dish.Name).ToArray());	
		}
		
		public virtual int TotalPoints
		{
			get {
				if(Entries == null)
					return 0;
				return (int)Entries.Sum(e => e.Dish.Points * e.Amount);
			}
		}
		
		public Order()
		{
		}
	}
}

