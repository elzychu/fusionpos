using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	public enum TableType
	{
		Round, Rect
	}
	
	[DataContract(IsReference = true)] [Serializable]
	public class Table: INamedDomainObject
	{
		[DataMember] public virtual long Id { get; set; }
		public virtual TableType Type { get; set; }
		public virtual uint Number { get; set; }
		public virtual uint X { get; set; }
		public virtual uint Y { get; set; }
		public virtual uint Radius { get; set; }
		public virtual uint Width { get; set; }
		public virtual uint Height { get; set; }
		[ScriptIgnore]
		public virtual Room Parent { get; set; }
		[ScriptIgnore]
		public virtual IList<Reservation> Reservations {get; set;}
		
		public virtual string Name
		{
			get { return string.Format("#{0} ({1})", Number, Parent.Name); }
			set { }
		}
		
		public Table()
		{
			
		}
	}
	
	public class Wall 
	{
		public virtual long Id { get; set; }
		public virtual uint X { get; set; }
		public virtual uint Y { get; set; }
		public virtual uint Width { get; set; }
		public virtual uint Height { get; set; }
		[ScriptIgnore]
		public virtual Room Parent { get; set; }
		public Wall()
		{
			
		}
	}
}

