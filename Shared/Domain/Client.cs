using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;

namespace FusionPOS.Domain
{
	[Serializable] [DataContract(IsReference = true)]
	public class Client: INamedDomainObject, IDeletableDomainObject
	{
		public virtual long Id { get; set; }
		[Required]
		public virtual string Name { get; set; }
		public virtual string PhoneNumber { get; set; }
		public virtual string Address { get; set; }
		public virtual string Description { get; set; }
		[Nip]
		public virtual string NIP { get; set; }
		public virtual bool Company { get; set; }
		public virtual long Points { get; set; }
		public virtual decimal Credit { get; set; }
		public virtual decimal MaxCredit { get; set; }
		public virtual bool Deleted { get; set; }
		[ScriptIgnore]
		public virtual IList<Reservation> Reservations { get; set; }
		
		public Client()
		{
		}
	}
}

