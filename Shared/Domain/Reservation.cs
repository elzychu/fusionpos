using System;
	
namespace FusionPOS.Domain
{
	public class Reservation: IDomainObject
	{
		public virtual long Id { get; set; }
		public virtual DateTime Date { get; set; }
		public virtual Client ParentClient { get; set; }
		public virtual Table ParentTable { get; set; }
		public virtual string AdditionalNotes { get; set; }
		
		public Reservation ()
		{ }
	}
}

