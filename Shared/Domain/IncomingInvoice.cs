using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using FusionPOS;

namespace FusionPOS.Domain
{	
	public class IncomingInvoice
	{
		public virtual long Id { get; set; }	
		public virtual string Number { get; set; }
		public virtual DateTime IssueTime { get; set; }
		public virtual DateTime SellTime { get; set; }
		[ScriptIgnore]
		public virtual IList<IncomingInvoiceComponent> Components { get; set; }
		public virtual string SellerName { get; set; }
		public virtual string SellerAddress { get; set; }
		public virtual string SellerNip { get; set; }
		public virtual PaymentMethod PaymentMethod { get; set; }
		public virtual string BankName { get; set; }
		public virtual string BankAccount { get; set; }
		public virtual DateTime PaymentTime { get; set; }
		public virtual string SellerPersonName { get; set; }
		
		public virtual decimal TotalNetValue
		{
			get {
				if(Components == null)
					return 0;
				else
				{
					decimal sum = 0m;
					foreach(IncomingInvoiceComponent i in Components)
					{
						sum += i.NetValue;
					}
					return sum;
				}
			}
		}
		
		public virtual decimal TotalGrossValue
		{
			get {
				if(Components == null)
					return 0;
				else
				{
					decimal sum = 0m;
					foreach(IncomingInvoiceComponent i in Components)
					{
						sum += i.GrossValue;
					}
					return sum;
				}
			}
		}
		
		public IncomingInvoice()
		{
		}
	}
}

