using System;
using Pango;

namespace FusionPOS
{
	public class FontSettings
	{
		public const string BiggerFont = "BigFont";
	}
	
	public class GuiStandards
	{
		public const int StandardButtonHeight = 100;
		public const int StandardButtonWidth = 150;
	}
}

