using System;
using System.Diagnostics;

namespace FusionPOS
{
	public class ProcessLauncher
	{
		public static void Launch(string name, params string[] args)
		{
			ProcessStartInfo psi = new ProcessStartInfo();
			psi.FileName = LookupFilename(name);
			psi.Arguments = string.Join(" ", args);
			
			Process p = Process.Start(psi);
			//p.WaitForExit();
			return;
		}
		
		private static string LookupFilename(string filename)
		{
			return string.Format("/Applications/{0}/{0}.exe", filename);	
		}
	}
}

