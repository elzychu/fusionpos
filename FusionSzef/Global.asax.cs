﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FusionSzef
{
	public class MvcApplication: System.Web.HttpApplication
	{
		public static void RegisterRoutes (RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.IgnoreRoute("{resource}.svc/{*pathInfo}");
			routes.MapRoute("AjaxList", "AjaxList/{domain}", new { controller = "Ajax", action = "AjaxList" });
			routes.MapRoute("AjaxListServer", "AjaxListServer/{domain}", new { controller = "Ajax", action = "AjaxListServer" });
			routes.MapRoute("AjaxListAutocomplete", "AjaxListAutocomplete/{domain}", new { controller = "Ajax", action = "AjaxListAutocomplete" });
			
			routes.MapRoute(
			    "Default",
			    "{controller}/{action}/{id}",
			    new { controller = "Home", action = "Index", id = "" });
		}
		
		public static Context PosContext
		{
			get { 
				if (m_Context == null) {
					m_Context = new Context();
					m_Context.Initialize();				
				}
				
				return m_Context; 
			}
		}

		protected void Application_Start()
		{
			#pragma warning disable 219
			FusionSzef.Context unused = MvcApplication.PosContext;
			RegisterRoutes(RouteTable.Routes);
		}
		
		private static Context m_Context;
	}
}
