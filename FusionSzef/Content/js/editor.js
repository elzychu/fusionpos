var ActionManager = new function() {
	this.actionsList = new Array();
	
	this.doActions = function()
	{
		for(var i=0; i<this.actionsList.length; ++i)
			this.actionsList[i]();
	};

	this.init = function(time)
	{
			window.setInterval("this.doActions", time);
	};
};

var MouseButton = new function() {
this.LEFT = 0; this.RIGHT = 1
};

function MouseClick(x, y, button) {
	this.x = x;
	this.y = y; 
	this.button = button; 
};

var ActionHandler = new function() {
	this.mouseMoveEventList = [];
	this.mouseDownEventList = [];
	this.mouseUpEventList = [];
	this.mouseDblClickEventList = [];
	this.x = 0;
	this.y = 0;
	this.rememberCoord = 0;


	this.dblclick = function(e) {
		for(var i=0; i<ActionHandler.mouseDblClickEventList.length; ++i)
		{
			ActionHandler.mouseDblClickEventList[i](e.clientX - Display.x,
				 e.clientY - Display.x);
		}
	};
	
	this.mouseDown = function(e) {
		for(var i=0; i<ActionHandler.mouseDownEventList.length; ++i)
		{
			var click = new MouseClick(e.clientX - Display.x, e.clientY - Display.y);
			if ( (navigator.appName.toLowerCase()).indexOf("explorer") != -1)
			{
				if(e.button == 1)
				{
					click.button = MouseButton.LEFT;
				}	
				else if(e.button == 2)
				{
					click.button = MouseButton.RIGHT;
				}
				else
					return;
			}
			else
			{
				if(e.button == 0)
				{
					click.button = MouseButton.LEFT;
				} 
				else if(e.button == 2)
				{
					click.button = MouseButton.RIGHT;
				}
				else
					return;
			}			
			
			ActionHandler.mouseDownEventList[i](click);
		}
	};
	
	this.mouseUp = function(e) {
		for(var i=0; i<ActionHandler.mouseUpEventList.length; ++i)
		{
			var click = new MouseClick(e.clientX - Display.x, e.clientY - Display.y);
			if ( (navigator.appName.toLowerCase()).indexOf("explorer") != -1)
			{
				if(e.button == 1)
				{
					click.button = MouseButton.LEFT;
				}	
				else if(e.button == 2)
				{
					click.button = MouseButton.RIGHT;
				}
				else
					return;
			}
			else
			{
				if(e.button == 0)
				{
					click.button = MouseButton.LEFT;
				} 
				else if(e.button == 2)
				{
					click.button = MouseButton.RIGHT;
				}
				else
					return;
			}			
			
			ActionHandler.mouseUpEventList[i](click);
		}
	};
	
	this.mouseMove = function(e) {
		ActionHandler.x = e.clientX - Display.x;
		ActionHandler.y = e.clientY - Display.y;

		for(var i=0; i<ActionHandler.mouseMoveEventList.length; ++i)
		{
			ActionHandler.mouseMoveEventList[i](e.clientX - Display.x,
				 e.clientY - Display.y);
		}
	};

	this.init = function()
	{            
            Display.canvas.addEventListener("mousemove", this.mouseMove, true);
            Display.canvas.addEventListener("mousedown", this.mouseDown, true);
            Display.canvas.addEventListener("mouseup", this.mouseUp, true);
            Display.canvas.addEventListener("dblclick", this.dblclick, true);
	};
	
};

var Display = new function() {
	this.width = 0;
	this.x = 0;
	this.y = 0;
	this.height = 0;
	this.ctx = 0;
	this.canvas = 0;
	
	this.init = function()
	{
		this.canvas = document.getElementById("canvas");
		this.ctx = this.canvas.getContext("2d");
		this.updatePos();	
		this.width = this.canvas.width;
		this.height = this.canvas.height;
		
		document.addEventListener("scroll", this.updatePos, true);
	};
	
	this.updatePos = function(e)
	{
		Display.x = 0;
		Display.y = 0;
		var obj = Display.canvas;
		while (obj.tagName != 'BODY') {
			Display.x += obj.offsetLeft;
			Display.y += obj.offsetTop;
			obj = obj.offsetParent; 
		}
		
		Display.x -= window.pageXOffset;
		Display.y -= window.pageYOffset;
	}
	
	this.clrscr = function()
	{
		this.ctx.clearRect(0, 0, this.width, this.height);
		this.ctx.fillStyle = 'rgb(0, 0, 20, 0)';
		this.ctx.strokeRect(0, 0, this.width, this.height);
	};
};
