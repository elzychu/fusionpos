var dataTableDeleteRow = '<img src="/Content/images/delete.gif" onclick="$(this).parents(\'table\').dataTable().fnDeleteRow($(this).parents(\'tr\')[0]);"/>';
var dataTableDeleteLink = '<img src="/Content/images/delete.gif" onclick="$.pos.gourl(\'%s/%s\')"/>';
var dataTableHiddenId = '<input type="hidden" name="Groups[{{iDataRow}}].Id" value="{{aData.Id}}"/>';

jQuery().ready(function($) {
	$.fn.getRoute = function(element)
	{
		return "/" + eval($(this).attr("route")).join("/");
	};
	
	$.fn.dataTableExt.oApi.fnDataUpdate = function(settings, row, key, value)
	{
		var rowidx = settings.oInstance.fnGetPosition(row)[0];
		settings.aoData[rowidx]._aData[key] = value;
	}

	$.fn.dataTableAddUnique = function(row)
	{
		var exists = false;
		$.each($(this).dataTable().fnGetData(), function(idx, i) {
			if (i.Id == row.Id)
				exists = true;
		});
		
		if (!exists)
			$(this).dataTable().fnAddData(row);
		
		return !exists;
	};
	
	$.widget("ui.combo", {
		_create: function() {
			var widget = this;
			var self = this.element;

			var field = $("<input>")
			    .attr("type", "hidden")
			    //.attr("id", self.attr("hidden-id"))
			    .attr("name", self.attr("hidden-id"))
			    .val(self.attr("value-id"))
			    .insertAfter(self);
			
			self
			    .addClass("combobox")
			    .val(self.attr("value-name"))
			    .click(function(event, data) {
			        if (self.autocomplete("widget").is(":visible")) {
			                self.autocomplete("close");
			                return;
			        }
			        
			        //self.blur();
				self.autocomplete("search", "");
				self.focus();
			    })
			    .autocomplete({
				minLength: 0,
				source: self.attr("remote"),
				select: function(event, ui) {
					if (!ui.item) {
						$(this).val("");
						return false;
					}
					
					self.data = ui.item;
					field.val(ui.item.id);
					
					return widget._trigger("selected", event, ui);
				},
				
				change: function(event, ui) {
					if (!ui.item) {
						$(this).val("");
						return false;
					}
				}
			    });
			
		}
	
	});
	
	$.pos =
	{
		route: function(data)
		{
			return "/" + data.join("/");
		},
		
		go: function(data, params)
		{
			query = params != undefined ? "?" + $.param(params) : "";
			window.location.href = $.pos.route(data) + query;
		},
		
		gourl: function(url)
		{
			window.location.href = url;
		},
		
		reload: function(context) {
			$('div[feature="tabs"]', context).tabs();
			
			$('table[feature="datatable"]', context).each(function(index, i) {
				var columns = [];
				$(this).find("td").each(function(idx, i) {
					var cell = $(this);
					columns.push({
						bUseRendered: $(cell).attr("property") ? false : true,
						sTitle: $(cell).attr("name"),
						sClass: $(cell).attr("class"),
						sWidth: $(cell).attr("width"),
						mDataProp: $(cell).attr("property"),
						fnRender: $(cell).html().trim().length > 0 || $(cell).attr("serializer") || $(cell).attr("type") ? function(obj) {
							if ($(cell).attr("serializer"))
								return $.pos.serializeToInputs(sprintf("%s[%d]", $(cell).attr("serializer"), obj.iDataRow), obj.aData);
						
							if ($(cell).attr("type") == "date")
								return new Date(parseInt(obj.aData[$(cell).attr("property")].replace(/[^0-9 +]/g, '')));
						
							if ($(cell).attr("type") == "price")
								return sprintf("%.2f", obj.aData[$(cell).attr("property")]);
		
							obj.__delete = dataTableDeleteRow;
							obj.__indexer = function() {
							return function(text, render) {
								return sprintf('<input type="password" name="%s.Index" value="%d"/>',
									text, obj.iDataRow);
							}};
							obj.__deleteLink = function() {
							return function(text, render) {
								return sprintf(dataTableDeleteLink, text,
									obj.aData[$(cell).attr("property")]);
							}};
							obj.__boolean = function() {
							return function(text, render) {
								return sprintf('<img src="/Content/images/%s.png"/>', 
									render(text) == "true" ? "green" : "red");
							}};
							
							return Mustache.to_html($(cell).html(), obj);
						} : undefined
					});
				});
				
				var large = $(this).attr("large") != "false";
			
				$(i).dataTable({
					bProcessing: true,
					bPaginate: large,
					bFilter: large,
					bInfo: large,
					bSort: $(this).attr("sortable") != "false",
					bAutoWidth: $(this).attr("autowidth") == "true",
					bServerSide: $(this).attr("server-side") == "true",
					sPaginationType: "full_numbers",
					sDom: sprintf('<"dataTables_top"lf><"dataTables_%s"rt>ip', $(this).attr("type") ? $(this).attr("type") : "long"),
					aoColumns: columns,
					aaData: $(this).attr("data") ? eval($(this).attr("data")) : undefined,
					sAjaxSource: $(this).attr("source"),
					fnRowCallback: function(row, data, _1, _2) { 
						if ($(this).attr("row-edit")) {
							$(row).find("td:not(.noclick)").each(function(index) {		
								$(this).css("cursor", "pointer");
								$(this).click(function(event) {
									window.location.href = $(i).attr("row-edit") + "/" + data.Id;
								});
							});
						}
						
						$.pos.reload(row); 
						return row; 
					}
				});
				
				$(i).dataTable().fnAdjustColumnSizing();
			});
			
			$('div[feature="treeview"]', context).each(function(idx, i) {
				$(i).jstree({
					plugins: ["json_data"],
					json_data: {
						ajax: { url: $(i).attr("source") }
					}
				});
			
				$(i).bind("loaded.jstree", function(event, data) {
					$(i).jstree("open_all");
				});
			});
			
			$('div[feature="dynamic"]', context).each(function(index) {
				if ($(this).attr("route"))
					$(this).load($(this).getRoute());
				
				if ($(this).attr("remote"))
					$(this).load($(this).attr("remote"));
			});
			
			$('input[feature="autocomplete"]', context).each(function(idx, i) {
				$(i).combo();
				$(i).removeAttr("remote feature");
			});
			
			$('input[feature="datepicker"]', context).each(function(idx, i) {
				$(i).datepicker();
				$(i).addClass("datepicker");
				$(i).removeAttr("feature");
			});
			
			$('form').each(function() {
				var items = {};
				$(":input", $(this)).each(function() {
					if ($(this).attr("validation"))
						items[$(this).attr("name")] = eval('(' + $(this).attr("validation") + ')');
				});
			
				$(this).validate({rules: items});
			});
			
			$('#content').css('visibility', 'visible');
		},
		
		deleteRow: function(table, row)
		{
			var index = $(table).dataTable().fnGetPosition(row);
			var data = $(table).dataTable().fnGetData();
			data.splice(index, 1);
			$(table).dataTable().fnClearTable();
			$(table).dataTable().fnAddData(data);
		},
		
		serializeToInputs: function(prefix, obj)
		{
			var result = "";
			
			$.each(obj, function(key, value) {
				if (!isNaN(key))
					return;
					
				result += sprintf('<input type="password" name="%s.%s" value="%s"/>', prefix, key, value);
			});
			
			return result;
		}
	}
	
	$.pos.reload(document);
});
