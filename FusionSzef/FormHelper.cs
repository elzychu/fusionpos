using System;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Script.Serialization;
using FusionPOS;
using FusionPOS.Domain;

namespace FusionSzef
{
	public static class FormHelper
	{
		public static Form<T> Form<T>(this HtmlHelper<T> self, string action = "Save", string method = "post")
		{
			return new Form<T>(self, action, method);
		}
		
		public static Form<T> Form<T>(this HtmlHelper self, T model, string action = "Save", string method = "post")
		{
			return new Form<T>(self, model, action, method);
		}
	}
	
	public class Form<T>: IDisposable
	{
		public Form(HtmlHelper<T> helper, string action = "save", string method = "post")
		{
			m_Helper = helper;
			m_Model = helper.ViewData.Model;
			Action = action;
			Method = method;
			Begin();
		}
		
		public Form(HtmlHelper helper, T model, string action = "save", string method = "post")
		{
			m_Helper = helper;
			m_Model = model;
			Action = action;
			Method = method;
			Begin();
		}
		
		public Form<T> Add(FormElement field)
		{
			field.Helper = m_Helper;
			Write(field.Render());
			return this;
		}
		
		public Form<T> Add(FormField field, Expression<Func<T, object>> renderer)
		{
			field.Helper = m_Helper;
			field.GetName = () => string.Join(".", (renderer as LambdaExpression).ReadPropertyNames());
			field.GetValidation = () => ValidationAttributeReader.ReadProperty((renderer as LambdaExpression).GetPropertyInfo());
			field.GetValue = () => {
				try {
					return renderer.Compile().Invoke(m_Model);
				} catch (NullReferenceException e) {
					return null;
				}
			};
			
			Write(field.Render());
			return this;
		}
		
		public void Dispose()
		{
			Write("</form>");
		}
		
		public string Action
		{
			get; set;
		}
		
		public string Method
		{
			get; set;
		}
		
		private void Begin()
		{
			Write(string.Format("<form class=\"form\" action=\"{0}\" method=\"{1}\">", Action, Method));
		}
		
		private void Write(string text)
		{
			m_Helper.ViewContext.Writer.Write(text);
		}
			
		private HtmlHelper m_Helper;
		private T m_Model;
	}
	
	public abstract class FormElement
	{
		public HtmlHelper Helper { get; set; }
		public abstract string Render();
	}
	
	public class FormHeader: FormElement
	{
		public FormHeader(string name)
		{
			m_Name = name;
		}
		
		public override string Render()
		{
			return string.Format("<h3>{0}</h3>", m_Name);
		}
		
		private string m_Name;
	}
	
	public class SubmitButton: FormElement
	{
		public SubmitButton(string title)
		{
			m_Title = title;
		}
		
		public override string Render()
		{
			return string.Format("<input type=\"submit\" value=\"{0}\"/>", m_Title);
		}
		
		private string m_Title;
	}
	
	public abstract class FormField: FormElement
	{
		public abstract string TemplateName { get; }
		public virtual string Title { get; set; }
		public virtual IDictionary<string, string> Attributes { get; set; }
		public Func<string> GetName { get; set; }
		public Func<object> GetValue { get; set; }
		public Func<object> GetValidation { get; set; }
		
		public FormField()
		{
			Attributes = new Dictionary<string, string>();
			Attributes["type"] = "text";
		}
		
		public FormField(string title): this()
		{
			Title = title;
		}
		
		public virtual string Name()
		{
			return GetName();
		}
		
		public virtual string Value()
		{
			var result = GetValue();
			return result == null ? string.Empty : result.ToString();
		}
		
		public virtual string ValidationInfo()
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			return js.Serialize(GetValidation()).Replace('"', '\'');
		}
		
		public string ValidationMessage()
		{
			return Helper.ValidationMessage(Name()).ToStringOrNull();
		}
		
		public string RenderAttributes()
		{
			return string.Join(" ", Attributes.Select(i => string.Format("{0}=\"{1}\"", i.Key, i.Value)));
		}
		
		public override string Render()
		{
			return Helper.Partial(TemplateName, this).ToString();
		}

		protected HtmlHelper m_Helper;
	}

	public class TextField: FormField
	{
		public TextField(string title): base(title)
		{
			Attributes["type"] = "text";	
		}
		
		public TextField(string title, string feature): base(title)
		{
			Attributes["feature"] = feature;
		}
		
		public override string TemplateName
		{
			get { return "_TextField"; }
		}	
	}
	
	public class TextAreaField: FormField
	{
		public TextAreaField(string title): base(title)
		{
		}
		
		public override string TemplateName
		{
			 get { return "_TextAreaField"; }
		}
	}
	
	public class HiddenDataField: FormField
	{
		public HiddenDataField(): base()
		{ 
			Attributes["type"] = "hidden";	
		}
		
		public override string TemplateName
		{
			get { return "_HiddenField"; }
		}
	}
	
	public class SelectField: FormField
	{
		public IDictionary<int, string> Options { get; set; }
		
		public SelectField(string title, IDictionary<int, string> values): base(title)
		{
			Options = values;
		}
		
		public override string Value()
		{
			var result = GetValue();
			return result == null ? string.Empty : Convert.ToInt32(result).ToString();
		}
		
		public override string TemplateName
		{
			get { return "_SelectField"; }
		}
	}
	
	public class AutocompleteField: FormField
	{
		public string Source { get; set; }
		
		public AutocompleteField(string title, string source): base(title)
		{
			Source = source;
		}
		
		public long ValueId()
		{
			if (GetValue() == null)
				return 0;
			
			INamedDomainObject obj = GetValue() as INamedDomainObject;
			return obj.Id;
		}
		
		public string ValueName()
		{
			if (GetValue() == null)
				return string.Empty;
			
			INamedDomainObject obj = GetValue() as INamedDomainObject;
			return obj.Name;
		}
		
		public override string TemplateName
		{
			 get { return "_AutocompleteField"; }
		}
	}
	
	public class CheckboxField: FormField
	{	
		public CheckboxField(string title): base(title)
		{
		}
		
		public override string TemplateName
		{
			get { return "_CheckboxField"; }
		}
	}
	
	public class ValidationAttributeReader
	{
		public static IDictionary<string, object> ReadProperty(PropertyInfo property)
		{
			IDictionary<string, object> result = new Dictionary<string, object>();

			foreach (object i in property.GetCustomAttributes(true)) {
				if (i is RequiredAttribute)
					result.AddOverwrite("required", true);
				
				if (i is NumberAttribute || property.IsNumber())
					result.AddOverwrite("number", true);
				
				if (i is NipAttribute)
					result.AddOverwrite("nip", true);
			}
			
			return result;
		}
	}
}

