using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace FusionSzef
{
	public static class TreeHelper
	{
		public static Tree<T> Tree<T>(this HtmlHelper self, IList<T> data, T root, T selected)
		{
			return new Tree<T>(self, data, root, selected);
		}
	}
	
	public class Tree<T>
	{
		public Tree(HtmlHelper helper, IList<T> data, T root, T selected)
		{
			m_Helper = helper;
			m_Data = data;
			m_Root = root;
			Selected = selected;
		}
		
		public Tree<T> Configure(Func<T, long> id, Func<T, string> label, Func<T, T> parent)
		{
			Id = id;
			Label = label;
			Parent = parent;
			return this;
		}
		
		public Tree<T> Link(Func<T, object> generator)
		{
			LinkGenerator = generator;
			return this;
		}
		
		public IEnumerable<T> Children(T parent)
		{
			return m_Data.Where(x => Parent(x) != null && Parent(x).Equals(parent));
		}
		
		public override string ToString()
		{
			return m_Helper.Partial("_Tree", new { Tree = this, Root = m_Root }).ToString();
		}
		
		public Func<T, T> Parent
		{
			get; set;
		}
		
		public Func<T, long> Id
		{
			get; set;
		}
		
		public Func<T, string> Label
		{
			get; set;
		}
		
		public Func<T, object> LinkGenerator
		{
			get; set;
		}
		
		public T Selected
		{
			get; set;	
		}
		
		private HtmlHelper m_Helper;
		private IList<T> m_Data;
		private T m_Root;
	}
}

