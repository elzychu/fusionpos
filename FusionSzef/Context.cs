using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Linq;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Web.Security;
using NHibernate;
using NHibernate.Cfg;
using FusionPOS.Config;
using FusionPOS.Logging;
using FusionPOS.Translations;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class Context
	{
		public Context()
		{
		}
		
		public void Initialize()
		{
			/* Initialize logger */
			Logger = new LoggerContext();
			Logger.AddOutputStream(Console.Out);
			Logger.Log("SZEF", LogPriority.Info, "Loaded");
			m_Logger = new Logger(Logger, "SZEF");
			m_DbLogger = new Logger(Logger, "DB");
			m_ServiceLogger = new Logger(Logger, "SERVICE");
			
			/* Read XML configuration */
			Configuration = new ConfigContext();
			Configuration.ReadFile("/Configuration/Application.xml");
			Configuration.ReadFile("/Configuration/Server.xml");
			/* ... */
			
			/* Initialize translations */
			I18N = new TranslationContext();
			//I18N.Logger = m_I18NLogger;
			I18N.AddTranslationsDirectory("/Configuration/Languages/");
			Translator = new Translator("FusionSzef");
			
			/* Set desired language */
			string lang = Configuration.Sections["application"].Properties["language"];
			if (I18N.HasLanguage(lang))
				Translator.LoadLanguage(I18N.GetLanguage(lang));

			/* Build connection string */
			string connstr = String.Format(
			    "Server={0};Database={1};User ID={2};Password={3};AutoEnlist=false",
			    Configuration.Sections["database"].Properties["host"],
			    Configuration.Sections["database"].Properties["db"],
			    Configuration.Sections["database"].Properties["user"],
			    Configuration.Sections["database"].Properties["password"]);
			
			m_DbLogger.Info(string.Format("Connection string: {0}", connstr));
			
			/* Create database connection */
			m_NhConfiguration = new Configuration();
			m_NhConfiguration.Configure();
			m_NhConfiguration.SetProperty("connection.connection_string", connstr);
			m_NhConfiguration.SetProperty("show_sql", "true");
			m_NhConfiguration.AddAssembly("Shared");
			m_NhSessionFactory = m_NhConfiguration.BuildSessionFactory();
		
			/* Publish services */
			m_Services = new List<ServiceHost>();
			foreach (ConfigNode i in Configuration.Sections["services"].Children.Values) {
				Uri uri = new Uri(i.Properties["uri"]);
				Type contractType = Type.GetType(i.Properties["contract"]);
				Type serviceType = Type.GetType(i.Properties["class"]);

				object service = Activator.CreateInstance(serviceType, this);
				ServiceHost host = new ServiceHost(service, uri);
				host.Description.Behaviors.Add(new ServiceMetadataBehavior() { HttpGetEnabled = true });
				host.AddServiceEndpoint(contractType, new WSHttpBinding(SecurityMode.None), uri);
				host.Open();
				m_Services.Add(host);
				m_ServiceLogger.Standard(string.Format("Service {0} launched", i.Properties["class"]));
			}
		}
		
		public LoggerContext Logger
		{
			get; set;
		}

		public ConfigContext Configuration
		{
			get; set;
		}
		
		public TranslationContext I18N
		{
			get; set;
		}
		
		public Translator Translator
		{
			get; set;
		}
		
		public User LoggedUser
		{
			get { return m_LoggedUser; }
		}
		
		public ISession OpenSession()
		{
			return m_NhSessionFactory.OpenSession();
		}
		
		public void LoginUser(User user, string password)
		{
			SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
			byte[] hash = sha1.ComputeHash(UTF8Encoding.Default.GetBytes(password));
			string str = string.Concat(hash.Select(i => i.ToString("x2")));
			
			if (str != user.Password)
				throw new AuthenticationException();
			
			m_LoggedUser = user;
			m_Logger.Standard(string.Format("User {0}/pin:{1} logged in",
			    user.Name, user.Pin));
			
			FormsAuthentication.SetAuthCookie(user.Name, false);
			
			//if (UserLoggedIn != null)
			//	UserLoggedIn(this, m_LoggedUser);
		}
		
		private IList<ServiceHost> m_Services;
		private Logger m_Logger;
		private Logger m_DbLogger;
		private Logger m_ServiceLogger;
		private User m_LoggedUser;
		private Configuration m_NhConfiguration;
		private ISessionFactory m_NhSessionFactory;
		private static Context m_Instance;
	}
}

