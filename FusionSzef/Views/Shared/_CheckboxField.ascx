﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<span>
<label></label>
<input type="checkbox" name="<%= Model.Name() %>" value="<%= Model.Value() %>"
	<% foreach (var attr in Model.Attributes) { %>
		<%= attr.Key %>=<%= attr.Value %>
	<% } %>
/>
<%= Model.Title %>	
<% if (Model.ValidationMessage() != null) { %>	
	<span class="error"><%= Model.ValidationMessage() %></span>
<% } %>	
</span>	