﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<FusionSzef.MenuEntry>" %>
<%@ Import Namespace="FusionPOS.Translations" %>
<li>
	<a href="<%= Url.Action("index", Model.Controller) %>">
		<img src="<%= Url.Content("~/Content/images/icons/" + Model.Image) %>"/>
	</a>
	<div class="title">
		<a href="<%= Url.Action("index", Model.Controller) %>">
			<%= Model.Title.Tr() %>
		</a>
	</div>
</li>