﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<span>
<label><%= Model.Title %></label>
<input <%= Model.RenderAttributes() %> name="<%= Model.Name() %>" value="<%= Model.Value() %>" validation="<%= Model.ValidationInfo() %>"/>
<% if (Model.ValidationMessage() != null) { %>	
	<span class="error"><%= Model.ValidationMessage() %></span>
<% } %>	
</span>	