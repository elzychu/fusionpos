﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<span>
	<label><%= Model.Title %></label>
	<input feature="autocomplete" hidden-id="<%= Model.Name() %>.Id"
	    remote="<%= Model.Source %>" value-id="<%= Model.ValueId() %>"
	    value-name="<%= Model.ValueName() %>"/>
</span>