﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<span>
<label><%= Model.Title %></label>
<textarea name="<%= Model.Name() %>"><%= Model.Value() %></textarea>
<% if (Model.ValidationMessage() != null) { %>	
	<span class="error"><%= Model.ValidationMessage() %></span>
<% } %>	
</span>