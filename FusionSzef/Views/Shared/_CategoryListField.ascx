﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="FusionSzef" %>
<%@ Import Namespace="FusionPOS.Domain" %>
<script type="text/x-jquery-tmpl" id="CategoriesTemplate">	
	<div feature="ajax" name="<%= (string)Model.Name() + "[${index}]" %>" id="<%= (string)Model.Name() + "[${index}]" %>" value="" remote="<%= Url.RouteUrl(new { Controller = "Categories", Action = "AjaxList" }) %>"></div>
</script>
<% int n = 0; %>	
<% foreach (DishCategory i in Model.Values()) { %>
	<span>		
		<div feature="ajax" name="<%= string.Format("{0}[{1}]", (string)Model.Name(), n++) %>" id="<%= (string)Model.Name() + "[${index}]" %>" value="<%= i %>" remote="<%= Url.RouteUrl(new { Controller = "Categories", Action = "AjaxList" }) %>"></div>
	</span>
<% } %>
<button onclick="$('#CategoriesTemplate').tmpl({index: $('#Categories > span').size() + 1}).appendTo('#Categories'); $.pos.reload(); return false;">Dodaj</button>