﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<span>
	<label><%= Model.Title %></label>
	<select name="<%= Model.Name() %>">
	<% foreach (var i in Model.Options) { %>
		<option <% if (Model.Value() == i.Key.ToString()) { %> selected="selected" <% } %> value="<%= i.Key.ToString() %>"><%= i.Value %></option>	
	<% } %>
	</select>
</span>