﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<form class="form" method="<%= Model.Method %>" action="<%= Url.Action(Model.Action) %>">
	<% foreach(var i in Model.Fields) { %>	
		<%= i.Element.Render() %>
	<% } %>
	<input type="submit" value="Zapisz"/>
</form>