using System;
using System.ComponentModel.DataAnnotations;
using FusionPOS;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class CompanyDataModel
	{
		[Required]
		public string Name { get; set; }
		[Required]
		public string Street { get; set; }
		[Required]
		public string City { get; set; }
		[Required]
		[RegularExpression(@"\d{2}-\d{3}")]
		public string Zip { get; set; }
		[StringLength(16)]
		public string Phone { get; set; }
		[Required]
		[Nip]
		public string Nip { get; set; }
		[Regon]
		public string Regon { get; set; }
		[RegularExpression(@"(\d{4}){5}")]
		public string BankAccount { get; set; }
	}
	
	public class InvoiceSettingsModel
	{
		[Required]
		public string Format { get; set; }
	}
	
	public class NetworkSettingsModel
	{
		[Required]
		[IpAddress]
		public string IpAddress { get; set; }
		[Required]
		[Netmask]
		public string Netmask { get; set; }
		[Required]
		[IpAddress]
		public string Gateway { get; set; }
		[Required]
		[IpAddress]
		public string Dns1 { get; set; }
		[IpAddress]
		public string Dns2 { get; set; }
		[Required]
		public string MacAddress { get; set; }
		[Required]
		public bool UseDhcp { get; set; }
		public string TimeServer { get; set; }
	}
	
	public class PrinterSettingsModel
	{
		[Required]
		[IpAddress]
		public string IpAddress { get; set; }		
	}
	
	public class CurrencySettingsModel
	{
		public Currency MainCurrency { get; set; }
		public bool AutoUpdate { get; set; }
	}
}

