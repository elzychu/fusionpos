using System;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Criterion;

namespace FusionSzef
{
	public interface DataTableModel
	{
		object Fetch();
	}
	
	public class DataTableModel<T>: DataTableModel where T: class
	{
		public DataTableModel(ISession session, NameValueCollection query)
		{
			m_Session = session;
			m_Columns = int.Parse(query.Get("iColumns"));
			m_Start = int.Parse(query.Get("iDisplayStart"));
			m_Count = int.Parse(query.Get("iDisplayLength"));
			m_Search = query.Get("sSearch");
			m_Echo = query.Get("sEcho");

			m_Properties = new Dictionary<int, string>();
			for (int i = 0; i < m_Columns; i++)
				m_Properties.Add(i, query.Get(string.Format("mDataProp_{0}", i)));
			
			m_Filters = new Dictionary<string, string>();
			for (int i = 0; i < m_Columns; i++) {
				string filter = query.Get(string.Format("sSearch_{0}", i));
				if (!string.IsNullOrEmpty(filter))
					m_Filters.Add(m_Properties[i], filter);
			}
			
			m_Sorts = new Dictionary<string, bool>();
			int sortColumns = int.Parse(query.Get("iSortingCols"));
			for (int i = 0; i < sortColumns; i++) {
				m_Sorts.Add(
				    m_Properties[int.Parse(query.Get(string.Format("iSortCol_{0}", i)))],
				    query.Get(string.Format("sSortDir_{0}", i)) == "asc");
			}
		}
		
		public object Fetch()
		{
			IQueryOver<T, T> query = m_Session.QueryOver<T>();
			
			if (!string.IsNullOrWhiteSpace(m_Search)) {
				Disjunction d = new Disjunction();
				foreach (string i in m_Properties.Values)
					d.Add(new LikeExpression(i, "%" + m_Search + "%", null, true));
				
				query = query.Where(d);
			}
			
			foreach (KeyValuePair<string, string> i in m_Filters)
				query = query.Where(new LikeExpression(i.Key, "%" + i.Value + "%"));
					
			foreach (KeyValuePair<string, bool> i in m_Sorts) {
				query = i.Value 
				    ? query.OrderBy(Projections.Property(i.Key)).Asc
				    : query.OrderBy(Projections.Property(i.Key)).Desc;
			}
			
			IList<T> results = query.Skip(m_Start).Take(m_Count).List();
			return new {
				aaData = results,
				iTotalRecords = m_Session.QueryOver<T>().RowCount(),
				iTotalDisplayRecords = query.RowCount(),
				sEcho = m_Echo
			};
		}
		
		private ISession m_Session;
		private string m_Echo;
		private int m_Columns;
		private int m_Start;
		private int m_Count;
		private string m_Search;
		private IDictionary<int, string> m_Properties;
		private IDictionary<string, string> m_Filters;
		private IDictionary<string, bool> m_Sorts;
	}
}

