using System;
using NHibernate;

namespace FusionSzef
{
	public class DualListModel<T1, T2>
	{
		public ListModel<T1> First { get; set; }
		public ListModel<T2> Second { get; set; }
		
		public void Fetch(ISession session)
		{
			First.Fetch(session);
			Second.Fetch(session);
		}
	}
}

