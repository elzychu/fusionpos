using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Linq;

namespace FusionSzef
{
	public class SearchListModel<T, S>: ListModel<T>
	{
		public S Template;
		public IList<Expression<Func<T, bool>>> Parameters;
		
		public SearchListModel()
		{
			Parameters = new List<Expression<Func<T, bool>>>();
		}
		
		public override void Fetch(ISession session)
		{
			IQueryable<T> query = session.Query<T>();
			Parameters.ForEach(i => query = query.Where(i));
			
			Items = query.ToList();
		}
	}
}

