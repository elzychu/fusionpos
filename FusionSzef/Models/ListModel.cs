using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using NHibernate;
using NHibernate.Linq;

namespace FusionSzef
{
	public class ListModel<T>
	{
		public int PageNumber { get; set; }
		public int PageSize { get; set; }
		public string SortString { get; set; }
		public IEnumerable<T> Items { get; set; }
		
		public HttpRequestBase Request
		{
			set {
				SortString = value.Params.GetOrDefault("sort", "+Name");
				PageNumber = int.Parse(value.Params.GetOrDefault("page", "0"));
			}
		}
		
		public virtual void Fetch(ISession session)
		{
			Items = session.Query<T>()
			    .OrderByName(SortString)
			    .Skip(PageNumber * PageSize)
			    .Take(PageSize);
		}
	}
}

