using System;
using System.Linq;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class CategoryListModel<TCategory, TItem>: ListModel<TItem>
	{
		public IList<TCategory> Categories { get; set; }
		public TCategory RootCategory { get; set; }
		public TCategory Selected { get; set; }
		public Func<TItem, bool> CategorySelector { get; set; }
		
		public override void Fetch(ISession session)
		{
			Items = session.Query<TItem>()
			    .Where(CategorySelector)
			    .OrderByName(SortString)
			    .Skip(PageNumber * PageSize)
			    .Take(PageSize);
		}
	}
}

