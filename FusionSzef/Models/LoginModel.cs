using System;
using System.ComponentModel.DataAnnotations;

namespace FusionSzef
{
	public class LoginModel
	{
		[Required]
		public string Username { get; set; }
		[Required]
		public string Password { get; set; }
		
		public LoginModel()
		{
		}
	}
}

