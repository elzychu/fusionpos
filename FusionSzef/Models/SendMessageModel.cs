using System;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class SendMessageModel
	{
		public Device POS { get; set; }
		public string Message { get; set; }
	}
}

