using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace FusionSzef
{
	public static class GridHelper
	{
		public static Grid<T> Grid<T>(this HtmlHelper html, ListModel<T> model)
		{
			return new Grid<T>(html, model);
		}
		
		public static string Huj(this HtmlHelper html)
		{
			return "huj";	
		}
	}
	
	public class Grid<T>
	{
		public Grid(HtmlHelper helper, ListModel<T> model)
		{
			m_Helper = helper;
			m_DataSource = model;
			m_Columns = new List<GridColumn<T>>();
		}
				
		public Grid<T> Configure(Action<Grid<T>> action)
		{
			action(this);
			return this;
		}
		
		public Grid<T> Column(string name, Expression<Func<T, object>> renderer)
		{
			GridColumn<T> column = new GridColumn<T>();
			column.Name = name;
			column.Renderer = renderer;
			m_Columns.Add(column);
			return this;
		}
		
		public Grid<T> Link(Func<T, object> generator)
		{
			LinkGenerator = generator;
			return this;
		}
		
		public IList<GridColumn<T>> Columns 
		{
			get { return m_Columns; }
		}
		
		public IEnumerable<T> Items
		{
			get { return m_DataSource.Items; }
		}
		
		public int PageNumber
		{
			get { return m_DataSource.PageNumber; }
		}
		
		public string SortString
		{
			get { return m_DataSource.SortString; }
		}

		public Func<T, object> LinkGenerator
		{
			get; set;
		}
		
		public override string ToString()
		{
			return m_Helper.Partial("_Grid", this).ToString();
		}
		
		private HtmlHelper m_Helper;
		private ListModel<T> m_DataSource;
		private IList<GridColumn<T>> m_Columns;
	}
	
	public class GridColumn<T>
	{
		public string Name { get; set; }
		public Expression<Func<T, object>> Renderer { get; set; }
		
		public string Render(T model)
		{
			return Renderer.Compile().Invoke(model).ToString();	
		}
		
		public string SortString(bool desc)
		{
			MemberExpression prop = Renderer.Body as MemberExpression;
			if (prop == null)
				return string.Empty;
			
			return string.Format("{0}{1}", desc ? "-" : "+", prop.Member.Name);
		}
	}
}
