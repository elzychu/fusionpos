using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Script.Serialization;
using FusionPOS.Domain;
using NHibernate.Linq;

namespace FusionSzef
{
	public class ClientsSearchModel
	{
		public string Query { get; set; }
		public string NIP { get; set; }
	}
	
	[Authorize]
	public class ClientsController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");	
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List(ClientsSearchModel search)
		{
			SearchListModel<Client, ClientsSearchModel> model = new SearchListModel<Client, ClientsSearchModel>() { 
				SortString = Request.Params.GetOrDefault("sort", "+Name"),
				PageNumber = int.Parse(Request.Params.GetOrDefault("page", "0")),
				PageSize = 10,
				Template = search ?? new ClientsSearchModel()
			};
			
			if (!string.IsNullOrEmpty(search.NIP))
				model.Parameters.Add(m => m.NIP == search.NIP);
			
			if (!string.IsNullOrEmpty(search.Query))
				model.Parameters.Add(m => 
				    m.Name.Contains(search.Query) || 
				    m.Address.Contains(search.Query) || 
				    m.Description.Contains(search.Query) || 
				    m.PhoneNumber.Contains(search.Query));
			
			model.Fetch(DbSession);
			
			if (model.Items.Count() == 1)
				return RedirectToAction("Edit", new { Id = model.Items.First().Id });
			
			return View(model);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxList()
		{
			var model = new DataTableModel<Client>(DbSession, Request.Params);
			return Json(model.Fetch(), JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Summary(long id)
		{
			Client client = DbSession.Get<Client>(id);
			if(client == null)
				return HttpNotFound("Klient nie istnieje");
			
			IEnumerable<OrderDish> dishes = DbSession.Query<OrderDish>().Where(i => i.Order.Client == client);
			IEnumerable<Order> orders = DbSession.Query<Order>().Where(p => p.Client == client);
			
			//ViewBag.Last = dishes.Max(p => p.Order.Time.Ticks).Dish.Name;

			OrderDish last = dishes.OrderBy(i => i.Order.Time).FirstOrDefault();
			OrderDish first = dishes.OrderByDescending(i => i.Order.Time).FirstOrDefault();
			
			IEnumerable<IEnumerable<OrderDish>> groups = dishes.GroupBy(i => i.Dish.Name);
			
			IEnumerable<OrderDish> max = groups.OrderBy(i => i.Count()).FirstOrDefault();
			string name = max.First().Dish.Name;
			//double overalTurnover =  DbSession.Query<Transaction>().Where(i => i.Client.Id == client.Id).Sum(i => i.TotalValue);
			ViewBag.Last = last.Dish.Name;
			ViewBag.FreqDish = name;
			IEnumerable< IEnumerable<OrderDish> > days =  dishes.GroupBy(i => i.Order.Time.DayOfWeek);
			ViewBag.WeekVisit = days.Where(p => days.All(r => p.Count() >= r.Count() )).First().First().Order.Time.DayOfWeek;

			decimal dayscount = (decimal)(DateTime.Now - first.Order.Time).TotalDays;
			decimal monthscount = dayscount / 31;
			decimal weeklyscount = dayscount / 368;
			
			//XXX orders.Sum poprawić na transaction
			ViewBag.DayliTurnover = dayscount != 0 ? orders.Sum(p => p.TotalValue) / dayscount
					: orders.Sum(p => p.TotalValue);
			ViewBag.MonthlyTurnover = dayscount != 0 ? orders.Sum(p => p.TotalValue) / monthscount
					: orders.Sum(p => p.TotalValue);
			ViewBag.WeeklyTurnover = dayscount != 0 ? orders.Sum(p => p.TotalValue) / weeklyscount
					: orders.Sum(p => p.TotalValue);
			ViewBag.OveralTurnover = orders.Sum (p => p.TotalValue);
			return View(client);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult SummaryTurnoverChart(long id, string type)
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			ViewBag.StartDate = DateTime.Today;
			ViewBag.Turnover =  js.Serialize(new int[] { 1, 2, 3, 10, 1, 4, 20, 1, 5 });
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Edit(long? id)
		{
			Client c = id.HasValue ? DbSession.Get<Client>(id) : new Client();
			
			if (c == null)
				return HttpNotFound("Client not found");
			
			return View(c);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Save(Client client)
		{
			if (!ModelState.IsValid)
				return View("Edit", client);
			
			/* XXX */
			DbSession.Merge(client);
			return Redirect(Url.Action("List"));
		}
	}
}

