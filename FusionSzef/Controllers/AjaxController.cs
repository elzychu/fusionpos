using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class AjaxController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxList(string domain)
		{
			Type domainType = Type.GetType(string.Format("FusionPOS.Domain.{0}, Shared", domain));
			ICriteria c = DbSession.CreateCriteria(domainType);
			return Json(new { aaData = c.List() }, JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxListServer(string domain)
		{
			Type domainType = Type.GetType(string.Format("FusionPOS.Domain.{0}, Shared", domain));
			Type modelType = typeof(DataTableModel<>);
			DataTableModel model = (DataTableModel)Activator.CreateInstance(modelType.MakeGenericType(domainType), DbSession, Request.Params);
			return Json(model.Fetch(), JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxListAutocomplete(string domain)
		{
			Type domainType = Type.GetType(string.Format("FusionPOS.Domain.{0}, Shared", domain));
			ICriteria c = DbSession.CreateCriteria(domainType);
			IList<INamedDomainObject> results = c.List<INamedDomainObject>();
			return Json(results.Select(i => new {id = i.Id, label = i.Name}), JsonRequestBehavior.AllowGet);
		}
	}
}

