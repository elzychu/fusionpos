using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Script.Serialization;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Transaction;
using FusionPOS.Domain;
using FusionPOS;


namespace FusionSzef
{
	[Authorize]
	public class IngredientsController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");	
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List(long? id)
		{
			ViewBag.Selected = DbSession.Get<IngredientCategory>(id ?? IngredientCategory.RootCategory); // XXX
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxListAutocomplete(string term)
		{
			IList list = DbSession.Query<Ingredient>()
			    .Where(i => i.Name.Contains(term))
			    .Select(i => new {label = i.Name, id = i.Id, unit = Enum.GetName(typeof(UnitOfMeasure), i.Unit)}).ToList();
			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxListGroupsAutocomplete(string term)
		{
			IList list = DbSession.Query<IngredientGroup>()
			    .Where(i => i.Name.Contains(term))
			    .Select(i => new {label = i.Name, id = i.Id}).ToList();
			return Json(list, JsonRequestBehavior.AllowGet);
		}	
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxList(long id)
		{
			IngredientCategory cat = DbSession.Get<IngredientCategory>(id);
			return Json(new { aaData =  id != 0 ? cat.Ingredients : DbSession
			    .Query<Ingredient>().Where(i => i.Category.Id == 0).ToList()
			    }, JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxCategoryList()
		{
			IList list = DbSession.Query<IngredientCategory>()
			    .Select(i => new {name = i.Name, id = i.Id}).ToList();
			return Json(new { results = list, count = list.Count },
			    JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult ListGroups()
		{
			var model = new ListModel<IngredientGroup>() {
				
			};
			
			return View(model);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Edit(long? id, long? parentId)
		{
			IngredientCategory parent = parentId.HasValue ? DbSession.Get<IngredientCategory>(parentId) : null;
			Ingredient i = id.HasValue ? DbSession.Get<Ingredient>(id) : new Ingredient();
			JavaScriptSerializer js = new JavaScriptSerializer();
			
			if (i == null)
				return HttpNotFound("Ingredient not found");
			
			if (parent != null)
				i.Category = parent;
			
			ViewBag.Groups = i.Groups != null ? js.Serialize(i.Groups) : "{}";
			
			return View(i);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Edit(Ingredient model)
		{
			if (model.Groups == null)
				model.Groups = new List<IngredientGroup>();
			
			model.Groups.RemoveDuplicates(i => i.Id);
			model.Groups.ForEach(i => DbSession.Load(i, i.Id));

			DbSession.Load(model.Category, model.Category.Id);
			
			using (var tx = DbSession.BeginTransaction()) {
				model = DbSession.Merge(model);
				tx.Commit();
			}
			
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditCategory(long? id, long? parent)
		{
			IngredientCategory o = id.HasValue ? DbSession.Get<IngredientCategory>(id) : new IngredientCategory();
			IngredientCategory po = parent.HasValue ? DbSession.Get<IngredientCategory>(parent) : null;
			
			if (o == null)
				return HttpNotFound("Category not found");
			
			if (po != null)
				o.Parent = po;
			
			ViewBag.Categories = DbSession.Query<IngredientCategory>().ToDictionary(i => (int)i.Id, i => i.Name);
			return View(o);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult EditCategory(IngredientCategory c)
		{
			if(c.Children != null)
				c.Children.Each(p => DbSession.Refresh(p));
			if(c.Ingredients != null)
				c.Ingredients.Each(p => DbSession.Refresh(p));
			if(c.Parent != null)
				DbSession.Refresh(c.Parent);
			using(ITransaction tr = DbSession.BeginTransaction())
			{
				DbSession.Merge(c);
				tr.Commit();
			}
			
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult DeleteCategory(long id, int? confirm)
		{
			IngredientCategory u = DbSession.Get<IngredientCategory>(id);
			
			if(u.Id == IngredientCategory.RootCategory)
				return HttpNotFound("Nie można usunąć kategorii");
			
			if (u == null)
				return HttpNotFound("Category not found");
		
			if (confirm.HasValue && confirm != 0) {
				using (ITransaction tx = DbSession.BeginTransaction()) {
					DbSession.Query<Ingredient>().Where(d => d.Category.Id == u.Id).ForEach(c => {
						c.Category = DbSession.Get<IngredientCategory>(u.Parent.Id);
						DbSession.Update(c);
					});
					DbSession.Delete(u);
					tx.Commit();
				}
				return RedirectToAction("List");
			}
			return View(u);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult DeleteGroup(long id, int? confirm)
		{
			IngredientGroup u = DbSession.Get<IngredientGroup>(id);
			
			if (u == null)
				return HttpNotFound("Dish group not found");
		
			if (confirm.HasValue && confirm != 0) {
				using (ITransaction tx = DbSession.BeginTransaction()) {
					DbSession.Delete(u);
					tx.Commit();
				}
				return RedirectToAction("ListGroups");
			}
			return View(u);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditGroup(long? id)
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			IngredientGroup o = id.HasValue ? DbSession.Get<IngredientGroup>(id) : new IngredientGroup();
			
			if (o == null)
				return HttpNotFound();
			
			if (!id.HasValue)
				o.Ingredients = new List<Ingredient>();

			ViewBag.Ingredients = js.Serialize(o.Ingredients);
			ViewBag.Categories = DbSession.Query<IngredientGroup>().ToDictionary(i => (int)i.Id, i => i.Name);
			return View(o);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult EditGroup(IngredientGroup grp)
		{
			if (grp.Ingredients == null)
				grp.Ingredients = new List<Ingredient>();
			
			grp.Ingredients.Each(i => DbSession.Refresh(i));
			using (ITransaction tx = DbSession.BeginTransaction()) {
				DbSession.Merge(grp);
				tx.Commit();
			}
			
			TempData["Message"] = "Grupa zapisana";
			return RedirectToAction("ListGroups");
		}
		
		public ActionResult AjaxTree()
		{
			return Json(AjaxSelectCategory(DbSession.Get<IngredientCategory>(IngredientCategory.RootCategory)), JsonRequestBehavior.AllowGet); // XXX
		}
		
		private object AjaxSelectCategory(IngredientCategory c)
		{
			IList<object> categories = new List<object>();
			if(c.Id == IngredientCategory.RootCategory)
			{
				IngredientCategory category = new IngredientCategory();
				category.Id = 0;
				
				categories.Add(new {
					data = "Poza kategoriami",
					metadata = category,
					children = 0
				});
			}
			
			c.Children.Select(i => AjaxSelectCategory(i)).ForEach(j => categories.Add(j));
			return new {
				data = c.Name,
				metadata = c,
				children = categories
			};
		}
	}
}

