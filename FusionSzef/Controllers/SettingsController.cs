using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate.Linq;
using FusionPOS.Domain;

namespace FusionSzef
{
	[Authorize]
	public class SettingsController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditCompanyData()
		{
			var model = new CompanyDataModel();
			Config.Sections["company"].ReadToObject(model);
			
			return View(model);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult EditCompanyData(CompanyDataModel model)
		{
			if (!ModelState.IsValid)
				return View(model);
			
			// XXX
			return RedirectToAction("Index");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditInvoiceSettings()
		{
			var model = new InvoiceSettingsModel();
			return View(model);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult ListTaxRates()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditTaxRate(long? id)
		{
			Tax t = id.HasValue ? DbSession.Get<Tax>(id) : new Tax();
			
			if (t == null)
				return HttpNotFound("Tax rate not found");
			
			return View(t);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult EditTaxRate(Tax rate)
		{
			using (var tx = DbSession.BeginTransaction()) {
				DbSession.Merge(rate);
				tx.Commit();
			}
			
			return RedirectToAction("ListTaxRates");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditCurrency(long? id)
		{
			Currency c = id.HasValue ? DbSession.Get<Currency>(id) : new Currency();
			
			if (c == null)
				return HttpNotFound("Currency not found");
			
			return View(c);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult EditCurrency(Currency currency)
		{
			using (var tx = DbSession.BeginTransaction()) {
				DbSession.Merge(currency);
				tx.Commit();
			}
			
			return RedirectToAction("ListCurrencies");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult ListCurrencies()
		{
			ViewBag.CurrencySettings = new CurrencySettingsModel();
			ViewBag.CurrencyList = DbSession.Query<Currency>().ToDictionary(i => (int)i.Id, i => i.Name);
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditNetworkSettings()
		{
			return View(new NetworkSettingsModel());
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditPrinterSettings()
		{
			return View(new PrinterSettingsModel());
		}
	}
}

