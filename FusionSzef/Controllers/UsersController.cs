using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Domain;

namespace FusionSzef
{
	[Authorize]
	public class UsersController: BaseController
	{	
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult LogonHistory()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult LogonHistoryForUser(long id)
		{
			return Json(new { aaData = DbSession.Query<LogonHistory>().Where(p => p.User.Id == id).ToList() },
			    JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Summary(long id)
		{
			User u = DbSession.Get<User>(id);
			
			if (u == null)
				return HttpNotFound("User not found");
			SessionItem sessionItem = DbSession.Get<SessionItem>("CurrentSettlement");
			Settlement currSettlement = DbSession.Get<Settlement>( BitConverter.ToInt64(sessionItem.Value, 0) );
			decimal currentBilance = 0;
			decimal lastWeekBilance = 0;
			decimal generalBilance = 0;
			DbSession.Query<Order>()
			    .Where(p => p.Waiter.Id == u.Id && p.Time > currSettlement.From)
			    .ForEach(r => currentBilance += (r.TotalValue != 0 ? r.TotalValue : r.ValueNet));
			DbSession.Query<Order>()
			    .Where(p => p.Waiter.Id == u.Id && p.Time > DateTime.Now.AddDays( - (int)DateTime.Now.DayOfWeek -7)
			        && p.Time < DateTime.Now.AddDays( - (int)DateTime.Now.DayOfWeek))
			    .ForEach(r => lastWeekBilance += (r.TotalValue != 0 ? r.TotalValue : r.ValueNet));
			DbSession.Query<Order>()
			    .Where(p => p.Waiter.Id == u.Id)
			    .ForEach(r => generalBilance += (r.TotalValue != 0 ? r.TotalValue : r.ValueNet));

			ViewBag.CurrentBilance = currentBilance;
			ViewBag.LastWeekBilance = lastWeekBilance;
			ViewBag.GeneralBilance = generalBilance;
			return View(u);
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Edit(long? id)
		{
			User u = id.HasValue ? DbSession.Get<User>(id) : new User();
			
			if (u == null)
				return HttpNotFound("User not found");
			
			return View(u);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Delete(long id, int? confirm)
		{
			User u = DbSession.Get<User>(id);
			
			if (u == null)
				return HttpNotFound("User not found");
		
			if (confirm.HasValue && confirm != 0) {
				using (ITransaction tx = DbSession.BeginTransaction()) {
					DbSession.Delete(u);
					tx.Commit();
				}
				return RedirectToAction("List");
			}
			return View(u);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Save(User user)
		{
			using (ITransaction tx = DbSession.BeginTransaction()) {
					DbSession.SaveOrUpdate(user);
					tx.Commit();
				}
			TempData["Message"] = "Użytkownik zapisany";
			return Redirect(Url.Action("List"));
		}
	}
}

