	using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Domain;
using FusionPOS;

namespace FusionSzef
{
	public class IncomingInvoiceComponentModel
	{
		public string Name { get; set; }
	}
	[Authorize]
	public class IncomingInvoicesController: BaseController
	{	
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List()
		{
			return View();
		}

		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Add()
		{
			return View(new IncomingInvoice());
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult View(long id)
		{
			IncomingInvoice u = DbSession.Get<IncomingInvoice>(id);
			DbSession.Refresh(u);
			if (u == null)
				return HttpNotFound("Faktura nie znaleziona");

			return View(u);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Save(IncomingInvoice invoice, List<IncomingInvoiceComponent> items)
		{
			using (ITransaction tx = DbSession.BeginTransaction()) {
				invoice.Components = items;
				invoice.Components.Each(p => {
					DbSession.Refresh(p.Ingredient);
					p.Ingredient.Quantity += p.Amount;
					DbSession.Update(p.Ingredient);
				});
				DbSession.Save(invoice);
				tx.Commit();
			}
			TempData["Message"] = "Faktura zapisana";
			return Redirect(Url.Action("List"));
		}
		
		/*
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxComponentsList(long id)
		{
			IncomingInvoice inv = DbSession.Get<IncomingInvoice>(id);
			return Json(new { aaData =  inv.Components }, JsonRequestBehavior.AllowGet);
		}
		*/
	}
}

