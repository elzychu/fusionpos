using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using FusionPOS.Domain;
using FusionPOS;

namespace FusionSzef
{
	public enum OutgoingInvoiceType
	{
		Standard,
		AsCateringService,
		AsCateringServiceAlternate
	}
	
	public class OutgoingInvoiceModel
	{
		public Order Order { get; set; }
		public Client Client { get; set; }
		public string BuyerName { get; set; }
		public string BuyerAddress { get; set; }
		public string BuyerNip { get; set; }
		public PaymentMethod PaymentMethod { get; set; }
		public OutgoingInvoiceType InvoiceType { get; set; }
	}
	
	public class OutgoingInvoicesController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult New(long orderId)
		{
			OutgoingInvoiceModel model = new OutgoingInvoiceModel();
			model.Order = DbSession.Get<Order>(orderId);
			
			if (model.Order.Client != null) {
				model.BuyerName = model.Order.Client.Name;
				model.BuyerAddress = model.Order.Client.Address;
				model.BuyerNip = model.Order.Client.NIP;
			}
			
			return View(model);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public void New(OutgoingInvoiceModel model)
		{
			//VatInvoice invoice = new VatInvoice(model.Order);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult View(long id)
		{
			OutgoingInvoice invoice = DbSession.Get<OutgoingInvoice>(id);
			if (invoice == null)
				return HttpNotFound("Faktura nie znaleziona");
			
			VatInvoice report = new VatInvoice(invoice);
			StringWriter writer = new StringWriter();
			report.GenerateHTML(writer);
			return Content(writer.ToString());
		}
	}
}

