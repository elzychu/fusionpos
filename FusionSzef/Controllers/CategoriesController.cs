using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate.Linq;
using NHibernate;
using FusionPOS.Domain;

namespace FusionSzef
{
	[Authorize]
	public class CategoriesController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List(long? id)
		{
			ViewBag.Selected = DbSession.Get<DishCategory>(id ?? DishCategory.RootCategory);
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxListAutocomplete(string term)
		{
			IList list = DbSession.Query<DishCategory>()
			    .Where(i => i.Name.Contains(term))
			    .Select(i => new {label = i.Name, id = i.Id}).ToList();
			return Json(list, JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxTree()
		{
			return Json(AjaxSelectCategory(DbSession.Get<DishCategory>(DishCategory.RootCategory)), JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Edit(long? id, long? parent)
		{
			DishCategory o = id.HasValue ? DbSession.Get<DishCategory>(id) : new DishCategory();
			DishCategory po = parent.HasValue ? DbSession.Get<DishCategory>(parent) : null;
			
			if (o == null)
				return HttpNotFound("Category not found");
			
			if (po != null)
				o.Parent = po;
			
			ViewBag.Categories = DbSession.Query<DishCategory>().ToDictionary(i => (int)i.Id, i => i.Name);
			return View(o);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Edit(DishCategory category)
		{
			DbSession.SaveOrUpdate(category);
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Delete(long id, int? confirm)
		{
			DishCategory u = DbSession.Get<DishCategory>(id);
			
			if (u == null)
				return HttpNotFound("Category not found");
		
			if (confirm.HasValue && confirm != 0) {
				using (ITransaction tx = DbSession.BeginTransaction()) {
					DbSession.Delete(u);
					DbSession.Query<Dish>().Where(d => d.Categories.Contains(u)).ForEach(c => {
						c.Categories.Remove(u);
						DbSession.Update(c);
					});
					tx.Commit();
				}
				return RedirectToAction("List");
			}
			return View(u);
		}
		
		private object AjaxSelectCategory(DishCategory c)
		{
			IList<object> categories = new List<object>();
			if(c.Id == DishCategory.RootCategory)
			{
				DishCategory category = new DishCategory();
				category.Id = 0;
				
				categories.Add(new {
					data = "Poza kategoriami",
					metadata = category,
					children = 0,
				});
			}
			
			c.Children.Select(i => AjaxSelectCategory(i)).ForEach(j => categories.Add(j));
			
			return new {
				data = c.Name,
				metadata = c,
				children = categories
			};
		}
	}
}

