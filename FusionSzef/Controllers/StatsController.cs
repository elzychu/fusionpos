using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Script.Serialization;
using NHibernate.Linq;
using FusionPOS.Domain;
using FusionPOS;

namespace FusionSzef
{
	public class TurnoverModel
	{
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
	}

	public class StatsController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Turnover(TurnoverModel model)
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			IQueryable<Transaction> query = DbSession.Query<Transaction>();
			
			if (model == null) {
				model = new TurnoverModel {
					Start = DateTime.Today,
					End = DateTime.Today.AddDays(1)
				};
			}

			var turnover = Utils.IterateHourly(
			    model.Start,
			    model.End, 1,
			    (s, e) => query.Where(o => o.Time > s && o.Time < e).ToArray().Sum(o => o.TotalValue));
			
			var count = Utils.IterateHourly(
			    model.Start, 
			    model.End, 1,
			    (s, e) => query.Where(o => o.Time > s && o.Time < e).ToArray().Count());
			
			ViewBag.StartDate = model.Start.ToLongDateString();
			ViewBag.Turnover = js.Serialize(turnover);
			ViewBag.Count = js.Serialize(count);
			
			return View(model);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult TurnoverPaymentForms(TurnoverModel model)
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			IQueryable<Order> query = DbSession.Query<Order>();
			IDictionary<PaymentMethod, IEnumerable<decimal>> series = new Dictionary<PaymentMethod, IEnumerable<decimal>>();
			
			if (model == null) {
				model = new TurnoverModel {
					Start = DateTime.Today,
					End = DateTime.Today.AddDays(1)
				};
			}
			
			/*XXXforeach (PaymentMethod i in Enum.GetValues(typeof(PaymentMethod))) {
				series[i] = Utils.IterateHourly(
				    model.Start, 
				    model.End, 1, 
				    (s, e) => query.Where(
					o => o.Time > s && 
					o.Time < e && 
					o.PaymentMethod == i).ToArray().Sum(o => o.TotalValue)).ToArray();
			}
			*/
			ViewBag.Series = js.Serialize(series.Select(k => new {
				type = "area",
				name = k.Key,
				data = k.Value,
				pointInterval = 3600 * 1000,
				startDate = DateTime.Today.ToShortDateString()
			}));
			
			ViewBag.StartDate = model.Start.ToLongDateString();
			
				return View();
		}
	}
}

