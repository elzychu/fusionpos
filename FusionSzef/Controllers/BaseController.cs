using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate;
using FusionPOS.Config;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class BaseController: Controller
	{
		public BaseController()
		{
		}
		
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			m_Context = MvcApplication.PosContext;
			m_Session = m_Context.OpenSession();
		}
		
		protected override void OnResultExecuted(ResultExecutedContext filterContext)
		{
			m_Session.Dispose();
		}
		
		protected ActionResult AbstractCreate<T>() where T: new()
		{
			object o = new T();

			return View(o);
		}
		
		protected ActionResult AbstractEdit<T>(long? id)
		{
			object o = DbSession.Get<T>(id);
			
			if (o == null)
				return HttpNotFound("Object not found");

			return View(o);
		}
		
		protected ActionResult AbstractSave<T>(T obj)
		{
			return null;
		}
		
		protected ISession DbSession
		{
			get { return m_Session; }	
		}
		
		protected ConfigContext Config
		{
			get { return m_Context.Configuration; }
		}
		
		protected Context Context
		{
			get { return m_Context; }
		}
		
		private ISession m_Session;
		private Context m_Context;
	}
}

