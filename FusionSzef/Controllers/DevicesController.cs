using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using FusionPOS.Domain;
using FusionPOS.Services;
using NHibernate;

namespace FusionSzef
{
	[Authorize]
	public class DevicesController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditDevice(long? id)
		{
			if(id.HasValue)
				return base.AbstractEdit<Device>(id);
			else
				return base.AbstractCreate<Device>();
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult EditDevice(Device device)
		{
			using(ITransaction tr = DbSession.BeginTransaction())
			{
				DbSession.SaveOrUpdate(device);
				tr.Commit();
			}
			
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditKitchen(long? id)
		{
			if(id.HasValue)
				return base.AbstractEdit<Kitchen>(id);
			else
				return base.AbstractCreate<Kitchen>();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult SendMessage(long id)
		{
			SendMessageModel model = new SendMessageModel();
			model.POS = DbSession.Get<Device>(id);
			return View(model);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult SendMessage(SendMessageModel model)
		{
			string uri = Config.Sections["server"].Properties["access"];
			DbSession.Refresh(model.POS);
			
			try {
				IAccessService access = ChannelFactory<IAccessService>.CreateChannel(new WSHttpBinding(SecurityMode.None), new EndpointAddress(uri));
				if (!access.IsDeviceOnline(model.POS.Name)) {
					TempData["Error"] = "POS nie jest online";
					return RedirectToAction("List");
				}
				
				IList<ServiceInfo> services = access.GetServices(model.POS.Name);
				ServiceInfo dc = services.Where(i => i.ContractName == "IDeviceControlService").FirstOrDefault();
				
				if (dc == null) {	
					TempData["Error"] = "Nie znaleziono usługi wysyłania wiadmości";
					return RedirectToAction("List");
				}
				
				IDeviceControlService control = ChannelFactory<IDeviceControlService>.CreateChannel(new WSHttpBinding(SecurityMode.None), new EndpointAddress(dc.Uri));
				control.SendMessage(model.Message);
			} catch (Exception e) {
				ViewBag.Online = false;
			}
			return RedirectToAction("List");
		}
		
		public ActionResult AjaxDeviceStatus(long id)
		{
			Device dev = DbSession.Get<Device>(id);
			string uri = String.Format(Config.Sections["client-services"].Properties["system-info"], dev.IpAddress);
			
			var binding = new WSHttpBinding(SecurityMode.None);
			try {
				var client = ChannelFactory<ISystemInfoService>.CreateChannel(binding, new EndpointAddress(uri));
				ViewBag.Online = client.CheckStatus();
				ViewBag.BootupTime = client.GetBootupTime();
			} catch (Exception e) {
				ViewBag.Online = false;
			}
			
			return View();
		}
		
		public ActionResult AjaxServerStatus()
		{
			string uri = Config.Sections["server"].Properties["access"];
			
			try {
				IAccessService client = ChannelFactory<IAccessService>.CreateChannel(new WSHttpBinding(SecurityMode.None), new EndpointAddress(uri));
				ViewBag.Online = client.IsOnline();
				ViewBag.Version = client.SystemVersion();
			} catch (Exception e) {
				ViewBag.Online = false;
			}
			
			return View();
		}

	}
}
