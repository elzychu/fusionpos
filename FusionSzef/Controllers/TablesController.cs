using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate;
using FusionPOS.Domain;

namespace FusionSzef
{
	[Authorize]
	public class TablesController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List()
		{
			return View();	
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Editor(long? id)
		{
			Room r = id.HasValue ? DbSession.Get<Room>(id.Value) : new Room();
			
			if (r == null)
				return HttpNotFound("Room not found");
			
			return View(r);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult Load(long id)
		{
			Room room = DbSession.Get<Room>(id);
			
			return Json(room.Tables);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Save(long id, Room room)
		{
			using (ITransaction tx = DbSession.BeginTransaction()) {
				DbSession.Merge(room);
				tx.Commit();
			}
			
			return RedirectToAction("List");
		}
	}
}

