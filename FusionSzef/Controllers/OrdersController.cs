using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate.Linq;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class OrdersSearchModel
	{
		public User Waiter { get; set; }
		public long Id { get; set; }
		public DateTime DateFrom { get; set; }
		public DateTime DateTo { get; set; }
	}
	
	[Authorize]
	public class OrdersController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");	
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Edit(long id)
		{
			Order o = DbSession.Get<Order>(id);
			
			if (o == null)
				return HttpNotFound("Client not found");
			
			return View(o);		
		}
	}
}

