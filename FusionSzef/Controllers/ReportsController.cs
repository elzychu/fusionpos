using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using FusionPOS.Domain;
using FusionPOS;

namespace FusionSzef
{
	public class CargoReportModel
	{
		public DateTime FromDate { get; set; }
		public DateTime ToDate { get; set; }
		public IList<IngredientCategory> CategoryFilter { get; set; }
		public IList<IngredientGroup> GroupFilter { get; set; }
		public IList<Ingredient> Filter { get; set; }
	}
	
	public class FinancialReportModel
	{
		public DateTime FromDate { get; set; }
		public DateTime ToDate { get; set; }
		public User User { get; set; }
		public Settlement Settlement { get; set; }
		public IList<DishCategory> CategoryFilter { get; set; }
		public IList<DishGroup> GroupFilter { get; set; }
	}
	
	public class ReportsController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult GetCargoReport(CargoReportModel model)
		{
			CargoReport cr = new CargoReport(model.FromDate, model.ToDate, DbSession);
			if(model.CategoryFilter != null)
				model.CategoryFilter.Each(p => DbSession.Refresh(p));
			if(model.GroupFilter != null)
				model.GroupFilter.Each(p => DbSession.Refresh(p));
			if(model.Filter != null)
				model.Filter.Each(p => DbSession.Refresh(p));
			
			cr.CategoriesFilter = model.CategoryFilter == null ? 
			    new List<IngredientCategory>() : model.CategoryFilter;
			cr.GroupsFilter = model.GroupFilter == null ?
				new List<IngredientGroup>() : model.GroupFilter;
			cr.IngredientsFilter = model.Filter == null ? 
				new List<Ingredient>() : model.Filter;
			
			Response.ContentType = "text/html";
			Response.AppendHeader("Content-Disposition", "inline");
			StringWriter sw = new StringWriter();
			cr.GenerateHTML(sw);
			return Content(sw.ToString());

		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult GetFinancialReport(FinancialReportModel model, string reportType)
		{
			
			return View ();
			//FinancialReport fr = new FinancialReport(
			 //   model.FromDate, model.ToDate, DbSession);
			
			return View();
		}
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult FinancialReport()
		{
			return View (new FinancialReportModel());
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult CargoReport()
		{
			return View(new CargoReportModel());
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return View();
		}
	}
	
}

