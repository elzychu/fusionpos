using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Security;
using System.Security.Authentication;
using NHibernate.Linq;
using FusionPOS.Domain;
using FusionPOS;

namespace FusionSzef
{
	[HandleError]
	public class HomeController: BaseController
	{
		[Authorize]
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			ViewBag.DevicesList = DbSession.Query<Device>()
			    .Where(x => x.Enabled)
			    .ToDictionary(x => (int)x.Id, x => x.Name);
			
			ViewBag.Message = new SendMessageModel();
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Login()
		{
			return View(new LoginModel());
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Login(LoginModel model, string ReturnUrl)
		{
			IEnumerable<User> result = DbSession.Query<User>().Where(u => u.Name == model.Username);
			
			if (result.FirstOrDefault() == null) {
				ModelState.AddModelError("Username", "Nieprawidłowa nazwa użytkownika lub hasło");
				return View(model);
			}
			
			User user = result.First();
			try {
				Context.LoginUser(user, model.Password ?? string.Empty);
			} catch (AuthenticationException e) {
				ModelState.AddModelError("Username", "Nieprawidłowa nazwa użytkownika lub hasło");
				return View(model);
			}
			
			return RedirectToAction("Index");
		}
		
		[Authorize]
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Logout()
		{
			FormsAuthentication.SignOut();
			return RedirectToAction("Login");
		}
		
		[Authorize]
		public ActionResult SummaryInfo()
		{
			SessionItem sessionItem = DbSession.Get<SessionItem>("CurrentSettlement");
			Settlement currSettlement = DbSession.Get<Settlement>( BitConverter.ToInt64(sessionItem.Value, 0) );
			
			LogonHistory history = DbSession.Query<LogonHistory>().Where(i => i.Timestamp >= DateTime.Today).OrderBy(p => p.Timestamp).FirstOrDefault();
			if(history != null)
				ViewBag.FirstLogon = history.Timestamp.ToString("hh:mm");
			else
				ViewBag.FirstLogon = "Nie dokonano dzisiaj logowań";
			int loggedCount = 0;
			DbSession.Query<LogonHistory>().Where(i => i.Timestamp >= DateTime.Today).ToList().Classify(p => p.User).Each(u => { 
				LogonHistory h = u.OrderBy(t => t.Timestamp).LastOrDefault();
				if(h != null)
					if(h.Action == LogonAction.Logon)
						loggedCount ++;
			});
			
			ViewBag.WaitersCount = loggedCount;
			
			ViewBag.Takeover = DbSession.Query<Order>().Where(i => i.Time >= DateTime.Today).ToArray().Sum(i =>i.TotalValue != 0 ? i.TotalValue : i.FinalValue);
			ViewBag.TotalRebates = DbSession.Query<Order>().Where(i => i.Time >= DateTime.Today).ToArray().Sum(i => i.RebateAmount);
			ViewBag.CashSum = DbSession.Query<Payment>().Where(i => i.Time >= currSettlement.From).ToArray().Sum(s => s.TotalValue);
			return View();
		}
		
		[Authorize]
		public ActionResult WarehouseInfo()
		{
			ViewBag.Missing = DbSession.Query<Ingredient>().Where(i => i.Quantity == 0);
			ViewBag.Exhausting = DbSession.Query<Ingredient>().Where(i => i.Quantity < 10);
			return View();
		}
		
		[Authorize]
		public ActionResult LogonInfo()
		{
			ViewBag.Username = Context.LoggedUser.Name;
			//ViewBag.LastLogin = DbSession.Query<LogonHistory>().Where(o => o.User == Context.LoggedUser).OrderBy(o => o.Timestamp).
			return View();
		}
		
		[Authorize]
		public JsonResult DailyTurnover()
		{
			return Json(null);
		}
		
		[Authorize]
		public ActionResult CompanyName()
		{
			return Content(Config.Sections["company"].Properties["name"]);
		}
	}
}

