using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class ReservationsController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult List()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Edit(long? id)
		{
			return base.AbstractEdit<Reservation>(id);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Calendar(int? year, int? month, int? day, int? monthly)
		{			
			DateTime frm;
			DateTime to;
			
			if(year.HasValue)
			{
				if(monthly.HasValue)
				{
					if(monthly.Value == 1)
					{
						frm = new DateTime(year.Value, month.Value, 1);
						to = frm.AddDays(DateTime.DaysInMonth(year.Value, month.Value)); 	
					}
					else
					{
						frm = new DateTime(year.Value, month.Value, day.Value);
						to = frm.AddDays(7);	
					}
				}
				else
				{
					frm = new DateTime(year.Value, month.Value, day.Value);
					to = frm.AddDays(7);
				}
			}
			else
			{
				frm = DateTime.Now.AddDays(-1 * (int)DateTime.Now.DayOfWeek + 1);
				to = DateTime.Now.AddDays(7-(int)DateTime.Now.DayOfWeek);	
			}
			
			frm = frm + (new TimeSpan(0,0,0));
			
			to = to + (new TimeSpan(0,0,0));
			ViewBag.Entities = DbSession.Query<Reservation>()
				.Where(p => p.Date>=frm)
				.Where(p => p.Date<=to).ToList();
			//XXX
			//Z pliku xml godziny otwarcia i zamknięcia
			if(!monthly.HasValue || (monthly.HasValue ? monthly.Value == 0 : true))
			{
				ViewBag.StartHour = new DateTime(2000, 1, 1, 7, 0,0);
				ViewBag.EndHour = new DateTime(2000, 1, 1, 22, 0,0);			
			}
			
			ViewBag.From = frm;
			ViewBag.To = to;
			
			ViewBag.Monthly = monthly.HasValue ? monthly.Value : 0;
			return View();
		}
	}
}

