using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Script.Serialization;
using NHibernate;
using NHibernate.Linq;
using FusionPOS;
using FusionPOS.Domain;

namespace FusionSzef
{
	public class IngredientModel
	{
		public string Type { get; set; }
		public long Id { get; set; }
		public string Name { get; set; }
		public int Amount { get; set; }
		public bool Optional { get; set; }
	}
	
	[Authorize]
	public class DishesController: BaseController
	{
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Index()
		{
			return RedirectToAction("List");	
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult ListGroups()
		{
			return View();
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult EditGroup(long? id)
		{
			DishGroup u = id.HasValue ? DbSession.Get<DishGroup>(id) 
			    : new DishGroup();
			JavaScriptSerializer js = new JavaScriptSerializer();
			IList<Dish> Dishes = id.HasValue ? DbSession.Query<Dish>()
			    .Where(p => p.Groups.Contains(u)).ToList() :
			    new List<Dish>();
			
			ViewBag.Dishes = Dishes != null ? js.Serialize(Dishes.Select(i => new {
				Id = i.Id,
				Name = i.Name
			})) : "{}";
			return View (u);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult EditGroup(DishGroup gr)
		{
			using(ITransaction tr = DbSession.BeginTransaction())
			{
				gr.Dishes.Each(p => DbSession.Refresh(p));
				DbSession.Merge(gr);
				tr.Commit();
			}
			return RedirectToAction("ListGroups");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult DeleteGroup(long id, int? confirm)
		{
			DishGroup u = DbSession.Get<DishGroup>(id);
			
			if (u == null)
				return HttpNotFound("Dish group not found");
		
			if (confirm.HasValue && confirm != 0) {
				using (ITransaction tx = DbSession.BeginTransaction()) {
					DbSession.Delete(u);
					tx.Commit();
				}
				return RedirectToAction("ListGroups");
			}
			return View(u);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Edit(long? id, long? parent)
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			Dish u = id.HasValue ? DbSession.Get<Dish>(id) : new Dish() { Recipe = new Recipe() };
			
			if (u == null)
				return HttpNotFound("Dish not found");
			
			if (parent.HasValue) {
				DishCategory c = DbSession.Get<DishCategory>(parent);
				if (c == null)
					return HttpNotFound("Parent category not found");
				
				u.Categories = new List<DishCategory>();
				u.Categories.Add(c);
			}
			
			ViewBag.TaxRates = DbSession.Query<Tax>().ToDictionary(i => (int)i.Id, i => i.Name);
			ViewBag.Kitchens = DbSession.Query<Kitchen>().ToDictionary(i => (int)i.Id, i => i.Name);
			
			if (u.Recipe.Ingredients != null) {
				IList<object> ingredients = new List<object>();
				foreach(RecipeIngredient ingredient in u.Recipe.Ingredients)
				{
					if(ingredient is RecipeIngredientIngredient)
					{
						RecipeIngredientIngredient i = ingredient as RecipeIngredientIngredient;
						ingredients.Add(new {
							Id = i.Ingredient.Id,
							Type = "Ingredient",
							Unit = Enum.GetName(typeof(UnitOfMeasure), i.Ingredient.Unit),
							Name = i.Name,
							Amount = i.Amount,
							Optional = i.Optional
						});
					}
					
					if(ingredient is RecipeIngredientDish)
					{
						RecipeIngredientDish i = ingredient as RecipeIngredientDish;
						ingredients.Add(new {
							Id = ingredient.Component.Id,
							Type = "Dish",
							Unit = "szt.",
							Name = i.Name,
							Amount = i.Amount,
							Optional = i.Optional
						});
					}
					
					if(ingredient is RecipeIngredientIngredientGroup)
					{
						
					}
					
					if(ingredient is RecipeIngredientDishGroup)
					{
						
					}
				}
				
				ViewBag.Ingredients = js.Serialize(ingredients);
			} else
				ViewBag.Ingredients = "{}";
			
			ViewBag.Categories = u.Categories != null ? js.Serialize(u.Categories.Select(i => new {
				Id = i.Id,
				Name = i.Name
			})) : "{}";
			
			ViewBag.Groups = u.Groups != null ? js.Serialize(u.Groups.Select(i => new { 
				Id = i.Id,
				Name = i.Name })) : "{}";
	
			return View(u);
		}
		
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Edit(Dish model, List<IngredientModel> ingredients)
		{
 			if(model.Categories != null)
			{
				
				model.Categories.RemoveDuplicates(i => i.Id);
				model.Categories.ForEach(i => DbSession.Load(i, i.Id));
			}
			if(model.Groups != null)
			{
				model.Groups.RemoveDuplicates(i => i.Id);
				model.Groups.ForEach(i => DbSession.Load(i, i.Id));
			}
			model.Recipe.Ingredients = new List<RecipeIngredient>();
			if(model.Id != 0)
				model.Recipe.Id = model.Id;
			else
				model.Recipe.Dish = model;

			
			using (ITransaction tx = DbSession.BeginTransaction()) {
				
				if(model.Id != 0) 
					model = DbSession.Merge(model);
				ingredients.RemoveDuplicates(i => i.Id);
				foreach (IngredientModel i in ingredients) {
					if(i.Type == "Ingredient")
					{       
						RecipeIngredientIngredient ingredient = new RecipeIngredientIngredient();
						ingredient.Amount = i.Amount;
						ingredient.Optional = i.Optional;
						ingredient.Recipe = model.Recipe;
						ingredient.Ingredient = DbSession.Get<Ingredient>(i.Id);
						model.Recipe.Ingredients.Add(ingredient);
					}
					
					if(i.Type == "Dish")
					{
						RecipeIngredientDish ingredient = new RecipeIngredientDish();
						ingredient.Amount = i.Amount;
						ingredient.Optional = i.Optional;
						ingredient.Recipe = model.Recipe;
						ingredient.Dish = DbSession.Get<Dish>(i.Id);
						model.Recipe.Ingredients.Add(ingredient);	
					}
					
					if(i.Type == "IngredientGroup")
					{
						RecipeIngredientIngredientGroup ingredient = new RecipeIngredientIngredientGroup();
						ingredient.Amount = 1;
						ingredient.Optional = i.Optional;
						ingredient.Recipe = model.Recipe;
						ingredient.IngredientGroup = DbSession.Get<IngredientGroup>(i.Id);
						model.Recipe.Ingredients.Add(ingredient);
					}
					
					if(i.Type == "DishGroup")
					{
						RecipeIngredientDishGroup ingredient = new RecipeIngredientDishGroup();
						ingredient.Amount = 1;
						ingredient.Optional = i.Optional;
						ingredient.Recipe = model.Recipe;
						ingredient.DishGroup = DbSession.Get<DishGroup>(i.Id);
						model.Recipe.Ingredients.Add(ingredient);
					}
				}
				
				
				if(model.Id == 0)
				{
					DbSession.Save(model);
					DbSession.Save(model.Recipe);
				}
				tx.Commit();
			}
			
			return RedirectToAction("List", "Categories");
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxList(long id)
		{
			if(id != 0)
			{
				DishCategory category = DbSession.Get<DishCategory>(id);
				return Json(new { aaData = category.Dishes.Where(i => !i.Deleted) }, JsonRequestBehavior.AllowGet);
			}
			
			return Json (new { aaData = DbSession.Query<Dish>().Where(i => i.Categories.Count == 0 && !i.Deleted) }, JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxListAutocomplete(string term)
		{
			IList list = DbSession.Query<Dish>()
			    .Where(i => i.Name.Contains(term))
			    .Select(i => new {label = i.Name, id = i.Id}).ToList();
			return Json(list, JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public JsonResult AjaxListGroupsAutocomplete(string term)
		{
			return Json(DbSession.Query<DishGroup>()
			    .Where(i => i.Name.Contains(term))
			    .Select(i => new {label = i.Name, id = i.Id}),
			    JsonRequestBehavior.AllowGet);
		}
		
		[AcceptVerbs(HttpVerbs.Get)]
		public ActionResult Delete(long id)
		{
			using (ITransaction tx = DbSession.BeginTransaction())
			{
				Dish dish = DbSession.Get<Dish>(id);
				dish.Deleted = true;
				DbSession.Update(dish);
				tx.Commit();
			}
			
			return RedirectToAction("List", "Categories");
		}
	}
}

