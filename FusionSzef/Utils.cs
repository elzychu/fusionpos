using System;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace FusionSzef
{
	public static class Extensions
	{
		public static IOrderedEnumerable<TKey> OrderByName<TKey>(this IEnumerable<TKey> self, string columnName)
		{
			char sign = columnName[0];
			string rest = columnName.Substring(1);
			var param = Expression.Parameter(typeof(TKey));
			var expr = Expression.Lambda<Func<TKey, object>>(Expression.Property(param, rest), param);
			return sign == '-' ? self.OrderByDescending(expr.Compile()) : self.OrderBy(expr.Compile());
		}
		
		public static string GetOrDefault(this NameValueCollection self, string key, string def)
		{
			if (self[key] == null)
				return def;
			
			return self[key];
		}
		
		public static string ToStringOrNull(this MvcHtmlString self)
		{
			return self == null ? null : self.ToString();	
		}
		
		public static IList<string> ReadPropertyNames(this LambdaExpression self)
		{
			IList<string> result = new List<string>();
			Expression ex = self.Body;
			
			do {
				MemberExpression member = null;
				
				if (ex.NodeType == ExpressionType.Convert) {
					UnaryExpression unary = ex as UnaryExpression;
					member = unary.Operand as MemberExpression;
				} else if (ex.NodeType == ExpressionType.MemberAccess)
					member = ex as MemberExpression;
		
				if (member == null)
					break;
				
				result.Insert(0, member.Member.Name);
				ex = member.Expression;
			} while (true);
			
			return result;
		}
		
		public static PropertyInfo GetPropertyInfo(this LambdaExpression self)
		{
			PropertyInfo info = null;
			Expression ex = self.Body;
			
			do {
				MemberExpression member = null;
				
				if (ex.NodeType == ExpressionType.Convert) {
					UnaryExpression unary = ex as UnaryExpression;
					member = unary.Operand as MemberExpression;
				} else if (ex.NodeType == ExpressionType.MemberAccess)
					member = ex as MemberExpression;
		
				if (member == null)
					break;
				
				info = member.Member as PropertyInfo;
				ex = member.Expression;
			} while (true);
			
			return info;
		}
		
		public static bool IsNumber(this PropertyInfo prop)
		{
			return prop.PropertyType == typeof(int) 
			    || prop.PropertyType == typeof(long) 
			    || prop.PropertyType == typeof(float)
			    || prop.PropertyType == typeof(double);
		}
		
		public static IDictionary<int, string> ReadEnum(Type enumType)
		{
			return Enum.GetValues(enumType).Cast<int>().ToDictionary(i => i, i => Enum.GetName(enumType, i));
		}
		
		public static string ActionMerge(this UrlHelper self, string action, object parameters)
		{
			RouteValueDictionary dict = new RouteValueDictionary(parameters);
			foreach (var i in self.RequestContext.HttpContext.Request.QueryString.AllKeys) {
				if (!dict.ContainsKey(i))
					dict.Add(i, self.RequestContext.HttpContext.Request.QueryString.Get(i));
			}
			
			return self.Action(action, dict);
		}
		
		public static string ToJSON(this HtmlHelper self, object obj)
		{
			JavaScriptSerializer js = new JavaScriptSerializer();
			return js.Serialize(obj);
		}
	}
}

