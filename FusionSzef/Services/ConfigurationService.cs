using System;
using System.ServiceModel;
using FusionPOS;
using FusionPOS.Config;
using FusionPOS.Domain;
using FusionPOS.Services;

namespace FusionSzef
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
	public class ConfigurationService: IConfigurationService
	{
		public ConfigurationService(Context ctx)
		{
			m_Context = ctx;
		}
		
		public string ReadSection(string name)
		{
			return m_Context.Configuration.Sections[name].Name;
		}
		
		private Context m_Context;
	}	
}

