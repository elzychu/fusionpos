using System;
using System.Collections.Generic;

namespace FusionSzef
{
	public class MenuEntry
	{
		public string Title;
		public string Controller;
		public string Image;
	}
	
	public class SubsectionEntry
	{
		public string Title;
		public string Controller;
		public string Action;
	}
	
	public class Layout
	{
		public readonly static MenuEntry[] TopButtons = new MenuEntry[] 
		{
			new MenuEntry { Title = "Layout.Main", Controller = "Home", Image = "home.png" },
			new MenuEntry { Title = "Layout.Users", Controller = "Users", Image = "employees.png" },
			new MenuEntry { Title = "Layout.Menu", Controller = "Categories", Image = "menu.png" },
			new MenuEntry { Title = "Layout.Settings", Controller = "Settings", Image = "settings.png" },
			new MenuEntry { Title = "Layout.Orders", Controller = "Orders", Image = "orders.png" },
			new MenuEntry { Title = "Layout.Ingredients", Controller = "Ingredients", Image = "logout.png" },
		};
		
		public readonly static MenuEntry[] MenuButtons = new MenuEntry[] 
		{
			new MenuEntry { Title = "Layout.Reservtions", Controller = "Reservations" },
			new MenuEntry { Title = "Layout.OutgoingInvoices", Controller = "OutgoingInvoices" },	
		};		
		
		public readonly static MenuEntry[] BottomButtons = new MenuEntry[] 
		{
			new MenuEntry { Title = "Layout.Devices", Controller = "Devices", Image = "pos.png" },
			new MenuEntry { Title = "Layout.Clients", Controller = "Clients", Image = "clients.png" },
			new MenuEntry { Title = "Layout.Ingredients", Controller = "Ingredients", Image = "ingredients.png" },
			new MenuEntry { Title = "Layout.Tables", Controller = "Tables", Image = "tables.png" },
			new MenuEntry { Title = "Layout.Reports", Controller = "Reports", Image = "reports.png" },
			new MenuEntry { Title = "Layout.Statistics", Controller = "Stats", Image = "statistics.png" },
		};
				
		
		public readonly static string[] Javascripts = new string[]
		{
			"~/Content/js/jquery-1.7.1.min.js",
			"~/Content/js/jquery-ui-1.8.16.custom.min.js",
			"~/Content/js/jquery.dataTables.min.js",
			"~/Content/js/jquery.dataTables.reload.js",
			"~/Content/js/jquery.jstree.js",
 	        	"~/Content/js/jquery.tmpl.js",
			"~/Content/js/jquery.flexbox.js",
			"~/Content/js/jquery.validate.js",
			"~/Content/js/jquery.simplemodal.1.4.2.min.js",
			"~/Content/js/jquery.maskedinput-1.3.min.js",
			"~/Content/js/mustache.js",
			"~/Content/js/sprintf-0.7-beta1.js",
			"~/Content/js/highcharts/highcharts.js",
			"~/Content/js/admin.js"
		};
	}
}
