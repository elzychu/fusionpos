using System;
using System.Collections.Generic;
using System.ServiceModel;
using FusionPOS.Services;

namespace FusionPOS
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
	public class SystemInfoService: ISystemInfoService
	{
		public SystemInfoService(Context ctx)
		{
			m_Context = ctx;
			m_BootupTime = DateTime.Now;
		}
		
		public bool CheckStatus()
		{
			return true;
		}
		
		public DateTime GetBootupTime()
		{
			return m_BootupTime;
		}
		
		public string GetSoftwareVersion()
		{
			return "FusionPOS 1"; // XXX
		}
		
		public string GetSystemVersion()
		{
			return "Unknown"; // XXX
		}
		
		public List<ServiceInfo> DiscoveryServices()
		{
			List<ServiceInfo> result = new List<ServiceInfo>();
			
			foreach (ServiceHost i in m_Context.Services) {
				result.Add(new ServiceInfo() {
				    ContractName = i.Description.Endpoints[0].Contract.ContractType.Name,
				    Uri = i.Description.Endpoints[0].ListenUri.ToString()
				});
			}
			
			return result;
		}
		
		private Context m_Context;
		private DateTime m_BootupTime;
	}
}

