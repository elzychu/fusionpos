using System;
using System.Collections.Generic;
using System.ServiceModel;
using FusionPOS.Domain;
using FusionPOS.Services;

namespace FusionPOS
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
	public class DeviceControlService: IDeviceControlService
	{
		public DeviceControlService(Context context)
		{
			m_Context = context;
		}
		
		public void LockSales()
		{
			throw new NotImplementedException();
		}
		
		public void UnlockSales()
		{
			throw new NotImplementedException();
		}
		
		public void SendMessage(string message)
		{
			Gtk.Application.Invoke(delegate(object sender, EventArgs e) {
				MessageBox.Popup("Komunikat", message);
			});
		}
		
		public void ReloadConfiguration()
		{
			throw new NotImplementedException ();
		}
		
		public User GetLoggedIn()
		{
			throw new NotImplementedException ();
		}
		
		public IList<User> GetUsersAtWork()
		{
			throw new NotImplementedException ();
		}
		
		public IList<Order> GetActiveOrders()
		{
			throw new NotImplementedException ();
		}
		
		private Context m_Context;
	}
}

