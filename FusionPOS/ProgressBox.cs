using System;
using Gtk;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public class ProgressBox: Gtk.Window
	{
		public ProgressBox(): base(Gtk.WindowType.Popup)
		{
			Widgets.VBox vbox = new Widgets.VBox(false, 10);
			m_Progress = new Gtk.ProgressBar();
			m_Progress.SetSizeRequest(-1, 60);
			m_Message = new Widgets.Label();
			
			vbox.PackFill(m_Message);
			vbox.PackFill(m_Progress);
			
			Add(vbox);
			Modal = true;
			TransientFor = MainWindow.getInstance();
			WindowPosition = WindowPosition.Center;
			BorderWidth = 30;
			StyleContext.AddClass("ProgressBox");
			SetSizeRequest(500, 150);
		}
		
		public static void ShowProgress(string message)
		{
			Gtk.Application.Invoke(delegate(object sender, EventArgs e) {
				if (m_Instance == null)
					m_Instance = new ProgressBox();
				
				m_Instance.m_Progress.Fraction = 0;
				m_Instance.m_Message.Text = message;
				m_Instance.ShowAll();
				
				while (Application.EventsPending())
					Application.RunIteration();		
			});
		}
		
		public static void UpdateProgress(float fraction)
		{
			Gtk.Application.Invoke(delegate(object sender, EventArgs e) {
				m_Instance.m_Progress.Fraction = fraction;	
			});
		}
		
		public static void HideProgress()
		{
			Gtk.Application.Invoke(delegate(object sender, EventArgs e) {
				m_Instance.Hide();
			});
		}
		
		private Widgets.Label m_Message;
		private Gtk.ProgressBar m_Progress;
		private static ProgressBox m_Instance;
	}
}

