using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Gtk;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public class Keyboard: Gtk.EventBox
	{
		public Keyboard(Gtk.Window window)
		{
			Widgets.Button button;
			
			m_Keyboard = new Gtk.Table(5, 12, true);
			m_Window = window;
			
			foreach (KeyboardKey k in KeyboardKey.StandardKeys) {
				Widgets.Button key = AttachButton(k.Sign, k.X, k.X + 1, k.Y, k.Y + 1);
				key.Clicked += RegularKeyClicked;
			}
			
			/* Backspace */
			button = AttachButton("<-", 10U, 11U, 0U, 1U);
			button.Clicked += SpaceClicked;		
		
				/* Spacebar */
			button = AttachButton(" ", 1U, 8U, 4U, 5U);
			button.Clicked += SpaceClicked;
			
			/* Alt */
			button = AttachButton("Alt", 8U, 9U, 4U, 5U);
			button.Clicked += ShiftClicked;	
		
			/* Shift */
			button = AttachButton("Shift", 0U, 1U, 3U, 4U);
			button.Clicked += ShiftClicked;
			
			/* Enter */
			button = AttachButton("Enter\nkurwo", 10U, 11U, 1U, 3U);
			button.Clicked += ShiftClicked;			
			
			/* Left arrow */
			button = AttachButton("<-", 9U, 10U, 4U, 5U);
			button.Clicked += ShiftClicked;	
			
			/* Right arrow */
			button = AttachButton("->", 10U, 11U, 4U, 5U);
			button.Clicked += ShiftClicked;	
			
			/* Hide button */
			button = AttachButton("Ukryj", 11U, 12U, 4U, 5U);
			button.Clicked += delegate(object sender, EventArgs e) {
				Hide();
			};
			
			Add(m_Keyboard);
			ShowAll();
		}
		
		private Widgets.Button AttachButton(string label, uint left, uint right, uint top, uint bottom)
		{
			Widgets.Button button = new Widgets.Button(label);
			button.CanFocus = false;
			button.CanDefault = false;
			button.SetSizeRequest(10, 10);
			m_Keyboard.Attach(button, left, right, top, bottom);
			return button;
		}
	
		private void RegularKeyClicked(object s, EventArgs args)
		{
			Gtk.Button key = (Gtk.Button)s;
			if (m_Window.Focus is Gtk.Entry) {
				Gtk.Entry entry = (Gtk.Entry)m_Window.Focus;
				entry.InsertText(key.Label);
			}
		}
		
		private void SpaceClicked(object s, EventArgs args)
		{

		}
		
		private void ShiftClicked(object s, EventArgs args)
		{
		
		}
		
		private void AltClicked(object s, EventArgs args)
		{
		
		}
		
		private Gtk.Window m_Window;
		private Gtk.Entry m_TextField;
		private Gtk.Table m_Keyboard;
		private Widgets.Button[] m_Keys;
	}
}

