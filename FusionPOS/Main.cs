using System;
using System.Threading;
using Gtk;
using FusionPOS;

namespace FusionPOS
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			/* For XmlSchema working with ServiceHost */
			Environment.SetEnvironmentVariable("MONO_STRICT_MS_COMPLIANT", "yes");
			new MainClass(args);
		}
		
		public MainClass(string[] args)
		{
			Gtk.CssProvider css = new Gtk.CssProvider();
			Application.Init();
			css.LoadFromPath("/Configuration/Interface.rc");
			Gtk.StyleContext.AddProviderForScreen(Gdk.Screen.Default, css, 600);
			m_Context = new Context();
			m_MainWindow = new MainWindow();
			m_MainWindow.Context = m_Context;
			m_MainWindow.Show();
			
			GLib.Timeout.Add(100, new GLib.TimeoutHandler(Initialize));
			Application.Run();
		}
		
		public bool Initialize()
		{
			Thread thr = new Thread(delegate() {
				m_Context.Initialize();
				m_Context.ApplicationWindow = m_MainWindow;
			});
			
			thr.Start();
			return false;
		}
		
		private MainWindow m_MainWindow;
		private Context m_Context;
	}
}

