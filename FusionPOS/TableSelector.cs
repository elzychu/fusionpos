using System;
using Gtk;
using FusionPOS;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class TableSelector: Gtk.Entry
	{
		public TableSelector(MainWindow window): base()
		{
			m_MainWindow = window;
			Text = "Naciśnij, aby wybrać stolik";
			Name = "GreyedTextEntry";
		}
		
		public Domain.Table Table
		{
			get {
				return m_Table;
			}
			
			set {
				TableSelected(value);
			}
		}
		
		protected override bool OnButtonPressEvent(Gdk.EventButton evnt)
		{
			m_MainWindow.LoadForm(typeof(ShowRoomListForm), new TableSetter(TableSelected));
			return base.OnButtonPressEvent(evnt);
		}
		
		private void TableSelected(Domain.Table table)
		{
			m_Table = table;
			Text = String.Format("#{0} ({1})", table.Number, table.Parent.Name);
		}
		
		private Domain.Table m_Table;
		private MainWindow m_MainWindow;
	}
}

