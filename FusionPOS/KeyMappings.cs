using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FusionPOS
{
	struct KeyboardKey
	{
		public string Sign;
		public string Alternative;
		public uint X;
		public uint Y;
		
		public KeyboardKey(string sign, uint row, uint column)
		{
			Sign = sign;
			Alternative = null;
			X = column;
			Y = row;
		}
		
		public KeyboardKey(string sign, string alternative, uint row, uint column)
		{
			Sign = sign;
			X = column;
			Y = row;	
			Alternative = alternative;
		}
		
		public static IList<KeyboardKey> StandardKeys = new ReadOnlyCollection<KeyboardKey>(new[] {
			new KeyboardKey("1", 0U, 0U),
			new KeyboardKey("2", 0U, 1U),
			new KeyboardKey("3", 0U, 2U),
			new KeyboardKey("4", 0U, 3U),
			new KeyboardKey("5", 0U, 4U),
			new KeyboardKey("6", 0U, 5U),
			new KeyboardKey("7", 0U, 6U),
			new KeyboardKey("8", 0U, 7U),
			new KeyboardKey("9", 0U, 8U),
			new KeyboardKey("0", 0U, 9U),
			new KeyboardKey("Q", 1U, 0U),
			new KeyboardKey("W", 1U, 1U),
			new KeyboardKey("E", "Ę", 1U, 2U),
			new KeyboardKey("R", 1U, 3U),     
			new KeyboardKey("T", 1U, 4U),
			new KeyboardKey("Y", 1U, 5U),
			new KeyboardKey("U", 1U, 6U),
			new KeyboardKey("I", 1U, 7U),
			new KeyboardKey("O", "Ó", 1U, 8U),
			new KeyboardKey("P", 1U, 9U),
			new KeyboardKey("A", "Ą", 2U, 0U),
			new KeyboardKey("S", 2U, 1U),
			new KeyboardKey("D", 2U, 2U),
			new KeyboardKey("F", 2U, 3U),
			new KeyboardKey("G", 2U, 4U),
			new KeyboardKey("H", 2U, 5U),
			new KeyboardKey("J", 2U, 6U),
			new KeyboardKey("K", 2U, 7U),
			new KeyboardKey("L", 2U, 8U),
			new KeyboardKey(";", 3U, 9U),
			new KeyboardKey("Z", "Ż", 3U, 1U),
			new KeyboardKey("X", "Ź",3U, 2U),
			new KeyboardKey("C", 3U, 3U),
			new KeyboardKey("V", 3U, 4U),
			new KeyboardKey("B", 3U, 5U),
			new KeyboardKey("N", 3U, 6U),
			new KeyboardKey("M", 3U, 7U),
			new KeyboardKey(",", 3U, 8U),
			new KeyboardKey(".", 3U, 9U),
		});
	}
}

