using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using Gtk;
using FusionPOS;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public class MainWindow: BaseWindow
	{
		public MainWindow(): base()
		{
			Gtk.EventBox statusbar = new Gtk.EventBox();
			Gtk.HBox status = new Gtk.HBox();
			
			m_StatusLabel = new Widgets.Label("FusionPOS");
			m_StatusLabel.SetAlignment(0f, 0.5f);
			
			
			m_UserLabel = new Widgets.Label("Niezalogowany");
			m_UserLabel.SetAlignment(0.5f, 0.5f);
			m_TimeLabel = new Widgets.Label("21:37");
			m_TimeLabel.SetAlignment(1.0f, 0.5f);
			
			GLib.Idle.Add(delegate() {
				m_TimeLabel.Text = DateTime.Now.ToLongTimeString();
				return true;
			});
	
			status.PackStart(m_StatusLabel, true, true, 0);
			status.PackStart(m_UserLabel, true, true, 0);
			status.PackStart(m_TimeLabel, true, true, 0);
			statusbar.SetSizeRequest(-1, 30);
			statusbar.StyleContext.AddClass("Inverted");
			statusbar.Add(status);
			VBox.PackStart(statusbar, false, true, 0);
			VBox.PackStart(FormPlacement, true, true, 0);
			
			SetDefaultSize(1024, 768);
			SetSizeRequest(1024, 768);
			Resizable = false;
			ShowAll();
			
			//Fullscreen();
		}
		
		public void LoadForm(Type cls, params object[] args)
		{
			Domain.User loggedUser = null;
			if(Context != null)
				loggedUser = Context.GetLoggedUser();
			
			m_UserLabel.Text =
			    loggedUser == null ?
				"Niezalogowany" :
				String.Format("Zalogowany uzytkownik {0}", loggedUser.Name);
			
			Form form = (Form)Activator.CreateInstance(cls, Context);
			AddForm(form);
			form.Load(args);
		}
		
		public void GoHome(object s, EventArgs args)
		{
			m_FormStack.Clear();
			LoadForm(typeof(MainForm));
		}
		/*
		protected override void OnAdjustSizeRequest (Orientation orientation, out int minimum_size, out int natural_size)
		{
			if (orientation == Orientation.Horizontal) {
				minimum_size = natural_size = 1280;	
			} else {
				minimum_size = natural_size = 830;
				natural_size = 860;
			}		
		}

		protected override void OnAdjustSizeAllocation(Orientation orientation, out int minimum_size, out int natural_size, out int allocated_pos, out int allocated_size)
		{
			if (orientation == Orientation.Horizontal) {
				minimum_size = natural_size = allocated_size = 1280;
				allocated_pos = 0;
				
			} else {
				minimum_size = natural_size = allocated_size = 830;
				natural_size = 860;
				allocated_size = 860;
				allocated_pos = 0;
			}
		}*/
		
		public Context Context
		{
			get; set;
		}
		
		private Widgets.Label m_StatusLabel;
		private Widgets.Label m_TimeLabel;
		private Widgets.Label m_UserLabel;
	}
}

