using System;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class AdminLogin: TitleBox
	{
		public enum ResultType
		{
			Granted, Refused, Cancelled, None
		}
		
		public AdminLogin(Context context): base()
		{
			Widgets.VBox vbox = new Widgets.VBox(false, 10);
			Widgets.Button cancel = new Widgets.Button("Anuluj");
			cancel.SetSizeRequest(-1, 80);
			cancel.Clicked += (sender, e) => {
				m_Result = ResultType.Cancelled;
				Hide();
			};
			
			m_Login = new LoginPad();
			m_Login.LoginClicked += Authenticate;
			m_Result = ResultType.None;
			m_Context = context;
			m_TitleBar.Text = "Logowanie";
			vbox.PackExpand(m_Login);
			vbox.PackFill(cancel);
			m_Placement.Add(vbox);
			ShowAll();
		}

		public void Authenticate(object sender, EventArgs e)
		{
			using (ISession session = m_Context.OpenSession()) {
				User u = session.Query<User>()
				    .Where(i => i.Pin == m_Login.Value)
				    .Where(i => i.Type == UserType.Admin)
				    .FirstOrDefault();

				if (u == null) {
					MessageBox.Popup("Błąd logowania", "Nieprawidłowy PIN");
					m_Result = ResultType.Refused;
					return;
				}
				
				Hide();
			}
		}
		
		public static void Sudo(Context context, Action<bool> action)
		{
			AdminLogin login = new AdminLogin(context);
			login.ShowAll();
			
			while (login.m_Result != ResultType.None)
				Gtk.Main.Iteration();
			
			if (login.m_Result == ResultType.Granted)
				action(true);
			
			if (login.m_Result == ResultType.Refused)
				action(false);
			
			return;
		}
		
		private Context m_Context;
		private LoginPad m_Login;
		private ResultType m_Result;
	}
}

