using System;
using FusionPOS.Widgets;
using NHibernate;
using FusionPOS.Domain;
using System.Linq;

namespace FusionPOS
{
	public class AddPointsDialog: TitleBox
	{
		private class PointsEntry: Widgets.ValueEntry
		{
			public PointsEntry(Context ctx, Gtk.Widget parent)
			{
				m_Parent = parent;
				m_Context = ctx;
				//Sensitive = false;
				
				Changed += delegate(object sender, EventArgs e) {
					//Save ();
				};	
			}
			
			public long Save(long points)
			{
				long id = long.Parse(Text);
				long sum = 0;
				using(ISession ses = m_Context.OpenSession())
				{
					Client client = ses.Get<Client>(id);
					if(client != null)
					{
						client.Points += points;
						sum = client.Points;
						using(ITransaction tx = ses.BeginTransaction())
						{
							ses.SaveOrUpdate(client);
							tx.Commit();
						}
					}
				}
				
				return sum;
			}
			
			private Context m_Context;
			private Gtk.Widget m_Parent;
		}
		
		public AddPointsDialog(Context ctx): this(ctx, null, true, 0)
		{ }
		
		public AddPointsDialog(Context ctx, long clientId): this(ctx, null, true, clientId)
		{ }
		
		public AddPointsDialog (Context ctx, Order order, bool editable, long clientId)
		{
			Widgets.VBox vbox = new Widgets.VBox();
			
			AddPointsDialog.PointsEntry entry = new AddPointsDialog.PointsEntry(ctx, this);
			entry.Text = clientId.ToString();
						
			Widgets.ValueEntry textBox = new Widgets.ValueEntry(0);
			
			textBox.Sensitive = editable;

			long points = 0;
			
			if(order != null)
				points = order.TotalPoints;
			
			
			textBox.Text = points.ToString();
			
			vbox.PackFill(new Gtk.Label("Numer klienta:"));
			vbox.PackFill(entry);
			
			Widgets.HBox hbox = new Widgets.HBox();
			AddImageButton("Anuluj", "cancel", delegate(object sender, EventArgs e) {
				Hide (); 
			});
			
			
			Gtk.Table table = new Gtk.Table(2, 3, true);
			table.Attach(new Gtk.Label("Nazwa"), 0, 1, 1, 2);
			table.Attach(new Gtk.Label("Punkty"), 0, 1, 2, 3);

			Gtk.Label clientName = new Gtk.Label("----");
			Gtk.Label clientPoints = new Gtk.Label("----");
			
			if(clientId != 0)
				using (ISession ses = ctx.OpenSession())
				{
					Client client = ses.Get<Client>(clientId);
					if(client == null || client.Deleted)
						return;
					clientName.Text = client.Name;
					clientPoints.Text = client.Points.ToString();
				}
			
			table.Attach(clientName, 1, 2, 1, 2);
			table.Attach(clientPoints, 1, 2, 2, 3);
			
			
			entry.Changed += delegate(object sender, EventArgs e) {
					long id = long.Parse(entry.Text);
					using (ISession ses = ctx.OpenSession())
					{
						Client client = ses.Get<Client>(id);
						if(client == null || client.Deleted)
							return;
						clientName.Text = client.Name;
						clientPoints.Text = client.Points.ToString();
					}
				};
			
			
			AddImageButton("Dodaj", "plus", delegate(object sender, EventArgs e) {
				new PinEntryBox(ctx, delegate(bool isAdmin) {
					if(isAdmin)
					{       if(order == null || editable)
							points = int.Parse(textBox.Text);
						clientPoints.Text = entry.Save(points).ToString();	
					}
				});
			});
			
			vbox.PackFill(new Gtk.Label("Ilość punktów:"));
			vbox.PackFill(textBox);
			vbox.PackFill(table);
			vbox.PackFill(hbox);
			
			m_TitleBar.Text = "Punkty Partnerskie";
			
			
			m_Placement.Add (vbox);
			ShowAll();
		}
	}
}

