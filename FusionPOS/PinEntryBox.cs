using System;
using System.Linq;
using System.Collections;
using Gdk;
using Gtk;
using Pango;
using NHibernate;
using NHibernate.Linq;
using FusionPOS;
using FusionPOS.Widgets;
using FusionPOS.Domain;
using FusionPOS.Translations;

namespace FusionPOS
{
	public delegate void Authorize(bool isAdmin);
	public class PinEntryBox: TitleBox
	{
		public PinEntryBox(Context context, Authorize method)
		{
			Widgets.Table layout = new Widgets.Table(5, 3, false);
			Widgets.Button[] keys = new Widgets.Button[12];
			
			m_Context = context;
			m_Method = method;
			
			m_TitleBar.Text = "Logowanie administratora";
			m_PIN = new Widgets.TextBox();
			m_PIN.StyleContext.AddClass("BiggerFont");
			m_PIN.Visibility = false;
			
			layout.RowSpacing = 10;
			layout.ColumnSpacing = 10;
			layout.Attach(new Widgets.Label("Autoryzacja", FontSettings.BiggerFont), 0, 3, 0, 1);
			layout.Attach(m_PIN, 0, 3, 1, 2);
			
			for (uint j = 1; j < 10; j++) {
				keys[j] = new Widgets.Button((j).ToString(), FontSettings.BiggerFont, 120, 80);
				keys[j].Clicked += new EventHandler(KeypadClicked);
				uint i=j-1;
				layout.Attach(keys[j],
				   i % 3,
				   i % 3 + 1,
				   i / 3 + 2,
				   i / 3 + 3);
			}
			
			keys[0] = new Widgets.Button("0", FontSettings.BiggerFont, 120, 80);
			keys[0].Clicked += new EventHandler(KeypadClicked);
			
			layout.Attach(keys[0], 0, 3, 5, 6);
			
			keys[09] = new Widgets.Button("0", FontSettings.BiggerFont, 120, 80);
			keys[09].Clicked += new EventHandler(KeypadClicked);
			
			AddImageButton("Anuluj", "cancel", (object s, EventArgs args) => {
				Hide ();	
			});
			
			AddImageButton("Cofnij", "arrow_left_2", BackspaceClicked);
			AddImageButton("Potwierdź", "ok", LoginClicked);
			
			layout.Attach(keys[11], 2, 3, 5, 6);
			
			m_Placement.Add(layout);
			ShowAll();
		}
		
		private void KeypadClicked(object s, EventArgs args)
		{
			Widgets.Button button = (Widgets.Button)s;
			m_PIN.Text += button.Label;
		}
		
		private void BackspaceClicked(object s, EventArgs args)
		{
			if (m_PIN.Text.Length == 0)
				return;
			
			m_PIN.Text = m_PIN.Text.Remove(m_PIN.Text.Length - 1, 1);
		}

		private void LoginClicked(object s, EventArgs args)
		{
			using (ISession session = m_Context.OpenSession()) {
				User user = session.Query<User>().Where(p => p.Pin == m_PIN.Text).FirstOrDefault();
				
				if (user == null) {
					MessageBox.Popup("FusionPOS", "Nie ma takiego użytkownika");
					return;
				}
				
				if (user.Type != UserType.Admin && user.Type != UserType.Operator)
				{
					MessageBox.Popup("FusionPOS", "Użytkownik nie posiada odpowiednich uprawnień");
					return;
				}
				Hide();
				m_Method(true);
				
			}
		}
		
		private Context m_Context;
		private Authorize m_Method;
		private Gtk.Button[] Keys;
		private Widgets.TextBox m_PIN;
	}
}

