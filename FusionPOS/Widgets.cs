using System;
using FusionPOS.Domain;

namespace FusionPOS.Widgets
{
	public class DishBrick: Button
	{
		public DishBrick(Dish dish)
		{
			Table table = new Table(1, 1, false);
			Label number = new Label(dish.PositionNumber.ToString());
			number.SetAlignment(0, 1);
			table.AttachExpand(new Label(dish.Name), 0, 1, 0, 1, true, true);
			table.AttachExpand(number, 0, 1, 0, 1, true, true);
			Add(table);
		}
	}
}

