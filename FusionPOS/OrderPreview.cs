using System;
using System.Collections.Generic;
using System.Linq;
using FusionPOS.Domain;
using NHibernate;

namespace FusionPOS
{
	public class OrderPerview: TitleBox
	{
		/*public string WrapText(string text, int length)
		{
			if(length < 1)
				return text;
			
			string[] words = text.Split(' ');
			if(words.Length < 1)
				return text;
			
			string result = "";
			int idx = 0;
			string currentLine = "";
			while(true)
			{
				if(!(idx<words.Length))
					break;
					
				currentLine += words[idx] + " ";
				if(currentLine.Length > length)
				{
					result += currentLine + "\n";
					currentLine = "";
				}
				
				idx++;
			}
			
			return result;
		}*/
		
		public OrderPerview (Order order, Context ctx)
		{
			Widgets.Table table = new Widgets.Table(9, 2, false);
			
			Gtk.TreeView orderDishList = new Gtk.TreeView();
			Gtk.ListStore orderDishStore = new Gtk.ListStore(typeof(string), typeof(string),
			    typeof(string), typeof(Gdk.Color), typeof(OrderDish));
				
			Gtk.TreeViewColumn nameCol = new Gtk.TreeViewColumn();
			nameCol.Title = "Nazwa";
			
			Gtk.TreeViewColumn amountCol = new Gtk.TreeViewColumn();
			amountCol.Title = "Ilość";
			
			Gtk.TreeViewColumn priceCol = new Gtk.TreeViewColumn();
			priceCol.Title = "Cena";
					
			Gtk.CellRendererText crta = new Gtk.CellRendererText();
			nameCol.PackStart(crta, true);
			nameCol.AddAttribute(crta, "text", 0);
			
			priceCol.SetCellDataFunc(crta, delegate (Gtk.TreeViewColumn column, 
			                                     Gtk.CellRenderer cell, 
			                                     Gtk.TreeModel model,
			                                     Gtk.TreeIter iter)
			{
        		        Gdk.Color color = (Gdk.Color) model.GetValue (iter, 3);
    	    	                (cell as Gtk.CellRendererText).Text = (string)model.GetValue(iter, 0);
				(cell as Gtk.CellRendererText).ForegroundGdk = color;
			});
			
			Gtk.CellRendererText crtb = new Gtk.CellRendererText();
			amountCol.PackStart(crtb, true);
			amountCol.AddAttribute(crtb, "text", 0);
			
			amountCol.SetCellDataFunc(crtb, delegate(Gtk.TreeViewColumn tree_column, 
			                                        Gtk.CellRenderer cell, 
			                                        Gtk.TreeModel model, 
			                                        Gtk.TreeIter iter) {
        		        Gdk.Color color = (Gdk.Color) model.GetValue (iter, 3);
    	    	                (cell as Gtk.CellRendererText).Text = (string)model.GetValue(iter, 1);
				(cell as Gtk.CellRendererText).ForegroundGdk = color;					
			});
			
			Gtk.CellRendererText crtc = new Gtk.CellRendererText();
			priceCol.PackStart(crtc, true);
			priceCol.AddAttribute(crtc, "text", 0);
			
			priceCol.SetCellDataFunc(crtc, delegate (Gtk.TreeViewColumn column, 
			                                     Gtk.CellRenderer cell, 
			                                     Gtk.TreeModel model,
			                                     Gtk.TreeIter iter)
			{
        		        Gdk.Color color = (Gdk.Color) model.GetValue (iter, 3);
    	    	                (cell as Gtk.CellRendererText).Text = (string)model.GetValue(iter, 2);
				(cell as Gtk.CellRendererText).ForegroundGdk = color;
			});
			
			orderDishList.AppendColumn(nameCol);
			orderDishList.AppendColumn(amountCol);
			orderDishList.AppendColumn(priceCol);
			
			orderDishList.Model = orderDishStore;
			
			
			Gtk.TreeView modificationList = new Gtk.TreeView();
			Gtk.ListStore modificationsStore = new Gtk.ListStore(typeof(string) , typeof(string));

			Gtk.TreeViewColumn modNameCol = new Gtk.TreeViewColumn();
			Gtk.TreeViewColumn modAmountCol = new Gtk.TreeViewColumn();
			
			modNameCol.Title = "Nazwa";
			modAmountCol.Title = "Ilość";
			
			Gtk.CellRendererText crtd = new Gtk.CellRendererText();
			modNameCol.PackStart(crtd, true);
			modNameCol.AddAttribute(crtd, "text", 0);
			
			modNameCol.SetCellDataFunc(crtd, delegate (Gtk.TreeViewColumn column, 
			                                     Gtk.CellRenderer cell, 
			                                     Gtk.TreeModel model,
			                                     Gtk.TreeIter iter)
			{
    	    	                (cell as Gtk.CellRendererText).Text = (string)model.GetValue(iter, 0);
			});
			
			Gtk.CellRendererText crte = new Gtk.CellRendererText();
			modAmountCol.PackStart(crte, true);
			modAmountCol.AddAttribute(crte, "text", 0);
			
			modAmountCol.SetCellDataFunc(crte, delegate (Gtk.TreeViewColumn column, 
			                                     Gtk.CellRenderer cell, 
			                                     Gtk.TreeModel model,
			                                     Gtk.TreeIter iter)
			{
    	    	                (cell as Gtk.CellRendererText).Text = (string)model.GetValue(iter, 1);
			});
			
			
			modificationList.AppendColumn(modNameCol);
			modificationList.AppendColumn(modAmountCol);
			
			modificationList.Model = modificationsStore;
			
			Gtk.ScrolledWindow scrlWndRight = new Gtk.ScrolledWindow();
			Gtk.ScrolledWindow scrlWndLeft = new Gtk.ScrolledWindow();
			
			scrlWndLeft.Add(orderDishList);
			scrlWndRight.Add (modificationList);
			
			table.AttachFill(new Widgets.Label("Lista dań"), 0, 1, 0, 1);
			table.AttachFill(new Widgets.Label("Lista modyfikacji"), 1, 2, 0, 1);
			table.AttachExpand(scrlWndLeft, 0, 1, 1, 2, true, true);
			table.AttachExpand(scrlWndRight, 1, 2, 1, 2, true, true);
			
			table.AttachFill(new Widgets.Label("Status"), 0, 1, 2, 3);
			table.AttachFill(new Widgets.Label("Wartość zamowienia"), 0, 1, 3, 4);
			table.AttachFill(new Widgets.Label("Wartość ustalona"), 0, 1, 4, 5);
			table.AttachFill(new Widgets.Label("Rabat"), 0, 1, 5, 6);
			table.AttachFill(new Widgets.Label("Klient"), 0, 1, 6, 7);
			table.AttachFill(new Widgets.Label("Przeznaczenie"), 0, 1, 7, 8);
			table.AttachFill(new Widgets.Label("Kelner"), 0, 1, 8, 9);
			table.AttachFill(new Widgets.Label("Punkty w sumie"), 0, 1, 9, 10);
			table.AttachFill(new Widgets.Label("Uwagi do zamówienia"), 0, 1, 10, 11);
			
			using (ISession ses = ctx.OpenSession()) 
			{
				ses.Refresh(order);
				table.AttachFill(new Widgets.Label(order.Status.ToString()), 1, 2, 2, 3);
				table.AttachFill(new Widgets.Label(order.FinalValue.ToString("#.##PLN")), 1, 2, 3, 4);
				table.AttachFill(new Widgets.Label(order.TotalValue != 0 ? order.TotalValue.ToString("#.##PLN") : "----"), 1, 2, 4, 5);
				table.AttachFill(new Widgets.Label( ((order.RebatePercent > 0) ? 
				    order.RebatePercent.ToString("##\\%") : "----") ), 1, 2, 5, 6);
				table.AttachFill(new Widgets.Label(order.Client != null ? order.Client.Name : "----"), 1, 2, 6, 7);
				if(order.Destination is DestinationDelivery)
					table.AttachFill(new Widgets.Label("Dostawa: " + (order.Destination as DestinationDelivery).Address), 1, 2, 7, 8);
				if(order.Destination is DestinationOnSite)
					table.AttachFill(new Widgets.Label("Stolik: " + (order.Destination as DestinationOnSite).Table.Name), 1, 2, 7, 8);
				if(order.Destination is DestinationNone)
					table.AttachFill(new Widgets.Label("Na wynos"), 1, 2, 7, 8);
				
				//table.AttachFill(new Widgets.Label(order.Waiter.Name), 1, 2, 8, 9);
				table.AttachFill(new Widgets.Label(order.TotalPoints.ToString()), 1, 2, 9, 10);
				table.AttachFill(new Widgets.Label(order.Description), 1, 2, 10, 11);
				
				Gdk.Color colorEmpty = new Gdk.Color(0, 0, 0);
				Gdk.Color colorMod = new Gdk.Color(0, 255, 0);
	
				foreach(OrderDish orderDish in order.Entries)
					orderDishStore.AppendValues(orderDish.Dish.Name, orderDish.Amount.ToString("x#"), 
					orderDish.ValueNet.ToString("#.##PLN"), (orderDish.Modifications.Count > 0) ?
					colorMod : colorEmpty, orderDish);
			}
			
			
			orderDishList.CursorChanged += (sender, e) => {
				Gtk.TreeIter iter;
				Gtk.TreeModel model;
				Gtk.TreePath path;
				orderDishList.Selection.GetSelected(out model, out iter);
				path = orderDishStore.GetPath(iter);
				OrderDish dish = model.GetValue(iter, 4) as OrderDish;
				if (dish != null)
					using(ISession ses = ctx.OpenSession())
					{
						modificationsStore.Clear();
						ses.Lock(dish, LockMode.None);
						foreach(OrderDishModification mod in dish.Modifications)
							modificationsStore.AppendValues(mod.Ingredient.Name, mod.Amount.ToString());
					}
			};
			
			AddImageButton("Powrót", "arrow_left", (sender, e) => Hide () );
			AddImageButton("Edycja zamówienia", "edit", (sender, e) => {
				new PinEntryBox(ctx, (isAdmin) => {
					if(isAdmin){
						ctx.ApplicationWindow.LoadForm(typeof(MakeOrderForm), order);
						Hide ();
					}	
				});
			});
			
			AddImageButton("Faktura", "invoice", (sender, e) => {
				//new InvoiceSettingsBox(ctx, order).Show();
				this.Hide();
				ctx.ApplicationWindow.LoadForm(typeof(InvoiceSettingsBox), order);
			});
			
			AddImageButton("Płatności", "money", (sender, e) => {
				ctx.ApplicationWindow.LoadForm(typeof(OrderPaymentForm), order);
				Hide ();
			});
			
			/*AddImageButton("Finalizuj zamówienie", "ok", (sender, e) => {
				decimal sum = 0m;
				order.Payments.Each(p => sum += p.Amount);
				if(order.TotalValue >0 ? sum >= order.TotalValue : sum>= order.FinalValue)
				{
					ctx.FinishOrder(order);
					Hide ();
				}
				else
				{
					MessageBox.Popup("FusionPOS", "Płatność nie została dokonana");	
				}
			});*/
			
			AddImageButton("Anuluj zamówienie", "cancel", (sender, e) => {
				new PinEntryBox(ctx, (isAdmin) => {
					if(isAdmin){
						//XXX
						//To nie działa 
						//ctx.CancelOrder(order);
						using (ISession ses = ctx.OpenSession())
						{
							using(ITransaction tr = ses.BeginTransaction())
							{
								order.Status = OrderStatus.Canceled;
								ses.SaveOrUpdate(order);
								tr.Commit();
							}
						}
						Hide();
						ctx.ApplicationWindow.GoHome(sender, e);
					}
				});
			});
			
			scrlWndLeft.SetSizeRequest(200, 200);
			scrlWndRight.SetSizeRequest(200, 200);
			m_TitleBar.Text = "Podgląd zamówienia";
			m_Placement.Add(table);
			ShowAll();
		}
	}
}

