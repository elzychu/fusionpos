﻿using System;
using System.IO;
using System.IO.Ports;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using System.Linq;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Criterion;
using NHibernate.Linq;
using Mono.Unix;
using FusionPOS.Logging;
using FusionPOS.Config;
using FusionPOS.Domain;
using FusionPOS.Translations;
using FusionPOS.Services;
using FusionPOS.Drivers;
using Shared;

namespace FusionPOS
{
	public delegate void InitializeHandler(object sender);
	public delegate void UserLogEventHandler(object sender, User u);
	
	public class DecimalMysqlDialect: NHibernate.Dialect.MySQLDialect
	{
		public DecimalMysqlDialect(): base()
		{
			base.RegisterColumnType(DbType.Decimal, "DECIMAL(19,2)");
			base.RegisterColumnType(DbType.Decimal, 19, "DECIMAL($p,$s)");
		}
	}
	
	public class Context
	{
		public Context()
		{
		}
		
		public void Initialize()
		{
			ProgressBox.ShowProgress("Uruchamianie...");
			
			/* Initialize logger */
			Logger = new LoggerContext();
			Logger.AddOutputStream(Console.Out);
			Logger.Log("POS", LogPriority.Info, "Loaded");
			m_Logger = new Logger(Logger, "POS");
			m_DbLogger = new Logger(Logger, "DB");
			m_I18NLogger = new Logger(Logger, "I18N");
			m_ServiceLogger = new Logger(Logger, "SERVICE");
			
			ProgressBox.UpdateProgress(1/8f);
			
			/* Read XML configuration */
			Configuration = new ConfigContext();
			Configuration.Logger = new Logger(Logger, "CONF");
			Configuration.ReadFile("/Configuration/Application.xml");

						
			/* Initialize translations */
			I18N = new TranslationContext();
			I18N.Logger = m_I18NLogger;
			I18N.AddTranslationsDirectory("/Configuration/Languages/");
			Translator = new Translator("FusionPOS");
			
			/* Set desired language */
			string lang = Configuration.Sections["application"].Properties["language"];
			if (I18N.HasLanguage(lang))
				Translator.LoadLanguage(I18N.GetLanguage(lang));
			
			/* Detect operating system */
			m_SystemName = SystemName.Uname();
			m_Logger.Standard("Detected operating system {0}", m_SystemName);
			
			ProgressBox.UpdateProgress(2/8f);

			/* Build connection string */
			string connstr = String.Format(
			    "Server={0};Database={1};User ID={2};Password={3};AutoEnlist=false;CharSet=utf8",
			    Configuration.Sections["database"].Properties["host"],
			    Configuration.Sections["database"].Properties["db"],
			    Configuration.Sections["database"].Properties["user"],
			    Configuration.Sections["database"].Properties["password"]);
			
			m_DbLogger.Info(string.Format("Connection string: {0}", connstr));
			
			ProgressBox.UpdateProgress(3/8f);
			
			/* Create database connection */
			m_NhConfiguration = new Configuration();
			m_NhConfiguration.Configure();
			m_NhConfiguration.SetProperty("connection.connection_string", connstr);
			m_NhConfiguration.SetProperty("dialect", "FusionPOS.DecimalMysqlDialect, FusionPOS");
			m_NhConfiguration.AddAssembly("Shared");
			m_NhSessionFactory = m_NhConfiguration.BuildSessionFactory();
			
			ProgressBox.UpdateProgress(4/8f);
			
			/* Export schema (optional) */
			//SchemaExport sch = new SchemaExport(m_NhConfiguration);
			//sch.Execute(true, true, false);
			
			//SchemaUpdate sch = new SchemaUpdate(m_NhConfiguration);
			//sch.Execute(true, true);
			
			//SchemaValidator validator = new SchemaValidator(m_NhConfiguration);
			//validator.Validate();
			//
			/* Test connection */
			/*ISession session = m_NhSessionFactory.OpenSession();
			session.Disconnect();
			session.Reconnect();
			session.Dispose();*/
			
			if (m_SystemName == "Linux") {
				FileSystemWatcher watcher = new FileSystemWatcher("/dev");
				watcher.IncludeSubdirectories = false;
				watcher.Created += (sender, e) => DeviceConnected(e.FullPath);
				watcher.Deleted += (sender, e) => DeviceDisconnected(e.FullPath);
				watcher.EnableRaisingEvents = true;
			}
			
			if (m_SystemName == "FreeBSD") {
				DevdWatcher watcher = new DevdWatcher("/var/run/devd.pipe");
				watcher.Attached += DeviceConnected;
				watcher.Detached += DeviceDisconnected;
				watcher.Start();
			}
			
			
			ProgressBox.UpdateProgress(5/8f);
			
			/* Launch service host */
			m_Services = new List<ServiceHost>();
			foreach (ConfigNode i in Configuration.Sections["services"].Children.Values) {
				Uri uri = new Uri(i.Properties["uri"]);
				Type contractType = Type.GetType(i.Properties["contract"]);
				Type serviceType = Type.GetType(i.Properties["class"]);

				object service = Activator.CreateInstance(serviceType, this);
				ServiceHost host = new ServiceHost(service, uri);
				host.Description.Behaviors.Add(new ServiceMetadataBehavior() { HttpGetEnabled = true });
				host.AddServiceEndpoint(contractType, new WSHttpBinding(SecurityMode.None), uri);
				host.Open();
				
				m_Services.Add(host);
				m_ServiceLogger.Standard(string.Format("Service {0} launched", i.Properties["class"]));
			}
			
			/* Connect to services */
			/* XXX refactor */
			EndpointAddress transaction = new EndpointAddress(Configuration.Sections["server"].Properties["transactions"]);
			m_TransactionService = ChannelFactory<ITransactionService>.CreateChannel(new WSHttpBinding(SecurityMode.None), transaction);
			EndpointAddress configuration = new EndpointAddress(Configuration.Sections["server"].Properties["configuration"]);
			m_ConfigurationService = ChannelFactory<IConfigurationService>.CreateChannel(new WSHttpBinding(SecurityMode.None), configuration);
			EndpointAddress lifecycle = new EndpointAddress(Configuration.Sections["server"].Properties["lifecycle"]);
			m_LifecycleService = ChannelFactory<ILifecycleService>.CreateChannel(new WSHttpBinding(SecurityMode.None), lifecycle);
			
			ProgressBox.UpdateProgress(6/8f);
			
			/* Create fiscal printer driver instance */
			Type printerType = Type.GetType(string.Format("{0}, Drivers", Configuration.Sections["fiscal"].Properties["driver"]));
			m_FiscalPrinterKeepalive = new Thread(FiscalPrinterKeepalive);
			m_FiscalPrinter = (IFiscalPrinterDriver)Activator.CreateInstance(printerType);
			
			try {
				/* Connect to fiscal printer */
				SerialPort port = new SerialPort(Configuration.Sections["fiscal"].Properties["port"]);
				port.BaudRate = int.Parse(Configuration.Sections["fiscal"].Children["config"].Properties["port-speed"]);
				port.Handshake = Handshake.XOnXOff;
				port.ReadTimeout = 5000;
				port.Open();
				m_FiscalPrinterPort = port;
				m_FiscalPrinter.Configure(new DriverConfiguration { Port = m_FiscalPrinterPort });
				m_FiscalPrinterKeepalive.Start();
				m_FiscalPrinter.Initialize();
				m_FiscalPrinter.DisplayText("FusionPOS", 1);
				FiscalPrinterAvailable = true;
			} catch (Exception e) {
				/* XXX typ wyjatku */
				Gtk.Application.Invoke((sender, args) => {
					MessageBox.Popup("Błąd", 
					    "Nie można połączyć się z drukarką fiskalną.\n" + 
					    "Sprzedaż fiskalna nie będzie możliwa");
				});
				
				FiscalPrinterAvailable = false;
			}

			ProgressBox.UpdateProgress(7/8f);
			
			/* Login to server */
			try {
				m_LifecycleService.Login(Configuration.Sections["application"].Properties["name"], Configuration.Sections["services"].Children["system-info"].Properties["uri"]);
				m_ServiceLogger.Standard("Conntected to server at {0}", lifecycle.Uri.ToString());
			} catch (EndpointNotFoundException e) {
				Gtk.Application.Invoke((sender, ev) => {
					MessageBox.Popup("Błąd", "Nie można połączyć się z serwerem");
					System.Environment.Exit(0);
				});
			}
			
			/* Fire initialized event */
			if (Initialized != null)
				Initialized(this);

			ProgressBox.UpdateProgress(8/8f);
			System.Threading.Thread.Sleep(500);
			ProgressBox.HideProgress();
			
			Gtk.Application.Invoke(delegate(object sender, EventArgs e) {
				ApplicationWindow.LoadForm(typeof(LoginForm));
			});
		}

		private void DeviceConnected(string path)
		{
			if (path != Configuration.Sections["fiscal"].Properties["port"])
				return;
			
			FiscalPrinterAvailable = true;
			m_FiscalPrinterKeepalive.Start();
			Gtk.Application.Invoke((sender, e) => MessageBox.Popup("Drukarka fiskalna", "Drukarka fiskalna podłączona", false));
		}

		private void DeviceDisconnected(string path)
		{
			if (path != Configuration.Sections["fiscal"].Properties["port"])
				return;
			
			FiscalPrinterAvailable = false;	
			m_FiscalPrinterKeepalive.Abort();
			Gtk.Application.Invoke((sender, e) => MessageBox.Popup("Drukarka fiskalna", "Drukarka fiskalna odłączona, fiskalizacja niemożliwa", false));
		}
		
		private void FiscalPrinterKeepalive()
		{
			while (true) {
				Thread.Sleep(new TimeSpan(0, 0, 2));
				
				if (m_FiscalPrinter == null)
					continue;
				
				if (m_FiscalPrinter.IsAlive()) {
					if (FiscalPrinterAvailable)
						continue;
					
					FiscalPrinterAvailable = true;
					m_FiscalPrinter.Initialize();
					Gtk.Application.Invoke((sender, e) => 
					    MessageBox.Popup("Drukarka fiskalna", 
					    "Drukarka fiskalna została podłączona.", false));					
				} else {
					if (!FiscalPrinterAvailable)
						continue;
					
					FiscalPrinterAvailable = false;
					Gtk.Application.Invoke((sender, e) => 
					    MessageBox.Popup("Drukarka fiskalna", 
					    "Drukarka fiskalna została odłączona! Podłącz ja ponownie.", false));
				}
			}
		}
		
		public ISession OpenSession()
		{
			return m_NhSessionFactory.OpenSession();
		}
		
		public T ExecuteWithSession<T>(object obj, Func<ISession, T> func)
		{
			ISession s = OpenSession();
			s.Update(obj);
			T result = func(s);
			s.Dispose();
			return result;
		}
		
		public void LoginUser(User user)
		{
			m_LoggedUser = user;
			m_Logger.Standard(string.Format("User {0}/pin:{1} logged in",
			    user.Name, user.Pin));
			
			
			
			using(ISession session = OpenSession())
			{
				Settlement currSettlement = m_TransactionService.GetCurrentSettlement();
			
				if (currSettlement.Status == SettlementStatus.Closed)
				{
					NewSettlement();
					if(MessageBox.Popup("FusionPOS", "Czy chcesz dokonać wpłaty?") == MessageBox.ResultType.OK)
					{
						DoPayment(Widgets.ValuePicker.GetFloatValue("Wpłata"));
					}
				}				
			}
			
			if (UserLoggedIn != null)
				UserLoggedIn(this, m_LoggedUser);
		}
		
		public void ExecuteAuthorized(Action<bool> func)
		{
			
		}
		
		public void NewSettlement()
		{
			try {
				m_TransactionService.NewSettlement();
			} catch (TransactionException e) {
				MessageBox.Popup("FusionPOS", e.Message);
				return;
			}
			
			MessageBox.Popup("FusionPOS", "Nowa zmiana została otwarta");
		}
		
		public void CloseSettlement()
		{
			if (BalanceValidation())									
				Utils.AsyncInvoke<bool>(() => {
					try {
						m_TransactionService.CloseSettlement(m_LoggedUser.Id);
						Gtk.Application.Invoke((s, f) => LogoutUser());
					} catch (TransactionException e) {
						Gtk.Application.Invoke((s, f) => MessageBox.Popup("FusionPOS", e.Message));
					}
					
					return true;
				}, (o) => { });
			else
			    MessageBox.Popup("FusionPOS", "Nie wszyscy kelnerzy zostali rozliczeni");
		}
		
		public void DoPayment(decimal value)
		{
			m_TransactionService.DoPayment(value, m_LoggedUser.Id);
		}
		
		public void LogoutUser()
		{
			m_Logger.Standard(String.Format("User {0} logged out",
			    m_LoggedUser.Name));
			
			if (UserLoggedOut != null)
				UserLoggedOut(this, m_LoggedUser);
			
			m_LoggedUser = null;
			ApplicationWindow.LoadForm(typeof(LoginForm));
		}
		
		public void MakeOrder(Domain.Order order, bool print)
		{
			order.Time = DateTime.Now;
			order.Waiter = m_LoggedUser;
			order.Status = OrderStatus.Waiting;
			
			TransactionDTO tdto = new TransactionDTO();
			tdto.Pack(order);
			try
			{
				m_TransactionService.MakeOrder(tdto, print);
			}
			catch (CommunicationException e)
			{
				MessageBox.Popup("Błąd", e.Message);	
			}
			
			m_Logger.Standard("New order submitted");
		}
		
		public void UpdateOrder(Domain.Order order, bool print)
		{
			using(ISession session = OpenSession()) 
			{
				TransactionDTO tdto = new TransactionDTO();
				tdto.Pack(order);
				try
				{
					m_TransactionService.UpdateOrder(tdto, print);
				}
				catch (CommunicationException e)
				{
					MessageBox.Popup("Błąd", e.Message);
				}
			}
		}
		
		public void CancelOrder(Domain.Order order)
		{
			using(ISession session = OpenSession()) 
			{
				session.Lock(order.Waiter, LockMode.None);
			}
		}
		
		public void FinishOrder(Domain.Order order)
		{
			bool done = false;
			TransactionDTO orderDTO = new TransactionDTO();
			using (ISession ses = OpenSession())
			{
				ses.Refresh(order);
				orderDTO.Pack(order);
			}
			if (!FiscalPrinterAvailable) {
				if (MessageBox.Popup("Błąd", "Brak drukarki fiskalnej - czy wydrukować paragon zastępczy na drukarce termicznej?") == MessageBox.ResultType.OK)
				{
					orderDTO.Pack(order);
					try {
						m_TransactionService.FinishOrder(orderDTO, true);
					}
					catch (CommunicationException e)
					{
						MessageBox.Popup("Błąd", e.Message);
					}
				}
				else
					m_TransactionService.FinishOrder(orderDTO, false);
				return;
			}
			
			Thread thr = new Thread(delegate() {
				float i = 0;
				float count = order.Entries.Count + order.Payments.Count + 1;
				
				ProgressBox.ShowProgress("Łączenie z drukarką fiskalną...");
			
				m_FiscalPrinter.CashDeskNumber = "123";
				m_FiscalPrinter.CashierName = m_LoggedUser.Name;
				
				ProgressBox.UpdateProgress(1/count);
				
				using (IFiscalReceipt receipt = m_FiscalPrinter.BeginTransaction()) {
					if (order.RebateAmount != 0 || order.RebatePercent != 0)
						receipt.ApplyRebate(order.RebateAmount, order.RebatePercent, "Rabat");
					
					foreach (OrderDish od in order.Entries) {
						receipt.Add(od.Dish.Name, od.Amount, 'B', (decimal)(Math.Round(od.ValueForPieceGross, 2)), 0);
						ProgressBox.UpdateProgress(i++/count);
					}
					
					foreach (OrderPayment op in order.Payments) {
						receipt.AddPayment(op.Method, op.Amount);
						ProgressBox.UpdateProgress(i++/count);
					}
					
					if (order.Client != null)
						receipt.AddExtraLine(ReceiptLineType.RegularClient, order.Client.Name);
					 
					try {
						receipt.Commit(string.Format("ID{0}", order.Id));
					} catch (ArgumentException err) {
						Gtk.Application.Invoke((sender, e) => {
							MessageBox.Popup("huj", err.Message);
						});
					}
				}
				
				
				
				ProgressBox.HideProgress();
				done = true;
			});
			
			thr.Start();
			
			orderDTO.Pack(order);
			try {
				m_TransactionService.FinishOrder(orderDTO, false);
			}
			catch (CommunicationException e)
			{
				MessageBox.Popup("Błąd", e.Message);
			}
		}
		
		public void SaveWaiterSettlement(decimal value)
		{
			m_TransactionService.SaveWaiterSettlement(value, m_LoggedUser.Id);
		}

		public OutgoingInvoice CreateInvoice(FusionPOS.Domain.Order order)
		{
			int index = 0;
			OutgoingInvoice invoice = new OutgoingInvoice();
			invoice.Components = new List<OutgoingInvoiceComponent>();
			
			foreach (var i in order.Entries) {
				invoice.Components.Add(new OutgoingInvoiceComponent() {
					IdOnInvoice = index++,
					Name = i.Dish.Name,
					Amount = i.Amount,
					NetUnitPrice = i.ValueForPieceNet,
					VatPercent = i.Dish.Tax.PercentValue,
					UnitOfMeasure = UnitOfMeasure.PIECE,
				});
			}
			
			return invoice;
		}
		
		public void PrintInvoice(Kitchen kitchen, OutgoingInvoice oi)
		{
			OutgoingInvoiceDTO oiDTO = new OutgoingInvoiceDTO();
			oiDTO.Pack(oi);
			m_TransactionService.PrintInvoice(kitchen, oiDTO);	
		}

		public MainWindow ApplicationWindow
		{
			get; set;
		}
		
		public ConfigContext Configuration
		{
			get; set;
		}
		
		public TranslationContext I18N
		{
			get; set;
		}
		
		public Translator Translator
		{
			get; set; 
		}
		
		public LoggerContext Logger
		{
			get; set;
		}
		
		public IList<ServiceHost> Services
		{
			get {
				return m_Services;
			}
		}
		
		IList<Domain.Order> GetStornOrders(ISession session) {
			DateTime from = m_TransactionService.GetCurrentSettlement().From;
			DateTime to = DateTime.Now;
			ICriteria ordrCriteria = session.CreateCriteria<Domain.Order>().Add (Expression.Between("Time", from, to))
					.Add (Expression.Eq("Waiter", m_LoggedUser)).Add(Expression.Not(Expression.Eq("Status", OrderStatus.Completed)));			
			return ordrCriteria.List<Domain.Order>();
		}
		
		public Settlement GetCurrentSettlement() 
		{
			return m_TransactionService.GetCurrentSettlement();
		}
		
		public User GetLoggedUser()
		{
			return m_LoggedUser;
		}
		
		public bool FiscalPrinterAvailable
		{
			get; private set;
		}
		
		public bool BalanceValidation()
		{
			if( GetUnsettledUsers().Any() )
				return false;
			else
				return true;
		}
		
		public IList<User> GetUnsettledUsers()
		{
			using(ISession ses = OpenSession())
			{
				Settlement settlement = GetCurrentSettlement();
				IEnumerable<User> users = ses.Query<User>();
				
				IList<User> result = new List<User>();
				
				foreach(User u in users)
				{
					if(!IsUserSettled(u, ses, settlement))
						result.Add(u);
				}
				
				/*return users.Where(u => ses.Query<Domain.Order>().Where (o =>
					o.Waiter.Id == u.Id && o.Time > settlement.From)
				        .Any (p => ses.Query<UserBalance>().Where (b => 
				         b.Settlement.Id == settlement.Id)
				        .OrderBy(c => c.Time).First().Time < p.Time)).ToList();*/
				
				return result;
			}
		}
		
		private bool IsUserSettled(User user, ISession ses, Settlement s)
		{
			try {
				IList<Domain.Order> orders = ses.Query<Domain.Order>()
				    .Where(o => o.Waiter.Id == user.Id && o.Time > s.From && 
					       o.Status != OrderStatus.Canceled).ToList();
				
				if(orders.Any(p => ses.Query<UserBalance>().Where (b => 
					b.Settlement.Id == s.Id).OrderBy(c => c.Time)
					.First().Time < p.Time))
					return false;
				else
					return true;
			} catch (System.InvalidOperationException e)
			{
				return false;	
			}
		}
		
		private void OpenFiscalPrinter()
		{

		}
		
		private void CloseFiscalPrinter()
		{
			
		}
		
		public event InitializeHandler Initialized;
		public event UserLogEventHandler UserLoggedIn;
		public event UserLogEventHandler UserLoggedOut;
		
		private string m_SystemName;
		private IList<ServiceHost> m_Services;
		private Configuration m_NhConfiguration;
		private ISessionFactory m_NhSessionFactory;
		private IFiscalPrinterDriver m_FiscalPrinter;
		private ITransactionService m_TransactionService;
		private IConfigurationService m_ConfigurationService;
		private ILifecycleService m_LifecycleService;
		private SerialPort m_FiscalPrinterPort;
		private Thread m_FiscalPrinterKeepalive;
		private Logger m_Logger;
		private Logger m_DbLogger;
		private Logger m_I18NLogger;
		private Logger m_ServiceLogger;
		private User m_LoggedUser;      
	}
}
