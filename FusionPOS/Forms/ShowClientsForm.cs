using System;
using System.Linq;
using System.Collections.Generic;
using Gtk;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	delegate void ClientSetter(long Id);
	
	public class ShowClientsForm: Form
	{
	
		public ShowClientsForm(Context context): base(context)
		{
			m_List = new Gtk.TreeView();
			m_ClientList = new Gtk.ListStore(typeof(Client));
			m_SearchBox = new Widgets.TextBox();
			m_SearchBox.Changed += Search;
				
			ScrolledWindow scrWnd = new ScrolledWindow();
			scrWnd.Add(m_List);
			
			Widgets.SimpleTreeViewColumn nameColumn = new Widgets.SimpleTreeViewColumn("Nazwa", "Name");
			Widgets.SimpleTreeViewColumn addressColumn = new Widgets.SimpleTreeViewColumn("Adres", "Address");
			Widgets.SimpleTreeViewColumn telephoneNumberColumn = new Widgets.SimpleTreeViewColumn("Numer telefonu", "PhoneNumber");
			Widgets.SimpleTreeViewColumn descriptionColumn = new Widgets.SimpleTreeViewColumn("Opis", "Description");
			Widgets.SimpleTreeViewColumn pointsColumn = new Widgets.SimpleTreeViewColumn("Punkty", "Points");
			Widgets.SimpleTreeViewColumn creditColumn = new Widgets.SimpleTreeViewColumn("Dług", "Credit");
			Widgets.SimpleTreeViewColumn maxCreditColumn = new Widgets.SimpleTreeViewColumn("Maksymalny dług", "MaxCredit");
			m_List.AppendColumn(nameColumn);
			m_List.AppendColumn(addressColumn);
			m_List.AppendColumn(telephoneNumberColumn);
			m_List.AppendColumn(descriptionColumn);
			m_List.AppendColumn(pointsColumn);
			m_List.AppendColumn(creditColumn);
			m_List.AppendColumn(maxCreditColumn);
			
			m_List.Model = m_ClientList;
			
			Widgets.VBox vbox = new Widgets.VBox(false, 0);
			Widgets.HBox hbox = new Widgets.HBox(false, 0);			
			
			m_Buttons = new Widgets.ButtonsBar();
			m_Buttons.AddLeftImageButton("Powrót", "arrow_left", BackClick);
			m_Buttons.AddRightImageButton("Zarządzaj punktami", "mng_points", EditPointsClick);
			m_Buttons.AddRightImageButton("Edytuj", "edit",  EditClientClick);
			m_Buttons.AddRightImageButton("Usuń", "client_delete",EraseClientClick);
			m_Buttons.AddRightImageButton("Dodaj", "client", AddClientClick);
			
			
			Refresh();
			

			hbox.PackStart(m_Buttons, false, false, 1);	
			vbox.PackStart(m_SearchBox, false, false, 1);
			vbox.PackStart(scrWnd, true, true, 0);
			vbox.PackStart(hbox, false, false, 0);
			
			Add(vbox);			
			ShowAll();
			
		}
		
		public override void Load(params object [] args)
		{
			if (args.Length > 0)
			{
				m_ClientSetter = (ClientSetter)args[0];
				m_Buttons.AddLeftImageButton("Wybierz", "ok", ReturnClient);
				ShowAll();
			}
		}
		
		public void Refresh()
		{
			m_ClientList.Clear();
			using (ISession session = Context.OpenSession()) {
				session.Query<Client>().Where(i=>!i.Deleted).ForEach(i => m_ClientList.AppendValues(i));
			}
		}
		
		private void ReturnClient(object sender, EventArgs args)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			m_List.Selection.GetSelected(out model, out iter);
			Client client = (Client)model.GetValue(iter, 0);
			m_ClientSetter(client.Id);
			
			Context.ApplicationWindow.GoBack(sender, args);
		}
		
		private void Search(object sender, EventArgs args)
		{
			if(m_SearchBox.Text == "")
			{
				m_List.Model = m_ClientList;
				return;
			}
			
			if(m_SearchList == null)
				m_SearchList = new Gtk.ListStore(typeof(Client));
			
			m_SearchList.Clear();
			string criterium = m_SearchBox.Text;
			//criterium = "%" + criterium + "%";
			
			using (ISession session = Context.OpenSession()) {
				session.Query<Client>().Where(
				    c => c.Name.Contains(criterium) || 
				    c.Address.Contains(criterium) ||
				    c.Description.Contains(criterium) ||
				    c.PhoneNumber.Contains(criterium)).
				    ForEach(i => m_SearchList.AppendValues(i));
	
			}
			
			m_List.Model = m_SearchList;
		}
		
		private void BackClick(object sender, EventArgs args)
		{	
			Context.ApplicationWindow.GoBack(sender, args);	
		}
		
		private void AddClientClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(EditClientForm), new Refresher(Refresh));
		}
		
		private void EraseClientClick(object sender, EventArgs args)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			m_List.Selection.GetSelected(out model, out iter);
			Client client = (Client)model.GetValue(iter, 0);
			
			if( client == null)
				return;
			
			EraseClient(client);
		}
		
		private void EditClientClick(object sender, EventArgs args)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			m_List.Selection.GetSelected(out model, out iter);
			Client client = (Client)model.GetValue(iter, 0);
			if (client != null)
				Context.ApplicationWindow.LoadForm(typeof(EditClientForm), new Refresher(Refresh), client.Id);
		}
		
		private void EditPointsClick(object sender, EventArgs args)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			m_List.Selection.GetSelected(out model, out iter);
			Client client = (Client)model.GetValue(iter, 0);
			if(client != null)
				new AddPointsDialog(Context, client.Id);
		}
		
		private void EraseClient(Client cli)
		{
			using (ISession session = Context.OpenSession()) {
				ITransaction tran = session.BeginTransaction();
				cli.Deleted = true;
				session.Save(cli);
				tran.Commit();
			}
			
			Refresh();
		}
		
		private Gtk.TreeView m_List;
		private Gtk.ListStore m_ClientList;
		private Widgets.ButtonsBar m_Buttons;
		private Gtk.ListStore m_SearchList;
		private Widgets.TextBox m_SearchBox;
		private ClientSetter m_ClientSetter;
	}
	
	
}