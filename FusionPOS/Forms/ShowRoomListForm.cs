using System;
using FusionPOS.Domain;
using NHibernate;
using System.Collections.Generic;
using Gdk;
using FusionPOS.Widgets;
using System.Linq;
using NHibernate.Linq;

namespace FusionPOS
{
	delegate void ContextMenuEvent();
	public delegate void TableSetter(Domain.Table table);
	
	public class ShowOrders: TitleBox
	{
		public ShowOrders(Domain.Table table, Context context)
		{
			AddImageButton("Powrót", "arrow_left", (sender, e) => Hide ());
			m_Orders = new Widgets.Table(6, 6, false);
			m_Context = context;
			
			using(ISession ses = context.OpenSession())
			{
			
				IList<DestinationOnSite> orders = ses.Query<DestinationOnSite>()
				    .Where(p => (p.Table.Id == table.Id) && (p.Order.Status != OrderStatus.Completed) && 
				    (p.Order.Status != OrderStatus.Canceled)).OrderBy(p => p.Order.Time).ToList();
				if(orders.Count > 0)
					orders.Each(q => AddOrder(q.Order));
				else
				{
					MessageBox.Popup("FusionPOS", "Lista aktywnych zamówień jest pusta", false);
					return;
				}
			}
			
			m_Orders.SetSizeRequest(600, -1);
			m_Placement.Add(m_Orders);
			m_TitleBar.Text = String.Format("Zamówienia na stolik {0}", table.Number);
			ShowAll();
		}
		
		private void AddOrder(Domain.Order order)
		{   
			uint i = (uint)m_Orders.Children.Length;
			OrderButton button = new OrderButton(order, m_Context);
			button.Clicked += (sender, e) => Hide ();
			m_Orders.AttachExpand(button, 
			    i % 6, i % 6 + 1, 
			    i / 6, i / 6 + 1, true, true);
			button.SetSizeRequest(40, 40);
		}
		
		private Widgets.Table m_Orders;
		private Context m_Context;
	}
	
	public class TableMenu: TitleBox
	{
		public TableMenu(TableSetter setter, Domain.Table table,
		                 ShowRoomListForm outher, Context context)
		{
			VBox vbox = new VBox();
			Widgets.Button button;
			//vbox.SetSizeRequest(320, -1);
			
			if(setter!=null)
			{
				button = new Widgets.Button("Wybierz");
				button.Clicked += delegate(object sender, EventArgs e) {
					Hide ();
					context.ApplicationWindow.GoBack(sender, e);
					setter(table);
				};
				button.SetSizeRequest(300, 100);
				vbox.PackExpand(button);
			}
			button = new Widgets.Button("Pokaż zamówienia");
			button.Clicked += delegate(object sender, EventArgs e) {
				Hide ();
				new ShowOrders(table, context);
			};
			button.SetSizeRequest(300, 100);
			vbox.PackExpand(button);

			button = new Widgets.Button("Zarezerwuj na stolik");
			button.Clicked += delegate(object sender, EventArgs e) {
				Hide ();
				context.ApplicationWindow.LoadForm(typeof(AddReservationForm));
			};
			button.SetSizeRequest(300, 100);
			vbox.PackExpand(button);
		
			m_Placement.Add(vbox);
			m_TitleBar.Text = String.Format("Stolik nr. {0}", table.Id.ToString());
			AddImageButton("Anuluj", "arrow_left", delegate(object sender, EventArgs e) {
				Hide ();
			});
			
			ShowAll();	
		}
	}
	
	public class RoomView: VBox
	{
		public RoomView(int width, int height, Context context, ShowRoomListForm outher)
		{
			m_Outher = outher;
			m_Title = new WindowLabel("----", "Inverted");
			m_Area = new Gtk.DrawingArea();
			m_Area.SetSizeRequest(width, height);
			m_Area.ButtonPressEvent += SurfaceClicked;
			m_Area.AddEvents ((int) EventMask.ButtonPressMask    
                            |(int) EventMask.ButtonReleaseMask    
                            |(int) EventMask.KeyPressMask    
                            |(int) EventMask.PointerMotionMask);
			m_Area.Drawn += Render;
			
			m_Width = width;
			m_Height = height;
			m_Context = context;
			
			PackFill(m_Title);
			PackExpand(m_Area);
		}
		
		public void SetTableSetter(TableSetter setter)
		{
			m_Setter = setter;
		}
		
		public void SetRoom(long id)
		{
			using(ISession ses = m_Context.OpenSession())
			{
				m_Room = ses.Get<Room>(id);
				m_Title.Text = m_Room.Name;
				
				m_Area.QueueDraw();
			}
		}
		
		[GLib.ConnectBefore]
		private void SurfaceClicked(object o, Gtk.ButtonPressEventArgs args) 
		{
			double x = args.Event.X;
			double y = args.Event.Y;
			
			foreach(Domain.Table table in m_Room.Tables)
			{
				if(CheckIfTableCollide(table, x, y))
				{
					new TableMenu(m_Setter, table, m_Outher, m_Context);
				}
			}
		}
		
		bool CheckIfTableCollide(Domain.Table table, double x, double y)
		{
			if(table.Type == TableType.Rect)
				if (x>=TranslateX(table.X) && y>=TranslateY(table.Y)
				    && x<=TranslateX(table.X) + 
					TranslateScale(table.Width) && y<=TranslateY(table.Y) + TranslateScale(table.Height)) 
					return true;
				else 
					return false;
			if(table.Type == TableType.Round)
				if( Math.Sqrt((table.X - x)*(table.X - x) + (table.Y -y)*(table.Y - y)) < table.Radius )
					return true;
				else
					return false;
			return false;
		}
		
		private void Render(object o, Gtk.DrawnArgs args)
		{
			Cairo.Context cr = args.Cr;
			
			cr.LineWidth = 2.0;
			cr.SetSourceRGB(0.6, 0.6, 0.6);
			DrawStrippedRect(cr, true, 6.0, 6.0, m_Width + 12.0, m_Height + 12.0);
			
			if(m_Room != null)
			{
				using (ISession session = m_Context.OpenSession()) {
					session.Update(m_Room);
					
					double x = TranslateX(0);
					double y = TranslateY(0);
					double w = TranslateScale(m_Room.Width);
					double h = TranslateScale(m_Room.Height);
					
					
					cr.Rectangle(x, y, w, h);
					cr.SetSourceRGB(0.87, 0.9, 0.98);
					cr.FillPreserve();
					cr.SetSourceRGB(0.6, 0.6, 0.6);
					cr.Stroke();
					
					cr.LineWidth = 8.0;
					foreach(Domain.Table table in m_Room.Tables)
					{
						x = TranslateX(table.X);
						y = TranslateY(table.Y);
						w = TranslateScale(table.Width);
						h = TranslateScale(table.Height);
											
						DrawRectTable(cr, table.Id.ToString(),
						              table.Type, x, y, w, h);
					}
				}
			}
			
			(cr as IDisposable).Dispose();
		}
		
		private void DrawRectTable(Cairo.Context cr, String text, TableType type, double x, 
		                           double y, double w, double h)
		{
			cr.SelectFontFace("Arial", Cairo.FontSlant.Normal, Cairo.FontWeight.Bold);
			cr.SetFontSize(14);
			string name = String.Format("nr. {0}", text);
			double txtWidth = cr.TextExtents(name).Width;
			double txtHeight = cr.TextExtents(name).Height;
			switch(type)
			{
				case TableType.Rect:
					cr.SetSourceRGB(0.0, 0.0, 0.0);
					cr.Arc(x+2, y+2, 7, 0.0, Math.PI*2);
					cr.Fill();
					cr.Arc(x-2 + w, y+2, 7, 0.0, Math.PI*2);
					cr.Fill();
					cr.Arc(x-2 + w, y-2 + h, 7, 0.0, Math.PI*2);
					cr.Fill();
					cr.Arc(x+2, y + h-2, 7, 0.0, Math.PI*2);
					cr.Fill();
				
					cr.SetSourceRGB(0.9, 0.3, 0.1);
					cr.LineWidth = 5.0;
					cr.Rectangle(x+10, y+10, w-20, h-20);
					cr.Rectangle(x, y+10, 10, h-20);
					cr.Rectangle(x+w-10, y+10, 10, h-20);
					cr.Rectangle(x+10, y, w-20, 10);
					cr.Rectangle(x+10, y+h-10, w-20, 10);
					cr.Arc(x+10, y+10, 10, 0, Math.PI*2);
					cr.Arc(x+w-10, y+10, 10, 0, Math.PI*2);
					cr.Arc(x+10, y+h-10, 10, 0, Math.PI*2);
					cr.Arc(x+w-10, y+h-10, 10, 0, Math.PI*2);
					cr.Fill();
					
					cr.SetSourceRGB(0.0, 0.0, 0.0);

					cr.MoveTo(x+(w/2-txtWidth/2), y+(h/2-txtHeight/2));
					cr.ShowText(name);
					
					cr.Fill();
				break;
				
				case TableType.Round:
					cr.SetSourceRGB(0.9, 0.3, 0.1);
					cr.Arc(x, y, w, 0.0, Math.PI*2);
					cr.Fill();

					cr.MoveTo(x+(w/2-txtWidth/2), y-(h/2-txtHeight/2));
					cr.ShowText(name);
					cr.Fill();
				break;	
			}
		}
		
		private void DrawStrippedRect(Cairo.Context ctx, bool stroked,
		                      double x, double y, double w, double h)
		{
			if(stroked)
			{
				ctx.Rectangle(x, y, w, h);
				ctx.Stroke ();
			}
			
			int i=0;
			for(i=0; i<w; i+=10)
			{
				ctx.MoveTo(x + i, y);
				
				if(i<h)
					ctx.LineTo(x, y + i);
				else
					ctx.LineTo(x + i - h, y+h);
				ctx.FillPreserve();
				ctx.Stroke();
			}
			
			for(i=(int)(i-w); i<h; i+=10)
			{
				ctx.MoveTo(x + w, y+i);
				
				if((i+w)<h)
					ctx.LineTo(x, y + i + w);
				else
					ctx.LineTo(x + i + w - h, y + h);
				ctx.FillPreserve();
				ctx.Stroke();
			}
		}
		
		private double TranslateScale(double Value)
		{
			if(m_Room.Width > m_Width || m_Room.Height > m_Height)
			{
				if(m_Room.Width > m_Room.Height)
				{
					return Value * (m_Width / m_Room.Width);
				}
				else
				{
					return Value * (m_Height / m_Room.Height);
				}
			}
			
			return Value;
		}
				            
		private double TranslateX(double x)
		{
			if(m_Room == null)
				return x;
			
			if(m_Room.Width > m_Width || m_Room.Height > m_Height)
			{
				if(m_Room.Width > m_Room.Height)
				{
					return (x * (m_Width / m_Room.Width) + 12.0);
				}
				else
				{
					double lengthX = ((m_Room.Width * m_Height)/m_Room.Height);
					double margin = (m_Width - lengthX) / 2;
					
					return (x*(lengthX / m_Room.Width) +  margin) + 12.0;
				}
			}
			else
			{
				return x+12.0;	
			}
		}
		
		private double TranslateY(double y)
		{
			if(m_Room == null)
				return y;
			
			
			
			if(m_Room.Width > m_Width || m_Room.Height > m_Height)
			{
				if(m_Room.Width < m_Room.Height)
				{
					return (y * (m_Height / m_Room.Height) + 12.0);
				}
				else
				{
					double lengthY = ((m_Room.Height * m_Width)/m_Room.Width);
					double margin = ( m_Height - lengthY) / 2;
					
					return (y*(lengthY / m_Room.Height) + margin)  + 12.0;
				}
			}
			else
			{
				return y+12.0;
			}
		}
		
		private readonly double m_StrokeWidth = 6.0;
		private readonly double m_Margin = 12.0;
		private ShowRoomListForm m_Outher;
		private TableSetter m_Setter;
		private Gtk.DrawingArea m_Area;
		private double m_Width;
		private double m_Height;
		private Room m_Room;
		private Context m_Context;
		private WindowLabel m_Title;
	}
	
	public class ShowRoomListForm: Form
	{
		public ShowRoomListForm(Context context): base(context)
		{
			VBox mainBox = new VBox();
			HBox contentBox = new HBox();
			ButtonsBar bar = new ButtonsBar();
			bar.AddLeftImageButton("Powrót", "arrow_left", (object sender, EventArgs args) 
			                  => Context.ApplicationWindow.GoBack(sender, args));
			m_RoomView = new RoomView(770, 600, context, this);
			contentBox.PackExpand(m_RoomView);
			mainBox.PackExpand(contentBox);
			mainBox.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			mainBox.PackFill(bar);
			
			
			Gtk.ListStore roomList = new Gtk.ListStore(typeof(Room));
			Gtk.TreeView treeView = new Gtk.TreeView();
			using(ISession ses = context.OpenSession())
			{
				List<Room> rooms = ses.Query<Room>().ToList();
				foreach(Room room in rooms)
					roomList.AppendValues(room);
				if(rooms.Any())
					m_RoomView.SetRoom(rooms.FirstOrDefault().Id);
			}
			
			Widgets.SimpleTreeViewColumn nameColumn = new Widgets.SimpleTreeViewColumn("Nazwa", "Name");
			treeView.AppendColumn(nameColumn);
			treeView.CursorChanged += delegate(object o, EventArgs args) {
				Gtk.TreeIter iter;
				Gtk.TreeModel model;
				treeView.Selection.GetSelected(out model, out iter);
				object room = model.GetValue(iter, 0);
				if(room is Room)
					m_RoomView.SetRoom( ((Room)room).Id);
			};
						
			treeView.Model = roomList;
			//treeView.SetSizeRequest(250, -1);
			contentBox.PackFill(new Gtk.Separator(Gtk.Orientation.Vertical));
			contentBox.PackFill(treeView);
			Add (mainBox);
			ShowAll();
		}
		
		public override void Load(params object[] args)
		{
			if (args.Length > 0)
				m_RoomView.SetTableSetter((TableSetter)args[0]);
		}
		
		private RoomView m_RoomView;
	}
	
	/*public class ShowRoomListForm: Form
	{
		private class MenuBox: TitleBox
		{
			public MenuBox(ShowRoomListForm outher, Context context): base()
			{
				Gtk.VBox vbox = new Gtk.VBox();
				Widgets.Button button;
				
				m_Outher = outher;
				m_Context = context;
				if(m_Outher.m_TableSetter != null)
				{
					button = new Widgets.Button("Wybierz");
					button.Clicked += SelectClick;
					vbox.PackStartSimple(button);
				}
				
				vbox.SetSizeRequest(320, -1);
				
				button = new Widgets.Button("Wybierz");
				button.Clicked += SelectClick;
				vbox.PackStartSimple(button);
				
				button = new Widgets.Button("Pokaż zamówienia");
				button.Clicked += DoOrderClick;
				vbox.PackStartSimple(button);

				button = new Widgets.Button("Zarezerwuj na stolik");
				button.Clicked += DoReservationClick;
				vbox.PackStartSimple(button);
			
				m_Placement.Add(vbox);
				m_TitleBar.Text = "Opcje stolika";
				AddButton("Anuluj", GoBackClick);
				ShowAll();
			}
			
			private void SelectClick(object o, EventArgs args)
			{
				m_Outher.m_TableSetter(m_Outher.m_Checked);
				m_Context.ApplicationWindow.GoBack(o, args);
				Hide();
			}
			
			private void ShowOrdersClick(object o, EventArgs args)
			{
				
			}
			
			private void DoOrderClick(object o, EventArgs args)
			{
				
			}
			
			private void DoReservationClick(object o, EventArgs args)
			{
				
			}
			
			private void GoBackClick(object sender, EventArgs args)
			{
				Hide();
			}
			
			ShowRoomListForm m_Outher;
			Context m_Context;
		}
		
		
		public ShowRoomListForm (Context context): base(context)
		{
			System.Collections.IList criteriaList;
			
			using (ISession session = Context.OpenSession()) {
				ICriteria criteria = session.CreateCriteria(typeof(Room));
				criteriaList = criteria.List();
			}
			
			if(criteriaList.Count > 0)
				m_CurrentRoom = (Room)criteriaList[0];
			
			m_RoomList = new ListStore(typeof(Room));
			foreach(Room room in criteriaList)
				m_RoomList.AppendValues(room);
			
			m_TreeView = new Gtk.TreeView();
			Widgets.SimpleTreeViewColumn nameColumn = new Widgets.SimpleTreeViewColumn("Nazwa", "Name");
			m_TreeView.AppendColumn(nameColumn);
			m_TreeView.CursorChanged += ChangeRoomClick;
			
			m_TreeView.Model = m_RoomList;
			m_TreeView.SetSizeRequest(320, -1);
			
			m_DrawingArea = new DrawingArea();
			m_DrawingArea.ButtonPressEvent += CheckIfTableClicked;
			m_DrawingArea.AddEvents ((int) EventMask.ButtonPressMask    
                            |(int) EventMask.ButtonReleaseMask    
                            |(int) EventMask.KeyPressMask    
                            |(int) EventMask.PointerMotionMask);
			
			m_DrawingArea.SetSizeRequest(500, -1);
			
			m_DrawingArea.Drawn += Draw;
			Widgets.VBox vbox = new Widgets.VBox();
			Widgets.HBox hbox = new Widgets.HBox();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			
			buttons.AddLeftButton("<", GoBackClick);
			
			hbox.PackExpand(m_DrawingArea);
			hbox.PackFill(m_TreeView);
			
			vbox.PackExpand(hbox);
			vbox.PackFill(buttons);
			
			Add(vbox);
			ShowAll();
		}
		
		public override void Load(params object[] args)
		{
			if (args.Length > 0)
			{
				m_TableSetter = (TableSetter)args[0];
			}
		}
		
		private void ChangeRoomClick(object o, EventArgs args)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			m_TreeView.Selection.GetSelected(out model, out iter);
			m_CurrentRoom = (Room)model.GetValue(iter, 0);
			
			//Repaint();			
		}
		
		private void Draw(object o, DrawnArgs args)
		{
			//DrawingArea m_DrawingArea = (DrawingArea) o;	
			Repaint(args.Cr);
		}
		
		private void GoBackClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.GoBack(sender, args);	
		}
		
		private void DrawTable(uint id, uint x, uint y, uint width, uint height, TableType type)
		{
			m_Brush.Color = new Cairo.Color(0.67, 0.25, 0);
			m_Brush.SelectFontFace("Arial", Cairo.FontSlant.Normal, Cairo.FontWeight.Bold);
			m_Brush.SetFontSize(14);
			switch(type)
			{
			case TableType.Rect:
				m_Brush.Rectangle(x+10, y+10, width-20, height-20);
				m_Brush.Rectangle(x, y+10, 10, height-20);
				m_Brush.Rectangle(x+width-10, y+10, 10, height-20);
				m_Brush.Rectangle(x+10, y, width-20, 10);
				m_Brush.Rectangle(x+10, y+height-10, width-20, 10);
				m_Brush.Arc(x+10, y+10, 10, 0, Math.PI*2);
				m_Brush.Arc(x+width-10, y+10, 10, 0, Math.PI*2);
				m_Brush.Arc(x+10, y+height-10, 10, 0, Math.PI*2);
				m_Brush.Arc(x+width-10, y+height-10, 10, 0, Math.PI*2);
				m_Brush.Fill();
				
				m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);

				m_Brush.MoveTo(x+2, y+20);
				m_Brush.ShowText("Stolik nr." + id.ToString());
			break;
			case TableType.Round:
				m_Brush.Arc(x, y+width, width, 0, 2*Math.PI);
				m_Brush.Fill();
				m_Brush.MoveTo(x-width, y+width);
				m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
				m_Brush.ShowText("Stolik nr." + id.ToString());
			break; 
			}
			
			
		}
		
		private void DrawWall(uint x, uint y, uint width, uint height)
		{
			m_Brush.Color = new Cairo.Color(0.9, 0.9, 0.9);
			m_Brush.Rectangle(x, y, width, height);
			m_Brush.Fill();
			m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
			m_Brush.Rectangle(x, y, width, height);
			for(int i=0; i<=(width+height); i+=10)
			{
				m_Brush.MoveTo( (i<=width) ? (x+i) : (x+width),
						(i<=width) ?  y : (y + i - width) );
				m_Brush.LineTo( (i<=height) ? x : (x + i - height),
						(i<=height) ? (y+i) : (y+height));
			}
			m_Brush.Stroke();
		}
		
	        private void Repaint(Cairo.Context cr)
		{
			if(m_CurrentRoom == null)
				return;
			
			m_Brush = cr;
			
			m_Brush.Color = new Cairo.Color(1.0, 1.0, 1.0);
			m_Brush.Rectangle(2, 2,m_CurrentRoom.Width, m_CurrentRoom.Height);
			m_Brush.Fill();
			m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
			m_Brush.Rectangle(2, 2, m_CurrentRoom.Width, m_CurrentRoom.Height);
			m_Brush.Stroke();
			
			m_Brush.Color = new Cairo.Color(1.0, 1.0, 1.0);
			m_Brush.Rectangle(0, 0, m_CurrentRoom.Width, m_CurrentRoom.Height);
			m_Brush.Fill();
			m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
			m_Brush.Rectangle(0, 0, m_CurrentRoom.Width, m_CurrentRoom.Height);
			m_Brush.Stroke();
			
			using (ISession session = Context.OpenSession()) {
				session.Update(m_CurrentRoom);
				foreach(Domain.Table table in m_CurrentRoom.Tables)
				{
					if(table.Type == TableType.Rect)
					{
						DrawTable(table.Number, table.X, table.Y,
							table.Width, table.Height, table.Type);
					} else if(table.Type == TableType.Round)
					{
						DrawTable(table.Number, table.X, table.Y,
							table.Radius, table.Radius, table.Type);
					}
				}
			
				foreach(Wall wall in m_CurrentRoom.Walls)
				{
					DrawWall(wall.X>m_CurrentRoom.Width?
						m_CurrentRoom.Width : wall.X, 
						wall.Y>m_CurrentRoom.Height?
						m_CurrentRoom.Height: wall.Y, 
						wall.Width+wall.X>m_CurrentRoom.Width?
						m_CurrentRoom.Width-wall.X : wall.Width, 
						wall.Height+wall.Y>m_CurrentRoom.Height?
						m_CurrentRoom.Height-wall.Y:wall.Height);	
				}
			}
			m_DrawingArea.QueueDraw();
			((IDisposable)m_Brush.Target).Dispose();
			((IDisposable)m_Brush).Dispose();
		}
		
		[GLib.ConnectBefore]
		private void CheckIfTableClicked(object o, ButtonPressEventArgs evnt)
		{
			foreach(Domain.Table table in m_CurrentRoom.Tables)
			{
				if (CheckIfTableCollide(table, evnt.Event.X, evnt.Event.Y))
				{
					m_Checked = table;
					MenuBox menuBox = new MenuBox(this, Context);
					menuBox.ShowAll();
					return;
				}
			}
			
			m_Checked = null;
		}
		
		protected bool CheckIfTableCollide(Domain.Table table, double x, double y)
		{
			if(table.Type == TableType.Rect)
				if (x>=table.X && y>=table.Y && x<=table.X + 
					table.Width && y<=table.Y + table.Height) 
					return true;
				else 
					return false;
			if(table.Type == TableType.Round)
				if( Math.Sqrt((table.X - x)*(table.X - x) + (table.Y -y)*(table.Y - y)) < table.Radius )
					return true;
				else
					return false;
			return false;
		}
		Cairo.Context m_Brush;
		DrawingArea m_DrawingArea;
		Room m_CurrentRoom;
		Domain.Table m_Checked;
		Gtk.TreeView m_TreeView;
		Gtk.ListStore m_RoomList;
		TableSetter m_TableSetter;
	}*/
}