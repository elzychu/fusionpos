using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	//public delegate void OnEntryChanged (object sender, EventArgs e);
	public class InvoiceSettingsBox: Form
	{
		public InvoiceSettingsBox(Context context): base(context)
		{ }
		
		public override void Load(params object[] args)
		{
			Order order = args[0] as Order;
			if(order.Invoice != null)
			{
				Context.ApplicationWindow.LoadForm(typeof(OrderInvoiceForm), order.Invoice);
				Hide();
			}
			
			//m_TitleBar.Text = "Faktura VAT";
			VBox mainConatiner = new Widgets.VBox();
			Widgets.ButtonsBar buttonsBar = new ButtonsBar();
			
			Widgets.VBox vbox = new Widgets.VBox();
			Widgets.FormTable form = new Widgets.FormTable(3, 200);
			Gtk.RadioButton first;
			
			m_InvoiceName = new Widgets.TextBox();
			m_InvoiceAddress = new Widgets.TextView();
			m_InvoiceNIP = new Widgets.TextBox();
			m_BankName = new Widgets.TextBox();
			m_BankAccount = new Widgets.TextBox();
			
			this.BorderWidth = 10;
			form.BorderWidth = 10;
			form.RowSpacing = 10;
			form.PackElement(new Widgets.Label("Nazwa:"), m_InvoiceName, 0, false);
			form.PackElement(new Widgets.Label("Adres:"), m_InvoiceAddress, 1, false);
			form.PackElement(new Widgets.Label("NIP:"), m_InvoiceNIP, 2, false);
			form.PackElement(new Widgets.Label("Numer konta bankowego:"), m_BankAccount, 3, false);
			form.PackElement(new Widgets.Label("Nazwa banku:"), m_BankName, 4, false);
			
			vbox.PackFill(new WindowLabel("Dane kontrahenta:", "Inverted"));
			vbox.PackFill(form);
			vbox.PackFill(new WindowLabel("Rodzaj faktury:", "Inverted"));
			vbox.PackFill(first = new Gtk.RadioButton(null, "Treść zamówienia"));
			vbox.PackFill(new Gtk.RadioButton(first, "Usługa gastronomiczna"));
			vbox.PackFill(new Gtk.RadioButton(first, "Usługa cateringowa"));
			
			mainConatiner.PackStart(vbox, true, true, 0);
			mainConatiner.PackStart(buttonsBar, false, false, 15);
			
			if(order.Client != null)
			{
				m_InvoiceName.Text = order.Client.Name;
				m_InvoiceNIP.Text = order.Client.NIP;
				m_InvoiceAddress.Text = order.Client.Address;
			}
			
			
			buttonsBar.AddLeftButton("Wystaw", delegate(object sender, EventArgs e) {
				Hide();
				OutgoingInvoice invoice = new OutgoingInvoice();
				invoice.BankAccount = m_BankAccount.Text;
				invoice.BankName = m_BankName.Text;
				invoice.BuyerAddress = m_InvoiceAddress.Text;
				invoice.BuyerName = m_InvoiceName.Text;
				invoice.BuyerNip = m_InvoiceNIP.Text;
				invoice.IssueTime = DateTime.Now;
				invoice.SellTime = order.Time;
				if(order.Payments.Count > 0)
					invoice.PaymentTime = order.Time;
				invoice.Number = "0";
				invoice.Components = new List<OutgoingInvoiceComponent>();
				int id=0;
				foreach(var entity in order.Entries)
				{
					++id;
					OutgoingInvoiceComponent oic = new OutgoingInvoiceComponent();
					oic.Amount = entity.Amount;
					oic.IdOnInvoice = id;
					oic.Name = entity.Dish.Name;
					oic.NetUnitPrice = entity.ValueForPieceNet;
					oic.Pkwiu = "";
					oic.UnitOfMeasure = UnitOfMeasure.PIECE;
					oic.VatPercent = entity.TaxValue;
					invoice.Components.Add(oic);
				}
			
				order.Invoice = invoice;
				Context.ApplicationWindow.GoBack(sender, e);
				Context.ApplicationWindow.LoadForm(typeof(OrderInvoiceForm), order);
			});
			
			buttonsBar.AddLeftButton("Anuluj", delegate(object sender, EventArgs e) {
				Context.ApplicationWindow.GoBack(sender, e);
			});
			
			Add(mainConatiner);
			ShowAll();		
		}
		
		private Widgets.TextBox m_InvoiceName;
		private Widgets.TextView m_InvoiceAddress;
		private Widgets.TextBox m_InvoiceNIP;
		private Widgets.TextBox m_BankAccount;
		private Widgets.TextBox m_BankName;
	}
	
	public class PaymentMethodField: Widgets.Table
	{
		public class PaymentRow
		{
			public PaymentRow(String name, PaymentMethod method)
			{
				Button = new Widgets.ToggleButton(name);
				Entry = new Widgets.FloatEntry(2);
				PaymentMethod = method;
			}

			public Widgets.ToggleButton Button { get; set; }
			public Widgets.FloatEntry Entry { get; set; }
			public PaymentMethod PaymentMethod { get; set; }
		}
		
		public PaymentMethodField(uint rows, Context ctx): base(rows+3, 2, false)
		{
			m_Ctx = ctx;
			m_SinglePayment = true;
			m_PaymentForms = new Dictionary<PaymentMethod, PaymentRow>();
			PaymentRow row = new PaymentRow("Gotówka", PaymentMethod.Cash);
			m_Dominating = row;
			
			row.Entry.Changed += delegate(object sender, EventArgs e) {
				decimal sum = 0;
				decimal remainder;
				
				foreach(PaymentRow r in m_PaymentForms.Values)
				{
					if(r.Entry.Sensitive)
						sum += decimal.Parse(r.Entry.Text);
				}
				
				remainder = sum - m_Charge;
				m_Sum.Text = remainder.ToString(".##");
				
				if (remainder < 0)
					m_Sum.StyleContext.AddClass("Red");
			};
			
			row.Button.Active = true;
			row.Button.Toggled += PaymentToggled;
			m_PaymentForms.Add(PaymentMethod.Cash, row);
		}
		
		public void SetCharge(decimal charge)
		{
			m_Charge = charge;
			m_PaymentForms.Values.First().Entry.Text = charge.ToString();
		}
		
		public void AddButton(string name, PaymentMethod method)
		{
			PaymentRow row = new PaymentRow(name, method);
			row.Entry.Sensitive = false;
			
			row.Entry.Changed += delegate(object sender, EventArgs e) {
				decimal sum=0;
				foreach(PaymentRow r in m_PaymentForms.Values)
				{
					if(r.Entry.Sensitive)
						sum += decimal.Parse(r.Entry.Text);
				}
				
				m_Sum.Text = (sum - m_Charge).ToString(".##");
			};
			
			row.Button.Toggled += PaymentToggled;
			m_PaymentForms.Add(method, row);
		}
		
		public IDictionary<PaymentMethod, decimal> Values
		{
			get {
				return m_PaymentForms
				    .Where(v => v.Value.Entry.Sensitive)
				    .ToDictionary(k => k.Key, v => v.Value.Entry.GetDecimalValue());
			}
		}
		
		public void Prepare()
		{
			uint x = 0;
			
			Widgets.HBox modeBox = new Widgets.HBox(true, 1);
			Gtk.RadioButton singlePayment = new Gtk.RadioButton(null, "jedna forma płatności");
			Gtk.RadioButton multiplePayment = new Gtk.RadioButton(singlePayment, "wiele form płatności");
			singlePayment.DrawIndicator = false;
			multiplePayment.DrawIndicator = false;
			singlePayment.Toggled += (sender, e) => m_SinglePayment = true;	
			multiplePayment.Toggled += (sender, e) => m_SinglePayment = false;
			modeBox.PackFill(singlePayment);
			modeBox.PackFill(multiplePayment);
			
			Attach(modeBox, 0, 2, 0, 1);
			
			foreach(PaymentRow row in m_PaymentForms.Values)
			{
				Attach(row.Button, 0, 1, x+1, x+2);
				Attach(row.Entry, 1, 2, x+1, x+2);
				x++;
			}
			
			m_Sum = new Widgets.WindowLabel("0,00");
			Attach(new Widgets.WindowLabel("Reszta"), 0, 1, x+1, x+2);
			Attach(m_Sum, 1, 2, x+1, x+2);
			using(ISession ses = m_Ctx.OpenSession())
			{
				IEnumerable<Currency> currency = ses.Query<Currency>();
				List<String> rows = new List<String>();
				int main=0;
				foreach(Currency cur in currency)
				{
					if(cur.MainCurrency)
						main = rows.Count;
					rows.Add(cur.Name);
				}
				m_CurrencyBox = new Widgets.ComboBox(rows.ToArray());
				m_CurrencyBox.Active = main;
			}
			
			Attach(m_CurrencyBox, 0, 2, x+2, x+3);
		}
		
		private void PaymentToggled(object sender, EventArgs args)
		{
			if (m_SinglePayment) {
				if (!(sender as Widgets.ToggleButton).Active)
					return;
				
				m_PaymentForms.Values.Where(v => v.Button != sender).Each(v => {
					v.Button.Active = false;
					v.Button.Sensitive = true;
					v.Entry.Sensitive = false;
					v.Entry.Text = string.Empty;
				});
				
				PaymentRow activeRow = m_PaymentForms.Values.Where(v => v.Button == sender).First();
				activeRow.Button.Sensitive = false;
				activeRow.Entry.Sensitive = true;
				activeRow.Entry.Text = m_Charge.ToString();
			} else {
				if (!(sender as Widgets.ToggleButton).Active) {
					if (m_PaymentForms.Values.Count(v => v.Button.Active) == 0)
						(sender as Widgets.ToggleButton).Active = true;
				}
				
				m_PaymentForms.Values.Each(v => {
					v.Button.Sensitive = true;
					v.Entry.Sensitive = v.Button.Active;
				});
			}	
		}
		
		private bool CheckIfAnyFiledToggled()
		{
			bool flag=false;
			foreach(PaymentRow row in m_PaymentForms.Values)
				if(row.Button.Active)
					flag=true;
			
			return flag;
		}
		
		private void IfMoreThanOneActiveUnblock()
		{
			int count=0;
			foreach(PaymentRow row in m_PaymentForms.Values)
				if(row.Button.Active)
					count++;
			if(count>1)
			{
				foreach(PaymentRow row in m_PaymentForms.Values)
					row.Button.Sensitive = true;
			}
		}
		
		private IDictionary<PaymentMethod, PaymentRow> m_PaymentForms;
		private bool m_SinglePayment;
		private PaymentRow m_Dominating;
		private decimal m_Charge;
		private Widgets.WindowLabel m_Sum;
		private Context m_Ctx;
		private Widgets.ComboBox m_CurrencyBox;
	}
	
	public class OrderPaymentForm: Form
	{
		public OrderPaymentForm(Context context): base(context)
		{
			m_Context = context;
			Widgets.VBox mainBox = new Widgets.VBox();
			Widgets.HBox contentBox = new Widgets.HBox();
			m_SummaryBox = new Widgets.VBox();
			Widgets.HBox menuBox = new Widgets.HBox();
			
			m_SetValueButton = Widgets.ImageButtonFactory.ImageToggleButton("Wartość ustalona", "money_value");
			m_SetValueButton.Toggled += delegate(object sender, EventArgs e) {
				if (m_SetValueButton.Active) {
					m_RebateButton.Sensitive = false;
					m_Order.RebateAmount = m_Order.ValueGross - ValuePicker.GetFloatValue("Wartość zamówienia:");
					m_Payments.SetCharge(m_Order.ValueGross - (m_Order.ValueGross * (m_Order.RebatePercent / 100)) - m_Order.RebateAmount);
				} else {
					m_RebateButton.Sensitive = true;
					m_Order.RebateAmount = 0;
					m_Payments.SetCharge(m_Order.FinalValue);
				}
				
				DynamicLabel.RefreshAll(this);
			};
			
			m_RebateButton = Widgets.ImageButtonFactory.ImageToggleButton("Rabat", "percent");
			m_RebateButton.Toggled += delegate(object sender, EventArgs e) {
				if (m_RebateButton.Active) {
					m_SetValueButton.Sensitive = false;
					m_Order.RebatePercent = ValuePicker.GetFloatValue("Rabat procentowy:");
				} else {
					m_SetValueButton.Sensitive = true;
					m_Order.RebatePercent = 0;
					m_Payments.SetCharge(m_Order.FinalValue);
				}
				
				DynamicLabel.RefreshAll(this);
			};
			
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			buttons.AddLeftImageButton("Powrót", "arrow_left", (s, args) => { Context.ApplicationWindow.GoHome(s, args); });
			buttons.AddLeftImageButton("Zapłać", "money", SubmitOrder);
			buttons.AddRightButton(m_SetValueButton);
			buttons.AddRightButton(m_RebateButton);
		/*	buttons.AddRightButton("Wartość ustalona", delegate(object sender, EventArgs e) {
				m_Order.RebateAmount = m_Order.FinalValueGross - ValuePicker.GetFloatValue("Wartość zamówienia:");
				m_Payments.SetCharge(m_Order.FinalValueGross - (m_Order.FinalValueGross * (m_Order.RebatePercent / 100)) - m_Order.RebateAmount);
			});
			buttons.AddRightButton("Rabat", delegate(object sender, EventArgs e) {
				m_Order.RebatePercent = ValuePicker.GetFloatValue("Rabat procentowy:");
				m_Payments.SetCharge(m_Order.FinalValueGross - (m_Order.FinalValueGross * (m_Order.RebatePercent / 100)));
			});*/
			buttons.AddRightImageToggle("Faktura", "invoice", delegate(object sender, EventArgs e) {
				Context.ApplicationWindow.LoadForm(typeof(InvoiceSettingsBox), m_Order);
			});
			buttons.AddRightImageToggle("Strata", "lost", Context.ApplicationWindow.GoBack);
			m_Payments = new PaymentMethodField(5, context);
			m_Payments.AddButton("Karta", PaymentMethod.Card);
			m_Payments.AddButton("Bon", PaymentMethod.Voucher);
			m_Payments.AddButton("Kredyt", PaymentMethod.Credit);
			m_Payments.AddButton("Punkty", PaymentMethod.Points);
			m_Payments.Prepare();
			m_Payments.SetSizeRequest(360, -1);
			
			Gtk.Separator separator = new Gtk.Separator(Gtk.Orientation.Vertical);
			
			menuBox.PackFill(buttons);
			
			Widgets.FormTable client = new Widgets.FormTable(1);
			client.PackElement(new Widgets.Label("Stały klient:"), new DynamicLabel(
			    "MonospaceFont", 
			    () => m_Order.Client != null ? m_Order.Client.Name : "----"), 0, false);

			Widgets.FormTable delivery = new Widgets.FormTable(1);
			delivery.PackElement(new Widgets.Label("Dostawa:"), new DynamicLabel(
			    "MonospaceFont", delegate() {
			        if(m_Order.Destination is DestinationOnSite)
					return String.Format("Stolik nr. {0}", (m_Order.Destination as 
					    DestinationOnSite).Table.Number);
				if(m_Order.Destination is DestinationDelivery)
					return String.Format("Dostawa na adres {0}",
				            (m_Order.Destination as DestinationDelivery).Address);
				return "----";
			     }), 0, false);
			
			Widgets.FormTable prices  = new Widgets.FormTable(5);
			prices.PackElement(new Widgets.Label("Rabat procentowy:"), new DynamicLabel(
			    "MonospaceFont",
			    () => m_Order.RebatePercent > 0 ? m_Order.RebatePercent.ToString("##\\%") : "----"), 0, false);
			
			prices.PackElement(new Widgets.Label("Wartość ustalona:"), new DynamicLabel(
			    "MonospaceFont",
			    () => m_Order.TotalValue != 0 ? m_Order.TotalValue.ToString() : "----"), 1, false);
	
			prices.PackElement(new Widgets.Label("Faktura VAT:"), new DynamicLabel(
			    "MonospaceFont",
			    () => m_Order.Invoice != null ? m_Order.Invoice.Number : "----"), 2, false);
	
			DynamicLabel total = new DynamicLabel("Do zapłaty", () => m_Order.FinalValue.ToString("Total: <tt>#.##PLN</tt>"));
			Gtk.Alignment totalAlign = new Gtk.Alignment(1, 0, 0, 1);
			totalAlign.Add(total);
			
			m_SummaryBox.PackFill(new WindowLabel("Klient:", "Inverted"));
			m_SummaryBox.PackFill(client);
			m_SummaryBox.PackFill(new WindowLabel("Dostawa:", "Inverted"));
			m_SummaryBox.PackFill(delivery);
			m_SummaryBox.PackFill(new WindowLabel("Podsumowanie:", "Inverted"));
			m_SummaryBox.PackFill(prices);
			m_SummaryBox.PackExpand(new Gtk.EventBox());
			m_SummaryBox.PackFill(totalAlign);
			
			contentBox.PackFill(m_Payments);
			contentBox.PackFill(separator);
			contentBox.PackExpand(m_SummaryBox);
			mainBox.PackExpand(contentBox);
			mainBox.PackFill(menuBox);
			Add(mainBox);
			ShowAll();
		}
		
		public override void Load(params object[] args)
		{
			m_Order = args[0] as Order;
			m_Payments.SetCharge(m_Order.FinalValue);
			
			if(m_Order.Payments != null)
				foreach(OrderPayment payment in m_Order.Payments)
					m_Payments.Values.AddOverwrite(payment.Method, payment.Amount);
			
			DynamicLabel.RefreshAll(this);
			ShowAll();
		}
		
		private void SubmitOrder(object s, EventArgs args)
		{
			m_Order.Payments = new List<OrderPayment>();
			m_Payments.Values.Where(i => i.Value != 0).Each(i => m_Order.Payments.Add(new OrderPayment() 
			    { Method = i.Key, Amount = i.Value }));
			
			decimal sum = 0m;
			m_Order.Payments.Each(p => sum += p.Amount);
			if(m_Order.TotalValue > 0 ? sum >= m_Order.TotalValue : sum >= m_Order.FinalValue)
			{
				Context.FinishOrder(m_Order);
				new AddPointsDialog(Context, m_Order, false, 0);
				Context.ApplicationWindow.GoHome(s, args);
			}
			else
			{
				MessageBox.Popup("FusionPOS", "Płatność nie została dokonana");	
			}
		}
		
		private Order m_Order;
		private Context m_Context;
		private PaymentMethodField m_Payments;
		private Widgets.VBox m_SummaryBox;
		private Gtk.ToggleButton m_RebateButton;
		private Gtk.ToggleButton m_SetValueButton;
		
	}
}

