using System;
using Gtk;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class PointsEditForm: Form
	{
		public PointsEditForm(Context context): base(context)
		{
			Widgets.VBox vbox = new Widgets.VBox();
			Gtk.Table table = new Gtk.Table(3, 2, false);
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			
			m_CardNumber = new Widgets.TextBox();
			m_CardValue = new Widgets.TextBox();
			
			buttons.AddLeftButton("<", Context.ApplicationWindow.GoBack);
			
			table.Attach(new Widgets.Label("Numer karty:"), 0, 1, 0, 1);
			table.Attach(m_CardNumber, 1, 2, 0, 1);
			table.Attach(new Widgets.Label("Ilość punktów"), 0, 1, 1, 2);
			table.Attach(new Widgets.Label("2137"), 1, 2, 1, 2);
			table.Attach(new Widgets.Label("Doładuj:"), 0, 1, 2, 3);
			table.Attach(m_CardValue, 1, 2, 2, 3);
			
			vbox.PackStart(table, true, true, 0);
			vbox.PackStart(buttons, false, false, 0);
			Add(vbox);
			ShowAll();
		}
		
		private Widgets.TextBox m_CardNumber;
		private Widgets.TextBox m_CardValue;
	}
}

