using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FusionPOS.Widgets;
using FusionPOS.Domain;
using NHibernate;
using NHibernate.Linq;
namespace FusionPOS
{
	public delegate void IngredientPickerFunc(long id);
	public class IngredientPicker: TitleBox
	{
		public class GroupButton: Button
		{
			public GroupButton(IngredientGroup group_): base(group_.Name, "Category", true)
			{
				Group = group_;
			}
			
			public IngredientGroup Group;
		}
		
		public class IngredientButton: Button
		{
			public IngredientButton(RecipeIngredient ingredient): 
				base(ingredient.Component.Name, "Dish", true)
			{
				Ingredient = ingredient;
			}
			
			public RecipeIngredient Ingredient;
		}
		
		public IngredientPicker(IngredientPickerFunc picker, Dish dish, Context context)
		{
			if(dish==null)
			{
				Hide ();
				return;
			}
			
			VBox mainBox = new VBox();
			
			m_Picker = picker;
			m_Ctx = context;
			m_Table = new BrickTable(8, 8);
			m_Table.StyleContext.AddClass("MenuTable");
			SetSizeRequest(800, 600);
			Gtk.Button button = Widgets.ImageButtonFactory.ImageButton("Anuluj", "cancel");
			button.Clicked += delegate(object sender, EventArgs e) {
				Hide();
			};
			
			//using(ISession ses = m_Ctx.OpenSession())
			//{
			//	ses.Lock(dish, LockMode.None);
				List<RecipeIngredient> ingredients = 
					dish.Recipe.Ingredients.Where (p => p.Optional && !(p.Component as IDeletableDomainObject).Deleted).ToList();
			
				SetIngredients(ingredients);
			//}
			mainBox.PackFill(new WindowLabel("Wybierz składnik", "Inverted"));
			mainBox.PackExpand(m_Table);
			mainBox.PackFill(button);
			m_Placement.Add(mainBox);
			ShowAll();
		}
		
		private void SetIngredients(List<RecipeIngredient> ingredients)
		{
			m_Table.ClearBricks();
			
			foreach(RecipeIngredient ingredient in ingredients)
			{
				if( (ingredient.Component as IDeletableDomainObject).Deleted)
					continue;
				if(ingredient.Component is Ingredient)
				{
					IngredientButton button = 
						new IngredientButton(ingredient);
					button.Clicked  += delegate(object sender, EventArgs e) {
						m_Picker((sender as IngredientButton).Ingredient.Id);
						Hide();
					};
					m_Table.AddBrick(button);
				}
			}
			
			ShowAll();
		}
		
		private Context m_Ctx;
		private IngredientPickerFunc m_Picker;
		private BrickTable m_Table;
	}
	
	public delegate void DishPickerFunc(long id);
	public class DishPicker: TitleBox 
	{
		public class CategoryButton: Button
		{
			public CategoryButton(DishCategory category): base(category.Name, "Category", true)
			{
				Category = category;
			}
			
			public DishCategory Category;
		}
		
		public class DishButton: Button
		{
			public DishButton(Dish dish): base(dish.Name, "Dish", true)
			{
				Dish = dish;
			}
			
			public Dish Dish;
		}
		
		public DishPicker(DishPickerFunc picker, Context context)
		{
			VBox mainBox = new VBox();
			
			m_Picker = picker;
			m_Ctx = context;
			m_Table = new BrickTable(8, 8);
			m_Table.StyleContext.AddClass("MenuTable");
			SetSizeRequest(800, 600);
			Gtk.Button button = Widgets.ImageButtonFactory.ImageButton("Anuluj", "cancel");
			button.Clicked += delegate(object sender, EventArgs e) {
				Hide();
			};
			
			SetRoot(DishCategory.RootCategory);
			
			mainBox.PackFill(new WindowLabel("Wybierz pizze", "Inverted"));
			mainBox.PackExpand(m_Table);
			mainBox.PackFill(button);
			m_Placement.Add(mainBox);
			ShowAll();
		}
		
		private void SetRoot(long root)
		{
			m_Table.ClearBricks();
			
			using (ISession ses = m_Ctx.OpenSession())
			{
				DishCategory category = ses.Get<DishCategory>(root);
				if(category.Parent != null)
				{
					CategoryButton button = new CategoryButton(category.Parent);
					button.Clicked += delegate(object sender, EventArgs e) {
						SetRoot(category.Parent.Id);
					};
					
					m_Table.AddBrick(button);	
				}
				foreach(DishCategory childCat in category.Children)
				{
					if(childCat.Deleted)
						continue;
					CategoryButton button = new CategoryButton(childCat);
					button.Clicked += delegate(object sender, EventArgs e) {
						SetRoot(button.Category.Id);
					};
					m_Table.AddBrick(button);	
				}
				foreach(Dish childDish in category.Dishes)
				{
					if(!childDish.IsPizza || childDish.Deleted)
						continue;
					DishButton button = new DishButton(childDish);
					button.Clicked += delegate(object sender, EventArgs e) {
						m_Picker(button.Dish.Id);
						Hide();
					};
					m_Table.AddBrick(button);
				}
			}
			
			ShowAll();
		}
		
		private Context m_Ctx;
		private DishPickerFunc m_Picker;
		private BrickTable m_Table;
	}
	
	public class PizzaChart: Gtk.DrawingArea
	{
		public PizzaChart(int parts): base()
		{
			SetSizeRequest(300, 300);
			Drawn += Render;
			m_Parts=parts;
		}
		
		public void SetParts(decimal parts)
		{
			m_Parts = parts;
			QueueDraw();
		}
	
		public void Render(object o, Gtk.DrawnArgs args)
		{
			Cairo.Context cr = args.Cr;
			
			double step = (2*Math.PI) / (double)m_Parts;
			double a = 150;
			double b = 150;
			double radius = 100;
			cr.LineWidth = 8.0;
			cr.Rectangle(6.0, 6.0, 288.0, 288.0);
			cr.SetSourceRGB(0.98, 0.9, 0.8);
			cr.FillPreserve();
			cr.SetSourceRGB(0.0, 0.0, 0.0);
			cr.Stroke();
			cr.LineWidth = 12.0;
			for(double i=(2*Math.PI); i>0; i-=step)
			{
				cr.SetSourceRGB(i/(2*Math.PI), 
					(new Random()).NextDouble(), (new Random()).NextDouble());
				
				if(m_Parts==1)
				{
					cr.Arc(a, b, radius, 0, Math.PI * 2);
				}
				else 
				{
					double x = a + 15*Math.Cos(i+Math.PI/(double)m_Parts);
					double y = b + 15*Math.Sin(i+Math.PI/(double)m_Parts);
					
					cr.NewPath();
					cr.MoveTo(x, y);
					cr.LineTo(x + radius*Math.Cos (i), y + radius*Math.Sin(i));
					cr.Arc(x, y, radius, i, i+step);
					cr.LineTo(x, y);
					cr.ClosePath();
				}
				
				cr.FillPreserve();
				cr.SetSourceRGB(0.0, 0.0, 0.0);
				cr.Stroke();
			}
			(cr as IDisposable).Dispose();
		}
		
		private decimal m_Parts;
	}
	
	public class PizzaColumn: HBox
	{
		public class PizzaTreeView: Gtk.TreeView
		{
			public PizzaTreeView()
			{
				m_StoreView = new Gtk.ListStore(typeof(string), 
				                                typeof(string),
				                                typeof(object),
				                                typeof(Gdk.Color));
				
				Gtk.TreeViewColumn nameCol = new Gtk.TreeViewColumn();
				nameCol.Title = "Nazwa";
				
				Gtk.TreeViewColumn amountCol = new Gtk.TreeViewColumn();
				amountCol.Title = "Ilość";
								
				Gtk.CellRendererText crtb = new Gtk.CellRendererText();
				amountCol.PackStart(crtb, true);
				amountCol.AddAttribute(crtb, "text", 0);
				
				amountCol.SetCellDataFunc(crtb, delegate(Gtk.TreeViewColumn tree_column, 
				                                        Gtk.CellRenderer cell, 
				                                        Gtk.TreeModel model, 
				                                        Gtk.TreeIter iter) {
	        		        Gdk.Color color = (Gdk.Color) model.GetValue (iter, 3);
	    	    	                (cell as Gtk.CellRendererText).Text = (string)Model.GetValue(iter, 1);
					(cell as Gtk.CellRendererText).ForegroundGdk = color;					
				});
				
				Gtk.CellRendererText crta = new Gtk.CellRendererText();
				nameCol.PackStart(crta, true);
				nameCol.AddAttribute(crta, "text", 0);
				
				nameCol.SetCellDataFunc(crta, delegate (Gtk.TreeViewColumn column, 
				                                     Gtk.CellRenderer cell, 
				                                     Gtk.TreeModel model,
				                                     Gtk.TreeIter iter)
				{
	        		        Gdk.Color color = (Gdk.Color) model.GetValue (iter, 3);
	    	    	                (cell as Gtk.CellRendererText).Text = (string)Model.GetValue(iter, 0);
					(cell as Gtk.CellRendererText).ForegroundGdk = color;
				});
				
				AppendColumn(nameCol);
				AppendColumn(amountCol);
				Model = m_StoreView;
			}
			
			public OrderDish GetDish()
			{
				return m_Dish;
			}
			
			public void SetOrderDish(OrderDish dish)
			{
				m_Dish = dish;
				Refresh ();
			}
			
			public void SetPizza(Dish dish)
			{
				if(!dish.IsPizza)
					return;
				
				m_Dish = new OrderDish();
				m_Dish.Dish = dish;
				m_Dish.Amount = m_Amount;
				m_Dish.Modifications = new List<OrderDishModification>();
				
				Refresh();
			}
			
			public void IncAmount(int i)
			{
				if(m_Dish == null)
					return;
				
				i = (i>0) ? 1 : -1;
				object mod = GetCheckedValue();
				
				if(mod==null)
					return;
				
				OrderDishModification modification;
				
				if(mod is RecipeIngredient)
				{
					RecipeIngredient ingredient = mod as RecipeIngredient;
					modification =
						m_Dish.Modifications.FirstOrDefault(p =>
						p.Ingredient.Equals(ingredient));
					
					if(modification == null)
					{
						modification = new OrderDishModification();
						modification.Ingredient = ingredient;
						modification.Amount = i;
						m_Dish.Modifications.Add(modification);
						Refresh();
						return;
					}
					
					if(modification.Amount+i >= -4)
						modification.Amount += i;
					if(modification.Amount == 4)
						m_Dish.Modifications.Remove(modification);
				}
				
				if(mod is OrderDishModification)
				{
					modification = mod as OrderDishModification;
					if(modification.Amount+i > 0)
						modification.Amount += i;
					else
						m_Dish.Modifications.Remove(modification);
				}
				
				Refresh();
			}
			
			public void AddIngredient(RecipeIngredient ingredient)
			{
				OrderDishModification mod = new OrderDishModification();
				mod.Amount = 4;
				mod.Ingredient = ingredient;
				mod.PriceChange = 0;
				m_Dish.Modifications.Add(mod);
				Refresh();
			}
			
			public void SetAmount(decimal amount)
			{
				m_Amount = amount;
				if(m_Dish != null)
				{
					m_Dish.Amount = amount;
					Refresh ();
				}
			}
			
			private object GetCheckedValue()
			{
				Gtk.TreeIter iter;
				Gtk.TreeModel model;
				Selection.GetSelected(out model, out iter);
				if(model == null)
					return null;
				
				return model.GetValue(iter, 2);
			}
			
			private void Refresh()
			{
				m_StoreView.Clear();
				Gdk.Color color;
	
				foreach(RecipeIngredient ingredient in m_Dish.Dish.Recipe.Ingredients)
				{
					OrderDishModification mod = m_Dish.Modifications.FirstOrDefault(p => 
						p.Ingredient.Equals(ingredient));
					float amount = 1;
					
					m_Colors.TryGetValue("NativeNormal", out color);
					
					if(mod != null)
					{
						amount += ((float)mod.Amount)/4;
						if(amount == 0)
							m_Colors.TryGetValue("NativeNone", out color);
						
						if(amount < 1 && amount > 0)
							m_Colors.TryGetValue("NativeLess", out color);
						
						if(amount > 1)
							m_Colors.TryGetValue("NativeMore", out color);
					}

					m_StoreView.AppendValues(ingredient.Component.Name, 
						amount.ToString(), ingredient, color);
				}
				
				m_Colors.TryGetValue("Additional", out color);
				
				foreach(OrderDishModification mod in m_Dish.Modifications)
				{
					if(m_Dish.Dish.Recipe.Ingredients.Any(p 
						=> p.Component.Equals(mod.Ingredient.Component)))
					   continue;
					   
					m_StoreView.AppendValues(mod.Ingredient.Component.Name,
					                         ((float)(mod.Amount)/4).ToString(), mod, color);
				}
				
				Gtk.Widget widget = this;
				while( !(widget is Form) && widget.Parent != null)
					widget = widget.Parent;
				
				DynamicLabel.RefreshAll(widget as Gtk.Container);
			}
			
			private readonly IDictionary<string, Gdk.Color> m_Colors = new Dictionary<string, Gdk.Color>()
				{{"NativeNone", new Gdk.Color(255, 0, 0)}, {"NativeNormal", new Gdk.Color(0, 0, 255)}, 
				{"NativeLess", new Gdk.Color(180, 30, 30)}, {"NativeMore", new Gdk.Color(0, 255, 0)}, 
				{"Additional", new Gdk.Color(100, 0, 100)} };
			
			private OrderDish m_Dish;
			private Gtk.ListStore m_StoreView;
			private decimal m_Amount;
		}
		
		public PizzaColumn(Context ctx): base(false, 1)
		{
			WindowLabel label = new WindowLabel("Część pizzy", "Inverted");
			Label price = new Label("Cena: ");
			DynamicLabel priceValue = new DynamicLabel("0,00", delegate {
				if(m_Tree.GetDish() != null)
					return m_Tree.GetDish().ValueNet.ToString("F2");
				else
					return "0,00";
			});
			
			m_Tree = new PizzaTreeView();
			
			Button choosePizza = new Button("Wybierz pizze");
			choosePizza.Clicked += delegate(object sender, EventArgs e) {
				new DishPicker(delegate(long id) {
					using(ISession ses = ctx.OpenSession())
					{
						m_Tree.SetPizza(ses.Get<Dish>(id));
					}
				}, ctx);
			};
			
			
			Button addIngredient = new Button("Dodaj składnik");
			addIngredient.Clicked += delegate(object sender, EventArgs e) {
				if(m_Tree.GetDish() == null)
					return;
				new IngredientPicker(delegate(long id) {
					if(m_Tree.GetDish() == null)
						return;
					
					using(ISession ses = ctx.OpenSession())
					{
						m_Tree.AddIngredient(ses.Get<RecipeIngredient>(id));
					}
				}, m_Tree.GetDish().Dish, ctx);
			};
			
			Button plusButton = new Button("+");
			plusButton.Clicked += delegate(object sender, EventArgs e) {
				m_Tree.IncAmount(1);
			};
			
			Button minusButton = new Button("-");
			minusButton.Clicked += delegate(object sender, EventArgs e) {
				m_Tree.IncAmount(-1);
			};
			
			HBox editBox = new HBox();
			editBox.PackExpand(plusButton);
			editBox.PackExpand(minusButton);
			
			VBox main = new VBox();
			
			main.PackFill(label);
			main.PackFill(choosePizza);
			main.PackFill(addIngredient);
			main.PackFill(editBox);
			Gtk.ScrolledWindow scrwnd = new Gtk.ScrolledWindow();
			scrwnd.Add(m_Tree);
			main.PackExpand(scrwnd);
			main.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			HBox priceBox = new HBox();
			priceBox.PackFill(price);
			priceBox.PackFill(priceValue);
			main.PackFill(priceBox);
			
			PackFill(new Gtk.Separator(Gtk.Orientation.Vertical));
			PackExpand(main);
		}
		
		public OrderDish GetOrderDish()
		{
			return m_Tree.GetDish();		
		}
		
		public void SetOrderDish(OrderDish dish)
		{
			m_Tree.SetOrderDish(dish);	
		}
		
		public void SetDish(Dish dish)
		{
			m_Tree.SetPizza(dish);	
		}
		
		public void SetAmount(decimal amount)
		{
			m_Tree.SetAmount(amount);
		}
		
		private PizzaTreeView m_Tree;
	}
	
	public delegate void AmountSetter(decimal i);
	public class PartsCountPicker: VBox
	{
		public class PickButton: Gtk.ToggleButton
		{
			public PickButton(int i): base((i+1).ToString())
			{
				Amount = i;
			}
			public int Amount;
		}
		
		public void SetParts(int amount)
		{	
			m_Buttons[amount - 1].Active = true;
			
			for(int i=0; i<4; ++i)
			{
				if(i != amount -1)
				{
					m_Buttons[i].Active = false;
					m_Buttons[i].Sensitive = true;
				}
			}
			m_Setter(amount);
		}
		
		public PartsCountPicker(AmountSetter setter): base()
		{
			WindowLabel howManyPartsLabel = new WindowLabel("Ile części", "Inverted");
			m_Buttons = new PickButton[4];
			m_Setter = setter;
			
			for(int i=0; i<4; ++i)
			{
				m_Buttons[i] = new PickButton(i);
				m_Buttons[i].Clicked += delegate(object sender, EventArgs e) {
					PickButton button = sender as PickButton;
					
					foreach(PickButton currButton in m_Buttons)
					{
						if(!button.Equals(currButton))
						{
							currButton.Active = false;
							currButton.Sensitive = true;
						}
					}
					
					
					button.Sensitive = false;
					setter(button.Amount + 1);
				};
			}
			
			m_Buttons[0].Active = true;
			m_Buttons[0].Sensitive = false;
			setter(1);
			
			HBox hbox1 = new HBox();
			HBox hbox2 = new HBox();
			hbox1.PackExpand(m_Buttons[0]);
			hbox1.PackExpand(m_Buttons[1]);
			hbox2.PackExpand(m_Buttons[2]);
			hbox2.PackExpand(m_Buttons[3]);
			
			PackFill(howManyPartsLabel);
			PackExpand(hbox1);
			PackExpand(hbox2);
		}
		
		private PickButton [] m_Buttons;
		private AmountSetter m_Setter;
	}
	
	public delegate void ShareOrder(IList<OrderDish> dishes);
	public class PizzaEditForm: Form
	{
		public PizzaEditForm (Context context): base(context)
		{
			VBox mainBox = new VBox();
			HBox menuBox = new HBox();
			HBox contentBox = new HBox();
			HBox modulesBox = new HBox();
			VBox rightBox = new VBox();
			
			
			PizzaChart chart = new PizzaChart(2);
			
			WindowLabel price = new WindowLabel("Suma: ", "Inverted");

			
			
			
			m_Cols = new List<PizzaColumn>();
			
			for(int i=0; i<4; ++i)
			{
				PizzaColumn col = new PizzaColumn(context);
				m_Cols.Add(col);
				modulesBox.PackExpand(col);
			}
			
			modulesBox.PackFill(new Gtk.Separator(Gtk.Orientation.Vertical));
			
			decimal parts=1;
			m_Picker = new PartsCountPicker(delegate(decimal i) {
				chart.SetParts(i);
			
				for(int j=3; j>i-1; --j)
					m_Cols[j].Visible = false;				
				for(int j=0; j<i; ++j)
				{
					m_Cols[j].Visible = true;
					m_Cols[j].SetAmount(1m/(i));
				}
				
				parts = i-1;
			});
			
			DynamicLabel priceValue = new DynamicLabel("F2", delegate {
				if(m_Cols.Count() == 0)
					return "0.00";
				else
				{
					decimal val = 0m;					
					for(int i=0; i<=parts; ++i)
					{
						OrderDish dish = m_Cols[i].GetOrderDish();
						if(dish != null)
							val += dish.ValueNet;
					}
					return val.ToString("F2");
				}
			});
			
			ButtonsBar bar = new ButtonsBar();
			bar.AddLeftImageButton("Powrót", "arrow_left", Context.ApplicationWindow.GoBack);
			bar.AddRightImageButton("Zapisz", "save", delegate(object sender, EventArgs args) {
				List<OrderDish> result = new List<OrderDish>();
				for(int i=0; i<=parts; ++i)
				{
					OrderDish dish = m_Cols[i].GetOrderDish();
					if(dish == null)
						continue;
					result.Add(dish);
				}
				Context.ApplicationWindow.GoBack(sender, args);
				m_OrderSetter(result);
			});

			rightBox.PackExpand(chart);
			rightBox.PackFill(m_Picker);
			rightBox.PackFill(price);
			rightBox.PackFill(priceValue);
			
			menuBox.PackExpand(bar);
			contentBox.PackExpand(modulesBox);
			rightBox.SetSizeRequest(300, -1);
			contentBox.PackFill(rightBox);
			
			mainBox.PackExpand(contentBox);
			mainBox.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			mainBox.PackFill(menuBox);
			Add (mainBox);
			ShowAll();
			
			m_Cols[1].Visible = false;
			m_Cols[2].Visible = false;
			m_Cols[3].Visible = false;
		}
		
		public override void Load(params object[] args)
		{
			if(args.Count()<1)
				return;
			
			m_OrderSetter = args[0] as ShareOrder;
			if(args.Count() > 0)
			{
				OrderDish dish = args[1] as OrderDish;
				m_Cols[0].SetOrderDish(dish);
			}
		}
					
		private PartsCountPicker m_Picker;
		private ShareOrder m_OrderSetter;
		private List<PizzaColumn> m_Cols;
	}
}

