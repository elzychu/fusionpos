using System;
using Gtk;
using Gdk;
using Cairo;
using FusionPOS.Domain;
using System.Collections.Generic;
using NHibernate;

namespace FusionPOS
{
	public class ShowRoomForm: Form
	{
		public ShowRoomForm (Context context): base(context)
		{
			m_DrawingArea = new DrawingArea();
			m_Brush = CairoHelper.Create(m_DrawingArea.GdkWindow);
			m_DrawingArea.ButtonPressEvent += CheckIfTableClicked;
			m_DrawingArea.ExposeEvent += DrawTables;
			VBox vbox = new VBox();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			
			buttons.AddLeftButton("<", GoBackClick);
			
			
			vbox.PackStart(m_DrawingArea, true, true, 1);
			vbox.PackStart(buttons, false, false, 1);
			
			Add(vbox);
			ShowAll();
		}

		private void DrawTables(object o, ExposeEventArgs args)
		{
			DrawingArea m_DrawingArea = (DrawingArea) o;
			m_Brush = CairoHelper.Create(m_DrawingArea.GdkWindow);
			m_Brush.Color = new Cairo.Color(1.0, 1.0, 1.0);
			m_Brush.Rectangle(2, 2, 600, 600);
			m_Brush.Fill();
			m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
			m_Brush.Rectangle(2, 2, 600, 600);
			m_Brush.Stroke();
			DrawTable(1, 100, 100, 100, 100);
			DrawWall(200, 200, 123, 222);
		}
		
		private void GoBackClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.GoBack(sender, args);	
		}
		
		private void DrawTable(uint id, uint x, uint y, uint width, uint height)
		{
			m_Brush.Color = new Cairo.Color(0.67, 0.25, 0);
			m_Brush.Rectangle(x+10, y+10, width-20, height-20);
			m_Brush.Rectangle(x, y+10, 10, height-20);
			m_Brush.Rectangle(x+width-10, y+10, 10, height-20);
			m_Brush.Rectangle(x+10, y, width-20, 10);
			m_Brush.Rectangle(x+10, y+height-10, width-20, 10);
			m_Brush.Arc(x+10, y+10, 10, 0, Math.PI*2);
			m_Brush.Arc(x+width-10, y+10, 10, 0, Math.PI*2);
			m_Brush.Arc(x+10, y+height-10, 10, 0, Math.PI*2);
			m_Brush.Arc(x+width-10, y+height-10, 10, 0, Math.PI*2);
			m_Brush.Fill();
			
			m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
			m_Brush.SelectFontFace("Arial", FontSlant.Normal, FontWeight.Bold);
			m_Brush.SetFontSize(1.5);
			
			m_Brush.MoveTo((x-10 + width)/2, (y-10+height)/2);
			m_Brush.ShowText(id.ToString());
		}
		
		private void DrawWall(uint x, uint y, uint width, uint height)
		{
			/*m_Brush.Color = new Cairo.Color(0.9, 0.9, 0.9);
			m_Brush.Rectangle(x, y, width, height);
			m_Brush.Fill();
			m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
			m_Brush.Rectangle(x, y, width, height);
			for(int i=0; i<=(width+height); i+=10)
			{
				m_Brush.MoveTo( (i<=width) ? (x+i) : (x+width),
						(i<=width) ?  y : (y + i - width) );
				m_Brush.LineTo( (i<=height) ? x : (x + i - height),
						(i<=height) ? (y+i) : (y+height));
			}
			m_Brush.Stroke();*/
		}
		
		public override void Load(params object[] args)
		{
			if(args.Length > 0)
			{
				using (ISession session = Context.OpenSession()) {
					m_CurrentRoom = session.Get<Room>((long)args[0]);
				}
				Repaint();
			}
			else
			{
					
			}
		}
		
	        private void Repaint()
		{
			m_Brush.Color = new Cairo.Color(1.0, 1.0, 1.0);
			m_Brush.Rectangle(0, 0, m_CurrentRoom.Width, m_CurrentRoom.Height);
			m_Brush.Fill();
			m_Brush.Color = new Cairo.Color(0.0, 0.0, 0.0);
			m_Brush.Rectangle(0, 0, m_CurrentRoom.Width, m_CurrentRoom.Height);
			m_Brush.Stroke();
			
			foreach(Domain.Table table in m_CurrentRoom.Tables)
			{
				DrawTable(table.Number, table.X, table.Y, 
					table.Width, table.Height);
			}
			
			foreach(Wall wall in m_CurrentRoom.Walls)
			{
				DrawWall(wall.X, wall.Y, 
					wall.Width, wall.Height);	
			}
		}
		
		private void CheckIfTableClicked(object o, ButtonPressEventArgs evnt)
		{
			foreach(Domain.Table table in m_CurrentRoom.Tables)
			{
				if (CheckIfTableCollide(table, evnt.Event.X, evnt.Event.Y))
				{
					m_Checked = table;
					
					return;
				}
			}
			
			m_Checked = null;
		}
		
		protected bool CheckIfTableCollide(Domain.Table table, double x, double y)
		{
			if (x>=table.X && y>=table.Y && x<=table.X + 
				table.Width && y<=table.Y + table.Height) 
				return true;
			else 
				return false;
		}
		
		Cairo.Context m_Brush;
		DrawingArea m_DrawingArea;
		Room m_CurrentRoom;
		Domain.Table m_Checked;
	}
	

}

