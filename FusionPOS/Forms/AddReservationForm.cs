using System;
using NHibernate;
using NHibernate.Criterion;
using FusionPOS.Domain;
using FusionPOS.Widgets;
using System.Collections.Generic;

namespace FusionPOS
{
	public class AddReservationForm: Form
	{
		public AddReservationForm (Context context): base(context)
		{
			WindowLabel infoLabel = new WindowLabel("Dane rezerwacji", "Inverted");
			WindowLabel calendarlabel = new WindowLabel("Kalendarz", "Inverted");
			WindowLabel otherLabel = new WindowLabel("Pozostałe rezerwacje na wybrany dzień", "Inverted");
		
			m_HoursEntry = new Widgets.HourEntry();
			m_Calendar = new Gtk.Calendar();
			m_Calendar.ButtonPressEvent += OnDateChanged;
			m_Calendar.Date = DateTime.Now;
			
			m_Reservation = new Reservation();
			
			m_ShowTable = new TextBox();
			m_ShowTable.FocusGrabbed += TakeTableClick;
			
			m_ShowClient = new TextBox();
			m_ShowClient.FocusGrabbed += TakeClientClick;
			
			m_ButtonsBar = new Widgets.ButtonsBar();
			
			m_ButtonsBar.AddLeftImageButton("Powrót", "arrow_left", delegate (object sender, EventArgs args) {
				if(m_Refresh != null)
					m_Refresh();
				Context.ApplicationWindow.GoBack(sender, args);	
			});
			m_ButtonsBar.AddLeftButton("Zapisz", SaveClick);
			
			m_Description = new Widgets.TextView();
			
			HBox mainHbox = new HBox();
			VBox mainVbox = new VBox();
			VBox leftHBox = new VBox();
			VBox rightVBox = new VBox();
			Widgets.FormTable table = new FormTable(4, 200);
			table.SetRowSpacing(3, 10);
			table.PackElement(new Label("Klient"), m_ShowClient, 0, false);
			table.PackElement(new Label("Czas"), m_HoursEntry, 1, false);
			table.PackElement(new Label("Stolik"), m_ShowTable, 2, false);
			table.PackElement(new Label("Adnotacje"), m_Description, 3, true);
			
			leftHBox.PackStart(infoLabel, false, false, 0);
			leftHBox.PackStart(table, true, true, 0);
		
			mainHbox.PackStart(leftHBox, true, true, 1);
			rightVBox.PackStart(calendarlabel, false, false, 0);
			rightVBox.PackStart(m_Calendar, false, false, 5);
			rightVBox.PackStart(otherLabel, false, false, 0);
			
			//Lista rezerwacji
			/*Lista*/
			m_TreeView = new Gtk.TreeView();
			m_ReservationList = new Gtk.ListStore(typeof(Domain.Reservation));
			Widgets.SimpleTreeViewColumn nameColumn = new Widgets.SimpleTreeViewColumn("Rezerwujący", "ParentClient.Name");
			Widgets.SimpleTreeViewColumn tableColumn = new Widgets.SimpleTreeViewColumn("Stolik", "ParentTable.Id");
			Widgets.SimpleTreeViewColumn dateColumn = new Widgets.SimpleTreeViewColumn("Czas", "Date.TimeOfDay");
			
			m_TreeView.AppendColumn(dateColumn);
			m_TreeView.AppendColumn(nameColumn);
			m_TreeView.AppendColumn(tableColumn);
			m_TreeView.Model = m_ReservationList;
			m_TreeView.Name = "SmallerCalendarLabel";
			Gtk.ScrolledWindow scrWin = new Gtk.ScrolledWindow();
			
			scrWin.Add(m_TreeView);
			
			rightVBox.PackStart(scrWin, true, true, 0);
			
			mainHbox.PackStart(rightVBox, false, false, 0);
			
			OnDateChanged(m_Calendar, null);
			
			mainVbox.PackExpand(mainHbox);
			mainVbox.PackFill(m_ButtonsBar);
			
			Add(mainVbox);
			ShowAll();
		}

		private void OnDateChanged(object o, Gtk.ButtonPressEventArgs args)
		{
			m_ReservationList.Clear();
			using (ISession session = Context.OpenSession()) {
				DateTime now = ((Gtk.Calendar)o).GetDate();
				ICriteria criteria = session.CreateCriteria(typeof(Domain.Reservation));
				criteria.Add(Expression.Between("Date", new DateTime(now.Year, now.Month, now.Day, 0, 0, 0), 
				new DateTime(now.Year, now.Month, now.Day, 23, 59, 0)));
				IList<Domain.Reservation> criteriaList = criteria.List<Domain.Reservation>();
				
				foreach(Domain.Reservation res in criteriaList)
				{
					m_ReservationList.AppendValues(res);
				}
			}
		}
		
		
		
		private void onClientSelected(long Id)
		{
			using (ISession session = Context.OpenSession()) {
				m_Reservation.ParentClient = session.Get<Client>(Id);
				m_ShowClient.Text = m_Reservation.ParentClient.Name;
			}
		}
		
		public void onTableSelected(Domain.Table table)
		{
			m_Reservation.ParentTable = table;
			m_ShowTable.Text = m_Reservation.ParentTable.Number.ToString();
		}
		
		private void TakeClientClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(ShowClientsForm), new ClientSetter(onClientSelected));
		}
		
		private void TakeTableClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(ShowRoomListForm), new TableSetter(onTableSelected));
		}
		
		private void SaveClick(object sender, EventArgs args)
		{	
			if(m_Reservation.ParentTable == null)
			{
				m_ShowTable.Text = "Wybierz stolik";
				return;
			}
			
			m_Reservation.Date = m_Calendar.GetDate();
			m_Reservation.Date += new TimeSpan((int)m_HoursEntry.HoursVal, (int)m_HoursEntry.MinutesVal, 0);
			m_Reservation.AdditionalNotes = m_Description.Text;
			
			using (ISession session = Context.OpenSession()) {
				ITransaction tran = session.BeginTransaction();
				if(m_Reservation.Id != 0)
					session.Update(m_Reservation);
				else 
					session.Save(m_Reservation);
				tran.Commit();
			}
			
			if(m_Refresh != null)
				m_Refresh();
			Context.ApplicationWindow.GoBack(sender, args);
		}
		
		private static bool IsType(object obj, string type)
		{
			Type t = Type.GetType(type, true, true);
		
			return t == obj.GetType() || obj.GetType().IsSubclassOf(t);
		}
		
		public override void Load(params object[] args)
		{ 
			if (args.Length>0)
			{
				m_Refresh = (Refresher)args[0];
				
			}
			if (args.Length>1)
			{
				if (IsType (args[1], "System.DateTime"))
				{
					DateTime date = (DateTime)args[1];
					
					m_Reservation = new Reservation();
					m_Reservation.Date = date;
					m_Calendar.Date = date;
					m_HoursEntry.SetHour((uint) date.Hour, 
						(uint) date.Minute);
				}
				else
				{
					using (ISession session = Context.OpenSession()) {
						m_Reservation = session.Get<Domain.Reservation>(args[1]);
	
						m_ShowTable.Text = m_Reservation.ParentTable.Number.ToString();
						m_ShowClient.Text = m_Reservation.ParentClient.Name;
						m_Description.Text = m_Reservation.AdditionalNotes;
						m_Calendar.Date = m_Reservation.Date;
						m_HoursEntry.SetHour( (uint)m_Reservation.Date.TimeOfDay.Hours,
									(uint)m_Reservation.Date.TimeOfDay.Minutes);
					}
				}

			}
		}
		
		private Gtk.TreeView m_TreeView;
		private Reservation m_Reservation;
		private Gtk.ListStore m_ReservationList;
		private Widgets.ButtonsBar m_ButtonsBar;		
		private TextBox m_ShowTable;
		private TextBox m_ShowClient;
		private Gtk.Calendar m_Calendar;
		private Widgets.HourEntry m_HoursEntry;
		private Widgets.TextView m_Description;
		private Widgets.ButtonsBar m_Buttons;
		private Refresher m_Refresh;
	}
}

