using System;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using FusionPOS;
using FusionPOS.Domain;
using FusionPOS.Widgets;
using System.Linq;
using NHibernate.Linq;

namespace FusionPOS
{
	public class IngredientSheet: HBox {
		public IngredientSheet(Context context)
		{
			m_Ctx = context;
			Label quantity = new Label("Ilość na magazynie: ");
			Label minimalQuantity = new Label("Minimalna ilość: ");
			
			WindowLabel label1 = new WindowLabel("Informacje ogólne", "Inverted");
			WindowLabel label2 = new WindowLabel("Ostatnie zakupy", "Inverted");
			WindowLabel label3 = new WindowLabel("Nazwa", "Inverted");
			
			DynamicLabel nameView = new DynamicLabel(() =>
				m_Ingredient != null ? m_Ingredient.Name : "----"
			);
			
			DynamicLabel quantityView = new DynamicLabel(() =>
				m_Ingredient != null ? String.Format("{0} {1}",
			                m_Ingredient.Quantity.ToString(), 
			                m_Ingredient.Unit.ToString()) : "----"
			);
			
			DynamicLabel minimalQuantityView = new DynamicLabel(() =>
				m_Ingredient != null ? m_Ingredient.MinimumQuantity.ToString() 
			                : "----"
			);
			
			Gtk.Table table = new Gtk.Table(12, 2, false);
			table.Attach(label3, 0, 2, 0, 1);
			table.Attach(nameView, 0, 2, 1, 2);
			table.Attach(label1, 0, 2, 2, 3);
			table.Attach(quantity, 0, 1, 3, 4);
			table.Attach(quantityView, 1, 2, 3, 4);
			table.Attach(minimalQuantity, 0, 1, 4, 5);
			table.Attach(minimalQuantityView, 1, 2, 4, 5);
			
			Gtk.TreeView historyView = new Gtk.TreeView();
			m_Store = new Gtk.ListStore(typeof(IncomingInvoiceComponent));
			SimpleTreeViewColumn nameCol = new
				Widgets.SimpleTreeViewColumn("Nazwa", 
					"IncomingInvoiceComponent.Ingredient.Name");
			SimpleTreeViewColumn amountCol = new
				Widgets.SimpleTreeViewColumn("Ilość", 
					"IncomingInvoiceComponent.Amount");
			
			historyView.AppendColumn(nameCol);
			historyView.AppendColumn(amountCol);
		
			historyView.Model = m_Store;
			
			table.Attach(label2, 0, 2, 9, 10);
			Gtk.ScrolledWindow scrWnd = new Gtk.ScrolledWindow();
			scrWnd.Add (historyView);
			scrWnd.SetSizeRequest(300, 300);
			table.Attach(scrWnd, 0, 2, 10, 11);
			PackExpand(table);
			
			DynamicLabel.RefreshAll(this);
		}
		
		public void SetIngredient(Ingredient ingredient)
		{
			m_Ingredient = ingredient;
			using(ISession ses = m_Ctx.OpenSession())
			{
				//xxx do poprawy kurwa bo coś tu nie jeździ i datami
				IList<IncomingInvoiceComponent> components = 
					ses.Query<IncomingInvoiceComponent>().ToList();
				
				components = components.Where(p => (DateTime.Compare(
						p.Invoice.IssueTime,
						DateTime.Now.AddMonths(-1)) > 0)).ToList();
				
				components = components.Where(r => r.Ingredient.Equals(ingredient)).ToList();
				m_Store.AppendValues(components);
			}
			
			m_Store.Clear();
			DynamicLabel.RefreshAll(this);
		}


		private Context m_Ctx;
		private Ingredient m_Ingredient;
		private Gtk.ListStore m_Store;
	}
	
	public class IngredientsListForm: Form
	{
		public IngredientsListForm(Context context): base(context)
		{
			Widgets.VBox vbox = new Widgets.VBox();
			Widgets.HBox contentBox = new Widgets.HBox();
			
			Gtk.ScrolledWindow treeScroll = new Gtk.ScrolledWindow();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			m_SheetForm = new IngredientSheet(context);
			
			m_SearchText = new Widgets.TextBox();
			m_SearchText.Changed += Search;
			m_TreeView = new Gtk.TreeView();
			m_TreeView.AppendColumn(new Widgets.SimpleTreeViewColumn("Nazwa", "Name"));
			m_TreeView.AppendColumn(new Widgets.TreeViewColumn("Ilość", delegate(object model)
			{
				if (model is Ingredient) {
					return (model as Ingredient).Quantity.ToString() + 
					    (model as Ingredient).Unit.ToString();
				}
				
				return "";
			}));
			
			m_TreeView.CursorChanged += TreeViewSelected;
			
			m_TreeStore = new Gtk.TreeStore(typeof(object));
			m_TreeView.Model = m_TreeStore;
			treeScroll.Add(m_TreeView);
			
			buttons.AddLeftImageButton("Powrót", "arrow_left", Context.ApplicationWindow.GoBack);
			buttons.AddRightImageButton("Ostatnie zmiany", "last_changes", Context.ApplicationWindow.GoBack);
			buttons.AddRightImageToggle("Brakujące składniki", "misssing_ingredients", delegate(object sender, EventArgs e) {
				if ((sender as Gtk.ToggleButton).Active)
					LoadMissing();
				else
					Load();
			});
			
			contentBox.PackExpand(treeScroll);
			contentBox.PackFill(new Gtk.Separator(Gtk.Orientation.Vertical));
			contentBox.PackExpand(m_SheetForm);
			
			vbox.PackFill(m_SearchText);
			vbox.PackExpand(contentBox);
			vbox.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			vbox.PackFill(buttons);
			Add(vbox);
			ShowAll();
		}
		
		private void TreeViewSelected(object sender, EventArgs e)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			Gtk.TreePath path;
			m_TreeView.Selection.GetSelected(out model, out iter);
			path = m_TreeStore.GetPath(iter);
			object obj = model.GetValue(iter, 0);
			if(obj is Ingredient)
			{
				using(ISession ses = Context.OpenSession())
				{
					m_Selected = (obj as Ingredient);
					m_SheetForm.SetIngredient(m_Selected);
				}				
			}
			if (m_TreeView.GetRowExpanded(path))
				m_TreeView.CollapseRow(path);
			else
				m_TreeView.ExpandRow(path, false);
		}
		
		public override void Load(params object[] args)
		{
			m_TreeStore.Clear();
			using (ISession session = Context.OpenSession()) {
				IngredientCategory cat =  session.Get<IngredientCategory>(1L);
				if (cat != null)
					LoadNode(cat, null);
			}
		}
		
		private void LoadMissing()
		{
			m_TreeStore.Clear();
			using (ISession session = Context.OpenSession()) {
				IList<Ingredient> missing = session.Query<Ingredient>()
					.Where(p => p.Quantity < p.MinimumQuantity)
					.Where(p => p.Deleted != true)
					.ToList();
				
				foreach (Ingredient i in missing)
					m_TreeStore.AppendValues(i);
			}
		}
		
		
		private void LoadNode(IngredientCategory category, Gtk.TreeIter? iter)
		{
			foreach (IngredientCategory i in category.Children) {
				if (i.Deleted)
					continue;
				Gtk.TreeIter it = m_TreeStore.AppendValuesRef(iter, i);
				LoadNode(i, it);
			}
			
			foreach (Ingredient i in category.Ingredients)
			{
				if(i.Deleted)
					continue;
				m_TreeStore.AppendValuesRef(iter, i);
			}
		}
		
		private void Search(object s, EventArgs args)
		{		
			if (m_SearchText.Text == "") {
				Load();
				return;
			}
			
			using (ISession session = Context.OpenSession())
			{
				string query = String.Format("%{0}%", m_SearchText.Text);
				IList<Ingredient> results = session.CreateCriteria<Ingredient>()
				    .Add(Expression.InsensitiveLike("Name", query))
				    .Add (Expression.Eq("Deleted", false))
				    .List<Ingredient>();
				
				m_TreeStore.Clear();
				foreach (Ingredient i in results)
					m_TreeStore.AppendValues(i);
			}
		}
		
		private Ingredient m_Selected;
		private Gtk.TreeView m_TreeView;
		private Gtk.TreeStore m_TreeStore;
		private IngredientSheet m_SheetForm;
		private Widgets.TextBox m_SearchText;
	}
}
