using System;
using NHibernate;
using FusionPOS.Widgets;
using FusionPOS.Domain;
using System.Linq;

namespace FusionPOS
{
	public class OrderDestinationForm: Form
	{
		public OrderDestinationForm(Context context): base(context)
		{
			VBox mainBox = new VBox();
			HBox contentBox = new HBox();
			HBox downBox = new HBox();
			VBox leftMenuBox = new VBox();
			VBox formBox = new VBox();
			VBox pickTableBox = new VBox();
			HBox pickTableHBox = new HBox();
			VBox commentBox = new VBox();
			VBox clientBox = new VBox();
			HBox clientHBox = new HBox();
			VBox clientButtonsBox = new VBox();
			
			Widgets.Table clientData = new Widgets.Table(3, 2, false);
			
			m_Name = new Widgets.Label("brak");
			m_Address = new Widgets.TextView();
			m_Address.Sensitive = false;
			
			m_Buttons = new Widgets.ButtonsBar();
			Widgets.Button selectClient = new Widgets.Button("Wybierz");
			selectClient.Clicked += delegate(object sender, EventArgs e) {
				m_killSession = false;
				Context.ApplicationWindow.LoadForm(
			                typeof(ShowClientsForm), 
			                new ClientSetter(delegate(long Id) {
						m_killSession = true;
						m_Client = m_Session.Get<Client>(Id);
						m_Name.Text = m_Client.Name;
						m_Address.Text = m_Client.Address;
					})
				);
			};
			
			Widgets.Button removeClient = new Widgets.Button("Usuń");
			removeClient.Clicked += delegate(object sender, EventArgs e) {
				m_Client = null;
				m_Name.Text = "brak";
			};

			m_Personally = new Gtk.ToggleButton("Na wynos");
			m_Personally.Active = true;
			m_Personally.Toggled += delegate(object sender, EventArgs e) {
				if (m_Personally.Active) {
					m_Onsite.Active = false;
					m_Onsite.Sensitive = true;
					m_Delivery.Active = false;
					m_Delivery.Sensitive = true;
					m_Personally.Sensitive = false;
					pickTableBox.Sensitive = false;
					m_Address.Sensitive = false;
				}
			};
			
			m_Personally.Sensitive = false;
			
			m_Delivery = new Gtk.ToggleButton("Dostawa");
			m_Delivery.Toggled += delegate(object sender, EventArgs e) {
				if (m_Delivery.Active) {
					m_Personally.Active = false;
					m_Personally.Sensitive = true;
					m_Onsite.Active = false;
					m_Onsite.Sensitive = true;
					m_Delivery.Sensitive = false;
					pickTableBox.Sensitive = false;
					m_Address.Sensitive = true;
				}
			};
			
			m_Onsite = new Gtk.ToggleButton("Na miejscu");
			m_Onsite.Toggled += delegate(object sender, EventArgs e) {
				if (m_Onsite.Active) {
					m_Personally.Active = false;
					m_Personally.Sensitive = true;
					m_Delivery.Active = false;
					m_Delivery.Sensitive = true;
					m_Onsite.Sensitive = false;
					pickTableBox.Sensitive = true;
					m_Address.Sensitive = false;
				}
			};
			m_OrderNotes = new Widgets.TextView();
			m_TableName = new Widgets.Label("brak");
			
			m_Buttons.AddLeftImageButton("Powrót", "arrow_left", Context.ApplicationWindow.GoBack);
						
			clientButtonsBox.PackExpand(selectClient);
			clientButtonsBox.PackExpand(removeClient);
			Gtk.Alignment a = new Gtk.Alignment(0f, 0f, 0f, 0f);
			Gtk.Alignment b = new Gtk.Alignment(0f, 0f, 0f, 0f);
			Gtk.Alignment c = new Gtk.Alignment(0f, 0f, 0f, 0f);
			a.Add(new WindowLabel("Nazwisko"));
			b.Add(m_Name);
			c.Add(new WindowLabel("Adres"));
			a.SetSizeRequest(150, -1);
			b.SetSizeRequest(150, -1);
			c.SetSizeRequest(150, -1);
			clientData.AttachFill(a, 0, 1, 0, 1);
			clientData.AttachFill(c, 0, 1, 2, 3);
			clientData.AttachFill(b, 1, 2, 0, 1);
			clientData.AttachExpand(m_Address, 1, 2, 2, 3, true, true);
			
			clientHBox.PackExpand(clientData);
			clientHBox.PackFill(clientButtonsBox);
			
			clientBox.PackFill(new Widgets.WindowLabel("Klient", "Inverted"));
			clientBox.PackExpand(clientHBox);
			
			pickTableBox.PackFill(new WindowLabel("Stolik", "Inverted"));
			Widgets.Button selectTableButton = new Widgets.Button("Wybierz");
			selectTableButton.Clicked += delegate(object sender, EventArgs e) {
				m_killSession = false;
				Context.ApplicationWindow.LoadForm(
			                typeof(ShowRoomListForm), 
			                new TableSetter(delegate(Domain.Table table) {
						m_killSession = true;
						m_Table = table;
						m_TableName.Text = table.Name;
					})
				);
			};
			
			m_Save = (u) => {
				m_Order.Description = m_OrderNotes.Text;
				m_Order.Client = m_Client;
				Destination destination;
				
				if(m_Personally.Active)
				{
					destination = new DestinationNone();
				}
				
				if(m_Onsite.Active)
				{
					if(m_Table == null)
					{
						MessageBox.Popup("Nie wszystkie pola uzupełnione", 
						                 "Nie został wybrany stolik");
						return;
					}
					DestinationOnSite onSite = new DestinationOnSite();
					onSite.Table = m_Table;
					destination = onSite;
				}
				
				if(m_Delivery.Active)
				{
					if(m_Address.Text == "")
					{
						MessageBox.Popup("Nie wszystkie pola uzupełnione", 
						                 "Wypełnij pole adresu");
						return;	
					}
					
					DestinationDelivery delivery = new DestinationDelivery();
					delivery.Address = m_Address.Text;
					delivery.DeliveryTime = DateTime.Now;
					
					destination = delivery;
				}
				
				destination.Description = m_OrderNotes.Text;
				if(m_Order.Destination != null)
					destination.Id = m_Order.Destination.Id;
				
				destination.Order = m_Order;
				
				m_Order.Destination = destination;				
				if(!u)
				{
					m_Order.Entries.Each(p => {
						if(p.Id != 0)	
							m_Session.Lock(p, LockMode.None);
					});
					context.MakeOrder(m_Order, true);
				}
				else
					context.UpdateOrder(m_Order, true);
				m_Session.Dispose();
			};			
						
			pickTableHBox.PackFill(new Widgets.Label("Numer stolika:"));
			Gtk.Table separator = new Gtk.Table(1, 1, true);
			pickTableHBox.PackFill(m_TableName);
			pickTableHBox.PackExpand(separator);
			pickTableHBox.PackFill(selectTableButton);
			pickTableBox.PackExpand(pickTableHBox);
			pickTableBox.Sensitive = false;
			commentBox.PackFill(new Widgets.WindowLabel("Dodatkowe informacje",
			    "Inverted"));
			commentBox.PackExpand(m_OrderNotes);
			
			formBox.PackExpand(clientBox);
			formBox.PackFill(pickTableBox);
			formBox.PackExpand(commentBox);
			
			leftMenuBox.PackFill(new Widgets.WindowLabel("Rodzaj zamówienia",
			    "Inverted"));
			leftMenuBox.PackExpand(m_Personally);
			leftMenuBox.PackExpand(m_Onsite);
			leftMenuBox.PackExpand(m_Delivery);
			
			contentBox.PackFill(leftMenuBox);
			contentBox.PackExpand(formBox);
			downBox.PackFill(m_Buttons);
			mainBox.PackExpand(contentBox);
			mainBox.PackFill(downBox);
			
			Add(mainBox);
			ShowAll();
		}

		public override void Load(params object[] args)
		{
			m_Order = args[0] as Order;
			m_Session = args[1] as ISession;
			
			m_OrderNotes.Text = m_Order.Description;
			
			if(m_Order.Client != null)
			{
				m_Client = m_Order.Client;
				m_Address.Text = m_Client.Address;
				m_Name.Text = m_Client.Name;	
			}
			
			if( !(m_Order.Destination is DestinationDelivery) &&
			    !(m_Order.Destination is DestinationDelivery) &&
			    !(m_Order.Destination is DestinationDelivery))
			{
				m_Personally.Active = true;
				m_Onsite.Active = false;
				m_Delivery.Active = false;
			}
			
			if(m_Order.Destination is DestinationDelivery)
			{
				m_Personally.Active = false;
				m_Onsite.Active = false;
				m_Delivery.Active = true;
				m_Address.Text = (m_Order.Destination as DestinationDelivery).Address;
			}
			
			if(m_Order.Destination is DestinationNone)
			{
				m_Personally.Active = true;
				m_Onsite.Active = false;
				m_Delivery.Active = false;	
			}
			
			if(m_Order.Destination is DestinationOnSite)
			{
				m_Personally.Active = false;
				m_Onsite.Active = true;
				m_Delivery.Active = false;
				m_Table = (m_Order.Destination as DestinationOnSite).Table;
				m_TableName.Text = m_Table.Name;
			}
			
			if(m_Order.Id == 0)
			{
				m_Buttons.AddRightImageButton("Utwórz zamówienie", "new_order", (sender, e) => {
					m_Save(false);
					Context.ApplicationWindow.GoHome(sender, e);
				});
				m_Buttons.AddRightImageButton("Płatność", "money", delegate(object sender, EventArgs e) {
					m_Save(false);
					Context.ApplicationWindow.LoadForm(typeof(OrderPaymentForm), m_Order);
				});
			} 
			else
			{
				m_Buttons.AddRightImageButton("Zapisz zmiany", "save", (sender, e) => {
					m_Save(true);
					Context.ApplicationWindow.GoHome(sender, e);
				});
			}
			
			ShowAll();
		}
			
		public override void OnLeaving()
		{
			if(m_killSession)
				m_Session.Dispose();
			base.OnLeaving();
		}		
		
		private bool m_killSession = true; 
		private ISession m_Session;
		private Order m_Order;
		private Gtk.ToggleButton m_Personally;
		private Gtk.ToggleButton m_Delivery;
		private Gtk.ToggleButton m_Onsite;
		private Widgets.TextView m_OrderNotes;
		private Widgets.TextView m_Address;
		Widgets.ButtonsBar m_Buttons;
		private Widgets.Label m_TableName;
		private Widgets.Label m_Name;
		private Client m_Client;
		private Domain.Table m_Table;
		private Action<bool> m_Save;
	}
}

