using System;
using System.Linq;
using System.Collections.Generic;
using NHibernate;
using Gtk;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public delegate void Refresher();
	
	public class ReservationsCell: Widgets.VBox
	{
		private class CLabel: Widgets.WindowLabel
		{  
			public CLabel(string title): base(title) 
			{
				Name = "CalendarLabel";
			} 
		}
		
		public ReservationsCell(IList<Domain.Reservation> reservations, ShowReservationForm parent, Context context, bool dark, DateTime date, bool active, bool showDate): base()
		{
			m_Context = context;
			m_Parent = parent;
			m_Date = date;
			//SetSizeRequest(100, 100);
			m_CellReservations = reservations;
			
			if(showDate)
			{
				CLabel dateLabel = new CLabel(date.Date.ToString("dd"));
				PackStart(dateLabel, false, true, 0);
			}
			
			if (active)
			{
				CLabel addLabel = new CLabel ("Kliknij aby dodać");
				addLabel.AddEvents((int)Gdk.EventMask.ButtonPressMask);
				addLabel.ButtonPressEvent += AddNewPositionLabelClicked;
				addLabel.Name = "SmallerCalendarLabel";
	
				if(m_CellReservations != null)
				{
					m_Click = new Dictionary<CLabel, Domain.Reservation>();
					foreach(Domain.Reservation reservation in m_CellReservations)
					{
						CLabel label = new CLabel(reservation.ParentClient.Name + " " + 
							reservation.ParentTable.Number + " " + reservation.Date.TimeOfDay);
						label.AddEvents((int)Gdk.EventMask.ButtonPressMask);
						label.ButtonPressEvent += LabelClicked;
						m_Click[label] = reservation;
						PackStart(label, false, true, 0);
					}
				}
				
				PackStart(addLabel, false, true, 0);
				if (dark)
					Name = "DarkCell";
				else
					Name = "LightCell";
			}	
			
		}
		
		void AddNewPositionLabelClicked(object o, ButtonPressEventArgs args)
		{
			m_Context.ApplicationWindow.LoadForm(typeof(AddReservationForm), new Refresher(m_Parent.Refresh), m_Date);	
		}
		
		void LabelClicked (object o, ButtonPressEventArgs args)
		{
			Domain.Reservation reservation = m_Click[(CLabel)o];
			m_Context.ApplicationWindow.LoadForm(typeof(AddReservationForm), new Refresher(m_Parent.Refresh), reservation.Id);
		}
		
		private DateTime m_Date;
		private Context m_Context;
		private ShowReservationForm m_Parent;
		private Dictionary<CLabel, Domain.Reservation> m_Click;
		private IList<Domain.Reservation> m_CellReservations;
	}
	
	public class ReservationsWeekCalendar: Gtk.Table
	{
		protected string [] WeekDays = {"Pon", "Wt", "Śr", "Czw","Pt", "Sob", "Nd"};
	
		public ReservationsWeekCalendar(IList<Domain.Reservation> reservations, Context context, ShowReservationForm parent): base(3, 8, false)
		{
			m_Context = context;
			m_Parent = parent;
			m_Checked = DateTime.Now;
			m_Reservations = reservations;
			
			EvalueWeek();
			
			m_DateRangeLabel = new Widgets.Label();
			m_RemoveList = new List<Widget>();
			
			ReRender();
			
			m_Backward = new Widgets.Button("<");
			m_Backward.Clicked += GoBack;
			m_Forward = new Widgets.Button(">");
			m_Forward.Clicked += GoForward;
			Attach(m_Backward, 1, 2, 0, 1);
			Attach(m_Forward, 7, 8, 0, 1);
			Attach(m_DateRangeLabel, 2, 7, 0, 1);
		}
		
		public void ReRender()
		{
			
			IList<Domain.Reservation> reservations = TakeReservationsFromThisWeek();
			DateTime dateTo = m_DateFrom.AddDays(7);
			
			foreach(Widget wdg in m_RemoveList) {
				Console.WriteLine(wdg.ToString());
				Remove(wdg);
			}
			m_RemoveList.Clear();
			
			for (uint i=0; i<7; ++i)
			{
				int month = m_DateFrom.AddDays(i).Month;
				int day = m_DateFrom.AddDays(i).Day;
				Widgets.Label weekDay = new Widgets.Label(WeekDays[i] +
					" (" + ((month<10)? "0"+month.ToString(): month.ToString()) + "." 
					+ ((day<10) ? "0"+day.ToString(): day.ToString()) + ") ");
				
				weekDay.Name = "CalendarLabel";
				Attach(weekDay, i+1, i+2, 1, 2);
				m_RemoveList.Add (weekDay);
			}

			uint numOfRows = ( (m_LatestHour - m_EarlierHour) / 3  ) + 3;
			
			for(uint i=2, j=m_EarlierHour; i<numOfRows; i++, j+=3)
			{
				//XXX
				Widgets.Label label = new Widgets.Label( ((j<10)? "0" + j.ToString() : j.ToString())
					+ ".00 - " + ((j+3<10)? "0" + (j+3).ToString() : (j+3).ToString()) + ".00");
				Attach(label, 0, 1, i, i+1);
			}
			
			IList<Domain.Reservation>[,] reservationsCellLists = new IList<Domain.Reservation>[numOfRows, 7];
			for (int i=0; i<numOfRows; ++i)
				for(int j=0; j<7; ++j)
					reservationsCellLists[i, j] = new List<Domain.Reservation>();	
			
			foreach(Domain.Reservation reservation in reservations)
			{
				if (reservation.Date.Hour < m_EarlierHour) continue;
				if (reservation.Date.Hour > m_LatestHour) continue;
				
				reservationsCellLists[(reservation.Date.Hour-m_EarlierHour)/3,
					( ((int)reservation.Date.DayOfWeek-1)<0 ? 6 : ((int)reservation.Date.DayOfWeek-1))].Add(reservation);
			}
			
			for(uint i=2; i<numOfRows; i++)
			{
				for(uint j=1; j<8; j++)
				{
					DateTime date = new DateTime();
				        date = m_DateFrom.Date.AddDays(j-1);
					date += new TimeSpan((int)(m_EarlierHour + (i -2)*3), 0, 0);
				
					ReservationsCell cell = new ReservationsCell(reservationsCellLists[i-2,j-1], m_Parent, m_Context, (i%2>0)? true : false, date, true, false);
					Attach (cell, j, j+1, i, i+1);
					m_RemoveList.Add(cell);
				}
			}
				
			ShowAll();
			
			m_DateRangeLabel.Text = "Data od " + m_DateFrom.ToString("dd/MM/yyyy") + " do " + dateTo.ToString("dd/MM/yyyy");
		}

		private void GoBack(object sender, EventArgs e)
		{
			m_DateFrom = m_DateFrom.AddDays(-7);
			ReRender();
		}	
		
		private void GoForward(object sender, EventArgs e)
		{
			m_DateFrom = m_DateFrom.AddDays(7);
			ReRender();
		}
		
		private IList<Domain.Reservation> TakeReservationsFromThisWeek()
		{
			DateTime dateTo = m_DateFrom.AddDays(7);
			m_EarlierHour = 6;
			m_LatestHour = 23;
			return new List<Domain.Reservation> (m_Reservations.Where(r => r.Date <= dateTo && r.Date >= m_DateFrom));
		}
		
		private void EvalueWeek()
		{
			DateTime now = DateTime.Now;
			int diff = now.DayOfWeek - DayOfWeek.Monday;
			if(diff < 0)
			{
				diff+=7;	
			}
			
			m_DateFrom = now.AddDays(-diff).Date;
			
			diff = DayOfWeek.Sunday - now.DayOfWeek;
		}
		
		protected ShowReservationForm m_Parent;
		protected Context m_Context;
		protected IList<Widget> m_RemoveList;
		protected uint m_EarlierHour;
		protected uint m_LatestHour;
		protected DateTime m_DateFrom;
		protected Widgets.Button m_Forward;
		protected Widgets.Button m_Backward;
		protected DateTime m_Checked;
		protected IList<Domain.Reservation> m_Reservations;
		protected Widgets.Label m_DateRangeLabel;
	
	}
	
	
	public class ReservationsMonthCalendar: Gtk.Table
	{
		protected string [] WeekDays = {"Pon", "Wt", "Śr", "Czw","Pt", "Sob", "Nd"};
	
		public ReservationsMonthCalendar(IList<Domain.Reservation> reservations, Context context, ShowReservationForm parent): base(7, 8, false)
		{
			m_Context = context;
			m_Parent = parent;
			m_Reservations = reservations;
			m_CurrentMonth = DateTime.Now;
			
			m_DateRangeLabel = new Widgets.Label();
			m_RemoveList = new List<Widget>();
			
			ReRender();
			
			m_Backward = new Widgets.Button("<");
			m_Backward.Clicked += GoBack;
			m_Forward = new Widgets.Button(">");
			m_Forward.Clicked += GoForward;
			Attach(m_Backward, 1, 2, 0, 1);
			Attach(m_Forward, 7, 8, 0, 1);
			Attach(m_DateRangeLabel, 2, 7, 0, 1);
		}
		
		public void ReRender()
		{
			
			IList<Domain.Reservation> reservations = TakeReservationsFromThisMonth();
			foreach(Widget wdg in m_RemoveList)
				Remove(wdg);
			
			for (uint i=0; i<7; ++i)
			{
				Widgets.Label weekDay = new Widgets.Label(WeekDays[i]);
				
				weekDay.Name = "CalendarLabel";
				Attach(weekDay, i+1, i+2, 1, 2);
			}
			
			IList<Domain.Reservation>[,] reservationsCellLists = new IList<Domain.Reservation>[6, 7];
			for (int i=0; i<6; ++i)
				for(int j=0; j<7; ++j)
					reservationsCellLists[i, j] = new List<Domain.Reservation>();	
			DateTime firstDay = new DateTime(m_CurrentMonth.Year, m_CurrentMonth.Month, 1);
			foreach(Domain.Reservation reservation in reservations)
			{			
				int row = (reservation.Date.Day + (int)firstDay.DayOfWeek) / 7;
				reservationsCellLists[row, ( ((int)reservation.Date.DayOfWeek-1)<0 ? 6 : ((int)reservation.Date.DayOfWeek-1))].Add(reservation);
			}
			
			for(uint i=2; i<8; i++)
			{
				for(uint j=1; j<8; j++)
				{
					int firstDayValue = ((int)firstDay.Date.DayOfWeek-1)<0 ? 6 : ((int)firstDay.Date.DayOfWeek-1);
					DateTime date = firstDay.AddDays((((i-2)*7) + (j-1)) - firstDayValue);
					
					ReservationsCell cell = new ReservationsCell(reservationsCellLists[i-2,j-1], m_Parent, m_Context, (i%2>0)? true : false, 
						date, (date.Month==m_CurrentMonth.Month)?true:false, true);
					m_RemoveList.Add(cell);
					Attach (cell, j, j+1, i, i+1);
				}
			}
				
			ShowAll();
			m_DateRangeLabel.Text = m_CurrentMonth.Date.ToString("yyyy/MM");
		}

		private void GoBack(object sender, EventArgs e)
		{
			m_CurrentMonth = m_CurrentMonth.AddMonths(-1);
			ReRender();
		}	
		
		private void GoForward(object sender, EventArgs e)
		{
			m_CurrentMonth = m_CurrentMonth.AddMonths(1);
			ReRender();
		}
		
		private IList<Domain.Reservation> TakeReservationsFromThisMonth()
		{
			return new List<Domain.Reservation> (m_Reservations.Where(r => r.Date.Month == m_CurrentMonth.Date.Month));
		}
		
		protected ShowReservationForm m_Parent;
		protected Context m_Context;
		protected IList<Widget> m_RemoveList;
		protected DateTime m_CurrentMonth;
		protected Widgets.Button m_Forward;
		protected Widgets.Button m_Backward;
		protected IList<Domain.Reservation> m_Reservations;
		protected Widgets.Label m_DateRangeLabel;
	
	}
	
	public class ShowReservationForm: Form
	{
		public ShowReservationForm (Context context): base(context)
		{
			/*Lista*/
			m_TreeView = new Gtk.TreeView();
			m_ReservationList = new Gtk.ListStore(typeof(Domain.Reservation));
			Widgets.SimpleTreeViewColumn nameColumn = new Widgets.SimpleTreeViewColumn("Rezerwujący", "ParentClient.Name");
			Widgets.SimpleTreeViewColumn tableColumn = new Widgets.SimpleTreeViewColumn("Stolik", "ParentTable.Id");
			Widgets.SimpleTreeViewColumn dateColumn = new Widgets.SimpleTreeViewColumn("Data", "Date");
			Widgets.SimpleTreeViewColumn descColumn = new Widgets.SimpleTreeViewColumn("Dodatkowe informacje", "AdditionalNotes");
			m_ListScrWnd = new ScrolledWindow();
			m_ListScrWnd.Add(m_TreeView);
			m_TreeView.AppendColumn(dateColumn);
			m_TreeView.AppendColumn(nameColumn);
			m_TreeView.AppendColumn(tableColumn);
			m_TreeView.AppendColumn(descColumn);
			m_TreeView.Model = m_ReservationList;
			Refresh();
			
			/*Kalendarz: tydzień*/
			m_WeekCalendarScrWnd = new ScrolledWindow();
			m_WeekCalendar = new ReservationsWeekCalendar(m_CriteriaList, Context, this);
			m_WeekCalendarScrWnd.AddWithViewport(m_WeekCalendar);
			
			
			/*Kalendarz miesiąc*/   
			m_MonthCalendarScrWnd = new ScrolledWindow();
			m_MonthCalendar = new ReservationsMonthCalendar(m_CriteriaList, Context, this);
			m_MonthCalendarScrWnd.AddWithViewport(m_MonthCalendar);
			
			/*Przyciski na dole*/
			Widgets.ButtonsBar bar = new FusionPOS.Widgets.ButtonsBar();
			bar.AddLeftImageButton("Powrót", "arrow_left", GoBackClick);
			bar.AddRightImageButton("Usuń Rezerwacje", "delete", DeleteReservationClick);
			bar.AddRightImageButton("Edytuj Rezerwacje", "edit", EditReservationClick);
			bar.AddRightImageButton("Dodaj Rezerwacje", "new_order", AddReservationClick);
			
			
			
			/*Przyciski zmiany widoku*/
			Widgets.ButtonsBar upperBar = new Widgets.ButtonsBar();
			
			upperBar.AddLeftImageButton("Lista", "list", ShowListClick);
			upperBar.AddLeftImageButton("Tydzień", "calendar_week", ShowWeekClick);
			upperBar.AddLeftImageButton("Miesiąc", "calendar_month", ShowMonthClick); 
			
			
			/*Pakowanie*/
			Widgets.VBox vbox = new Widgets.VBox();
			vbox.PackStart(upperBar, false, false, 5);
			vbox.PackStart(m_ListScrWnd, true, true, 1);
			vbox.PackStart(m_WeekCalendarScrWnd, true, true, 1);
			vbox.PackStart(m_MonthCalendarScrWnd, true, true, 1);
			vbox.PackStart(bar, false, false, 1);
			
			
			
			Add(vbox);
			
			ShowAll();
			m_WeekCalendarScrWnd.Hide();
			m_MonthCalendarScrWnd.Hide();
		}
		
		private void GoBackClick(object o, EventArgs args)
		{
			Context.ApplicationWindow.GoBack(o, args);
		}
		
		private void ShowMonthClick(object o, EventArgs args)
		{
			m_WeekCalendarScrWnd.Hide();
			m_ListScrWnd.Hide();
			m_MonthCalendarScrWnd.Show();
		}
		
		private void ShowWeekClick(object o, EventArgs args)
		{
			m_ListScrWnd.Hide();
			m_MonthCalendarScrWnd.Hide();
			m_WeekCalendarScrWnd.Show();
		}
		private void ShowListClick(object o, EventArgs args)
		{
			m_WeekCalendarScrWnd.Hide();
			m_MonthCalendarScrWnd.Hide();
			m_ListScrWnd.Show();
		}
		
		private void DeleteReservationClick(object o, EventArgs args)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			m_TreeView.Selection.GetSelected(out model, out iter);
			Domain.Reservation reservation = (Domain.Reservation)model.GetValue(iter, 0);
			
			if(reservation == null)
				return;
			
			using (ISession session = Context.OpenSession()) {
				ITransaction tran = session.BeginTransaction();
					session.Delete(reservation);
				tran.Commit();
			}
			
			Refresh();
		}
		
		public void Refresh()
		{
			m_ReservationList.Clear();
			using (ISession session = Context.OpenSession()) {
					ICriteria criteria = session.CreateCriteria(typeof(Domain.Reservation));
					m_CriteriaList = criteria.List<Domain.Reservation>();
				foreach(Domain.Reservation res in m_CriteriaList)
				{
					m_ReservationList.AppendValues(res);
				}
			}
			
			if(m_WeekCalendar != null)
				m_WeekCalendar.ReRender();
			if(m_MonthCalendar != null)
				m_MonthCalendar.ReRender();
		}
		
		private void EditReservationClick(object o, EventArgs args)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			m_TreeView.Selection.GetSelected(out model, out iter);
			Domain.Reservation reservation = (Domain.Reservation)model.GetValue(iter, 0);
			
			if(reservation == null)
				return;
			Context.ApplicationWindow.LoadForm(typeof(AddReservationForm), new Refresher(Refresh), reservation.Id);
		}
		
		private void AddReservationClick(object o, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(AddReservationForm), new Refresher(Refresh));
		}
		
		public override void Load(params object[] args)
		{
			if(args.Length > 0)
			{
				using (ISession session = Context.OpenSession()) {
					m_Table = session.Get<Domain.Table>((long)args[0]);	
					m_ReservationList.AppendValues(m_Table.Reservations);
				}				
			}
			else
			{
				/*using (ISession session = Context.OpenSession()) {
					ICriteria criteria = session.CreateCriteria(typeof(Domain.Reservation));
					System.Collections.IList criteriaList = criteria.List();
					//m_ReservationList.AppendValues(criteriaList);
				}*/
			}
		}
		
		private ScrolledWindow m_WeekCalendarScrWnd;
		private ScrolledWindow m_MonthCalendarScrWnd;
		private ScrolledWindow m_ListScrWnd;
		private IList<Domain.Reservation> m_CriteriaList;
		private Gtk.ListStore m_ReservationList;
		private Gtk.TreeView m_TreeView;
		private Domain.Table m_Table;
		private ReservationsWeekCalendar m_WeekCalendar;
		private ReservationsMonthCalendar m_MonthCalendar;
	}
}
