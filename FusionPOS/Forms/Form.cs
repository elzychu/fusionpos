using System;
using Gtk;
using FusionPOS;

namespace FusionPOS
{
	public class Form: BaseForm
	{
		public Form(Context context): base()
		{
			m_Context = context;
		}
		
		protected Context Context
		{
			get { return m_Context; }
		}
		
		protected virtual bool NeedsRefresh
		{
			get { return false; }	
		}
		
		private Context m_Context;
	}
}

