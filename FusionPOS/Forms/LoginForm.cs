using System;
using System.Collections;
using Gdk;
using Gtk;
using Pango;
using NHibernate;
using NHibernate.Criterion;
using FusionPOS;
using FusionPOS.Widgets;
using FusionPOS.Domain;
using FusionPOS.Translations;

namespace FusionPOS
{
	public class LoginForm: Form
	{
		private Widgets.Label Introduction;
		private Entry PinEntry;
		private Gtk.Button[] Keys;
		
		public LoginForm(Context context): base(context)
		{
			Gtk.Alignment align = new Gtk.Alignment(0.5f, 0.5f, 0f, 0f);
			Gtk.Table layout = new Gtk.Table(5, 3, false);
			Keys = new Gtk.Button[12];
			
			Introduction = new Widgets.Label("LoginForm.EnterPinOrCard".Tr(), FontSettings.BiggerFont);
			PinEntry = new Entry();
			PinEntry.Name = FontSettings.BiggerFont;
			PinEntry.Visibility = false;
			layout.RowSpacing = 10;
			layout.ColumnSpacing = 10;
			layout.Attach(Introduction, 0, 3, 0, 1);
			layout.Attach(PinEntry, 0, 3, 1, 2);
			
			for (uint i = 0; i < 9; i++) {
				Keys[i] = new Widgets.Button((i+1).ToString(), FontSettings.BiggerFont, 120, 80);
				Keys[i].Clicked += new EventHandler(KeypadClicked);
				
				layout.Attach(Keys[i],
				   i % 3,
				   i % 3 + 1,
				   i / 3 + 2,
				   i / 3 + 3);
				
			}
			
			Keys[09] = new Widgets.Button("0", FontSettings.BiggerFont, 120, 80);
			Keys[09].Clicked += new EventHandler(KeypadClicked);
			layout.Attach(Keys[09], 0, 1, 5, 6);
			
			Keys[10] = Widgets.ImageButtonFactory.ImageButton("", "arrow_left_2");
			Keys[10].Clicked += new EventHandler(BackspaceClicked);
			layout.Attach(Keys[10], 1, 2, 5, 6);
			
			Keys[11] = Widgets.ImageButtonFactory.ImageButton("", "ok");
			Keys[11].Clicked += new EventHandler(LoginClicked);
			
			layout.Attach(Keys[11], 2, 3, 5, 6);
			align.Add(layout);
			
			Add(align);
			ShowAll();
		}
		
		private void KeypadClicked(object s, EventArgs args)
		{
			Widgets.Button button = (Widgets.Button)s;
			PinEntry.Text += button.Label;
		}
		
		private void BackspaceClicked(object s, EventArgs args)
		{
			if (PinEntry.Text.Length == 0)
				return;
			
			PinEntry.Text = PinEntry.Text.Remove(PinEntry.Text.Length - 1, 1);
		}

		private void LoginClicked(object s, EventArgs args)
		{
			using (ISession session = Context.OpenSession()) {
				User user = session.CreateCriteria<User>()
				    .Add(Expression.Eq("Pin", PinEntry.Text))
				    .SetMaxResults(1)
				    .UniqueResult<User>();
				
				if (user == null) {
					MessageBox.Popup("FusionPOS", "Nie ma takiego użytkownika");
					return;
				}

				Context.LoginUser(user);
				Context.ApplicationWindow.LoadForm(typeof(MainForm));
			}
		}
	}
}

