using System;
using NHibernate;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class ProductSheet: Form
	{
		public ProductSheet(Context context): base(context)
		{
			Gtk.VBox vbox = new Gtk.VBox();
			Gtk.HBox hbox = new Gtk.HBox();
			Gtk.VBox recipeBox = new Gtk.VBox();
			Gtk.Table dishInfo = new Gtk.Table(5, 2, false);
			Gtk.ScrolledWindow recipeScroll = new Gtk.ScrolledWindow();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			buttons.AddLeftButton("<", Context.ApplicationWindow.GoBack);
			
			m_DishName = new Widgets.Label("", FontSettings.BiggerFont);
			m_RecipeList = new Gtk.TreeView();
			m_RecipeList.AppendColumn(new Widgets.SimpleTreeViewColumn("Nazwa", "Ingredient.Name"));
			m_RecipeList.AppendColumn(new Widgets.SimpleTreeViewColumn("Ilość", "Amount"));
			m_RecipeStore = new Gtk.ListStore(typeof(RecipeIngredient));
			m_RecipeList.Model = m_RecipeStore;
			m_RecipeProtips = new Gtk.TextView();
			
			recipeScroll.Add(m_RecipeList);
			recipeBox.PackStartSimple(recipeScroll);
			recipeBox.PackStartSimple(m_RecipeProtips);
			hbox.PackStartSimple(dishInfo);
			hbox.PackStartSimple(recipeBox);
			vbox.PackStart(m_DishName, false, false, 0);
			vbox.PackStart(hbox, true, true, 0);
			vbox.PackStart(buttons, false, false, 0);
			Add(vbox);
			ShowAll();
		}
		
		public override void Load(params object[] args)
		{
			long dishId = (long)args[0];
			
			using (ISession session = Context.OpenSession()) {
				m_Dish = session.Get<Dish>(dishId);
				m_DishName.Text = m_Dish.Name;
				
				if (m_Dish.Recipe != null) {
					m_RecipeProtips.Buffer.Text = m_Dish.Recipe.Protips;
					m_RecipeProtips.Editable = false;
					
					foreach (RecipeIngredient i in m_Dish.Recipe.Ingredients)
						m_RecipeStore.AppendValues(i);
				}
			}
		}
		
		private Dish m_Dish;
		private Widgets.Label m_DishName;
		private Gtk.TextView m_RecipeProtips;
		private Gtk.TreeView m_RecipeList;
		private Gtk.ListStore m_RecipeStore;
	}
}

