using System;
using Gtk;
using NHibernate;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class OrderViewForm: Form
	{
		public OrderViewForm(Context context): base(context)
		{
			Gtk.VBox vbox = new Gtk.VBox();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			
			buttons.AddLeftButton("<", Context.ApplicationWindow.GoBack);

			vbox.PackStartSimple(buttons);
			Add(vbox);
			ShowAll();
		}

		public override void Load(params object[] args)
		{
			m_Order = args[0] as Order;
		}
		
		private Order m_Order;
	}
}

