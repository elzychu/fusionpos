using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;
using FusionPOS.Drivers;
using Gtk;
using NHibernate;
using NHibernate.Linq;
using FusionPOS;
using FusionPOS.Domain;
using FusionPOS.Widgets;
using Cairo;
using Shared;

namespace FusionPOS
{
	public class OrderButton: Gtk.Button
	{
		public OrderButton(Domain.Order order, Context ctx)
		{
			m_Context = ctx;
			m_Order = order;
			m_VBox = new Widgets.VBox();
			if(order.Destination != null) {
				if(order.Destination is Domain.DestinationDelivery)
					m_VBox.PackFill(new Widgets.Label("Dostawa"));
				if(order.Destination is Domain.DestinationNone)
					m_VBox.PackFill(new Widgets.Label("Na wynos"));
				if(order.Destination is Domain.DestinationOnSite)
					m_VBox.PackFill(new Widgets.Label(String.Format("Stolik {0}", 
					    (order.Destination as Domain.DestinationOnSite).Table.Number)));
					               
			}
			if(order.Waiter != null)
				m_VBox.PackStartSimple(new Widgets.Label(order.Waiter.Name));
			m_VBox.PackStartSimple(new Widgets.Label(order.Time.ToShortTimeString()));
			m_VBox.PackStartSimple(new Widgets.Label(order.FinalValue.ToString()));
			Clicked += delegate(object sender, EventArgs e) {
			//	ctx.ApplicationWindow.LoadForm(typeof(MakeOrderForm), order);	
				new OrderPerview(order, ctx);
			};
			StyleContext.AddClass("SmallFont");
			Add(m_VBox);
			ShowAll();
		}
		
		public Order GetOrder()
		{
			return m_Order;	
		}
		
		private Widgets.VBox m_VBox;
		private Context m_Context;
		private Domain.Order m_Order;
	}
	
	public class MainForm: Form
	{
		public MainForm(Context context): base(context)
		{			
			Gtk.Button newOrderButton = Widgets.ImageButtonFactory.ImageButton("Nowe zamówienie", "new_order");
			Gtk.Button shareOrderButton = Widgets.ImageButtonFactory.ImageButton("Podziel i połącz zamówienia", "share_orders");
			Gtk.Button reservationsButton = Widgets.ImageButtonFactory.ImageButton("Rezerwacje", "reservation");
			Gtk.Button pointsButton = Widgets.ImageButtonFactory.ImageButton("Punkty partnerskie", "points");
			Gtk.Button settingsButton = Widgets.ImageButtonFactory.ImageButton("Ustawienia", "configuration");
			Gtk.Button tablesButton = Widgets.ImageButtonFactory.ImageButton("Stoliki", "tables");
			Gtk.Button settlementButton = Widgets.ImageButtonFactory.ImageButton("Rozliczenia", "settlement");
			Gtk.Button inventaryStateButton = Widgets.ImageButtonFactory.ImageButton("Stany magazynowe", "inventory_state");
			Gtk.Button clientsButton =  Widgets.ImageButtonFactory.ImageButton("Klienci", "clients");
			Gtk.Button productsButton =  Widgets.ImageButtonFactory.ImageButton("Produkty i receptury", "products");
			Gtk.Button logoutButton =  Widgets.ImageButtonFactory.ImageButton("Wyloguj", "logout");
			
			newOrderButton.Clicked += MakeOrderClick;
			shareOrderButton.Clicked += (o, args) => Context.ApplicationWindow.LoadForm(typeof(DivideOrderForm));
			reservationsButton.Clicked += ShowReservationsClick;
			pointsButton.Clicked += (o, args) => new AddPointsDialog(Context);
			//settingsButton.Clicked += (o, args) => ProcessLauncher.Launch("SystemConfig");
			settingsButton.Clicked += (o, DoneArgs) => {
				new PinEntryBox(Context, (bool isAction) => {
					if(isAction)
					{
						ProcessLauncher.Launch("SystemConfig");
					}
				});
			};
			
			tablesButton.Clicked += ShowTablesClick;
			settlementButton.Clicked += SettlementClick;
			inventaryStateButton.Clicked += IngredientsClick;
			clientsButton.Clicked += ClientsClick;
			productsButton.Clicked += ProductsRecipesClick;
			logoutButton.Clicked += LogoutClick;
			
			uint i;
			Widgets.Table table = new Widgets.Table(5, 6, true);
			
			m_Orders = new Widgets.Table(6, 6, true);
			table.Attach(newOrderButton, 0, 1, 0, 1);
			table.Attach(shareOrderButton, 0, 1, 1, 2);
			table.Attach(reservationsButton, 0, 1, 2, 3);
			table.Attach(pointsButton, 0, 1, 3, 4);
			table.Attach(settingsButton, 0, 1, 4, 5);
			table.Attach(inventaryStateButton, 1, 2, 4, 5);
			table.Attach(tablesButton, 2, 3, 4, 5);
			table.Attach(settlementButton, 3, 4, 4, 5);
			table.Attach(clientsButton, 4, 5, 4, 5);
			table.Attach(productsButton, 5, 6, 4, 5);
			table.Attach(logoutButton, 6, 7, 4, 5);
			
			table.Attach(m_Orders, 1, 7, 0, 4);
			Add(table);
			ShowAll();
		}
		
		public override void OnShow()
		{
			ISession session = Context.OpenSession();
			m_Orders.Children.Each(i => m_Orders.Remove(i));
			
			Utils.AsyncInvoke<IEnumerable<Order>>(delegate() {
				return session.Query<Domain.Order>()
				    .Where(i => (i.Status != OrderStatus.Completed) && (i.Status != OrderStatus.Canceled))
				    .Take(30);
				}, delegate(IEnumerable<Order> result) {
					Application.Invoke(delegate(object sender, EventArgs e) {
						result.Each(i => AddOrder(i));
						session.Dispose();
					});
				});
			
			m_Orders.ShowAll();
		}
		
		private void AddOrder(Domain.Order order)
		{   
			uint i = (uint)m_Orders.Children.Length;
			m_Orders.Attach(new OrderButton(order, Context), 
			    i % 6, i % 6 + 1, 
			    i / 6, i / 6 + 1);
		}
			
		private void MakeOrderClick(object sender, EventArgs args)
		{
			Domain.Order order = new Domain.Order();
			order.Time = DateTime.Now;
			Context.ApplicationWindow.LoadForm(typeof(MakeOrderForm), order);	
		}
		
		private void ShowReservationsClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(ShowReservationForm));	
		}
		
		private void ProductsRecipesClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(ProductsListForm));
		}
		
		private void ClientsClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(ShowClientsForm));	
		}
		
		private void IngredientsClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(IngredientsListForm));	
		}		
		
		private void LogoutClick(object sender, EventArgs args)
		{
			Context.LogoutUser();	
		}
		
		private void ShowTablesClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(ShowRoomListForm));	
		}
		
		private void NopeClick(object sender, EventArgs args)
		{
			
		}
		
		private void SettlementClick(object sender, EventArgs args)
		{
			Context.ApplicationWindow.LoadForm(typeof(SettlementForm));
		}
		private Widgets.Table m_Orders;
	}
}
