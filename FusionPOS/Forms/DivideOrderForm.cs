using System;
using NHibernate;
using FusionPOS.Widgets;
using FusionPOS.Domain;
using System.Linq;
using System.Collections.Generic;
using NHibernate.Linq;

namespace FusionPOS
{
	public delegate void OrderSetter(long id);
	
	public class OrderPicker: TitleBox
	{
		public OrderPicker(OrderSetter setter, Context context): base()
		{
			m_TitleBar.Text = "Wybierz zamówienie";
			m_Setter = setter;
			IEnumerable<Order> orders;
			Widgets.VBox box = new Widgets.VBox();
			//Gtk.ScrolledWindow scrolled = new Gtk.ScrolledWindow();
			//box.PackStart(scrolled, true, true, 1);
			using (ISession session = context.OpenSession() )
			{
				orders = session.Query<Order>().Where(p => (int)p.Status < 3);
				
				Widgets.Table table = new Widgets.Table((uint)(orders.Count() / 6), 6, true);
				box.PackExpand(table);
				
				{
					int i=0;
					int max=orders.Count();
					foreach(Order order in orders)
					{
						OrderButton button = new OrderButton(order, context);
						table.Attach(button, (uint)(i%6), (uint)(i%6+1), (uint)((i-1)/6), (uint)((i-1)/6 + 1));
						button.Clicked += delegate(object sender, EventArgs e) {
							m_Setter(button.GetOrder().Id);
							this.Visible = false;
						};
						++i;
					}
					
					table.SetSizeRequest(900, (max/6)*120 );
				}
			}
			
			Gtk.Button cancel = Widgets.ImageButtonFactory.ImageButton("Anuluj", "cancel");
			cancel.Clicked += delegate(object sender, EventArgs e) {
				Visible = false;
			};
		
			box.PackFill(cancel);
			m_Placement.Add(box);
			ShowAll();
		}
		
		private OrderSetter m_Setter;
	}
	
	public class StaticValuePicker: VBox
	{
		public StaticValuePicker(): base()
		{		
			m_Val = new Widgets.TextBox();
			m_Val.IsEditable = false;
			m_Val.Text = "0";
			m_Value = 0;
			
			Widgets.Button inc = new Widgets.Button("+1");
			Widgets.Button deinc = new Widgets.Button("-1");
			Widgets.Button incFrac = new Widgets.Button("+1/x");
			Widgets.Button decFrac = new Widgets.Button("-1/x");
			Gtk.CheckButton count = new Gtk.CheckButton("Ilość");
			
			inc.Sensitive = false;
			incFrac.Sensitive = false;
			deinc.Sensitive = false;
			decFrac.Sensitive = false;
			
			m_Val.Sensitive = false;
			
			inc.Clicked += delegate(object sender, EventArgs e) {
				if( m_Value+1>m_Max)
				        return;
				
				m_Value++;
				m_Val.Text = m_Value.ToString("F2");
			};
			
			incFrac.Clicked += delegate(object sender, EventArgs e) {
				int total = (int)(Math.Floor(m_Value));
				decimal rest = m_Value - total;
				decimal temp = 0;
				
				if(rest < 0.25m)
					temp = total + 0.25m;
				
				if(rest < 0.33m && rest >= 0.25m)
					temp = total + 0.33m;
				
				if(rest < 0.5m && rest >= 0.33m)
					temp = total + 0.5m;
				
				if(rest < 0.66m && rest >= 0.5m)
					temp = total + 0.66m;
				
				if(rest < 0.75m && rest >= 0.66m)
					temp = total + 0.75m;
				
				if(rest >= 0.75m)
					temp = total + 1.0m;
				
				if(temp<=m_Max)
				{
					m_Value = temp;
					m_Val.Text = m_Value.ToString("F2");
				}
			};
			
			deinc.Clicked += delegate(object sender, EventArgs e) {
				if( m_Value-1<0)
				        return;
				
				m_Value--;
				m_Val.Text = m_Value.ToString("F2");
			};
			
			decFrac.Clicked += delegate(object sender, EventArgs e) {
				int total = (int)(Math.Floor(m_Value));
				decimal rest = m_Value - total;
				decimal temp = 0;
				
				if(rest < 0.25m)
					temp = total - 0.25m;
				if(rest < 0.33m && rest >= 0.25m)
					temp = total;
				if(rest < 0.5m && rest >= 0.33m)
					temp = total + 0.25m;
				if(rest < 0.66m && rest >= 0.5m)
					temp = total + 0.33m;
				if(rest < 0.75m && rest >= 0.66m)
					temp = total + 0.5m;
				if(rest >= 0.75m)
					temp = total + 0.66m;
				
				if(temp >= 0)
				{
					m_Value = temp;
					m_Val.Text = m_Value.ToString("F2");
				}
			};
			
			count.Clicked += delegate(object sender, EventArgs e) {
				Checked = (sender as Gtk.CheckButton).Active;
				
				if(Checked)
				{
					inc.Sensitive = true;
					deinc.Sensitive = true;
					incFrac.Sensitive = true;
					decFrac.Sensitive = true;
					m_Val.Sensitive = true;
				}
				else
				{
					inc.Sensitive = false;
					deinc.Sensitive = false;
					incFrac.Sensitive = false;
					decFrac.Sensitive = false;
					m_Val.Sensitive = false;
				}
			};
			
			PackStart(count, false, false, 1);
			
			HBox upper = new HBox();
			HBox lower = new HBox();
			
			upper.PackExpand(inc);
			upper.PackExpand(incFrac);
			lower.PackExpand(deinc);
			lower.PackExpand(decFrac);
			
			PackStart(upper, false, false, 1);
			PackStart(m_Val, false, false, 1);
			PackStart(lower, false, false, 1);
		}
		
		public decimal GetCount()
		{
			return m_Value;	
		}
		
		public void SetMaxValue(decimal max)
		{
			if(max>0)
				m_Max = max;	
			if(m_Value>max)
				m_Value = max;
			
			m_Val.Text = m_Value.ToString();
		}
		
		private Widgets.TextBox m_Val;
		private decimal m_Value;
		private decimal m_Max;
		public bool Checked;
	}
	
	public class DivideOrderForm: Form
	{
		public DivideOrderForm(Context context): base(context)
		{
			Widgets.VBox mainBox = new Widgets.VBox();
			Widgets.HBox upperBox = new Widgets.HBox();
			Widgets.VBox leftBill = new Widgets.VBox();
			Widgets.VBox rightBill = new Widgets.VBox();
			Widgets.VBox centerBar = new Widgets.VBox();
			
			Widgets.Label leftTitle = new Widgets.Label("Rachunek 1:");
			Widgets.Label rightTitle = new Widgets.Label("Rachunek 2:");
			
			Widgets.Table leftBillInfo = new Widgets.Table(3, 2, false);
			Widgets.Table rightBillInfo = new Widgets.Table(3, 2, false);
			
			Widgets.Label cliNameRghtLbl = new Widgets.Label("Nazwa klienta");
			Widgets.Label timeRghtLbl = new Widgets.Label("Czas zamówienia");
			Widgets.Label usrRghtLbl = new Widgets.Label("Kelner");
			
			Widgets.Label cliNameRghtLblVal = new Widgets.Label();
			Widgets.Label timeRghtLblVal = new Widgets.Label();
			Widgets.Label usrRghtLblVal = new Widgets.Label();
			
			Widgets.Label cliNameLftLblVal = new Widgets.Label();
			Widgets.Label timeLftLblVal = new Widgets.Label();
			Widgets.Label usrLftLblVal = new Widgets.Label();
			
			Widgets.Label cliNameLftLbl = new Widgets.Label("Nazwa klienta");
			Widgets.Label timeLftLbl = new Widgets.Label("Czas zamówienia");
			Widgets.Label usrLftLbl = new Widgets.Label("Kelner");
			
			leftBillInfo.Attach(cliNameLftLbl, 0, 1, 0, 1);
			leftBillInfo.Attach(timeLftLbl, 0, 1, 1, 2);
			leftBillInfo.Attach(usrLftLbl, 0, 1, 2, 3);
			leftBillInfo.Attach(cliNameLftLblVal, 1, 2, 0, 1);
			leftBillInfo.Attach(timeLftLblVal, 1, 2, 1, 2);
			leftBillInfo.Attach(usrLftLblVal, 1, 2, 2, 3);
			
			rightBillInfo.Attach(cliNameRghtLbl, 0, 1, 0, 1);
			rightBillInfo.Attach(timeRghtLbl, 0, 1, 1, 2);
			rightBillInfo.Attach(usrRghtLbl, 0, 1, 2, 3);
			rightBillInfo.Attach(cliNameRghtLblVal, 1, 2, 0, 1);
			rightBillInfo.Attach(timeRghtLblVal, 1, 2, 1, 2);
			rightBillInfo.Attach(usrRghtLblVal, 1, 2, 2, 3);
			
			Gtk.TreeView leftBillView = new Gtk.TreeView();
			Gtk.ListStore leftBillStore= new Gtk.ListStore(typeof(OrderDish));
			
			Widgets.SimpleTreeViewColumn nameLftCol = new Widgets.SimpleTreeViewColumn("Nazwa pozycji", "Dish.Name");
			Widgets.SimpleTreeViewColumn amountLftCol = new Widgets.SimpleTreeViewColumn("Ilość", "Amount");
			leftBillView.AppendColumn(nameLftCol);
			leftBillView.AppendColumn(amountLftCol);
			leftBillView.Model = leftBillStore;
			
			Widgets.Button takeFromExistingLeft = new Widgets.Button("Wybierz z istniejących");
			
			Gtk.ScrolledWindow leftWnd = new Gtk.ScrolledWindow();
			leftWnd.Add(leftBillView);
			
			Order orderA = null;
			Order orderB = null;
			
			bool isNew = false;
			
			
			takeFromExistingLeft.Clicked += delegate(object sender, EventArgs e) {
				new OrderPicker(delegate (long id) {
					using (ISession session = context.OpenSession())
					{
						orderA = session.Get<Order>(id);
						if((orderA != null && orderB != null) ? orderA.Id != orderB.Id : true)
							m_OrdersA = session.Query<OrderDish>().Where(p => p.Order.Id == id).ToList();
						else
						{
							MessageBox.Popup("FusionPOS", "Wybrano dwa te same zamówienia.");
							orderA = null;
							return;
						}
						cliNameLftLblVal.Text = orderA.Client.Name;
						timeLftLblVal.Text = orderA.Time.ToString("hh:mm");
						usrLftLblVal.Text = orderA.Waiter.Name;
						
						foreach(OrderDish dish in m_OrdersA)
							if(dish.Dish.Name == null) dish.Dish.Name = "Brak Nazwy";
						
						m_OrdersA.ForEach(p => leftBillStore.AppendValues(p));
						
					}
					
					leftBillInfo.Visible = true;
					leftWnd.Visible = true;
					takeFromExistingLeft.Visible = false;
				}, context);
			};
			
			
			
			Gtk.TreeView rightBillView = new Gtk.TreeView();
			Gtk.ListStore rightBillStore= new Gtk.ListStore(typeof(OrderDish));
			
			Gtk.ScrolledWindow rightWnd = new Gtk.ScrolledWindow();
			rightWnd.Add (rightBillView);
			
			//XXX co zrobic z modyfikacjami w nazwie?
			//Widgets.SimpleTreeViewColumn nameRghtCol = new Widgets.SimpleTreeViewColumn("Nazwa", "Id");
			//Widgets.SimpleTreeViewColumn amountRghtCol = new Widgets.SimpleTreeViewColumn("Ilość", "Amount");
			Widgets.SimpleTreeViewColumn nameRghtCol = new Widgets.SimpleTreeViewColumn("Nazwa", "Dish.Name");
			Widgets.SimpleTreeViewColumn amountRghtCol = new Widgets.SimpleTreeViewColumn("Ilość", "Amount");
			
			rightBillView.AppendColumn(nameRghtCol);
			rightBillView.AppendColumn(amountRghtCol);
			rightBillView.Model = rightBillStore;
			
			
			Widgets.Button takeFromExistingRight = new Widgets.Button("Wybierz z istniejących");
			Widgets.Button createNewOrder = new Widgets.Button("Utwórz  nowy rachunek");
			
			createNewOrder.Clicked += delegate(object sender, EventArgs e) {
				isNew = true;
				orderB = new Order();
				orderB.Entries = new List<OrderDish>();
				m_OrdersB = new List<OrderDish>();
				
				rightBillInfo.Visible = true;
				rightWnd.Visible = true;
				takeFromExistingRight.Visible = false;
				createNewOrder.Visible = false;

			};
			
			takeFromExistingRight.Clicked += delegate(object sender, EventArgs e) {
				new OrderPicker(delegate (long id) {
					using (ISession session = context.OpenSession())
					{
						orderB = session.Get<Order>(id);
						session.Query<Order>().FetchMany(o => o.Entries).ThenFetchMany(d => d.Modifications);
						if((orderA != null && orderB != null) ? orderA.Id != orderB.Id : true)
							m_OrdersB = session.Query<OrderDish>().Where(p => p.Order.Id == id).ToList();
						else
						{
							orderB = null;
							MessageBox.Popup("FusionPOS", "Wybrano dwa te same zamówienia.");
							return;
						}
						cliNameRghtLblVal.Text = orderB.Client.Name;
						timeRghtLblVal.Text = orderB.Time.ToString("hh:mm");
						usrRghtLblVal.Text = orderB.Waiter.Name;
						
						foreach(OrderDish dish in m_OrdersB)
							if(dish.Dish.Name == null) dish.Dish.Name = "Brak Nazwy";
						
						m_OrdersB.ForEach(p => rightBillStore.AppendValues(p));
					}
					
					rightBillInfo.Visible = true;
					rightWnd.Visible = true;
					takeFromExistingRight.Visible = false;
					createNewOrder.Visible = false;
				}, context);
			};
			

			StaticValuePicker valPicker = new StaticValuePicker();	
			Gtk.Button fromLeftToRight = Widgets.ImageButtonFactory.ImageButton("", "arrow_right_2");
			Gtk.Button fromRightToLeft = Widgets.ImageButtonFactory.ImageButton("", "arrow_left_2");
			fromLeftToRight.SetSizeRequest(0,0);
			fromRightToLeft.SetSizeRequest(0,0);
			
			fromLeftToRight.Clicked += delegate(object sender, EventArgs e) {
				OrderDish selectedA = GetCheckedValue(leftBillView);
				OrderDish selectedB = GetCheckedValue(rightBillView);
				
				if(selectedA == null)
					return;
				
				if(selectedB != null)
				{
					if (selectedA.Dish.Id == selectedB.Dish.Id)
					{
						m_OrdersB.First(p => p.Id == selectedB.Id).Amount += selectedA.Amount;
						m_OrdersA.Remove(selectedA);
						Refresh(leftBillStore, rightBillStore);
						return;
					}
				}
				
				if(valPicker.Checked && valPicker.GetCount()!=0)
				{
					OrderDish newDish = new OrderDish();
					newDish.Direction = selectedA.Direction;
					newDish.Dish = selectedA.Dish;
					//newDish.Id = 0;
					newDish.Parent = selectedA.Parent;
					
					using (ISession session = context.OpenSession())
					{
						session.Lock(selectedA, LockMode.None);
						newDish.Modifications = new List<OrderDishModification>();
						OrderDishModification [] tempArray = new OrderDishModification[selectedA.Modifications.Count];
						selectedA.Modifications.CopyTo(tempArray, 0);
						newDish.Modifications = new List<OrderDishModification>(tempArray.AsEnumerable());
					}
					
					newDish.Amount = valPicker.GetCount();
					newDish.Order = selectedA.Order;
					
					m_OrdersB.Add(newDish);
					OrderDish oldDish = m_OrdersA.First (p => p == selectedA);
					
					if(oldDish.Amount - valPicker.GetCount() >= 0)
						oldDish.Amount -= valPicker.GetCount();
					else
						return;
					
					if(oldDish.Amount <= 0 )
						m_OrdersA.Remove(oldDish);
					
					Refresh(leftBillStore, rightBillStore);
					return;
				}
				
				else if(valPicker.Checked && valPicker.GetCount()==0)
				{	
					return;
				}
				
				m_OrdersA.Remove(selectedA);
				m_OrdersB.Add(selectedA);
				
				Refresh(leftBillStore, rightBillStore);
			};
			
			fromRightToLeft.Clicked += delegate(object sender, EventArgs e) {
				OrderDish selectedA = GetCheckedValue(leftBillView);
				OrderDish selectedB = GetCheckedValue(rightBillView);
				if(selectedB == null)
					return;
				
				if(selectedA != null)
				{
					if (selectedA.Dish.Id == selectedB.Dish.Id)
					{
						m_OrdersA.First(p => p.Id == selectedA.Id).Amount += selectedB.Amount;
						m_OrdersB.Remove(selectedB);
						Refresh(leftBillStore, rightBillStore);
						return;
					}
				}
				
				if(valPicker.Checked && valPicker.GetCount()!=0)
				{
					OrderDish newDish = new OrderDish();
					newDish.Direction = selectedB.Direction;
					newDish.Dish = selectedB.Dish;
					newDish.Id = 0;
					newDish.Parent = selectedB.Parent;
					using (ISession session = context.OpenSession())
					{
						session.Lock(selectedB, LockMode.None);
						newDish.Modifications = new List<OrderDishModification>();
						OrderDishModification [] tempArray = new OrderDishModification[selectedB.Modifications.Count];
						selectedB.Modifications.CopyTo(tempArray, 0);
						newDish.Modifications = new List<OrderDishModification>(tempArray.AsEnumerable());
					}
					newDish.Amount = valPicker.GetCount();
					newDish.Order = selectedB.Order;
					
					m_OrdersA.Add(newDish);
					OrderDish oldDish = m_OrdersB.First (p => p.Id == selectedB.Id);
					
					if(oldDish.Amount - valPicker.GetCount() >= 0)
						oldDish.Amount -= valPicker.GetCount();
					else
						return;
					
					if(oldDish.Amount <=0)
						m_OrdersB.Remove(oldDish);	
					
					Refresh(leftBillStore, rightBillStore);
					return;
				} 
				else if(valPicker.Checked && valPicker.GetCount() == 0)
				{
					return;
				}
				
				m_OrdersB.Remove(selectedB);
				m_OrdersA.Add(selectedB);
				
				Refresh(leftBillStore, rightBillStore);
			};
			
			
			leftBillView.Selection.Changed += delegate(object sender, EventArgs e) {
				OrderDish dish = GetCheckedValue(leftBillView);
				if(dish == null)
					return;
				valPicker.SetMaxValue(dish.Amount);
			};
			
			rightBillView.Selection.Changed += delegate(object sender, EventArgs e) {
				OrderDish dish = GetCheckedValue(rightBillView);
				if(dish == null)
					return;
				valPicker.SetMaxValue(dish.Amount);
			};
			
			
			Widgets.ButtonsBar menuBar = new Widgets.ButtonsBar();
			menuBar.AddLeftImageButton("powrót", "arrow_left", delegate(object sender, EventArgs e) {
				Context.ApplicationWindow.GoBack(sender, e);	
			});
			
			menuBar.AddLeftImageButton("Zapisz zmiany", "save", delegate(object sender, EventArgs e) {
				using (ISession session = context.OpenSession())
				{
					using (ITransaction tx = session.BeginTransaction())
					{				
						if(orderA == null || orderB == null 
						   || m_OrdersA == null || m_OrdersB == null)
						return;
						
						orderA.Entries = m_OrdersA;
						orderB.Entries = m_OrdersB;
						
						//session.Lock(orderA, LockMode.None);
						//session.Lock(orderB, LockMode.None);
					
						
						//XXX powinno działać a nie działa
						if(orderA.Entries.Count != 0)
							session.SaveOrUpdate(orderA);
							//Context.UpdateOrder(orderA);
						else
							session.Delete(orderA);
							//Context.CancelOrder(orderA);
						
						if(isNew)
							Context.ApplicationWindow.LoadForm(typeof(OrderDestinationForm), orderB);
						else
						{
							if(orderB.Entries.Count != 0)
								session.SaveOrUpdate(orderB);
								//Context.UpdateOrder(orderB);
							else
								//Context.CancelOrder(orderB);
								session.Delete(orderB);
						}
						
						tx.Commit();
					}
				}
			});
			
			menuBar.AddLeftImageButton("Resetuj zmiany", "undo", delegate(object sender, EventArgs e) {
				using (ISession session = context.OpenSession())
				{
					if(orderA==null || orderB==null)
						return;
					m_OrdersA = session.Query<OrderDish>().Where(p => p.Order.Id == orderA.Id).ToList();
					foreach(OrderDish dish in m_OrdersA)
						if(dish.Dish.Name == null) dish.Dish.Name = "Brak Nazwy";
					m_OrdersB = session.Query<OrderDish>().Where(p => p.Order.Id == orderB.Id).ToList();
					foreach(OrderDish dish in m_OrdersB)
						if(dish.Dish.Name == null) dish.Dish.Name = "Brak Nazwy";
					Refresh(leftBillStore, rightBillStore);
				}
				
			});
			
			menuBar.AddRightImageButton("Połącz rachunki", "merge", delegate(object sender, EventArgs e) {
				using (ISession session = context.OpenSession())
				{
					using (ITransaction tx = session.BeginTransaction())
					{				
						if(orderA == null || orderB == null 
						   || m_OrdersA == null || m_OrdersB == null)
						return;
						
						m_OrdersB.ForEach(p => m_OrdersA.Add(p));
						m_OrdersA.ForEach(p => m_OrdersB.Remove(p));
						m_OrdersA.ForEach(p => p.Order = orderA); 
						orderA.Entries = m_OrdersA;
						orderB.Entries = new List<OrderDish>();
						//session.Lock(orderA, LockMode.None);
						session.SaveOrUpdate(orderA);
						session.Delete(orderB);
						
						//XXX to w ogóle nie działa a powinno
						//Context.UpdateOrder(orderA);
						//Context.CancelOrder(orderB);
						
						tx.Commit();
						context.ApplicationWindow.GoBack(sender, e);
					}
				}
				
			});
			
			menuBar.AddRightImageButton("Resetuj zaznaczenie", "delete_mark", delegate(object sender, EventArgs e) {
				leftBillView.Selection.UnselectAll();
				rightBillView.Selection.UnselectAll();
			});
			
			leftBill.PackFill(leftTitle);
			leftBill.PackFill(leftBillInfo);
			leftBill.PackExpand(leftWnd);
			leftBill.PackExpand(takeFromExistingLeft);
			
			centerBar.PackExpand(new Gtk.Separator(Gtk.Orientation.Vertical));
			centerBar.PackExpand(fromRightToLeft);
			centerBar.PackExpand(fromLeftToRight);
			centerBar.PackExpand(new Gtk.Separator(Gtk.Orientation.Vertical));
			centerBar.PackExpand(valPicker);
			centerBar.PackExpand(new Gtk.Separator(Gtk.Orientation.Vertical));
			
			rightBill.PackFill(rightTitle);
			rightBill.PackFill(rightBillInfo);
			rightBill.PackExpand(rightWnd);
			rightBill.PackExpand(takeFromExistingRight);
			rightBill.PackExpand(createNewOrder);
			
			upperBox.PackExpand(leftBill);
			upperBox.PackFill(centerBar);
			upperBox.PackExpand(rightBill);
			mainBox.PackExpand(upperBox);
			mainBox.PackFill (new Gtk.Separator(Gtk.Orientation.Horizontal));
			mainBox.PackFill(menuBar);
			Add(mainBox);
			ShowAll();
			
			leftBillInfo.Visible = false;
			leftWnd.Visible = false;
			rightBillInfo.Visible = false;
			rightWnd.Visible = false;
		}
		
		private OrderDish GetCheckedValue(Gtk.TreeView list)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			list.Selection.GetSelected(out model, out iter);
			return (OrderDish)model.GetValue(iter, 0);	
		}
		
		public void Refresh(Gtk.ListStore a, Gtk.ListStore b)
		{
			a.Clear();
			m_OrdersA.ForEach(p => a.AppendValues(p));
			
			b.Clear();
			m_OrdersB.ForEach(p => b.AppendValues(p));
		}
		
		private IList<OrderDish> m_OrdersA;
		private IList<OrderDish> m_OrdersB;
	}
}

