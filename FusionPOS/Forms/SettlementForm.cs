using System;
using FusionPOS.Widgets;
using NHibernate;
using Gtk;
using FusionPOS.Domain;
using System.Collections.Generic;

namespace FusionPOS
{
	public class SettlementUserForm: TitleBox
	{
		public SettlementUserForm(string title, ValueSetterFloat setter): base()
		{ 
			/* XXX brzydkie */
			m_TitleBar.Text = title;
			Widgets.Table table = new Widgets.Table(15, 2, false);
			Widgets.Label lbl10gr = new Widgets.Label("10 gr");
			Widgets.Label lbl20gr = new Widgets.Label("20 gr");
			Widgets.Label lbl50gr = new Widgets.Label("50 gr");
			Widgets.Label lbl1zl = new Widgets.Label("1 PLN");
			Widgets.Label lbl2zl = new Widgets.Label("2 PLN");
			Widgets.Label lbl5zl = new Widgets.Label("5 PLN");
			Widgets.Label lbl10zl = new Widgets.Label("10 PLN");
			Widgets.Label lbl20zl = new Widgets.Label("20 PLN");
			Widgets.Label lbl50zl = new Widgets.Label("50 PLN");
			Widgets.Label lbl100zl = new Widgets.Label("100 PLN");
			Widgets.Label lbl200zl = new Widgets.Label("200 PLN");
			Widgets.Label lblbon = new Widgets.Label("Bony");
			Widgets.Label lblcards = new Widgets.Label("Karty");
			
			Widgets.ValueEntry ent10gr = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent20gr = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent50gr = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent1zl = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent2zl = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent5zl = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent10zl = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent20zl = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent50zl = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent100zl = new Widgets.ValueEntry(0);
			Widgets.ValueEntry ent200zl = new Widgets.ValueEntry(0);
			
			Widgets.ValueEntry bon = new Widgets.ValueEntry(0);
			Widgets.ValueEntry cards = new Widgets.ValueEntry(0);
			
			table.Attach(lbl10gr, 0, 1, 0, 1);
			table.Attach(lbl20gr, 0, 1, 1, 2);
			table.Attach(lbl50gr, 0, 1, 2, 3);
			table.Attach(lbl1zl, 0, 1, 3, 4);
			table.Attach(lbl2zl, 0, 1, 5, 6);
			table.Attach(lbl5zl, 0, 1, 6, 7);
			table.Attach(lbl10zl, 0, 1, 7, 8);
			table.Attach(lbl20zl, 0, 1, 8, 9);
			table.Attach(lbl50zl, 0, 1, 9, 10);
			table.Attach(lbl100zl, 0, 1, 10, 11);
			table.Attach(lbl200zl, 0, 1, 11, 12);
			table.Attach(lblbon, 0, 1, 12, 13);
			table.Attach(lblcards, 0, 1, 13, 14);
			
			table.Attach(ent10gr, 1, 2, 0, 1);
			table.Attach(ent20gr, 1, 2, 1, 2);
			table.Attach(ent50gr, 1, 2, 2, 3);
			table.Attach(ent1zl, 1, 2, 3, 4);
			table.Attach(ent2zl, 1, 2, 5, 6);
			table.Attach(ent5zl, 1, 2, 6, 7);
			table.Attach(ent10zl, 1, 2, 7, 8);
			table.Attach(ent20zl, 1, 2, 8, 9);
			table.Attach(ent50zl, 1, 2, 9, 10);
			table.Attach(ent100zl, 1, 2, 10, 11);
			table.Attach(ent200zl, 1, 2, 11, 12);
			table.Attach (bon, 1, 2, 12, 13);
			table.Attach(cards, 1, 2, 13, 14);
					
			m_Placement.Add(table);
			AddButton("OK", delegate(object sender, EventArgs e) {
				decimal suma = 0.1m *ent10gr.GetFloatValue() + 0.2m *ent20gr.GetFloatValue() +
					0.5m *ent50gr.GetFloatValue() + ent1zl.GetFloatValue() +
						2m *ent2zl.GetFloatValue() + 5m *ent5zl.GetFloatValue() +
						10m *ent10zl.GetFloatValue() + 20m*ent20zl.GetFloatValue() +
						50m *ent50zl.GetFloatValue() + 100m *ent100zl.GetFloatValue() +
						200m *ent200zl.GetFloatValue();
				
				setter(suma);
				Hide();
			});
			AddButton("Anuluj", delegate(object sender, EventArgs e) {
				Hide();
			});
			
			SetSizeRequest(280, 480);
			ShowAll();
		}
	}
	
	public class SettlementInfo:Gtk.Table {
		
		public SettlementInfo(Context context): base(4, 2, false) {
			WindowLabel name = new WindowLabel("Ostatnie rozliczenie", "Inverted");
			Settlement settlement;
			using (ISession ses = context.OpenSession())
			{
				settlement = context.GetCurrentSettlement();
				Widgets.Label created = new Widgets.Label(String.Format("{0}, {1}", 
					settlement.From.ToString("dd-MM-yyyy"),
				        settlement.From.ToString("hh:mm")));
				Widgets.Label createdDate = new Widgets.Label("Rozpoczęte: ");
				Widgets.Label user = new Widgets.Label("Utworzone przez: ");
				Widgets.Label userView = new Widgets.Label(settlement.Owner != null ?
					settlement.Owner.Name : "-----");
				
				Gtk.TreeView unsettledUsers = new Gtk.TreeView();
				Gtk.ListStore unsettledUsersStore = new Gtk.ListStore(typeof(string));
				Gtk.TreeViewColumn nameCol = new Gtk.TreeViewColumn();
				nameCol.Title = "Nazwa";
				Gtk.CellRendererText crta = new Gtk.CellRendererText();
				nameCol.PackStart(crta, false);
				nameCol.AddAttribute(crta, "text", 0);
				nameCol.SetCellDataFunc(crta, delegate(Gtk.TreeViewColumn tree_column, 
				                                        Gtk.CellRenderer cell, 
				                                        Gtk.TreeModel model, 
				                                        Gtk.TreeIter iter) {
	    	    	                (cell as Gtk.CellRendererText).Text = (string)model.GetValue(iter, 0);				
				});
				
				unsettledUsers.AppendColumn(nameCol);
				unsettledUsers.Model = unsettledUsersStore;
				
				unsettledUsersStore.AppendValues("Wczytywanie...");
				IList<User> users = new List<User>();
				
				Utils.AsyncInvoke<IList<User>>(delegate() {
					return context.GetUnsettledUsers();
				}, delegate(IList<User> result) {
					Application.Invoke(delegate(object sender, EventArgs e) {	
						unsettledUsersStore.Clear();
						if (result.Count == 0)
							unsettledUsersStore.AppendValues("Wszyscy kelnerzy rozliczeni");
						else
							result.Each(p => unsettledUsersStore.AppendValues(p.Name));
					});
				});
				
				Widgets.Label status = new Widgets.Label("Status: ");
				Widgets.Label statusView = new Widgets.Label(settlement.Status.ToString());
				Attach(name, 0, 2, 0, 1);
				Attach(createdDate, 0, 1, 1, 2);
				Attach(created, 1, 2, 1, 2);
				Attach(user, 0, 1, 2, 3);
				Attach(userView, 1, 2, 2, 3);
				Attach(status, 0, 1, 3, 4);
				Attach(statusView, 1, 2, 3, 4);
				Attach(new WindowLabel("Nierozliczeni kelnerzy", "Inverted"), 0, 2, 4, 5);
				Attach (unsettledUsers, 0, 2, 5, 6);
			}
		}
		
		
	}
	
	public class SettlementForm: Form
	{
		public SettlementForm (Context context): base(context)
		{
			Widgets.Button settlementUserButton = new Widgets.Button("Rozliczenie kelnera");
			Widgets.Button settlementTeamButton = new Widgets.Button("Zamknij zmianę");
			Widgets.Button settlementStornButton = new Widgets.Button("Rozliczenie storn");
			Widgets.Button checkinButton = new Widgets.Button("Wpłata");
			Widgets.HBox hbox = new Widgets.HBox();
			SettlementInfo infoSheet = new SettlementInfo(context);
			
			ButtonsBar bar = new ButtonsBar();			
			
			bar.AddLeftImageButton("Powrót", "arrow_left", delegate(object sender, EventArgs args){
				context.ApplicationWindow.GoBack(sender, args);
			});
			
			settlementUserButton.Clicked += delegate(object sender, EventArgs e) {
				SettlementUserForm form = new SettlementUserForm("Rozliczenie kelnera", context.SaveWaiterSettlement);
			};
			
			settlementTeamButton.Clicked += delegate(object sender, EventArgs e) {
				if (MessageBox.Popup("FusionPOS", "Czy napewno chcesz zamknąć zmianę?") == MessageBox.ResultType.OK)
					Context.CloseSettlement();
			};
			
			checkinButton.Clicked += delegate(object sender, EventArgs e) {
				Context.DoPayment(Widgets.ValuePicker.GetFloatValue("Wpłata"));
			};
			
			Gtk.Table table = new Gtk.Table(6, 5, false);
			Gtk.Table table2 = new Gtk.Table(1, 1, false);
			Widgets.VBox box = new Widgets.VBox();
			
			table.Attach(settlementUserButton, 0, 2, 0, 1);
			table.Attach(settlementTeamButton, 0, 2, 1, 2);
			table.Attach(settlementStornButton, 0, 2, 2, 3);
			table.Attach(checkinButton, 0, 2, 4, 5);
			
			Widgets.VBox leftBox = new Widgets.VBox();
			
			leftBox.PackFill(infoSheet);
			leftBox.PackExpand(table2);
			hbox.PackExpand(table);
			hbox.PackExpand(leftBox);
			box.PackExpand(hbox);
			box.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			box.PackFill(bar);
			
			Add (box);
			ShowAll();
		}
	}
	
	
}

