using System;
using System.Linq;
using NHibernate;
using FusionPOS.Domain;
using FusionPOS.Widgets;
using Gtk;

namespace FusionPOS
{
	public class EditClientForm: Form
	{
		public EditClientForm (Context context): base(context)
		{
			Widgets.FormTable table = new Widgets.FormTable(6);
			m_Buttons = new Widgets.ButtonsBar();
			m_Buttons.AddLeftImageButton("Powrót", "arrow_left", BackClick);
			m_Buttons.AddRightImageButton("Zapisz zmiany", "save", SaveClientClick);
			
			m_Name = new Widgets.TextBox();
			m_Address = new Widgets.TextBox();
			m_Phone = new Widgets.TextBox();
			m_NIP = new Widgets.TextBox();
			m_NIP.Sensitive = false;
			m_Description = new Widgets.TextView();
			m_Company = new Gtk.CheckButton("NIP");
			m_Company.Clicked += (sender, e) => m_NIP.Sensitive = m_Company.Active;
			m_RebateLimitActive = new Gtk.CheckButton("Maksymalne zadłużenie");
			m_RebateLimitActive.Clicked += (sender, e) => m_VEntry.Sensitive = m_RebateLimitActive.Active;
			m_VEntry = new Widgets.ValueEntry();
			m_VEntry.Sensitive = false;
			
			table.PackElement(new Widgets.Label("Nazwa"), m_Name, 0);
			table.PackElement(new Widgets.Label("Adres"), m_Address, 1);
			table.PackElement(new Widgets.Label("Telefon"), m_Phone, 2);
			table.PackElement(m_Company, m_NIP, 3);
			table.PackElement(m_RebateLimitActive, m_VEntry, 4);
			table.PackElement(new Widgets.Label("Opis"), m_Description, 5, true);
			


			
			//table.Attach(m_Description, 1, 2, 4, 5, AttachOptions.Expand | AttachOptions.Fill, AttachOptions.Expand | AttachOptions.Fill, 0, 0);
			
			Widgets.VBox vbox = new Widgets.VBox(false, 0);
			vbox.PackExpand(table);
			vbox.PackFill(m_Buttons);
			
			Add(vbox);
			ShowAll();
		}
		
		public override void Load(params object[] args)
		{	
			if(args.Length > 1)
			{
				using (ISession session = Context.OpenSession()) {
					m_Client = session.Get<Client>((long)args[1]);	
				}
				m_Name.Text = m_Client.Name;
				m_Address.Text = m_Client.Address;
				m_Phone.Text = m_Client.PhoneNumber;
				m_NIP.Text = m_Client.NIP;
				
				m_VEntry.SetValue(m_Client.MaxCredit);
				m_RebateLimitActive.Active = (m_Client.MaxCredit > 0);
				m_Description.Text = m_Client.Description;
				m_Company.Active = m_Client.NIP != null;
				
				if(m_Client.MaxCredit>0)
				{
					m_VEntry.Text = m_Client.MaxCredit.ToString();
					m_RebateLimitActive.Sensitive = true;
				}
			}
			else
			{
				m_Client = new Client();	
			}
			
			if(args.Length > 0)
			{
				m_Refresher = (Refresher)args[0];	
			}
		}
		
		private void BackClick(object sender, EventArgs args)
		{	
			Context.ApplicationWindow.GoBack(sender, args);	
		}
		
		private void SaveClientClick(object sender, EventArgs args)
		{
			if(m_Client == null)
				return;
			
			m_Client.Name = m_Name.Text;
			m_Client.Address = m_Address.Text;
			m_Client.PhoneNumber = m_Phone.Text;
			m_Client.Description = m_Description.Text;
			m_Client.NIP = m_NIP.Text;
			m_Client.MaxCredit = m_RebateLimitActive.Active ? m_VEntry.GetIntValue() : 0;
			
			if(m_Client.Id == 0)
			{
				using (ISession session = Context.OpenSession()) {
					ITransaction tran = session.BeginTransaction();
						session.Save(m_Client);
					tran.Commit();
				}
			}
			else
			{
				using (ISession session = Context.OpenSession()) {
					ITransaction tran = session.BeginTransaction();
						session.Update(m_Client);
					tran.Commit();
				}	
			}	
			
			m_Refresher();
			Context.ApplicationWindow.GoBack(sender, args);
		}

		private Client m_Client;
		private Widgets.ValueEntry m_VEntry;
		private Widgets.ButtonsBar m_Buttons;
		private Widgets.TextBox m_Name;
		private Widgets.TextBox m_Address;
		private Widgets.TextBox m_Phone;
		private Widgets.TextBox m_NIP;
		private Widgets.TextView m_Description;
		private Gtk.CheckButton m_RebateLimitActive;
		private Gtk.CheckButton m_Company;
		private Refresher m_Refresher;
	}
}

