using System;
using System.IO;
using WebKit;
using FusionPOS.Domain;
using NHibernate;
using NHibernate.Linq;
using System.Linq;
using System.Collections.Generic;

namespace FusionPOS
{
	public class PrintDialog: TitleBox
	{
		public PrintDialog(Context ctx, OutgoingInvoice oi): base()
		{ 
			
			Widgets.VBox vbox = new Widgets.VBox();
			Title = "Wybierz drukarkę";
			using(ISession ses = ctx.OpenSession())
			{
				IEnumerable<Kitchen> printers = ses.Query<Kitchen>();
				foreach(Kitchen printer in printers)
				{
					Widgets.Button button = new Widgets.Button(printer.Name);
					button.Clicked += delegate(object sender, EventArgs e) {
						ctx.PrintInvoice(printer, oi);
						Hide ();
					};
					
					vbox.PackFill(button);
				}
			}
			
			Gtk.Button cancel = Widgets.ImageButtonFactory.ImageButton("Anuluj", "cancel");
			cancel.Clicked += delegate(object sender, EventArgs e) {
				Hide ();
			};
			
			m_ButtonsBox.PackStart(cancel, false, false, 0);
			
			m_Placement.Add(vbox);
			ShowAll();
		}
	}
	public class OrderInvoiceForm: Form
	{
		public OrderInvoiceForm(Context ctx): base(ctx)
		{	
			Widgets.VBox vbox = new Widgets.VBox();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			m_Browser = new WebView();
			
			buttons.AddLeftImageButton("Powrót", "arrow_left", Context.ApplicationWindow.GoBack);
			buttons.AddLeftImageButton("Drukuj", "print", delegate(object sender, EventArgs e) {
				PrintDialog dialog = new PrintDialog(ctx, m_OutgoingInvoice);	
			});
			Gtk.ScrolledWindow srlwnd = new Gtk.ScrolledWindow();
			srlwnd.Add(m_Browser);
			vbox.PackExpand(srlwnd);
			vbox.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			vbox.PackFill(buttons);
			Add(vbox);
			ShowAll();
		}
		
		public override void Load (params object[] args)
		{
			if (args[0] is Order) {
				StringWriter writer = new StringWriter();
				VatInvoice invoice = new VatInvoice((args[0] as Order).Invoice);
				
				using(ISession ses = Context.OpenSession())
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						ses.Save((args[0] as Order).Invoice);
					}
				}
				
				Context.UpdateOrder((args[0] as Order), false);
				
				m_OutgoingInvoice = (args[0] as Order).Invoice;
				invoice.m_Order = (args[0] as Order);
				invoice.GenerateHTML(writer);
				m_Browser.LoadString(writer.ToString(), "text/html", "utf-8", "huj");
			}
		}
		
		private WebView m_Browser;
		private OutgoingInvoice m_OutgoingInvoice;
	}
}

