using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gtk;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class OrderItem: Gtk.Frame
	{
		public OrderItem(OrderDish item, MakeOrderForm parent, Context context, Order order)
		{
			m_Item = item;
			
			Widgets.Table mainItem = new Widgets.Table(2, 4, false);
			Widgets.Button modify = new Widgets.Button(Stock.Edit, IconSize.Button, 60, 60);
			Widgets.Button delete = new Widgets.Button(Stock.Delete, IconSize.Button, 60, 60);
			m_Content = new Widgets.VBox();
			m_DishName = new Widgets.Label(item.Dish.Name);
			m_Amount = new Widgets.Label();
			m_Value = new Widgets.Label();
			m_Value.SetAlignment(0.0f, 0.5f);
				
			//Pizza
			modify.Clicked += delegate(object sender, EventArgs e) {
				if (item.Dish.IsPizza && item.Amount == 1)
				{
					ShareOrder sordr = delegate (IList<OrderDish> dishes) {
						order.Entries.Remove(item);
						foreach(OrderDish dish in dishes)
						{
							order.Entries.Add (dish);
							dish.Order = order;
							(Parent as Gtk.VBox).PackStart(new OrderItem(dish, parent, context, order),
							    false, false, 0);
						}
						
						(Parent as Gtk.Container).Children.Each(i => (i as OrderItem).Reload());
						(Parent as Gtk.VBox).Remove(this);
					};					
					context.ApplicationWindow.LoadForm(typeof(PizzaEditForm), sordr, item);
				}
				else
					parent.LoadOrderDish(item, this);
			};
			
			delete.Clicked += (sender, e) => {
				order.Entries.Remove(item);
				(Parent as Gtk.Container).Remove(this);
			};
			
			mainItem.Attach(m_DishName, 0, 1, 0, 1);
			mainItem.Attach(m_Amount, 1, 2, 0, 1);
			mainItem.Attach(m_Value, 0, 2, 1, 2);
			mainItem.Attach(modify, 2, 3, 0, 2, AttachOptions.Fill, AttachOptions.Fill, 0, 0);
			mainItem.Attach(delete, 3, 4, 0, 2, AttachOptions.Fill, AttachOptions.Fill, 0, 0);
			m_Content.PackStart(mainItem, false, true, 0);
			m_Content.SetSizeRequest(-1, 80);
			
			StyleContext.AddClass("OrderItem");
			Add(m_Content);
			Reload();
			ShowAll();
		}
		
		public void Reload()
		{
			m_Amount.Text = m_Item.Amount.ToString();
			m_Value.Text = m_Item.ValueGross.ToString();
			m_Content.Children.Skip(1).Each(w => m_Content.Remove(w));
			
			foreach (OrderDishModification child in m_Item.Modifications) {
				Gtk.Frame wrapper = new Gtk.Frame();
				Gtk.HBox childItem = new Gtk.HBox(false, 5);
				wrapper.StyleContext.AddClass("Modification");
				childItem.SetSizeRequest(-1, 30);
				childItem.PackStartSimple(new Widgets.Label(child.Amount.ToString()));
				childItem.PackStartSimple(new Widgets.Label(child.Ingredient.Component.Name));
				wrapper.Add(childItem);
				m_Content.PackStart(wrapper, false, true, 0);
			}
			
			SetSizeRequest(-1, 80 + m_Item.Modifications.Count * 30);
			
			ShowAll();
		}
		
		public OrderDish Item
		{
			get { return m_Item; }
		}
		
		private Widgets.Label m_DishName;
		private Widgets.Label m_Amount;
		private Widgets.Label m_Value;
		private Widgets.VBox m_Content;
		private OrderDish m_Item;
	}
	
	public class DishEditPanel: Widgets.VBox
	{
		class TableItem: Widgets.Frame
		{
			public TableItem(RecipeIngredient ingredient, OrderDish orderDish, OrderItem orderItem)
			{
				m_Ingredient = ingredient;
				m_OrderDish = orderDish;
				m_OrderItem = orderItem;
				
				Widgets.HBox hbox = new Widgets.HBox(false, 0);
				Widgets.VBox buttons = new Widgets.VBox(true, 2);
				Widgets.Button plus = new Widgets.Button(Stock.Add, IconSize.Button);
				Gtk.Button minus = new Widgets.Button(Stock.Remove, IconSize.Button);
				
				plus.Clicked += PlusClicked;
				minus.Clicked += MinusClicked;
				
				m_Amount = new Widgets.Label(ingredient.Optional ? "0" : "1"); // XXX
				
				buttons.PackStartSimple(plus);
				buttons.PackStartSimple(minus);
				hbox.PackStart(new Widgets.Label(ingredient.Component.Name, "SmallFont"), true, true, 0);
				hbox.PackStart(m_Amount, false, true, 0);
				hbox.PackStart(buttons, false, true, 0);
				StyleContext.AddClass("ModificationItem");
				Add(hbox);
				ShowAll();
			}
		
			private void PlusClicked(object s, EventArgs a)
			{
				OrderDishModification existing = m_OrderDish.Modifications.FirstOrDefault(i => i.Ingredient == m_Ingredient);
				
				if (existing == null) {
					OrderDishModification mod = new OrderDishModification();
					mod.Target = m_OrderDish;
					mod.Ingredient = m_Ingredient;
					mod.Amount = 1;
					m_OrderDish.Modifications.Add(mod);
					m_Amount.Text = mod.Amount.ToString();
				} else {
					existing.Amount++;
					m_Amount.Text = existing.Amount.ToString();
					if (existing.Amount == 0)
						m_OrderDish.Modifications.Remove(existing);
				}
	
				m_OrderItem.Reload();
			}
			
			private void MinusClicked(object s, EventArgs a)
			{
				OrderDishModification existing = m_OrderDish.Modifications.FirstOrDefault(i => i.Ingredient == m_Ingredient);
				
				if (existing == null) {
					OrderDishModification mod = new OrderDishModification();
					mod.Target = m_OrderDish;
					mod.Ingredient = m_Ingredient;
					mod.Amount = -1;
					m_OrderDish.Modifications.Add(mod);
					m_Amount.Text = mod.Amount.ToString();
				} else {
					existing.Amount--;
					m_Amount.Text = existing.Amount.ToString();
					if (existing.Amount == 0)
						m_OrderDish.Modifications.Remove(existing);
				}
				
				m_OrderItem.Reload();
			}
			
			private Widgets.Label m_Amount;
			private RecipeIngredient m_Ingredient;
			private OrderDish m_OrderDish;
			private OrderItem m_OrderItem;
		}
		
		public DishEditPanel(ISession ses, MakeOrderForm parent): base(false, 5)
		{	
			Gtk.HBox top = new Gtk.HBox(false, 2);
			Widgets.Button back = new Widgets.Button(Stock.GoBack, IconSize.Button, 60, -1);
			Widgets.Button plus = new Widgets.Button(Stock.Add, IconSize.Button, 60, -1);
			Widgets.Button minus = new Widgets.Button(Stock.Remove, IconSize.Button, 60, -1);
			Widgets.Button delete = new Widgets.Button(Stock.Delete, IconSize.Button, 60, -1);
			m_Session = ses;
			
			back.Clicked += (sender, e) => {
				parent.ShowMenu();
			};
			
			plus.Clicked += (sender, e) => {
				m_OrderDish.Amount++;
				m_OrderItem.Reload();
				Widgets.DynamicLabel.RefreshAll(this);
			};
			
			minus.Clicked += (sender, e) => {
				if (m_OrderDish.Amount <= 1)
					return;
				
				m_OrderDish.Amount--;
				m_OrderItem.Reload();
				Widgets.DynamicLabel.RefreshAll(this);
			};
			
			delete.Clicked += (sender, e) => {
				m_OrderDish.Order.Entries.Remove(m_OrderDish);
				(m_OrderItem.Parent as Gtk.Container).Remove(m_OrderItem);
				parent.ShowMenu();
			};
			m_DishName = new Widgets.DynamicLabel(delegate () {
				return m_OrderDish != null ?
				    string.Format("{0} (ilość: {1})", m_OrderDish.Dish.Name, m_OrderDish.Amount) :
				    "";
			});
			m_IngredientsTable = new Widgets.BrickTable(6, 6);
			top.SetSizeRequest(-1, 80);
			top.PackStart(back, false, true, 0);
			top.PackStart(m_DishName, true, true, 0);
			top.PackStart(plus, false, true ,0);
			top.PackStart(minus, false, true, 0);
			top.PackStart(delete, false, true, 0);
			PackStart(top, false, true, 0);
			PackStart(new Widgets.WindowLabel("Składniki:", "Inverted", -1, 30), false, true, 0);
			PackStart(m_IngredientsTable, true, true, 0);
		}
		
		public void Load(OrderDish item, OrderItem orderItem)
		{
			m_OrderDish = item;
			m_OrderItem = orderItem;

			m_Session.Lock(item.Dish.Recipe, LockMode.None);
			m_IngredientsTable.ClearBricks();
			
			foreach (RecipeIngredient i in item.Dish.Recipe.Ingredients) {
				if( i is IDeletableDomainObject ? (i as IDeletableDomainObject).Deleted : false)
					continue;
				m_Session.Lock(i.Component, LockMode.None);
				TableItem t = new TableItem(i, m_OrderDish, orderItem);
				m_IngredientsTable.AddBrick(t);
			}
			
			Widgets.DynamicLabel.RefreshAll(this.Parent as Gtk.Container);
			
			ShowAll();
		}
		
		private OrderItem m_OrderItem;
		private OrderDish m_OrderDish;
		private Widgets.DynamicLabel m_DishName;
		private Widgets.BrickTable m_IngredientsTable;
		private ISession m_Session;
	}
	
	public class DishesPanel: Gtk.VBox
	{
		public event EventHandler DishButtonClicked;
		
		public DishesPanel(ISession session, MakeOrderForm parent): base(false, 5)
		{
			m_Session = session;
			m_Table = new Widgets.BrickTable(8, 8);
			m_Table.StyleContext.AddClass("MenuTable");
			m_CategoryLabel = new Widgets.WindowLabel("huj", "Inverted");
			PackStart(m_CategoryLabel, false, true, 0);
			PackStart(m_Table, true, true, 0);
		}
		
		public void LoadCategory(DishCategory category)
		{
			m_Session.Lock(category, LockMode.None);
			
			m_Table.ClearBricks();
			m_CategoryLabel.Text = category.Name;
			
			if (category.Id != DishCategory.RootCategory) {
				Widgets.Button button = new Widgets.Button("<", "Category", true);
				button.Data.Add("category", category.Parent);
				button.Clicked += CategoryButtonClick;
				m_Table.AddBrick(button);
			}
			
			foreach (DishCategory child in category.Children) {
				if(child.Deleted)
					continue;
				Widgets.Button button = new Widgets.Button(child.Name, "Category", true);
				button.Clicked += CategoryButtonClick;
				button.Data.Add("category", child);
				// XXX button.ModifyBg(StateType.Normal, new Gdk.Color(0xaa, 0xaa, 0xaa));
				//button.SetWrap(5);
				
				m_Table.AddBrick(button);
			}
			
			foreach (Dish dish in category.Dishes) {
				if(dish.Deleted)
					continue;
				Widgets.DishBrick button = new Widgets.DishBrick(dish);
				button.Clicked += delegate(object sender, EventArgs e) {
					DishButtonClicked(sender, e);
				};
				//button.LongClicked += DishButtonLongClick;
				button.Data.Add("dish", dish);
				//button.SetWrap(5);
				
				m_Table.AddBrick(button);
			}
			ShowAll();
		}
		
		private void CategoryButtonClick(object s, EventArgs args)
		{
			Widgets.Button b = (Widgets.Button)s;
			DishCategory category = (DishCategory)b.Data["category"];
			LoadCategory(category);
		}
		
		private Widgets.WindowLabel m_CategoryLabel;
		private Widgets.BrickTable m_Table;
		private ISession m_Session;
	}
	
	public class MakeOrderForm: Form
	{		
		public MakeOrderForm(Context context): base(context)
		{
			scroll = new Gtk.ScrolledWindow();
			Widgets.VBox vbox = new Widgets.VBox();
			Widgets.VBox left = new Widgets.VBox();
			Widgets.HBox hbox = new Widgets.HBox();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			Widgets.DynamicLabel total = new Widgets.DynamicLabel(() => {
				return string.Format("Total: {0}", m_Order.FinalValue);
			});
			
			m_Session = context.OpenSession();
			
			m_Context = context;
			m_Placement = new Gtk.EventBox();
			m_Menu = new DishesPanel(m_Session, this);
			m_Menu.DishButtonClicked += DishButtonClick;
			m_OrderList = new Gtk.VBox(false, 5);
			m_DishEditor = new DishEditPanel(m_Session, this);
			total.Margin = 20;
			
			scroll.HscrollbarPolicy = PolicyType.Never;
			scroll.SetSizeRequest(320, 0);
			
			buttons.AddLeftImageButton("Powrót", "arrow_left", BackButtonClick);
			buttons.AddRightImageButton("Dodaj po numerze", "quick_open", (sender, e) => {
				int position = ValuePicker.GetIntValue("Numer pozycji w menu:");
				Dish dish = m_Session.Query<Dish>().Where(i => i.PositionNumber == position).FirstOrDefault();
				if (dish == null) {
					MessageBox.Popup("Błąd", "Brak potrawy o tym numerze");	
					return;
				}
				
				AddDishItem(dish);
			});
			
			buttons.AddRightImageButton("Wybierz dostawę", "delivery", (sender, e) => {
				Context.ApplicationWindow.LoadForm(typeof(OrderDestinationForm), m_Order, m_Session);
			});
			
			m_Placement.Add(m_Menu);
				
			scroll.AddWithViewport(m_OrderList);
			left.PackFill(new Widgets.WindowLabel("Zamówienie:", "Inverted"));
			left.PackExpand(scroll);
			left.PackFill(total);
			hbox.PackFill(left);
			hbox.PackExpand(m_Placement);
			vbox.PackExpand(hbox);
			vbox.PackFill(buttons);
			Add(vbox);
			ShowAll();
		}
		
		public override void Load(params object[] args)
		{
			m_Order = args[0] as Order;
			
			if(m_Order.Id != 0)
			{
				foreach(OrderDish dish  in m_Order.Entries) {
					m_Session.Load(dish, dish.Id);
					m_OrderList.PackStart(new OrderItem(dish, this, Context, m_Order), 
						false, true, 0);
				}
			} 
			else {
				m_Order.Entries = new List<OrderDish>();
			}
			
			DishCategory c = m_Session.Get<DishCategory>(DishCategory.RootCategory);
			m_Menu.LoadCategory(c);			
			scroll.SetSizeRequest(320, 0);
		}
		
		public void LoadCategory(DishCategory c)
		{
			m_Placement.Remove(m_Placement.Child);
			m_Placement.Add(m_Menu);
			m_Menu.LoadCategory(c);
		}
		
		public void LoadOrderDish(OrderDish o, OrderItem i)
		{			
			m_Placement.Remove(m_Placement.Child);
			m_Placement.Add(m_DishEditor);
			m_DishEditor.Load(o, i);
		}
		
		public void ShowMenu()
		{
			m_Placement.Remove(m_Placement.Child);
			m_Placement.Add(m_Menu);
		}
		
		private void DishButtonClick(object s, EventArgs args)
		{
			Widgets.Button b = (Widgets.Button)s;
			Dish dish = (Dish)b.Data["dish"];
			AddDishItem(dish);
		}
		
		private void DishButtonLongClick(object s, EventArgs args)
		{
			Widgets.Button b = (Widgets.Button)s;
			Dish dish = (Dish)b.Data["dish"];
			Context.ApplicationWindow.LoadForm(typeof(ProductSheet), dish.Id);
		}
		
		private void BackButtonClick(object s, EventArgs args)
		{
			m_Session.Dispose();
			Context.ApplicationWindow.LoadForm(typeof(MainForm));	
		}
		
		private void AddDishItem(Dish dish)
		{
			OrderDish entry = m_Order.Entries.FirstOrDefault(e => e.Dish.Id == dish.Id && e.Modifications.Count == 0);

			if (entry == null) {
				entry = new OrderDish() {
					Dish = dish,
					Modifications = new List<OrderDishModification>(),
					Order = m_Order,
					Amount = 0
				};
				
				m_Order.Entries.Add(entry);
				m_OrderList.PackStart(new OrderItem(entry, this, Context, m_Order), false, true, 0);
			}
			
			entry.Amount++;
			m_OrderList.Children.Each(i => (i as OrderItem).Reload());
		}
		
		private Gtk.ScrolledWindow scroll;
		private ISession m_Session;
		private Order m_Order;
		private Gtk.VBox m_OrderList;
		private Gtk.EventBox m_Placement;
		private DishEditPanel m_DishEditor;
		private DishesPanel m_Menu;
		private Context m_Context;
	}
}

