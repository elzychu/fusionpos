using System;
using NHibernate;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class IngredientViewForm: Form
	{
		public IngredientViewForm(Context context): base(context)
		{
			Gtk.VBox vbox = new Gtk.VBox();
			Gtk.ScrolledWindow historyScroll = new Gtk.ScrolledWindow();
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			
			historyScroll.Add(m_HistoryList);
			
			vbox.PackStart(m_Name, false, false, 0);
			vbox.PackStart(historyScroll, true, true, 0);
			vbox.PackStart(buttons, false, false, 0);
			Add(vbox);
		}
		
		public override void Load(params object[] args)
		{
			if (args.Length < 1) {
				MessageBox.Popup("huj", "huj");
				return;
			}
			
			using (ISession session = Context.OpenSession()) {
				m_Ingredient = session.Get<Ingredient>(args[0]);
			}
			
		}
		
		private Ingredient m_Ingredient;
		private Widgets.Label m_Name;
		private Widgets.Label m_Quantity;
		private Gtk.TreeView m_HistoryList;
	}
}

