using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Widgets;
using FusionPOS.Domain;

namespace FusionPOS
{
	public class ProductSheet: HBox {
		public ProductSheet()
		{
			Label price = new Label("Cena preferowana: ");
			Label secondPrice = new Label("Inna cena: ");
			Label rebatePercent = new Label("Rabat procentowy: ");
			Label rebateAmount = new Label("Rabat kwotowy: ");
			Label points = new Label("Punkty: ");
			Label tax = new Label("Stawka podatkowa: ");
			
			WindowLabel label1 = new WindowLabel("Informacje ogólne", "Inverted");
			WindowLabel label2 = new WindowLabel("Składniki", "Inverted");
			WindowLabel label3 = new WindowLabel("Nazwa", "Inverted");
			
			DynamicLabel nameView = new DynamicLabel(() =>
				m_Dish != null ? m_Dish.Name : "----"
			);
			
			DynamicLabel priceView = new DynamicLabel(() =>
				m_Dish != null ? m_Dish.Price.ToString() : "----"
			);
			
			DynamicLabel secondPriceView = new DynamicLabel(() =>
				m_Dish != null ? m_Dish.Price1.ToString() : "----"
			);
			
			DynamicLabel rebatePercentView = new DynamicLabel(() =>
			        m_Dish != null ? m_Dish.RebatePercent.ToString() : "----"
			);
			
			DynamicLabel rebateAmountView = new DynamicLabel(() =>
				m_Dish != null ? m_Dish.RebateAmount.ToString() : "----"
			);
			
			DynamicLabel pointsView = new DynamicLabel(() =>
				m_Dish != null ? m_Dish.Points.ToString() : "----"
			);
			
			DynamicLabel taxView = new DynamicLabel(() => 
				m_Dish != null ? m_Dish.Tax.PercentValue.ToString() + "%"
			                                        : "----"
			);
			
			Gtk.Table table = new Gtk.Table(12, 2, false);
			table.Attach(label3, 0, 2, 0, 1);
			table.Attach(nameView, 0, 2, 1, 2);
			table.Attach(label1, 0, 2, 2, 3);
			table.Attach(price, 0, 1, 3, 4);
			table.Attach(secondPrice, 0, 1, 4, 5);
			table.Attach(rebateAmount, 0, 1, 5, 6);
			table.Attach(rebatePercent, 0, 1, 6, 7);
			table.Attach(points, 0, 1, 7, 8);
			table.Attach(tax, 0, 1, 8, 9);
			table.Attach(priceView, 1, 2, 3, 4);
			table.Attach(secondPriceView, 1, 2, 4, 5);
			table.Attach(rebateAmountView, 1, 2, 5, 6);
			table.Attach(rebatePercentView, 1, 2, 6, 7);
			table.Attach(pointsView, 1, 2, 7, 8);
			table.Attach(taxView, 1, 2, 8, 9);
			
			
			Gtk.TreeView recipeView = new Gtk.TreeView();
			m_Store = new Gtk.ListStore(typeof(RecipeIngredient));
			SimpleTreeViewColumn nameCol = new
				Widgets.SimpleTreeViewColumn("Nazwa", "Component.Name");
			SimpleTreeViewColumn amountCol = new
				Widgets.SimpleTreeViewColumn("Ilość", "Amount");
			
			recipeView.AppendColumn(nameCol);
			recipeView.AppendColumn(amountCol);
		
			recipeView.Model = m_Store;
			
			table.Attach(label2, 0, 2, 9, 10);
			Gtk.ScrolledWindow scrWnd = new Gtk.ScrolledWindow();
			scrWnd.Add (recipeView);
			scrWnd.SetSizeRequest(300, 300);
			table.Attach(scrWnd, 0, 2, 10, 11);
			PackExpand(table);
			
			DynamicLabel.RefreshAll(this);
		}
		
		public void SetDish(Dish dish)
		{
			m_Dish = dish;
			
			m_Store.Clear();
			foreach(RecipeIngredient ingredient in m_Dish.Recipe.Ingredients)
				m_Store.AppendValues(ingredient);
			
			DynamicLabel.RefreshAll(this);
		}
		
		private Dish m_Dish;
		private Gtk.ListStore m_Store;
	}
	
	public class ProductsListForm: Form
	{
		public ProductsListForm(Context context): base(context)
		{
			Widgets.VBox vbox = new Widgets.VBox();
			Widgets.HBox hbox = new Widgets.HBox();
			
			m_SheetForm = new ProductSheet();
			
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			Gtk.ScrolledWindow treeScroll = new Gtk.ScrolledWindow();
			Gtk.TreeViewColumn nameColumn = new Widgets.SimpleTreeViewColumn("Nazwa", "Name");
			WindowLabel label = new WindowLabel("Szukaj", "Inverted");
			
			m_SearchText = new Widgets.TextBox();
			m_TreeStore = new Gtk.TreeStore(typeof(object));
			m_TreeView = new Gtk.TreeView();
			m_TreeView.AppendColumn(nameColumn);
			m_TreeView.Model = m_TreeStore;
			m_TreeView.RulesHint = true;
			m_TreeView.CursorChanged += TreeViewSelected;
			m_TreeView.SetSizeRequest(-1, 10);
			treeScroll.Add(m_TreeView);
			m_SearchText.Changed += Search;
			
			buttons.AddLeftImageButton("Powrót", "arrow_left", Context.ApplicationWindow.GoBack);
			buttons.AddRightImageButton("Pokaż protipy", "protip", delegate (object sender, EventArgs args) {
				if(m_Selected != null)
					MessageBox.Popup(String.Format("Protipy dla: {0}", m_Selected.Name),
				                 m_Selected.Recipe.Protips);
			});
			vbox.PackFill(label);
			vbox.PackFill(m_SearchText);
			vbox.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			hbox.PackExpand(treeScroll);
			hbox.PackFill(new Gtk.Separator(Gtk.Orientation.Vertical));
			hbox.PackExpand(m_SheetForm);
			vbox.PackExpand(hbox);
			vbox.PackFill(new Gtk.Separator(Gtk.Orientation.Horizontal));
			vbox.PackFill(buttons);

			Add(vbox);
			ShowAll();
		}
		
		public override void Load(params object[] args)
		{
			m_TreeStore.Clear();
			using (ISession session = Context.OpenSession()) {
				DishCategory cat = session.Get<DishCategory>(DishCategory.RootCategory);
				if (cat != null)
					LoadTree(cat, null);
			}	
		}

		private void LoadTree(DishCategory category, Gtk.TreeIter? iter)
		{
			foreach (DishCategory child in category.Children) {
				if(child.Deleted)
					continue;
				Gtk.TreeIter it = m_TreeStore.AppendValuesRef(iter, child);
				LoadTree(child, it);
			}
			
			foreach (Dish child in category.Dishes)
			{	
				if(child.Deleted)
					continue;
				m_TreeStore.AppendValuesRef(iter, child);
			}
		}

		private void TreeViewSelected(object sender, EventArgs e)
		{
			Gtk.TreeIter iter;
			Gtk.TreeModel model;
			Gtk.TreePath path;
			m_TreeView.Selection.GetSelected(out model, out iter);
			path = m_TreeStore.GetPath(iter);
			Dish dish = model.GetValue(iter, 0) as Dish;
			if (dish != null)
				using(ISession ses = Context.OpenSession())
				{
					m_Selected = dish;
					//ses.Load(dish, dish.Id);
                                        ses.Lock(dish, LockMode.None);
					m_SheetForm.SetDish(dish);
				}				
			
			if (m_TreeView.GetRowExpanded(path))
				m_TreeView.CollapseRow(path);
			else
				m_TreeView.ExpandRow(path, false);
		}
		
		private void Search(object s, EventArgs args)
		{		
			if (m_SearchText.Text == "") {
				Load();
				return;
			}

			using (ISession session = Context.OpenSession())
			{
				IList<Dish> results = session.Query<Dish>()
				    .Where(i => i.Name.Contains(m_SearchText.Text))
				    .Where(i => !i.Deleted)
				    .ToList();
				
				m_TreeStore.Clear();
				foreach (Dish i in results)
					m_TreeStore.AppendValues(i);
			}
		}
		
		private void Nop(object s, EventArgs args)
		{
		}
		
		private Dish m_Selected;
		private ProductSheet m_SheetForm;
		private Widgets.TextBox m_SearchText;
		private Gtk.TreeView m_TreeView;
		private Gtk.TreeStore m_TreeStore;
	}
}

