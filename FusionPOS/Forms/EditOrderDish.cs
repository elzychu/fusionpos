using System;
using Gtk;
using FusionPOS.Widgets;

namespace FusionPOS
{
	public class EditOrderDish: TitleBox
	{
		public EditOrderDish(): base()
		{
			Gtk.VBox vbox = new Gtk.VBox(false, 10);
			Gtk.HBox hbox = new Gtk.HBox(false, 10);
			Widgets.Label count = new Widgets.Label("Ilość:");
			count.Justify = Justification.Left;
			
			Gtk.Button adder = Widgets.ImageButtonFactory.ImageButton("", "plus");
			adder.Clicked += AddClicked;
			
			Gtk.Button subtracter = Widgets.ImageButtonFactory.ImageButton("", "minus");
			subtracter.Clicked += SubtractClicked;
			
			Gtk.Button delete = Widgets.ImageButtonFactory.ImageButton("", "delete");
			delete.Clicked += DeleteClicked;
			
			m_Count = new Gtk.Entry();
			m_Count.Name = FontSettings.BiggerFont;

			hbox.PackStartSimple(m_Count);
			hbox.PackStartSimple(adder);
			hbox.PackStartSimple(subtracter);
			vbox.PackStart(count, false, true, 0);
			vbox.PackStartSimple(hbox);
			vbox.PackStartSimple(delete);
			
			m_Placement.Add(vbox);
			m_TitleBar.Text = "Edycja pozycji zamówienia";
			
			AddImageButton("Ok", "ok", OkClicked);
			AddImageButton("Anuluj", "cancel", CancelClicked);
		}
		
		private void OkClicked(object s, EventArgs args)
		{
		}
		
		private void CancelClicked(object s, EventArgs args)
		{
			Hide();
		}
		
		private void DeleteClicked(object s, EventArgs args)
		{
		}

		private void AddClicked(object s, EventArgs args)
		{
			m_Count.Text = (int.Parse(m_Count.Text) + 1).ToString();
		}
		
		private void SubtractClicked(object s, EventArgs args)
		{
			m_Count.Text = (int.Parse(m_Count.Text) - 1).ToString();
		}
		
		private Gtk.Entry m_Count;
	}
}

