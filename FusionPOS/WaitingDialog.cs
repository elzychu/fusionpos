using System;

namespace FusionPOS
{
	public class WaitingDialog: TitleBox
	{
		public WaitingDialog ()
		{
			m_TitleBar.Text = "FusionPOS";
			m_Placement.Add(new Gtk.Label("Oczekiwanie..."));
		}
	}
}

