using System;
using Gtk;

namespace Terminal
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			Gtk.Rc.AddDefaultFile("/home/el/FusionPOS/FusionPOS/Configuration/Interface.rc");
			Gtk.Rc.Parse("/home/el/FusionPOS/FusionPOS/Configuration/Interface.rc");
			MainWindow win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}
	}
}
