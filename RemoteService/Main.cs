using System;

namespace FusionServer
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Context context = new Context();
			context.Initialize();
		}
	}
}
