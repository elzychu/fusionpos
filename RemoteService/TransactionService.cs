using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.ServiceModel;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Services;
using FusionPOS.Domain;
using FusionPOS;
using FusionPOS.Logging;
using Shared;
using FusionPOS.Drivers;

namespace FusionServer
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
	
	public class TransactionService: ITransactionService
	{
		public TransactionService(Context context)
		{
			m_Context = context;
			m_Logger = new Logger(context.Logger, "TRANSACTION");
		}
		
		public void MakeOrder(TransactionDTO transaction, bool print)
		{
			using (var session = m_Context.OpenSession()) {
				var order = transaction.Unpack(session) as Order;
				IEnumerable<IEnumerable<OrderDish>> orderDishes;
				
				if(!order.Entries.Any (q => q.Dish.Kitchen == null))
					orderDishes = order.Entries.Where(r => r.Id == 0)
				            .ToList().Classify(p => p.Dish.Kitchen);
				else
				{
					throw new DatabaseDamaged("Baza danych uszkodzona");
					orderDishes = new List<List<OrderDish>>();
				}  
				using(var tr = session.BeginTransaction()) {
					session.Save(order);
					session.Save(order.Destination);
					tr.Commit();
				}
				
				session.Refresh(order);
				if(print)
				{
					orderDishes.ForEach(p => {
						IList<OrderDish> ods = p as IList<OrderDish>;
						Shared.OrderSummary os = new Shared.OrderSummary(ods, true);
						os.PrintText(m_Context.GetPrinter(p.First().Dish.Kitchen));	
					});
				}

				m_Logger.Info("Created order ID:{0}", order.Id);
			}
		}
		
		public void UpdateOrder(TransactionDTO order,  bool print)
		{
			using (ISession session = m_Context.OpenSession()) {			
				Order updatedOrder = order.Unpack(session) as Order;
				
				if(print)
				{
					updatedOrder.Entries.Where(p => p.Id == 0).ToList().Classify(r => r.Dish.Kitchen).ForEach(p => {
						IList<OrderDish> ods = p.ToList();
					        Shared.OrderSummary os = new Shared.OrderSummary(ods, false);
						os.PrintText(m_Context.GetPrinter(p.First().Dish.Kitchen));
					});
					
				}
				
				using (ITransaction tr = session.BeginTransaction()) {
					session.Merge(updatedOrder);
					tr.Commit();
				}
			}
			
			m_Logger.Info("Updated order ID:{0}", order.Id);
		}
		
		public void FinishOrder(TransactionDTO orderDTO, bool receipt)
		{
			using (ISession session = m_Context.OpenSession()) {
				Order order = orderDTO.Unpack(session) as Order;
				session.Refresh(order);
				
				using (ITransaction tr = session.BeginTransaction()) {
					SettleUsedIngredients(order, session);
					order.Status = OrderStatus.Completed;
					session.Merge(order);
					tr.Commit();
				}
				
				if(receipt) {
					Receipt printReceipt = 	new Receipt(order);
					printReceipt.Print(GetAvailablePrinter(null));
				}
				
				m_Logger.Info("Finished order ID:{0}", order.Id);
			}			
		}
		
		public void CancelOrder(TransactionDTO orderDTO)
		{
			using (ISession session = m_Context.OpenSession()) {
				Order order = orderDTO.Unpack(session) as Order;
				using (ITransaction tr = session.BeginTransaction()) {
					order.Status = OrderStatus.Canceled;
					session.Update(order);
					tr.Commit();
				}
				m_Logger.Info("Cancelled order ID:{0}", order.Id);
			}
		}
		
		public void DoPayment(decimal value, long userId)
		{
			using (ISession session = m_Context.OpenSession())
			{
				User user = session.Get<User>(userId);
				ITransaction transaction = session.BeginTransaction();
				
				Payment payment = new Payment();
				payment.Time = DateTime.Now;
				payment.Waiter = user;
				payment.TotalValue = value;
				
				session.Save(payment);
				transaction.Commit();
			}
		}
		
		public void NewSettlement()
		{
			using (ISession session = m_Context.OpenSession())
			{
				if (getCurrentSettlement(session).Status != SettlementStatus.Closed)
					throw new SettlementException("Ostatnia zmiana nie została zamknięta");
				
				ITransaction transaction = session.BeginTransaction();
				
				SessionItem sessionItem = session.Get<SessionItem>("CurrentSettlement");
				
				Settlement newSettlement = new Settlement();
				newSettlement = new Settlement();
				newSettlement.From = DateTime.Now;
				newSettlement.Status = SettlementStatus.Opened;
				session.Save(newSettlement);
				
				sessionItem.Value = BitConverter.GetBytes(newSettlement.Id);
				session.Update(sessionItem);
				transaction.Commit();
			}	
		}
		
		public void CloseSettlement(long userId)
		{
			//if( !CheckIfWaiterBalanceIsActual(user) )
			//	throw new SettlementException("Nie wszyscy pracownicy zostali rozliczeni");
			
			using (ISession session = m_Context.OpenSession())
			{
				User user = session.Get<User>(userId);
				ITransaction transaction = session.BeginTransaction();
				Settlement currSettlement = getCurrentSettlement(session);	
				currSettlement.Status = SettlementStatus.Closed;
				currSettlement.To = DateTime.Now;
				currSettlement.Owner = user;
				
				foreach (UserBalance balance in currSettlement.UserBalances)
					currSettlement.Balance += balance.Balance;
				
				session.Update(currSettlement);	
				transaction.Commit();
			}
		}
		
		private IList<Order> getStornOrders(ISession session, User user) {
			DateTime from = getCurrentSettlement(session).From;
			return session.Query<Order>().Where(p => DateTime.Compare(p.Time, from) > 0 &&
			                                    p.Status == OrderStatus.Canceled && 
			                                    p.Waiter == user).ToList();
		}
		
		private Settlement getCurrentSettlement(ISession session) {

			Settlement currSettlement;
			SessionItem sessionItem = session.Get<SessionItem>("CurrentSettlement");
			if (sessionItem == null)
			{
				sessionItem = new SessionItem();
				sessionItem.Key = "CurrentSettlement";
				session.Save(sessionItem);
				throw new NoLastSettlementException("Brak logowania ostaniej" + 
				                            "zmiany w bazie. Baza uszkodzona?");
			}
			
			if (sessionItem.Value == null)
			{
				throw new SettlementException("Baza danych uszkodzona");
			}
		
			//XXX
			currSettlement = session.Get<Settlement>(BitConverter.ToInt64(sessionItem.Value, 0));

			return currSettlement;
		}
		
		public Settlement GetCurrentSettlement() {
			using (ISession session = m_Context.OpenSession()) {
				return getCurrentSettlement(session);	
			}
		}
		
		public bool CheckIfWaiterBalanceIsActual(long userId)
		{
			using (ISession session = m_Context.OpenSession())
			{
				User user = session.Get<User>(userId);
				Settlement currSettlement = getCurrentSettlement(session);
				DateTime from = currSettlement.From;
				DateTime to = DateTime.Now;
				
				List<Order> orders =  session.Query<Order>()
				    .Where(p => (DateTime.Compare(from, p.Time) > 0) 
				    && (p.Waiter.Equals(user))).ToList();
				
				return orders.Any(p => !(p.Status != OrderStatus.Canceled 
				    && p.Status != OrderStatus.Finalized))
				    && orders.All(p => currSettlement.UserBalances
				    .Any(r => r.Owner.Equals(p.Waiter)));
			}
		}
		
		public void SaveWaiterSettlement(decimal value, long userId)
		{
			UserBalance userBalance = new UserBalance();			
			using (ISession session = m_Context.OpenSession())
			{
				User user = session.Get<User>(userId);
				ITransaction transaction = session.BeginTransaction();
				Settlement settlement = getCurrentSettlement(session);
				userBalance.Balance = value;
				userBalance.Owner = user;
				userBalance.Time = DateTime.Now;
				userBalance.Settlement = getCurrentSettlement(session);
				userBalance.Status = SettlementStatus.Closed;
				session.Save(userBalance);
				session.Update(settlement);
				settlement.UserBalances.Add(userBalance);
				transaction.Commit();
			}
		}
		
		public void PrintInvoice(Kitchen kitchen, OutgoingInvoiceDTO invoiceDTO)
		{
			using(ISession session = m_Context.OpenSession())
			{
				OutgoingInvoice invoice = invoiceDTO.Unpack(session);
				FusionPOS.VatInvoice vatInvoice = new FusionPOS.VatInvoice(invoice);
				FusionPOS.Drivers.ILinePrinterDriver drv = m_Context.GetPrinter(kitchen);
				vatInvoice.PrintText(drv);
			}
		}
		
		private void SettleUsedIngredients(Order order, ISession session)
		{
			m_Logger.Debug("Settling resources used by order ID:{0}", order.Id);
			
			foreach (OrderDish d in order.Entries) {
				d.Dish.Recipe.Ingredients.ForEach(i => SettleRecipeIngredient(i, d.Amount, session));
				d.Modifications.ForEach(i => {
					SettleRecipeIngredient(i.Ingredient, i.Amount, session);
				});
			}
		}
		
		private void SettleRecipeIngredient(RecipeIngredient ri, decimal multiplier, ISession session)
		{
			if (ri is RecipeIngredientIngredient) {
				RecipeIngredientIngredient rii = ri as RecipeIngredientIngredient;
				
				session.Lock(rii.Ingredient, LockMode.None);
				rii.Ingredient.Quantity -= rii.Amount * multiplier;
				session.Update(rii.Ingredient);
				
				m_Logger.Debug("Settled {0} x{1} of '{2}'", 
				   rii.Amount, multiplier, rii.Ingredient.Name);
			}
			
			if (ri is RecipeIngredientDish) {
				RecipeIngredientDish rid = ri as RecipeIngredientDish;
				rid.Dish.Recipe.Ingredients.ForEach(i => SettleRecipeIngredient(i, i.Amount, session));
			}
		}
		
		private IEnumerable<OrderDish> DiffOrders(Order first, Order second)
		{
			foreach (OrderDish od in second.Entries) {
				if (first.Entries.FirstOrDefault(i => i.Id == od.Id) == null)
					yield return od;
			}
		}
		
		private ILinePrinterDriver GetAvailablePrinter(Kitchen preferedPrinter)
		{
			ILinePrinterDriver drv = null;
			
			if(preferedPrinter != null)
			{
				drv = m_Context.GetPrinter(preferedPrinter);
				if(drv != null)
					return drv;
			}
			
			using(ISession session = m_Context.OpenSession())
			{
				Kitchen printer = session.Query<Kitchen>()
					.Where(k => k.MainPrinter==true).FirstOrDefault();
				if(printer != null)
					drv = m_Context.GetPrinter(printer);
				
				if(drv == null) {
					IList<Kitchen> kitchens = session.Query<Kitchen>().ToList();
					foreach(Kitchen kitchen in kitchens) {
						drv = m_Context.GetPrinter(kitchen);
						if(drv != null)
							break;
					}
				}
			}
			
			if(drv == null)
				throw new ServerException("Brak drukarki termicznej");
			
			return drv;
		}
		
		private Context m_Context;
		private Logger m_Logger;
	}
}

