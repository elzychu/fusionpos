using System;
using System.Linq;
using System.ServiceModel;
using NHibernate.Linq;
using FusionPOS.Services;
using FusionPOS.Logging;
using FusionPOS.Domain;

namespace FusionServer
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
	public class LifecycleService: ILifecycleService
	{
		public LifecycleService(Context context)
		{
			m_Context = context;
			m_Logger = new Logger(m_Context.Logger, "LIFECYCLE");
		}
		
		public void Login(string hostname, string lifecycle)
		{
			using (var session = m_Context.OpenSession()) {
				Device device = session.Query<Device>()
				    .Where(d => d.Name == hostname && d.Enabled == true)
				    .FirstOrDefault();
				
				if (device == null)
					throw new LifecycleException("Unregistered POS device");
				
				POSConnection pos = new POSConnection(m_Context, device);
				pos.Initialize(lifecycle);
				m_Context.Endpoints.Add(pos);
			}
			
			m_Logger.Standard("POS {0} online", hostname);
		}
		
		public void Logout(string hostname)
		{
			m_Logger.Standard("POS {0} offline", hostname);
		}
		
		public void Ping()
		{
		}
		
		private Logger m_Logger;
		private Context m_Context;
	}
}

