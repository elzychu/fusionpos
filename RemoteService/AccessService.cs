using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel;
using FusionPOS.Services;

namespace FusionServer
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
	public class AccessService: IAccessService
	{
		public AccessService(Context context)
		{
			m_Context = context;
		}
		
		public bool IsOnline()
		{
			return true;
		}
		
		public bool IsDeviceOnline(string hostname)
		{
			return m_Context.Endpoints.Where(i => i.Device.Name == hostname).FirstOrDefault() != null;	
		}
		
		public string SystemVersion()
		{
			return "FusionServer 0.0.1";
		}
		
		public List<ServiceInfo> GetServices(string hostname)
		{
			POSConnection pos = m_Context.Endpoints.Where(i => i.Device.Name == hostname).FirstOrDefault();
			if (pos == null)
				throw new ServerException("No such endpoint");
			
			return new List<ServiceInfo>(pos.ServicesInfo);
		}
		
		private Context m_Context;
	}
}

