﻿using System;
using System.Linq;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Criterion;
using Mono.Unix;
using FusionPOS;
using FusionPOS.Logging;
using FusionPOS.Config;
using FusionPOS.Domain;
using FusionPOS.Services;
using FusionPOS.Drivers;
	
namespace FusionServer
{	
	public class DecimalMysqlDialect: NHibernate.Dialect.MySQLDialect
	{
		public DecimalMysqlDialect(): base()
		{
			base.RegisterColumnType(DbType.Decimal, "DECIMAL(19,2)");
			base.RegisterColumnType(DbType.Decimal, 19, "DECIMAL($p,$s)");
		}
	}
	
	public class PrinterConnection
	{
		public PrinterConnection(Context context, Kitchen kitchen)
		{
			Printer = new EpsonLinePrinter();
			m_PortType = kitchen.PrinterPort.Split(':')[0];
			m_PortPath = kitchen.PrinterPort.Split(':')[1];
			Kitchen = kitchen;
			
			if (string.IsNullOrEmpty(kitchen.PrinterConfiguration))
				return;	
			
			m_Configuration = kitchen.PrinterConfiguration.Split(';').ToDictionary(k => k.Split('=')[0], v => v.Split('=')[1]);
		}
		
		public void Open()
		{
			if (m_PortType == "serial") {
				try {
					SerialPort port = new SerialPort(m_PortPath);
					port.BaudRate = int.Parse(m_Configuration["baudrate"]);
					port.Open();
					m_Stream = port.BaseStream;
					m_Connection = port;
				} catch (IOException e) {
					throw e;
				}

			}
			
			if (m_PortType == "usb") {
				try {
					StdioFileStream stream = new StdioFileStream(m_PortPath, "a+");
					m_Stream = stream;
					m_Connection = stream;
				} catch (IOException e) {
					throw e;
				}
			}
			
			if (m_PortType == "tcp") {
				string hostname = m_PortPath.Split(new char[] {':'}, 2)[0];
				int port = int.Parse(m_PortPath.Split(new char[] {':'}, 2)[1]);
				TcpClient socket = new TcpClient();
				socket.Connect(hostname, port);
				m_Stream = socket.GetStream();
				m_Connection = socket;
			}
			
			Printer = new EpsonLinePrinter();
			Printer.Initialize(new DriverConfiguration { Configuration = m_Configuration, Stream = m_Stream });
		}
		
		public void Close()
		{
			if (m_PortType == "serial")
				(m_Connection as SerialPort).Close();
			
			if (m_PortType == "usb")
				(m_Connection as StdioFileStream).Close();
			
			if (m_PortType == "tcp")
				(m_Connection as TcpClient).Close();
		}
		
		public ILinePrinterDriver Printer
		{
			get; private set;
		}
		
		public Kitchen Kitchen
		{
			get; private set;	
		}
		
		private Context m_Context;
		private object m_Connection;
		private string m_PortType;
		private string m_PortPath;
		private Stream m_Stream;
		
		private IDictionary<string, string> m_Configuration;
	}
	
	public class POSConnection
	{
		public POSConnection(Context context, Device device)
		{
			m_Logger = new Logger(context.Logger, "POSCONN");
			Device = device;
		}
		
		public void Initialize(string sysinfo)
		{
			m_SystemInfoServiceUri = new Uri(sysinfo);
			EndpointAddress endpoint = new EndpointAddress(m_SystemInfoServiceUri);
			ISystemInfoService info = ChannelFactory<ISystemInfoService>.CreateChannel(new WSHttpBinding(SecurityMode.None), endpoint);
			
			/* Discovery POS services */
			ServicesInfo = info.DiscoveryServices().ToList();
			foreach (var i in ServicesInfo) {
				m_Logger.Standard("Discovered service on {0}: {1} at {2}", 
				    Device.Name, i.ContractName, i.Uri);
			}
		}
				                  
		public Device Device
		{
			get; private set;
		}
		
		public DateTime LastAlive
		{
			get; private set;
		}
		
		public IList<ServiceInfo> ServicesInfo
		{
			get; private set;
		}
		
		public bool SupportsInterface<T>()
		{
			string name = typeof(T).Name;
			return ServicesInfo.Where(i => i.ContractName == name).FirstOrDefault() != null;
		}
		
		public T QueryInterface<T>()
		{
			string name = typeof(T).Name;
			ServiceInfo info = ServicesInfo.Where(i => i.ContractName == name).FirstOrDefault();
			
			if (info == null)
				throw new ServerException("Unsupported interface");
			
			EndpointAddress endpoint = new EndpointAddress(info.Uri);
			return ChannelFactory<T>.CreateChannel(new WSHttpBinding(SecurityMode.None), endpoint);
		}
		
		private Logger m_Logger;
		private Uri m_SystemInfoServiceUri;
		private IList<ServiceInfo> m_ServicesInfo;
	}
	
	public class Context
	{
		public Context()
		{
		}
		
		
		public void Initialize()
		{	
			/* Initialize logger */
			Logger = new LoggerContext();
			Logger.AddOutputStream(Console.Out);
			Logger.Log("SERVER", LogPriority.Standard, "Loaded");
			m_Logger = new Logger(Logger, "SERVER");
			m_DbLogger = new Logger(Logger, "DB");
			m_ServiceLogger = new Logger(Logger, "SERVICE");
			
			/* Read XML configuration */
			Configuration = new ConfigContext();
			Configuration.Logger = new Logger(Logger, "CONF");
			Configuration.ReadFile("/Configuration/Server.xml");
			/* ... */
			
			/* Detect operating system */
			m_SystemName = SystemName.Uname();
			m_Logger.Standard("Detected operating system {0}", m_SystemName);

			/* Build connection string */
			string connstr = String.Format(
			    "Server={0};Database={1};User ID={2};Password={3};AutoEnlist=false;CharSet=utf8",
			    Configuration.Sections["database"].Properties["host"],
			    Configuration.Sections["database"].Properties["db"],
			    Configuration.Sections["database"].Properties["user"],
			    Configuration.Sections["database"].Properties["password"]);
			
			m_DbLogger.Info("Connection string: {0}", connstr);
			
			/* Create database connection */
			m_NhConfiguration = new Configuration();
			m_NhConfiguration.Configure();
			m_NhConfiguration.SetProperty("connection.connection_string", connstr);
			m_NhConfiguration.SetProperty("dialect", "FusionServer.DecimalMysqlDialect, FusionServer");
			m_NhConfiguration.AddAssembly("Shared");
			m_NhSessionFactory = m_NhConfiguration.BuildSessionFactory();
			
			/* Export schema (optional) */
			//SchemaExport sch = new SchemaExport(m_NhConfiguration);
			//sch.Execute(true, true, false);
			
			//SchemaUpdate sch = new SchemaUpdate(m_NhConfiguration);
			//sch.Execute(true, true);
			
			SchemaValidator validator = new SchemaValidator(m_NhConfiguration);
			validator.Validate();
			//
			/* Test connection */
			/*ISession session = m_NhSessionFactory.OpenSession();
			session.Disconnect();
			session.Reconnect();
			session.Dispose();*/
			m_Endpoints = new List<POSConnection>();
			m_Printers = new List<PrinterConnection>();
			
			if (m_SystemName == "Linux") {
				/* Enable printer connection monitoring */
				FileSystemWatcher deviceWatcher = new FileSystemWatcher("/dev");
				deviceWatcher.Created += (sender, e) => PrinterConnected(e.FullPath);
				//deviceWatcher.Changed += (sender, e) => PrinterConnected(e.FullPath);
				deviceWatcher.Deleted += (sender, e) => PrinterDisconnected(e.FullPath);
				deviceWatcher.IncludeSubdirectories = true;
				deviceWatcher.EnableRaisingEvents = true;
			}

			if (m_SystemName == "FreeBSD") {
				/* FreeBSD one */
				DevdWatcher devd = new DevdWatcher("/var/run/devd.pipe");
				devd.Attached += PrinterConnected;
				devd.Detached += PrinterDisconnected;
				devd.Start();
			}
			
			/* Try to connect to configured printers */
			using (ISession session = OpenSession()) {
				session.Query<Kitchen>().Each(kitchen => {
					try {
						PrinterConnection conn = new PrinterConnection(this, kitchen);
						conn.Open();
						m_Printers.Add(conn);
						m_Logger.Standard("Printer {0} at {1} online", kitchen.Name, kitchen.PrinterPort);
					} catch (IOException) {
						m_Logger.Standard("Printer {0} unavailable", kitchen.Name);
					}
				});
			}

			/* Launch service host */
			m_Services = new List<ServiceHost>();
			foreach (ConfigNode i in Configuration.Sections["services"].Children.Values) {
				Uri uri = new Uri(i.Properties["uri"]);
				Type contractType = Type.GetType(i.Properties["contract"]);
				Type serviceType = Type.GetType(i.Properties["class"]);
				
				object service = Activator.CreateInstance(serviceType, this);
				ServiceHost host = new ServiceHost(service, uri);
				host.Description.Behaviors.Add(new ServiceMetadataBehavior() { HttpGetEnabled = true });
				host.AddServiceEndpoint(contractType, new WSHttpBinding(SecurityMode.None), uri);
				host.Open();
				m_Services.Add(host);
				m_ServiceLogger.Standard("Service {0} launched", i.Properties["class"]);
			}
		}

		void PrinterConnected(string path)
		{
			m_Logger.Debug("Device {0} attached", path);
			
			using (var session = OpenSession()) {
				Kitchen printer = session.Query<Kitchen>().Where(i =>
				    i.PrinterPort == "usb:" + path || 
				    i.PrinterPort == "serial:" + path).FirstOrDefault();
				
				if (printer == null)
					return;
				
				try {
					PrinterConnection conn = new PrinterConnection(this, printer);
					conn.Open();
					m_Printers.Add(conn);
					m_Logger.Standard("Printer {0} at {1} online", printer.Name, printer.PrinterPort);
				} catch (IOException) {
					return;
				}
			}
		}
		
		void PrinterDisconnected(string path)
		{
			m_Logger.Debug("Device {0} detached", path);
			
			PrinterConnection printer = m_Printers.Where(i =>
			    i.Kitchen.PrinterPort == "usb:" + path || 
			    i.Kitchen.PrinterPort == "serial:" + path).FirstOrDefault();
			
			if (printer == null)
				return;
			
			m_Printers.Remove(printer);
			m_Logger.Standard("Printer {0} at {1} offline", printer.Kitchen.Name, printer.Kitchen.PrinterPort);
		}
		
		public ILinePrinterDriver GetPrinter(Kitchen kitchen)
		{
			PrinterConnection pc = m_Printers.Where(p => p.Kitchen.Id == kitchen.Id)
				.FirstOrDefault();
			return pc == null ? null : pc.Printer;
		}
		
		public ISession OpenSession()
		{
			return m_NhSessionFactory.OpenSession();
		}

		public ConfigContext Configuration
		{
			get; set;
		}
		
		public LoggerContext Logger
		{
			get; set;
		}
	
		public IList<POSConnection> Endpoints
		{
			get {
				return m_Endpoints;
			}
		}
		
		private string m_SystemName;
		private IList<PrinterConnection> m_Printers;
		private IList<POSConnection> m_Endpoints;
		private IList<ServiceHost> m_Services;
		private Configuration m_NhConfiguration;
		private ISessionFactory m_NhSessionFactory;
		private Logger m_Logger;
		private Logger m_DbLogger;
		private Logger m_ServiceLogger;
	}
}
