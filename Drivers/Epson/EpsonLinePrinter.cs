using System;
using System.Threading;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FusionPOS.Drivers
{
	public class EpsonLinePrinter: ILinePrinterDriver
	{
		const byte ESC = 0x1b;
		const byte INIT = (byte)'@';
		
		public EpsonLinePrinter()
		{
		}

		public void Initialize(DriverConfiguration configuration)
		{
			m_Port = configuration.Stream;
			m_Port.WriteByte(ESC);
			m_Port.WriteByte(INIT);
		}
		
		public void SendText(string text)
		{
			Encoding enc = Encoding.GetEncoding("ISO-8859-1");
			byte[] encoded = enc.GetBytes(text);
			m_Port.Write(encoded, 0, encoded.Length);
			m_Port.Flush();
		}
		
		public void SendImage(LinePrinterImage image)
		{
			for (int i = 0; i < image.Height; i += 24) {
				IList<byte> buffer = new List<byte>();
				for (int j = 0; j < image.Width; j++) {
					for (int k = 0; k < 3; k++) {
						byte d = 0;
						for (byte bit = 0; bit < 8; bit++) {
							if (image.GetXY(j, i + (k * 8) + bit))
								d = (byte)(d | (1 << (7 - bit)));
						}
						
						buffer.Add(d);
					}
				}
		
				m_Port.WriteByte(ESC);
				m_Port.WriteByte((byte)'*');
				m_Port.WriteByte(32);
				m_Port.WriteByte((byte)(image.Width % 256));
				m_Port.WriteByte((byte)(image.Width / 256));
				buffer.Each(b => m_Port.WriteByte(b));
				WaitReady();
			}
		}
		
		public void SetCharacterImage(byte code, LinePrinterImage image)
		{
		}
		
		public void SetFont(LinePrinterFont font)
		{
			if (font == LinePrinterFont.FontA) {
				m_Port.WriteByte(ESC);
				m_Port.WriteByte((byte)'M');
				m_Port.WriteByte(0);
			}
			
			if (font == LinePrinterFont.FontB) {
				m_Port.WriteByte(ESC);
				m_Port.WriteByte((byte)'M');
				m_Port.WriteByte(1);
			}		
		}
		
		public void SetUnderline(bool underline)
		{
			m_Port.WriteByte(ESC);
			m_Port.WriteByte((byte)'-');
			m_Port.WriteByte((byte)(underline ? 1 : 0));
		}

		public void SetBold(bool bold)
		{
			m_Port.WriteByte(ESC);
			m_Port.WriteByte((byte)'E');
			m_Port.WriteByte((byte)(bold ? 1 : 0));
		}
		
		public void SetItalic(bool italic)
		{
			m_Port.WriteByte(ESC);
			m_Port.WriteByte((byte)(italic ? '4' : '5'));
		}
		
		public void SetDoubleHeight(bool height)
		{
			m_Port.WriteByte(ESC);
			m_Port.WriteByte((byte)'!');
			m_Port.WriteByte((byte)(height ? 16 : 0));
		}
		
		public void SetDoubleWidth(bool width)
		{
			m_Port.WriteByte(ESC);
			m_Port.WriteByte((byte)'W');
			m_Port.WriteByte((byte)(width ? 1 : 0));
		}
		
		public void PaperFeed()
		{
			
		}
		
		private void WaitReady()
		{
		/*	m_Port.WriteByte(0x10);
			/*m_Port.WriteByte(0x10);
			m_Port.WriteByte(0x04);
			m_Port.WriteByte(3);
			m_Port.ReadByte();*/
		}
		
		private Stream m_Port;
	}
}

