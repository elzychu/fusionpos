using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using FusionPOS.Domain;

namespace FusionPOS.Drivers
{
	class PosnetFiscalPrinter: IFiscalPrinterDriver
	{
		public class PosnetError
		{
			public int Min { get; set; }
			public int Max { get; set; }
			public string Message { get; set; }
			
			public PosnetError(int min, int max, string message)
			{
				Min = min;
				Max = max;
				Message = message;
			}
		}
		
		public static readonly IList<PosnetError> errors = new List<PosnetError>()
		{
			new PosnetError(1, 15 ,"Błąd ramki"),
			new PosnetError(30, 56, "Błąd wykonywania operacji przez drukarkę"),
			new PosnetError(360, 361, "Błąd zwory serwisowej"),
			new PosnetError(364, 364, "Zły format hasła"),
			new PosnetError(365, 365, "Błędne hasło"),
			new PosnetError(1950, 1950, "Totalizery paragonu przepełnione"),
			new PosnetError(1951, 2009, "Błędny paragon wysłany do drukarki"),
			new PosnetError(2010, 2010, "Totalizery dobowe przepełnione"),
			new PosnetError(2038, 2038, "Transakcja już rozpoczęta")
		};
		
		internal class PosnetCommand
		{
			public const byte STX = 0x02;
			public const byte TAB = 0x09;
			public const byte ETX = 0x03;
			public const byte HASH = (byte)'#';
			public const byte QMARK = (byte)'?';
			
			public PosnetCommand(string command)
			{
				m_Command = command;
				m_Parameters = new Dictionary<string, string>();
			}
			
			public void AddParameter(string id, string value)
			{
				m_Parameters.Add(id, value);
			}
			
			public void AddParameter(string id, int value)
			{
				m_Parameters.Add(id, value.ToString());
			}
			
			public string GetParameter(string id)
			{
				return m_Parameters[id];
			}
			
			public bool HasParameter(string id)
			{
				return m_Parameters.ContainsKey(id);
			}
			
			public string Command
			{
				get { return m_Command; }
			}
			
			public void WriteTo(SerialPort port)
			{
				Crc16Ccitt crc = new Crc16Ccitt(0);
				List<byte> result = new List<byte>();
				result.Add(STX);
				result.AddRange(m_Command.ToCP1250());
				result.Add(TAB);
				
				foreach (var i in m_Parameters) {
					result.AddRange(i.Key.ToCP1250());
					result.AddRange(i.Value.ToCP1250());
					result.Add(TAB);
				}
				
				ushort cksum = crc.ComputeChecksum(result.Skip(1).ToArray());
				
				result.Add(HASH);
				result.AddRange(string.Format("{0:x2}", cksum).ToCP1250());
				result.Add(ETX);
				port.Write(result.ToArray(), 0, result.Count);
				
				PosnetFiscalPrinter.DumpBytes("command", result.ToArray());
			}
			
			public void ReadResponse(SerialPort port)
			{
				byte read;
				char pida, pidb;
				string pname;
				
				read = (byte)port.ReadByte();
				if (read != STX && read != 0x11)
					throw new InvalidDataException(string.Format("Invalid printer response: 0x{0:x2}", read));
				
				m_Command = port.ReadUntil(TAB).FromCP1250();
				m_Parameters.Clear();
	
				for (;;) {
					pida = (char)port.ReadByte();
					switch (pida) {
					case '?':
						m_Parameters["ERR"] = port.ReadUntil(TAB).FromCP1250();
						break;
					case '#':
						m_Parameters["CRC"] = port.ReadUntil(ETX).FromCP1250();
						return;
					default:
						pidb = (char)port.ReadByte();
						pname = string.Format("{0}{1}", pida, pidb);
						m_Parameters[pname] = port.ReadUntil(TAB).FromCP1250();
						break;
					}
				}
			}
			
			private string m_Command;
			private IDictionary<string, string> m_Parameters;
		}
		
		class PosnetFiscalReceipt: IFiscalReceipt
		{
			public PosnetFiscalReceipt(SerialPort port, PosnetFiscalPrinter parent)
			{
				m_Port = port;
				m_Parent = parent;
				m_Payments = new Dictionary<PaymentMethod, decimal>();
				m_ExtraLines = new Dictionary<ReceiptLineType, string>();
				
				/* Open transaction */
				PosnetCommand cmd = new PosnetCommand("trinit");
				cmd.AddParameter("bm", "0");
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				m_Parent.VerifyResponse(cmd);
			}
			
			public void Commit(string systemNumber)
			{
				PosnetCommand cmd;
				
				if (m_RebatePercent > 0) {
					cmd = new PosnetCommand("trdiscntbill");
					cmd.AddParameter("na", m_RebateName);
					cmd.AddParameter("rd", "T");
					cmd.AddParameter("rp", m_RebatePercent.PosnetValue());
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);
				}
			
				if (m_RebateAmount > 0) {
					cmd = new PosnetCommand("trdiscntbill");
					cmd.AddParameter("na", m_RebateName);
					cmd.AddParameter("rd", "T");
					cmd.AddParameter("rw", m_RebateAmount.PosnetValue());
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);
				}
				
				cmd = new PosnetCommand("ftrcfg");
				cmd.AddParameter("cc", m_Parent.CashierName);
				cmd.AddParameter("cn", m_Parent.CashDeskNumber);
				cmd.AddParameter("sn", systemNumber);
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				m_Parent.VerifyResponse(cmd);
				
				foreach (var i in m_Payments) {
					cmd = new PosnetCommand("trpayment");
					cmd.AddParameter("ty", PosnetFiscalPrinter.PaymentMethodToNative(i.Key));
					cmd.AddParameter("wa", i.Value.PosnetValue());
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);
				}
				
				decimal payments = m_Payments.Sum(i => i.Value);
				decimal total = m_Total - (m_Total * (m_RebatePercent / 100)) - m_RebateAmount;
				decimal remainder = payments - total;
				
				if (payments < total)
					throw new ArgumentException("Supplied payments cannot satisfy total receipt value");
				
				if (remainder > 0) {
					cmd = new PosnetCommand("trpayment");
					cmd.AddParameter("ty", PosnetFiscalPrinter.PaymentMethodToNative(PaymentMethod.Cash));
					cmd.AddParameter("wa", remainder.PosnetValue());
					cmd.AddParameter("re", "T");
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);	
				}
				
				cmd = new PosnetCommand("trend");
				cmd.AddParameter("to", total.PosnetValue());
				cmd.AddParameter("fp", payments.PosnetValue());
				if (remainder > 0)
					cmd.AddParameter("re", remainder.PosnetValue());
				
				cmd.AddParameter("fe", 0);
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				m_Parent.VerifyResponse(cmd);
				
				foreach (var i in m_ExtraLines) {
					cmd = new PosnetCommand("trftrln");
					cmd.AddParameter("id", (int)i.Key);
					cmd.AddParameter("na", i.Value);
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);
				}
				
				cmd = new PosnetCommand("trftrend");
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				m_Parent.VerifyResponse(cmd);
			}
			
			public void Abort()
			{
				m_Parent.Abort();
			}
			
			public void Dispose()
			{
			}
			
			public void Add(string name, decimal amount, char ptu, decimal price, decimal total)
			{
				//amount = Math.Round(amount, 2);
				//price = Math.Round(total, 2);
				PosnetCommand cmd = new PosnetCommand("trline");
				cmd.AddParameter("na", name);
				cmd.AddParameter("vt", ptu - 'A');
				cmd.AddParameter("pr", price.PosnetValue());
				cmd.AddParameter("il", amount.ToString());
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				m_Parent.VerifyResponse(cmd);
				m_Total += Math.Round(amount * price, 2);
			}
			
			public void AddExtraLine(ReceiptLineType type, string value)
			{
				m_ExtraLines[type] = value;
			}
			
			public void AddPayment(PaymentMethod method, decimal amount)
			{
				m_Payments[method] = amount;
			}
			
			public void ApplyRebate(decimal amount, decimal percent, string name)
			{
				m_RebateAmount = amount;
				m_RebatePercent = percent;
				m_RebateName = name;
			}
				
			private SerialPort m_Port;
			private string m_RebateName;
			private decimal m_RebateAmount;
			private decimal m_RebatePercent;
			private decimal m_Total;
			private IDictionary<PaymentMethod, decimal> m_Payments;
			private IDictionary<ReceiptLineType, string> m_ExtraLines;
			private PosnetFiscalPrinter m_Parent;
		}
		
		public string CashierName { get; set; }
		public string CashDeskNumber { get; set; }
		
		public string Footer {
			get {
				return null;
			}
			set {
				PosnetCommand cmd = new PosnetCommand("ftrinfoset");
				cmd.AddParameter("tx", value);
				cmd.AddParameter("lb", "true");
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				VerifyResponse(cmd);
			}
		}
		
		public string ModelName {
			get {
				return "huj";
			}
		}
		
		public PosnetFiscalPrinter()
		{
		}
		
		public void Configure(DriverConfiguration configuration)
		{
			m_Port = configuration.Port;
		}
		
		public void Initialize()
		{
			PosnetCommand cmd = new PosnetCommand("!sdev");
			cmd.WriteTo(m_Port);
			cmd.ReadResponse(m_Port);
			
			if (cmd.GetParameter("ds") != "0")
				throw new InvalidOperationException("Printer is waiting for user feedback");			
		}
		
		public bool IsAlive()
		{	
			return true;
		}
		
		public void DisplayTextOperator(string text)
		{
		}
		
		public void DisplayText(string text, int line)
		{
			if (line < 1 || line > 2)
				throw new ArgumentException("Line number must be in range [1,2]");
			
			PosnetCommand cmd = new PosnetCommand("dsptxtline");
			cmd.AddParameter("id", "0");
			cmd.AddParameter("no", (line - 1).ToString());
			cmd.AddParameter("ln", text);
			cmd.WriteTo(m_Port);
			cmd.ReadResponse(m_Port);
			VerifyResponse(cmd);
		}
		
		public void DisplayDateTime()
		{
		}
	
		public void Break()
		{
			string buffer = "!break";
			m_Port.WriteByte(PosnetCommand.STX);
			m_Port.Write(buffer.ToCP1250(), 0, buffer.Length);
		}	
			
		public void Abort()
		{
			PosnetCommand cmd = new PosnetCommand("trcancel");
			cmd.WriteTo(m_Port);
			cmd.ReadResponse(m_Port);
			VerifyResponse(cmd);
		}
		
		public void PrintDailyReport()
		{
			PosnetCommand cmd;
			
			cmd = new PosnetCommand("ftrcfg");
			cmd.AddParameter("cc", CashierName);
			cmd.AddParameter("cn", CashDeskNumber);
			cmd.WriteTo(m_Port);
			cmd.ReadResponse(m_Port);
			VerifyResponse(cmd);
			
			cmd = new PosnetCommand("dailyrep");
			cmd.AddParameter("da", DateTime.Today.ToString("yyyy-MM-dd"));
			cmd.WriteTo(m_Port);
			cmd.ReadResponse(m_Port);
			VerifyResponse(cmd);
		}
		
		public void PrintReportByDates(DateTime start, DateTime end)
		{
		}
		
		public IFiscalReceipt BeginTransaction()
		{
			return new PosnetFiscalReceipt(m_Port, this);
		}
		
		internal static int PaymentMethodToNative(PaymentMethod method)
		{
			switch (method) {
			case PaymentMethod.Cash: return 0;
			case PaymentMethod.Card: return 2;
			case PaymentMethod.Check: return 3;
			case PaymentMethod.Credit: return 5;
			case PaymentMethod.Voucher: return 7;
			case PaymentMethod.Points: return 8;
			}
			
			return -1;
		}

		internal void VerifyResponse(PosnetCommand cmd)
		{
			if (cmd.HasParameter("ERR")) {
				int code = int.Parse(cmd.GetParameter("ERR"));
				string command = cmd.Command == "ERR" ? cmd.GetParameter("cm") : cmd.Command;
				PosnetError err = errors.Where(i => code > i.Min && code < i.Max).FirstOrDefault();
				
				if (err == null)
					err = new PosnetError(0, 0, "Nieznany błąd drukarki fiskalnej");
				
				Abort();
				throw new FiscalPrinterError(err.Message, command, code);
			}
		}
		
		internal static void DumpBytes(string name, byte[] src)
		{
			Console.Write("{0}: ", name);
			foreach (byte i in src)
				Console.Write(i >= 32 ? ((char)i).ToString() : string.Format("<#{0:x2}>", i));
			Console.Write('\n');
		}
		
		private SerialPort m_Port;
	}
}

