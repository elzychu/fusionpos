using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using FusionPOS.Domain;

namespace FusionPOS.Drivers
{
	public class NovitusFiscalPrinter: IFiscalPrinterDriver
	{
		public class NovitusError
		{
			public int Min { get; set; }
			public int Max { get; set; }
			public string Message { get; set; }
			
			public NovitusError(int min, int max, string message)
			{
				Min = min;
				Max = max;
				Message = message;
			}
		}
		
		public static readonly IList<NovitusError> errors = new List<NovitusError>()
		{
			new NovitusError(1, 5, "Wewnętrzny błąd drukarki fiskalnej"),
			new NovitusError(7, 7, "Nieprawidłowa data"),
			new NovitusError(8, 30, "Nieprawidłowy paragon wysłany do drukarki"),
			new NovitusError(31, 31, "Przepełnienie stanu kasy"),
			new NovitusError(37, 37, "Operacja anulowana przez użytkownika"),
			new NovitusError(1002, 1002, "Paragon jest już rozpoczęty"),
		};
		
		internal class NovitusCommand
		{       
			internal const byte ESC = 0x1b;
			internal const byte CR = 0x0d;
			internal const byte P = (byte)'P';
			internal const byte SEP = (byte)'/';
			internal const byte END = (byte)'\\';
			
			public NovitusCommand(string opcode, bool checksum = true)
			{
				m_NumericParams = new List<int>();
				m_Arguments = new List<byte>();
				m_Opcode = opcode;
				m_Checksum = checksum;
			}
			
			public void AddNumericParameter(int param)
			{
				m_NumericParams.Add(param);
			}
			
			public void AddArgument(string arg)
			{
				m_Arguments.AddRange(arg.ToCP1250());
			}
			
			public void AddArgumentCR()
			{
				m_Arguments.Add(CR);
			}
			
			public void AddArgumentSEP()
			{
				m_Arguments.Add(SEP);
			}
			
			public string Command
			{
				get { return m_Opcode; }
			}
			
			public int ErrorCode
			{
				get { return m_Errcode; }
			}

			private string Checksum(byte[] input)
			{
				byte sum = 0xff;
				foreach (byte i in input)
					sum = (byte)(sum ^ i);
				
				return string.Format("{0:x2}", sum);
			}
			
			public void WriteTo(SerialPort stream)
			{
				List<byte> result = new List<byte>();
				result.Add(ESC);
				result.Add(P);
				result.AddRange(string.Join(";", m_NumericParams.Select(i => i.ToString())).ToCP1250());
				result.AddRange(m_Opcode.ToCP1250());
				result.AddRange(m_Arguments);
				if (m_Checksum)
					result.AddRange(Checksum(result.Skip(2).ToArray()).ToCP1250());
				result.Add(ESC);
				result.Add(END);
				
				stream.Write(result.ToArray(), 0, result.Count);
				
Console.Write("command: ");

foreach (byte i in result)
	Console.Write(i >= 32 ? ((char)i).ToString() : string.Format("<#{0:x2}>", i));

Console.Write('\n');
			}
			
			internal void ReadResponse(SerialPort port)
			{
				byte read;
				
				read = (byte)port.ReadByte();
				if (read != NovitusCommand.ESC)
					throw new InvalidDataException(string.Format("Invalid response from printer: 1:0x{0:x2}", read));
				
				read = (byte)port.ReadByte();
				if (read != (byte)'P')
					throw new InvalidDataException(string.Format("Invalid response from printer 2:0x{0:x2}", read));
				
				string errnum = port.ReadUntil((byte)'#').FromCP1250();
				m_Errcode = int.Parse(errnum);
				port.ReadUntil(NovitusCommand.END);
			}
			
			private int m_Errcode;
			private string m_Opcode;
			private bool m_Checksum;
			private List<int> m_NumericParams;
			private List<byte> m_Arguments;
		}
		
		class NovitusFiscalReceipt: IFiscalReceipt
		{
			internal NovitusFiscalReceipt(SerialPort port, NovitusFiscalPrinter parent)
			{
				m_Parent = parent;
				m_Port = port;
				m_LineNumber = 1;
				m_Payments = new Dictionary<PaymentMethod, decimal>();
				
				/* Open transaction */
				NovitusCommand cmd = new NovitusCommand("$h");
				cmd.AddNumericParameter(0);
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				m_Parent.VerifyResponse(cmd);
			}
			
			public void Commit(string systemNumber)
			{
				lock (m_Parent) {
					if (TotalValue() > m_Payments.Sum(i => i.Value))
						throw new ArgumentException("Supplied payments cannot satisfy total receipt value");
					
					NovitusCommand cmd = new NovitusCommand("$y");
					foreach (var i in Enumerable.Range(0, 4))
						cmd.AddNumericParameter(0);
					cmd.AddNumericParameter(m_Rebate != 0 ? (m_RebatePercent ? 1 : 3) : 0);
					foreach (var i in Enumerable.Range(0, 2))
						cmd.AddNumericParameter(0);
					cmd.AddNumericParameter(1);
					cmd.AddNumericParameter(m_Payments.Where(i => i.Key != PaymentMethod.Cash).Count());
					cmd.AddNumericParameter(0);
					cmd.AddNumericParameter(m_Payments.ContainsKey(PaymentMethod.Cash) ? 1 : 0);
					foreach (var i in m_Payments.Where(i => i.Key != PaymentMethod.Cash).OrderBy(i => i.Key))
						cmd.AddNumericParameter(PaymentMethodToNative(i.Key));
					
					cmd.AddArgument(m_Parent.CashDeskNumber);
					cmd.AddArgumentCR();
					cmd.AddArgument(m_Parent.CashierName);
					cmd.AddArgumentCR();
					cmd.AddArgument(systemNumber);
					cmd.AddArgumentCR();
					
					foreach (var i in m_Payments.Where(i => i.Key != PaymentMethod.Cash))
						cmd.AddArgumentCR();
					
					cmd.AddArgument(m_Total.NovitusValue());
					cmd.AddArgumentSEP();
					cmd.AddArgument("0");
					cmd.AddArgumentSEP();
					cmd.AddArgument(m_Rebate.NovitusValue());
					cmd.AddArgumentSEP();
					cmd.AddArgument(m_Payments.ContainsKey(PaymentMethod.Cash) ? m_Payments[PaymentMethod.Cash].NovitusValue() : "0");
					cmd.AddArgumentSEP();
					
					foreach (var i in m_Payments.Where(i => i.Key != PaymentMethod.Cash).OrderBy(i => i.Key)) {
						cmd.AddArgument(i.Value.NovitusValue());
						cmd.AddArgumentSEP();
					}
					
					cmd.AddArgument("0");
					cmd.AddArgumentSEP();
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);
				}
			}
			
			public void Abort()
			{
				lock (m_Parent) {
					NovitusCommand cmd = new NovitusCommand("$e");
					cmd.AddNumericParameter(0);
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);
				}
			}
			
			public void Add(string name, decimal amount, char ptu, decimal price, decimal t)
			{
				lock (m_Parent) {
					decimal total = amount * price;
					NovitusCommand cmd = new NovitusCommand("$l");
					cmd.AddNumericParameter(m_LineNumber++);
					cmd.AddArgument(name);
					cmd.AddArgumentCR();
					cmd.AddArgument(amount.NovitusValue());
					cmd.AddArgumentCR();
					cmd.AddArgument(ptu.ToString());
					cmd.AddArgumentSEP();
					cmd.AddArgument(price.NovitusValue());
					cmd.AddArgumentSEP();
					cmd.AddArgument(total.NovitusValue());
					cmd.AddArgumentSEP();
					cmd.WriteTo(m_Port);
					cmd.ReadResponse(m_Port);
					m_Parent.VerifyResponse(cmd);
					m_Total += total;
				}
			}
			
			public void AddExtraLine(ReceiptLineType type, string value)
			{
			}
			
			public void AddPayment(PaymentMethod method, decimal amount)
			{
				m_Payments[method] = amount;
			}
			
			public void ApplyRebate(decimal amount, decimal percent, string name)
			{
				m_RebateName = name;
				
				if (amount > 0) {
					m_Rebate = amount;
					m_RebatePercent = false;
					return;
				}
			
				if (percent > 0) {
					m_Rebate = percent;
					m_RebatePercent = true;
				}
			}
			
			public void Dispose()
			{
			}
			
			private decimal TotalValue()
			{
				return m_Total - (m_RebatePercent ? (m_Total * (m_Rebate/ 100)) : m_Rebate);
			}
			
			private decimal m_Rebate;
			private bool m_RebatePercent;
			private string m_RebateName;
			private int m_LineNumber;
			private decimal m_Total;
			private NovitusFiscalPrinter m_Parent;
			private SerialPort m_Port;
			private IDictionary<PaymentMethod, decimal> m_Payments;
		}
		
		public NovitusFiscalPrinter()
		{
			ResponseVisitor = delegate(byte[] src) {
				Console.Write("response: ");
				foreach (byte i in src)
					Console.Write(i >= 32 ? ((char)i).ToString() : string.Format("<#{0:x2}>", i));
				Console.Write('\n');
			};
		}
		
		public DateTime RTC { 
			set {
				lock (this) {
					NovitusCommand cmd = new NovitusCommand("$c");
					cmd.AddNumericParameter(value.Year - 2000);
					cmd.AddNumericParameter(value.Month);
					cmd.AddNumericParameter(value.Day);
					cmd.AddNumericParameter(value.Hour);
					cmd.AddNumericParameter(value.Minute);
					cmd.AddNumericParameter(value.Second);
					cmd.AddArgument(CashDeskNumber);
					cmd.AddArgumentCR();
					cmd.AddArgument(CashierName);
					cmd.AddArgumentCR();
					cmd.WriteTo(m_Port);
				}
			} 
		}
		
		public string ModelName {
			get {
				lock (this) {
					NovitusCommand cmd = new NovitusCommand("#v", false);
					cmd.WriteTo(m_Port);
					byte[] response = m_Port.ReadUntil(NovitusCommand.END).ToArray();
					return response.Skip(5).TakeWhile(i => i != 0x1b).ToArray().FromCP1250();
				}
			}
		}
		
		public string Header { get; set; }
		public string Footer { get; set; }
		public string CashierName { get; set; }
		public string CashDeskNumber { get; set; }
		public string SystemNumber { get; set; }
		public IDictionary<char, decimal> VatRates { get; set; }
		public Action<byte[]> ResponseVisitor { get; set; }
		
		public void Configure(DriverConfiguration configuration)
		{
			m_Configuration = configuration;
			m_Port = configuration.Port;
		}
		
		public void Initialize()
		{
			lock (this) {
				NovitusCommand cmd;
				
				SendENQ();
		
				cmd = new NovitusCommand("#e");
				cmd.AddNumericParameter(2);
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				VerifyResponse(cmd);
				
				cmd = new NovitusCommand("$e");
				cmd.AddNumericParameter(0);
				
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				VerifyResponse(cmd);
			}
		}
		
		public bool IsAlive()
		{
			lock (this) {
				try {
					return SendENQ() != -1;
				} catch (TimeoutException) {
					return false;
				}
			}
		}
		
		public void DisplayText(string text, int line)
		{
			lock (this) {
				if (line < 1 || line > 6)
					throw new ArgumentException("Line number must be in range [1,6]");
				
				NovitusCommand cmd = new NovitusCommand("$d");
				cmd.AddNumericParameter(100 + line);
				cmd.AddArgument(text);
				cmd.AddArgumentCR();
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				VerifyResponse(cmd);
			}
		}
	
		public void DisplayTextOperator(string text)
		{
			lock (this) {
				NovitusCommand cmd = new NovitusCommand("$d", false);
				cmd.AddNumericParameter(2);
				cmd.AddArgument(text);
				cmd.WriteTo(m_Port);
				cmd.ReadResponse(m_Port);
				VerifyResponse(cmd);
			}
		}
		
		public void DisplayDateTime()
		{
		}
		
		public void Break()
		{
		}
		
		public void PrintDailyReport()
		{
			NovitusCommand cmd = new NovitusCommand("#r");
			cmd.AddNumericParameter(DateTime.Today.Year - 2000);
			cmd.AddNumericParameter(DateTime.Today.Month);
			cmd.AddNumericParameter(DateTime.Today.Day + 4);
			cmd.AddArgument(CashDeskNumber);
			cmd.AddArgumentCR();
			cmd.AddArgument(CashierName);
			cmd.AddArgumentCR();
			cmd.WriteTo(m_Port);
			cmd.ReadResponse(m_Port);
			VerifyResponse(cmd);
		}
		
		public void PrintReportByDates(DateTime start, DateTime end)
		{
			NovitusCommand cmd = new NovitusCommand("#o");
			cmd.AddNumericParameter(start.Year - 2000);
			cmd.AddNumericParameter(start.Month);
			cmd.AddNumericParameter(start.Day);
			cmd.AddNumericParameter(end.Year - 2000);
			cmd.AddNumericParameter(end.Month);
			cmd.AddNumericParameter(end.Day);
			cmd.AddNumericParameter(0);
			cmd.AddArgument(CashDeskNumber);
			cmd.AddArgumentCR();
			cmd.AddArgument(CashierName);
			cmd.AddArgumentCR();
			cmd.WriteTo(m_Port);
			cmd.ReadResponse(m_Port);
			VerifyResponse(cmd);
		}
		
		public IFiscalReceipt BeginTransaction()
		{
			return new NovitusFiscalReceipt(m_Port, this);
		}
		
		internal int SendENQ()
		{
			m_Port.DiscardInBuffer();
			m_Port.DiscardOutBuffer();
			m_Port.WriteByte(0x05);
			return m_Port.ReadByte();
				
		}
		
		internal static int PaymentMethodToNative(PaymentMethod method)
		{
			switch (method) {
			case PaymentMethod.Cash: return 0;
			case PaymentMethod.Card: return 1;
			case PaymentMethod.Check: return 2;
			case PaymentMethod.Credit: return 4;
			case PaymentMethod.Voucher: return 3;
			case PaymentMethod.Points: return 6;
			}
			
			return -1;
		}
		
		internal void VerifyResponse(NovitusCommand cmd)
		{
			if (cmd.ErrorCode != 0) {
				NovitusError err = errors.Where(i => cmd.ErrorCode > i.Min && cmd.ErrorCode < i.Max).FirstOrDefault();
				if (err == null)
					err = new NovitusError(0, 0, "Nieznany błąd drukarki fiskalnej");
				
				throw new FiscalPrinterError(err.Message, cmd.Command, cmd.ErrorCode);
			}
		}
		
		private SerialPort m_Port;
		private DriverConfiguration m_Configuration;
	}
}

