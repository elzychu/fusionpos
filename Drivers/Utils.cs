using System;
using System.Linq;
using System.IO.Ports;
using System.Globalization;
using System.Text;
using System.Collections.Generic;

namespace FusionPOS.Drivers
{
	public static class CP1250Extensions
	{
		public static byte[] ToCP1250(this string str)
		{
			Encoding enc = Encoding.GetEncoding("CP1250");
			return enc.GetBytes(str);
		}
		
		public static string FromCP1250(this IEnumerable<byte> self)
		{
			Encoding enc = Encoding.GetEncoding("CP1250");
			return enc.GetString(self.ToArray());
		}
	}
	
	public static class DecimalExtensions
	{
		public static string PosnetValue(this decimal self)
		{
			return (self * 100).ToString("########");
		}
		
		public static string NovitusValue(this decimal self)
		{
			NumberFormatInfo nfi = new NumberFormatInfo();
			nfi.NumberDecimalSeparator = ".";
			return self.ToString("########.##", nfi);       
		}
	}
	
	public static class StreamExtensions
	{
		public static IList<byte> ReadUntil(this SerialPort self, byte terminator)
		{
			IList<byte> response = new List<byte>();
			byte read;
			
			for (;;) {
				read = (byte)self.ReadByte();
				if (read == terminator)
					return response;
				
				response.Add(read);
			}
		}
		
		public static IList<byte> ReadCount(this SerialPort self, int count)
		{
			byte[] response = new byte[count];
			self.Read(response, 0, count);
			return new List<byte>(response);
		}
		
		public static void WriteByte(this SerialPort self, byte data)
		{
			self.Write(new byte[] { data }, 0, 1);	
		}
	}
}

