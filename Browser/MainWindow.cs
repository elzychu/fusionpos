using System;
using Gtk;
using WebKit;
using FusionPOS;
using FusionPOS.Translations;
using Widgets = FusionPOS.Widgets;


public class MainWindow: BaseWindow
{	
	public MainWindow(): base ()
	{
		Gtk.EventBox statusbar = new Gtk.EventBox();
		Gtk.HBox status = new Gtk.HBox();
		
		Widgets.Label statusLabel = new Widgets.Label("FusionPOS: Browser");
		statusLabel.SetAlignment(0f, 0.5f);
		
		Widgets.Label timeLabel = new Widgets.Label("21:37");
		timeLabel.SetAlignment(1.0f, 0.5f);
		
		GLib.Idle.Add(delegate() {
			timeLabel.Text = DateTime.Now.ToLongTimeString();
			return true;
		});
	
		m_WebView = new WebView();
		m_AddressBox = new Gtk.Entry();
		Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
		ScrolledWindow scroll = new ScrolledWindow();
		
		FusionPOS.Config.ConfigContext ctx = new FusionPOS.Config.ConfigContext();
		
		ctx.ReadFile("/Configuration/Application.xml");
		string chiefAddress = ctx.Sections["server"].Properties["chief"];
		
		
		buttons.AddLeftImageButton("Wstecz", "go-previous-5",  delegate(object s, EventArgs args) {
			m_WebView.GoBack();
		});
		
		buttons.AddLeftImageButton("Naprzód", "go-next-5", delegate(object s, EventArgs args) {
			m_WebView.GoForward();
		});
		buttons.AddLeftImageButton("Przejdź", "go-jump-3", delegate(object sender, EventArgs e) {
			m_WebView.Open(m_AddressBox.Text);	
		});
		buttons.AddLeftImageButton("Panel Szefa", "configure-4", delegate(object sender, EventArgs e) {
			m_WebView.Open(chiefAddress);	
		});
		
		bool keyboard;
		buttons.AddRightImageToggle("Klawiatura", "go-jump-locationbar", (sender, e) => {
			Gtk.ToggleButton button = sender as Gtk.ToggleButton;
			if( button.Active )
				BaseWindow.getInstance().ShowKeyboard();
			else
				BaseWindow.getInstance().HideKeyboard();
		});
		buttons.AddRightImageButton("Wyjdź", "application-exit", delegate(object sender, EventArgs e) {
			Application.Quit();	
		});
		
		
		m_WebView.NavigationRequested += delegate(object o, NavigationRequestedArgs args) {
			m_AddressBox.Text = args.Request.Uri.ToString();
		};
		
		m_WebView.SetSizeRequest(-1, 0);
		//m_WebView.FocusInEvent += (o, args) => ShowKeyboard();
		//m_WebView.FocusOutEvent += (o, args) => HideKeyboard();
		
		m_WebView.Open("http://google.com");
		
		scroll.Add(m_WebView);
		status.PackStart(statusLabel, true, true, 0);
		status.PackStart(timeLabel, true, true, 0);
		statusbar.SetSizeRequest(-1, 30);
		statusbar.StyleContext.AddClass("Inverted");
		statusbar.Add(status);
		VBox.PackStart(statusbar, false, true, 0);
		VBox.PackStart(m_AddressBox, false, false, 0);
		VBox.PackStart(buttons, false, false, 0);
		VBox.PackStart(new Gtk.Separator(Orientation.Horizontal), false, false, 0);
		VBox.PackStart(scroll, true, true, 0);
		VBox.PackStart(new Gtk.Separator(Orientation.Horizontal), false, false, 0);
		SetSizeRequest(1024, 720);
		ShowAll();
		Fullscreen();
	}
	
	/*protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit();
		a.RetVal = true;
	}*/
	
	private WebView m_WebView;
	private Gtk.Entry m_AddressBox;
}
