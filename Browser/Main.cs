using System;
using System.Collections.Generic;
using System.Linq;
using Gtk;
using FusionPOS.Logging;
using FusionPOS.Translations;

namespace Browser
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Gtk.CssProvider css = new Gtk.CssProvider();
			Application.Init ();
			css.LoadFromPath("/Configuration/Interface.rc");
			Gtk.StyleContext.AddProviderForScreen(Gdk.Screen.Default, css, 600);
			
			LoggerContext lcx = new LoggerContext();
			lcx.AddOutputStream(Console.Out);
			Logger l = new Logger(lcx, "I18N");
			TranslationContext tcx = new TranslationContext();
			tcx.Logger = l;
			tcx.AddTranslationsDirectory("/Configuration/Languages");
			Translator t = new Translator("Browser");
			t.LoadLanguage(tcx.GetLanguage("pl_PL"));
			MainWindow win = new MainWindow();
			win.Show ();
			Application.Run ();
		}
	}
}
