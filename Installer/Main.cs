using System;
using Gtk;

namespace Installer
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			Gtk.Rc.AddDefaultFile("../../../Interface.rc");
			Gtk.Rc.Parse("../../../Interface.rc");
			MainWindow win = new MainWindow ();
			win.Show ();
			Application.Run ();
		}
	}
}
