using System;
using FusionPOS;
using Widgets = FusionPOS.Widgets;
using Gtk;

namespace Installer
{
	public class InstallationProgressForm: BaseForm
	{
		Gtk.ProgressBar m_ProgressBar;
		Widgets.Label m_CurrentProcess;
		Gtk.Image m_Bitmap;
		
		public InstallationProgressForm (): base()
		{
			m_ProgressBar = new ProgressBar();
			m_CurrentProcess = new Widgets.Label("Aktualny proces");
			m_Bitmap = new Image();
			
			VBox vbox = new VBox(false, 1);
			
			vbox.PackStart(m_Bitmap, true, true, 0);
			vbox.PackStart(m_CurrentProcess, true, true, 0);
			vbox.PackStart(m_CurrentProcess, false, false, 0);
			
			Add(vbox);
			ShowAll();
		}
	}
}

