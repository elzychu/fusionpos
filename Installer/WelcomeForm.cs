using System;
using Gtk;
using FusionPOS;

namespace Installer
{
	public class WelcomeForm: WizardForm
	{
		private String m_Text = "Witaj w instalatorze rozwiązania FusionPOS!" +
			"\n    Zapraszamy do instalacji";
		
		public WelcomeForm (): base()
		{
			Title = "Witaj!";
			Label label = new Label(m_Text);
			Add(label);
			ShowAll();
		}
		
		public override FormStatus Validate ()
		{
			return FormStatus.V_OK;
		}
	}
}

