using System;
using FusionPOS;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;
using Gtk;

namespace Installer
{
	public class InetConfigForm: WizardForm
	{
		public InetConfigForm ()
		{
			Title = "Konfiguracja sieci";
			Gtk.RadioButton dhcpOption = new Gtk.RadioButton("DHCP");
			Gtk.RadioButton manualOption = new Gtk.RadioButton(dhcpOption, "Ustawienia ręczne");
			Gtk.RadioButton nonetOption = new Gtk.RadioButton(manualOption, "Bez sieci");
			
			Widgets.Button getaddrButton = new Widgets.Button("Pobierz adres automatycznie");
			
			Widgets.Label ipaddrLabel = new Widgets.Label("Adres ip");
			Widgets.IpEntry ipentry = new Widgets.IpEntry("192.168.0.1");
			
			Widgets.Label netmaskLabel = new Widgets.Label("Maska podsieci ip");
			Widgets.IpEntry maskentry = new Widgets.IpEntry("255.255.255.0");
			
			Widgets.Label gateLabel = new Widgets.Label("Bramka domyślna");
			Widgets.IpEntry gateentry = new Widgets.IpEntry("192.168.0.1");
			
			Widgets.Label dnsLabel = new Widgets.Label("Serwery DNS");
			Widgets.IpEntry dns1entry = new Widgets.IpEntry("127.0.0.1");
			Widgets.IpEntry dns2entry = new Widgets.IpEntry("127.0.0.1");
			
			Widgets.VBox vbox = new Widgets.VBox();
			
			Widgets.Table table = new Widgets.Table(5, 2, false);
			
			table.Sensitive = false;
			
			dhcpOption.Clicked += delegate(object sender, EventArgs e) {
				table.Sensitive = false;
				getaddrButton.Sensitive = true;
			};
			
			manualOption.Clicked += delegate(object sender, EventArgs e) {
				table.Sensitive = true;
				getaddrButton.Sensitive = false;
			};
			
			nonetOption.Clicked += delegate(object sender, EventArgs e) {
				table.Sensitive = false;
				getaddrButton.Sensitive = false;
			};
			
			vbox.PackStartSimple(dhcpOption);
			vbox.PackStart(getaddrButton, false, false, 0);
			vbox.PackStartSimple(manualOption);
			
			table.Attach(ipaddrLabel, 0, 1, 0, 1);
			table.Attach(ipentry, 1, 2, 0, 1);

			table.Attach(netmaskLabel, 0, 1, 1, 2);
			table.Attach(maskentry, 1, 2, 1, 2);
			
			table.Attach(gateLabel, 0, 1, 2, 3);
			table.Attach(gateentry, 1, 2, 2, 3);
			
			table.Attach(dnsLabel, 0, 1, 3, 5);
			table.Attach(dns1entry, 1, 2, 3, 4);
			table.Attach(dns2entry, 1, 2, 4, 5);
			
			vbox.PackStart(table, true, true, 0);

			vbox.PackStartSimple(nonetOption);
			
			Add (vbox);
			ShowAll();
		}
		
		public override FormStatus Validate ()
		{
			
			return FormStatus.V_NOTFILLED;
		}
	}
}

