using System;
using System.Collections;
using System.Collections.Generic;
using Gtk;
using FusionPOS;
using Widgets = FusionPOS.Widgets;
using Installer;
using FusionPOS.Translations;


public enum FormStatus {V_OK, V_PROGRESS, V_STHWRONG, V_NOTFILLED}

public abstract class WizardForm: BaseForm {
	public string Title;
	abstract public FormStatus Validate();
}

public class StatusLabel: HBox
{
	private Image m_Ico;
	public StatusLabel(string text): base(false, 0)
	{
		Widgets.Label label = new Widgets.Label(text);
		m_Ico = new Gtk.Image(Stock.DialogQuestion, IconSize.Menu);
		
		m_Ico.SetSizeRequest(30, 30);
		PackStart(m_Ico, false, true, 0);
		PackStart(label, true, true, 0);
	}
	
	public void Done()
	{
		m_Ico.SetFromStock(Stock.Apply, IconSize.Menu);
	}
	
	public void InProgress()
	{
		m_Ico.SetFromStock(Stock.MediaPlay, IconSize.Menu);
	}
	
	public void SomethingWrong()
	{
		m_Ico.SetFromStock(Stock.DialogWarning, IconSize.Menu);
	}
	
	public void NotEverythingFilled()
	{
		m_Ico.SetFromStock(Stock.DialogQuestion, IconSize.Menu);
	}
}

public class FormStatusView: VBox
{
	private IList<StatusLabel> m_Labels;
	private IList<WizardForm> m_Forms;
	public FormStatusView(List<WizardForm> list): base(false, 1)
	{
		m_Forms = list;
		m_Labels = new List<StatusLabel>();
		foreach(WizardForm form in list)
		{
			Gtk.Alignment aligment = new Gtk.Alignment(0, 0, 0, 0);
			StatusLabel label = new StatusLabel(form.Title);
			m_Labels.Add(label);
			
			aligment.Add(label);
			PackStart(aligment, false, true, 0);
		}
		
		m_Labels[0].InProgress();
	}
	
	public void Update(int id)
	{
		for(int i=0; i<3; ++i)
		{
			if((i-1+id)<0 || (i-1+id)>=m_Forms.Count)
				continue;
			if(i==1)
			{
				m_Labels[id].InProgress();
				continue;
			}
			
			WizardForm form = m_Forms[i-1+id];
			switch(form.Validate())
			{
			case FormStatus.V_OK:
				m_Labels[i-1+id].Done();		
			break;
			case FormStatus.V_STHWRONG:
				m_Labels[i-1+id].SomethingWrong();	
			break;
			case FormStatus.V_PROGRESS:
				m_Labels[i-1+id].InProgress();	
			break;
			case FormStatus.V_NOTFILLED:
				m_Labels[i-1+id].NotEverythingFilled();
			break;
			}
		}
	}
}

public class MainWindow: BaseWindow
{	
	private IList<WizardForm> m_Forms;
	private InstallationProgressForm m_InstallationForm;
	private int m_CurrentFormIndex;
	private EventBox m_EventBox;
	private Widgets.WindowLabel m_Title;
	private TranslationContext m_TContext;
	public MainWindow (): base ()
	{
		m_Forms = new List<WizardForm>();
		m_CurrentFormIndex = 0;
		m_EventBox = new EventBox();
		m_Title = new Widgets.WindowLabel();
		m_TContext = new TranslationContext();
		m_TContext.AddTranslationsDirectory("../../../");
		
		AccountSettingsForm accsetfr = new AccountSettingsForm();
		SelectLanguageForm slctlg = new SelectLanguageForm(m_TContext);
		WelcomeForm wlcform = new WelcomeForm();
		InetConfigForm inetform = new InetConfigForm();
		CompanyDataForm cmpdtform = new CompanyDataForm();
		
		m_InstallationForm = new InstallationProgressForm();
		
		m_Forms.Add(slctlg);
		m_Forms.Add(wlcform);
		m_Forms.Add(accsetfr);
		m_Forms.Add(inetform);
		m_Forms.Add(cmpdtform);
		
		m_EventBox.Add(slctlg);
		m_Title.Text = slctlg.Title;
		
		Button nextBnt = new Widgets.Button("Następny");
		Button preBnt = new Widgets.Button("Poprzedni");
		Button finBnt = new Widgets.Button("Finisz");
		Button clsBnt = new Widgets.Button("Zamknij");
		
		
		preBnt.Sensitive = false;
		finBnt.Sensitive = false;
		
		FormStatusView statusView = new FormStatusView((List<WizardForm>)m_Forms);
		
		nextBnt.Clicked += delegate(object sender, EventArgs e) {
			if (m_CurrentFormIndex < m_Forms.Count)
			{
				++m_CurrentFormIndex;
				m_EventBox.Remove(m_EventBox.Child);
				m_EventBox.Add(m_Forms[m_CurrentFormIndex]);
				m_Title.Text = m_Forms[m_CurrentFormIndex].Title;
				if(m_CurrentFormIndex == m_Forms.Count-1)
				{
					nextBnt.Visible = false;
					finBnt.Visible = true;
					nextBnt.Sensitive = false;
					
					bool flag=true;
					/*foreach(WizardForm form in m_Forms)
					{
						if(form.Validate() != FormStatus.V_OK)
						{
							flag=false;
							break;
						}
					}*/
					
					//if(flag)
						finBnt.Sensitive = true;
				}
				
				if(m_CurrentFormIndex==1)
					preBnt.Sensitive = true;
				statusView.Update(m_CurrentFormIndex);
			}
		};
		
		preBnt.Clicked += delegate(object sender, EventArgs e) {
			if (m_CurrentFormIndex > 0)
			{
				--m_CurrentFormIndex;
				m_EventBox.Remove(m_EventBox.Child);
				m_EventBox.Add(m_Forms[m_CurrentFormIndex]);
				m_Title.Text = m_Forms[m_CurrentFormIndex].Title;
				
				if(m_CurrentFormIndex == 0)
					preBnt.Sensitive = false;
				
				if(m_CurrentFormIndex==m_Forms.Count-2)
				{
					nextBnt.Visible = true;
					finBnt.Visible = false;
					nextBnt.Sensitive = true;
				}
				
				
				
				statusView.Update(m_CurrentFormIndex);
			}	
		};
		 
		clsBnt.Clicked+= delegate(object sender, EventArgs e) {
			Application.Quit();
		};
		
		finBnt.Clicked+= delegate(object sender, EventArgs e) {
			m_EventBox.Remove(m_EventBox.Child);
			m_EventBox.Add(m_InstallationForm);
			m_Title.Text = "Trwa instalacja...";
		};
		
		ModifyBg(StateType.Normal, new Gdk.Color((byte)97, 
			(byte)186, (byte)250));
		
		HBox hbox = new HBox(false, 0);
		hbox.Add(preBnt);
		hbox.Add(clsBnt);
		hbox.Add(nextBnt);
		hbox.Add(finBnt);

		HBox upperHBox = new HBox(false, 0);
		
		Gtk.Alignment aligment = new Gtk.Alignment(0, 0.2f, 0, 0);
		aligment.Add(statusView);
		//Name = "InstallerTreeBG";
		Gtk.Alignment upperAligment = new Gtk.Alignment(1, 0, 0, 0);
		
		m_Title.Name = "Inverted";
		HBox titleBar = new HBox(false, 0);
		titleBar.PackStart(m_Title, true, true, 0);
		titleBar.PackStart(upperAligment, false, false, 0);
		
		upperHBox.PackStart(aligment, false, false, 0);
		upperHBox.PackStart(m_EventBox, true, true, 30);
		VBox.PackStart(titleBar, false, false, 0);
		VBox.PackStart(upperHBox, true, true, 30);
		VBox.PackStart(hbox, false, false, 0);
		ShowAll();	
		
		finBnt.Visible=false;
	}
	
	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}
}
