using System;
using Gtk;
using Widgets = FusionPOS.Widgets;

namespace Installer
{
	public class BankAccNumber: Widgets.TextBox
	{
		public BankAccNumber(): base()
		{
			SetSizeRequest(16*26, -1);	
			MaxLength = 26;
		}
	}
	
	public class NipEntry: Gtk.HBox
	{
		private Widgets.TextBox m_Nip;
		public bool Validate(string nip)
		{
			if (nip.Length != 10) return false;
 
			try { Int64.Parse(nip); }
			catch { return false; }
			
			int[] weights = new int[] { 6, 5, 7, 2, 3, 4, 5, 6, 7 };
			int sum = 0;
			for (int i = 0; i < weights.Length; i++)            
			sum += int.Parse(nip.Substring(i, 1)) * weights[i];                        
			
			return (sum % 11) == int.Parse(nip.Substring(9, 1));
		}
		
		public NipEntry(): base(true, 1)
		{
			Widgets.Label nipLbl = new Widgets.Label("niepoprawny");
			m_Nip = new Widgets.TextBox();
			m_Nip.MaxLength = 11;
			m_Nip.Changed+= delegate(object sender, EventArgs e) {
				if(Validate(m_Nip.Text))
					nipLbl.Text = "poprawny";
				else
					nipLbl.Text = "niepoprawny";
			};
			
			m_Nip.SetSizeRequest(16*11, -1);
			PackStart(m_Nip, true, true, 1);
			PackStart(nipLbl, false, false, 1);
		}
		
		public string GetNip()
		{
			if(Validate(m_Nip.Text))
				return m_Nip.Text;
			else
				return "";
		}
	}
	public class AddressEntry: Gtk.Alignment
	{
		private Widgets.TextBox m_Street;
		private Widgets.TextBox m_Postoffice;
		private Widgets.TextBox m_PostalCodeA;
		private Widgets.TextBox m_PostalCodeB;
		
		public AddressEntry(): base(0,0,0,0.1f)
		{
			Widgets.Label streetLbl = new Widgets.Label("Nazwa ulicy");
			Widgets.Label postofficeLbl = new Widgets.Label("Poczta");
			Widgets.Label postalcodeLbl = new Widgets.Label("Kod pocztowy");
			Widgets.Label kreska = new Widgets.Label("-");
			
			m_Street = new Widgets.TextBox();
			m_Postoffice = new Widgets.TextBox();
			m_PostalCodeA = new Widgets.TextBox();
			m_PostalCodeB = new Widgets.TextBox();
			
			m_PostalCodeA.MaxLength = 2;
			m_PostalCodeB.MaxLength = 3;
			
			Gtk.Alignment aligment1 = new Gtk.Alignment(0,0,0,0);
			Gtk.Alignment aligment2 = new Gtk.Alignment(0,0,0,0);
			
			aligment1.Add(streetLbl);
			aligment2.Add(postalcodeLbl);
			
			m_PostalCodeA.SetSizeRequest(40, -1);
			m_PostalCodeB.SetSizeRequest(60, -1);
			
			Table table = new Table(6, 2, false);
			
			table.Attach(aligment1,0, 1, 0, 1);
			table.Attach(m_Street, 1, 6, 0, 1);
			table.Attach(aligment2, 0, 1, 1, 2);
			table.Attach(m_PostalCodeA, 1, 2, 1, 2);
			table.Attach(kreska, 2, 3, 1, 2);
			table.Attach(m_PostalCodeB, 3, 4, 1, 2);
			table.Attach(postofficeLbl, 4, 5, 1, 2);
			table.Attach(m_Postoffice, 5, 6, 1, 2);
			
			Add(table);
		}
	}
	
	public class CompanyDataForm: WizardForm
	{
		private Widgets.TextBox m_CompanyName;
		private AddressEntry m_AddressEntry;
		private NipEntry m_NipEntry;
		private Widgets.TextBox m_RegonEntry;
		private BankAccNumber m_AccNumEntry;
		private Widgets.TextBox m_TelephoneEntry;
		
		public CompanyDataForm (): base()
		{
			Title = "Dane przedsiębiorstwa";
			
			Table table = new Table(7, 2, false);
			
			m_CompanyName = new Widgets.TextBox();
			m_AddressEntry = new AddressEntry();
			m_NipEntry = new NipEntry();
			m_RegonEntry = new Widgets.TextBox();
			m_AccNumEntry = new BankAccNumber();
			m_TelephoneEntry = new Widgets.TextBox();
				
			Widgets.Label companyNameLbl = new Widgets.Label("Nazwa firmy");
			Widgets.Label nipLbl = new Widgets.Label("Nip");
			Widgets.Label regonLbl = new Widgets.Label("Regon");
			Widgets.Label accountLbl = new Widgets.Label("Nr. Konta");
			Widgets.Label telephoneLbl = new Widgets.Label("Nr. Telefonu");
			
			Alignment allContainer = new Alignment(0,0,0,0.2f);
			Alignment aligment1 = new Alignment(0,0.5f,0,0);
			Alignment aligment2 = new Alignment(0,0,0,0);
			Alignment aligment3 = new Alignment(0,0,0,0);
			Alignment aligment4 = new Alignment(0,0,0,0);
			Alignment aligment5 = new Alignment(0,0,0,0);
			
			aligment1.Add(companyNameLbl);
			aligment2.Add(nipLbl);
			aligment3.Add(regonLbl);
			aligment4.Add(accountLbl);
			aligment5.Add(telephoneLbl);
			
			table.Attach(aligment1, 0, 1, 0, 1);
			table.Attach(m_CompanyName, 1, 2, 0, 1);
			table.Attach(m_AddressEntry, 0, 2, 1, 2);
			table.Attach(aligment2, 0, 1, 2, 3);
			table.Attach(m_NipEntry, 1, 2, 2, 3);
			table.Attach(aligment3, 0, 1, 3, 4);
			table.Attach(m_RegonEntry, 1, 2, 3, 4);
			table.Attach(aligment4, 0, 1, 4, 5);
			table.Attach(m_AccNumEntry, 1, 2, 4, 5);
			table.Attach(aligment5, 0, 1, 5, 6);
			table.Attach(m_TelephoneEntry, 1, 2, 5, 6);
			
			allContainer.Add(table);
			Add (allContainer);
			
			ShowAll();
		}
		
		public override FormStatus Validate ()
		{
			return FormStatus.V_NOTFILLED;
		}
	}
}

