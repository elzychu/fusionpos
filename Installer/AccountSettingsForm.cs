using System;
using Gtk;
using FusionPOS;
using System.Text.RegularExpressions;
using Widgets = FusionPOS.Widgets;

namespace Installer
{
	public class PasswdEntry: Widgets.TextBox
	{
		private Regex m_Regex;
		private uint m_Count;
		private string m_Buffer;
		
		public PasswdEntry(string format, uint count, string text): base()
		{
			m_Regex = new Regex(format);
			m_Count = count;
			Visibility = false;
		}
		
		public PasswdEntry(string format, uint count): this(format, count, "")
		{ }
		
		public PasswdEntry(uint count): this(".*", count, "")
		{ } 
		
		public PasswdEntry(string format): this(format, 0, "")
		{ }
		
		public PasswdEntry(): this(".*", 0, "")
		{ }
		
		protected override bool OnFocusOutEvent (Gdk.EventFocus evnt)
		{
			if (m_Regex.IsMatch(Text))
				m_Buffer = Text;
			else
			{
				Text = m_Buffer;
				MessageBox.Popup("FusionPOS", "Nieprawidłowy format hasła");
			}
			return base.OnFocusOutEvent (evnt);
		}
		
		public string GetPasswd()
		{
			return m_Buffer;	
		}
		
		public bool IsFormatCorrect()
		{
			return m_Regex.IsMatch(m_Buffer);	
		}
	}
	
	public class AccountSettingsForm: WizardForm
	{
		private PasswdEntry m_PasswdEntry;
		private Widgets.TextBox m_UserNameEntry;
		private PasswdEntry m_PinEntry;
		
		public AccountSettingsForm (): base()
		{
			Title = "Ustawienia konta";
			
			Widgets.Label pswdLbl = new Widgets.Label("Hasło na roota");
			Widgets.Label usrnameLbl = new Widgets.Label("Nazwa użytkownika");
			Widgets.Label pinLbl = new Widgets.Label("Pin dla użytkownika");
			
			m_PasswdEntry = new PasswdEntry(7);
			m_UserNameEntry = new Widgets.TextBox();
			m_PinEntry = new PasswdEntry("[0-9]{4}", 4);
			
			m_PasswdEntry.SetSizeRequest(300, -1);
			
			Table table = new Table(3, 2, false);
			table.SetSizeRequest(300, 300);
			
			Gtk.Alignment aligment = new Gtk.Alignment(0.2f, 0.2f, 0, 0);
			aligment.Add(table);
			
			Gtk.Alignment [] textBox = new Gtk.Alignment[3];
			for(int i=0; i<3; ++i)
				textBox[i] = new Gtk.Alignment(0,0.5f,0,0);
			
			textBox[0].Add(pswdLbl);
			textBox[1].Add(usrnameLbl);
			textBox[2].Add(pinLbl);
			
			table.Attach(textBox[0], 0, 1, 0, 1);
			table.Attach(textBox[1], 0, 1, 1, 2);
			table.Attach(textBox[2], 0, 1, 2, 3);
			table.Attach(m_PasswdEntry, 1, 2, 0, 1);
			table.Attach(m_UserNameEntry, 1, 2, 1, 2);
			table.Attach(m_PinEntry, 1, 2, 2, 3);
			
		
			Add (aligment);
			ShowAll();
		}
		
		public override FormStatus Validate ()
		{
			return FormStatus.V_NOTFILLED;
		}
	}
}

