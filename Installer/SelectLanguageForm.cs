using System;
using Gtk;
using FusionPOS;
using FusionPOS.Translations;
using Widgets = FusionPOS.Widgets;

namespace Installer
{
	public class SelectLanguageForm: WizardForm
	{
		private TranslationContext m_TranslationContext;
		public SelectLanguageForm (TranslationContext tContext): base()
		{
			m_TranslationContext = tContext;
			Title = "Select language";
			TreeView treeView = new TreeView();
			
			Label label = new Label("Select language");
			
			Gtk.ListStore store = new Gtk.ListStore(typeof(TranslationLanguage));
			treeView.Model =store;
			Widgets.TreeViewColumn col = new Widgets.TreeViewColumn("Język", (model) => (model as TranslationLanguage).Metadata["language"]);
			treeView.AppendColumn(col);
			
			foreach(TranslationLanguage tLang in m_TranslationContext.AvailableLanguages)
			{
				store.AppendValues(tLang);
			}
			Add(treeView);
			ShowAll();
		}
		
		public override FormStatus Validate ()
		{
			return FormStatus.V_NOTFILLED;
		}
	}
}

