using System;
using System.IO;
using System.IO.Ports;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Mono.Unix;
using FusionPOS.Domain;
using FusionPOS.Drivers;

namespace PrinterControl
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			new MainClass();
		}
		
		public MainClass()
		{
			m_Prompt = "printer> ";
			m_Variables = new Dictionary<string, string>();
			m_Variables["cashier"] = "Janusz";
			m_Variables["cashdesk"] = "1";
			MainLoop();
		}
		
		public void MainLoop()
		{
			Console.WriteLine("FusionPOS Fiscal Printer Control");
			for (;;) {
				string[] args;
				string fmt = m_ReceiptOpened ? "CmdReceipt_{0}" : "Cmd_{0}";
				
				Console.Write(m_Prompt);
				args = Console.ReadLine().Split(' ');
				
				MethodInfo method = GetType().GetMethod(string.Format(fmt, args[0]), BindingFlags.NonPublic | BindingFlags.Instance);
				if (method == null) {
					Console.WriteLine(string.Format("unknown command: {0}", args[0]));
					continue;
				}
			
				try {
					method.Invoke(this, new object[] { args });
				} catch (TargetInvocationException ex) {
					FiscalPrinterError err = ex.InnerException as FiscalPrinterError;
					if (err != null) {
						Console.WriteLine("ERROR code: {0}, command: {1}, message: {2}", 
						    err.ErrorCode, err.ErrorCommand, err.Message);
						continue;
					}
					
					throw ex.InnerException;
				}
			}
		}
		
		private void Cmd_load(string[] args)
		{
			Type type = Type.GetType(string.Format("FusionPOS.Drivers.{0}, Drivers", args[1]));
			m_Driver = (IFiscalPrinterDriver)Activator.CreateInstance(type);
			m_Driver.CashierName = m_Variables["cashier"];
			m_Driver.CashDeskNumber = m_Variables["cashdesk"];
		}
		
		private void Cmd_open(string[] args)
		{
			if (args.Length < 2) {
				Console.WriteLine("usage: open <port name>");
				return;
			}
			
			DriverConfiguration cfg = new DriverConfiguration();
			SerialPort port = new SerialPort(args[1], 19200);
			port.Open();
			cfg.Port = port;
			m_Driver.Configure(cfg);
			m_Driver.Initialize();
			Console.WriteLine("connected");
		}
		
		private void Cmd_display(string[] args)
		{
			if (args.Length < 3) {
				Console.WriteLine("usage: display <lineno> <text>");
				return;
			}
			
			m_Driver.DisplayText(string.Join(" ", args.Skip(2)), int.Parse(args[1]));
		}
		
		private void Cmd_displayop(string[] args)
		{
			if (args.Length < 3) {
				Console.WriteLine("usage: displayop <text>");
				return;
			}
			
			m_Driver.DisplayTextOperator(args[1]);
		}
		
		private void Cmd_set(string[] args)
		{
			if (args.Length == 1) {
				foreach (var pair in m_Variables)
					Console.WriteLine(string.Format("{0}={1}", pair.Key, pair.Value));
				
				return;
			}
			
			if (args.Length == 2) {
				if (!m_Variables.ContainsKey(args[1])) {
					Console.WriteLine("no such variable");
					return;
				}
				
				Console.WriteLine(string.Format("{0}={1}", args[1], m_Variables[args[1]]));
			}
			
			if (args.Length == 3) {
				m_Variables[args[1]] = args[2];
				if (m_Driver == null)
					return;
				if (args[1] == "cashier")
					m_Driver.CashierName = args[2];
				if (args[1] == "cashdesk")
					m_Driver.CashDeskNumber = args[2];
				if (args[1] == "footer")
					m_Driver.Footer = args[2];
			}
		}
		
		private void Cmd_version(string[] args)
		{
			Console.Write("model: ");
			Console.WriteLine(m_Driver.ModelName);
		}
		
		private void Cmd_receipt(string[] args)
		{
			m_ReceiptOpened = true;
			m_Prompt = "printer receipt> ";
			m_Receipt = m_Driver.BeginTransaction();
		}
		
		private void Cmd_report(string[] args)
		{
			if (args.Length < 2) {
				Console.WriteLine("usage: report daily");
				return;
			}
			
			m_Driver.PrintDailyReport();
		}
		
		private void Cmd_quit(string[] args)
		{
			Environment.Exit(0);
		}
		
		private void CmdReceipt_add(string[] args)
		{
			if (args.Length < 5) {
				Console.WriteLine("usage: add <name> <amount> <unit_price> <ptu>");
				return;
			}
			
			decimal unitPrice = decimal.Parse(args[3]);
			decimal amount = decimal.Parse(args[2]);
			m_Receipt.Add(args[1], amount, char.Parse(args[4]), unitPrice, 0);
		}
		
		private void CmdReceipt_addline(string[] args)
		{
			if (args.Length < 3) {
				Console.WriteLine("usage: addline <type> <value>");
				return;
			}
			
			m_Receipt.AddExtraLine((ReceiptLineType)Enum.Parse(typeof(ReceiptLineType), args[1]), args[2]);	
		}
		
		private void CmdReceipt_pay(string[] args)
		{
			if (args.Length < 3) {
				Console.WriteLine("usage: pay <method> <amount>");
				return;
			}
			
			m_Receipt.AddPayment((PaymentMethod)Enum.Parse(typeof(PaymentMethod), args[1]), decimal.Parse(args[2]));
		}
		
		private void CmdReceipt_rebate(string[] args)
		{
			if (args.Length < 3) {
				Console.WriteLine("usage: rebate <name> <percent>");
				return;
			}
			
			m_Receipt.ApplyRebate(0, decimal.Parse(args[2]), args[1]);
		}
		
		private void CmdReceipt_submit(string[] args)
		{
			if (args.Length < 2) {
				Console.WriteLine("usage: submit <system_number>");
				return;
			}
		
			m_Receipt.Commit(args[1]);
			m_Receipt.Dispose();
			m_ReceiptOpened = false;
			m_Prompt = "printer> ";
		}
		
		private void CmdReceipt_abort(string[] args)
		{
			m_Receipt.Abort();
			m_Receipt.Dispose();
			m_ReceiptOpened = false;
			m_Prompt = "printer> ";
		}
		
		private void PrintResponse(byte[] response)
		{
			Console.Write("response: ");
			
			foreach (byte i in response)
				Console.Write(i >= 32 ? ((char)i).ToString() : string.Format("<#{0:x2}>", i));
			
			Console.Write('\n');
		}
		
		private string m_Prompt;
		private bool m_Opened;
		private bool m_ReceiptOpened;
		private IFiscalReceipt m_Receipt;
		private IFiscalPrinterDriver m_Driver;
		private StdioFileStream m_Port;
		private IDictionary<string, string> m_Variables;
	}
}
