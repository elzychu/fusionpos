DROP DATABASE IF EXISTS fusionpos;
CREATE DATABASE fusionpos CHARACTER SET utf8;
GRANT ALL ON fusionpos.* TO 'posclient'@'localhost' IDENTIFIED BY 'client';
GRANT ALL ON fusionpos.* TO 'posserver'@'localhost' IDENTIFIED BY 'server';
