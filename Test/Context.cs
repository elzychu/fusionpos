using System;
using System.Collections.Generic;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using FusionPOS.Config;
using FusionPOS.Logging;
using FusionPOS.Services;
using System.ServiceModel;
using FusionPOS;
using FusionPOS.Domain;
using System.ServiceModel.Description;

namespace Test
{
	public class Context
	{
		public Context()
		{
		}
		
		public void Initialize()
		{
			/* Initialize logger */
			Logger = new LoggerContext();
			Logger.AddOutputStream(Console.Out);
			Logger.Log("SZEF", LogPriority.Info, "Loaded");
			m_Logger = new Logger(Logger, "SZEF");
			m_DbLogger = new Logger(Logger, "DB");
			m_ServiceLogger = new Logger(Logger, "SERVICE");
			
			/* Read XML configuration */
			Configuration = new ConfigContext();
			Configuration.ReadFile("/Configuration/Application.xml");
			Configuration.ReadFile("/Configuration/Server.xml");
			/* ... */	


			/* Build connection string */
			string connstr = String.Format(
			    "Server={0};Database={1};User ID={2};Password={3};AutoEnlist=false;CharSet=utf8",
			    Configuration.Sections["database"].Properties["host"],
			    Configuration.Sections["database"].Properties["db"],
			    Configuration.Sections["database"].Properties["user"],
			    Configuration.Sections["database"].Properties["password"]);
			
			m_DbLogger.Info(string.Format("Connection string: {0}", connstr));
			
			/* Create database connection */
			m_NhConfiguration = new Configuration();
			m_NhConfiguration.Configure();
			m_NhConfiguration.SetProperty("connection.connection_string", connstr);
			m_NhConfiguration.SetProperty("dialect", "FusionPOS.DecimalMysqlDialect, FusionPOS");
			m_NhConfiguration.SetProperty("show_sql", "true");
			m_NhConfiguration.AddAssembly("Shared");
			m_NhSessionFactory = m_NhConfiguration.BuildSessionFactory();
			
			SchemaExport sch = new SchemaExport(m_NhConfiguration);
			sch.Execute(true, true, false);
		}
		
		public LoggerContext Logger
		{
			get; set;
		}

		public ConfigContext Configuration
		{
			get; set;
		}
		
		public ISession OpenSession()
		{
			return m_NhSessionFactory.OpenSession();
		}
		
		private Logger m_Logger;
		private Logger m_DbLogger;
		private Logger m_ServiceLogger;
		private Configuration m_NhConfiguration;
		private ISessionFactory m_NhSessionFactory;
		private static Context m_Instance;
	}
}

