using System;
using System.Linq;
using System.Collections;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using Shared;
using NHibernate;
using NHibernate.Linq;
using FusionPOS.Domain;
using FusionPOS.Config;

namespace Test
{
	public class DatabaseGenerator
	{
		public static void Main(string [] args)
		{
			DatabaseGenerator generator = new DatabaseGenerator();
			generator.GetDataFromFile();
			generator.GetConfig();
			generator.PrepareDatabase();
			if(args.Count() > 0)
				if(args[0] == "-p")
					return;
			
			generator.Generate();
		}
		
		public DatabaseGenerator ()
		{
			m_Context = new Context();
			m_Context.Initialize();
		}
		
		public void PrepareDatabase()
		{
			using (ISession ses = m_Context.OpenSession())
			{
				Settlement settlement = new Settlement();
				SessionItem item = new SessionItem();
				
				using (ITransaction tr = ses.BeginTransaction())
				{
					DishCategory cat = new DishCategory();
					IngredientCategory icat = new IngredientCategory();
					
					cat.Id = 1L;
					cat.Color = "White";
					cat.Name = "Dania";
					
					icat.Id = 1L;
					icat.Name = "Składniki";
					
					User user = new User();
					user.FirstName = "";
					user.LastName = "";
					user.Name = "admin";
					user.Pin = "1234";
					SHA1 hash = new SHA1CryptoServiceProvider();
					byte[] hashpass = hash.ComputeHash(UTF8Encoding.Default.GetBytes(AdminPassword));
					user.Password = BitConverter.ToString(hashpass).Replace("-", "").ToLower();
					user.Type = UserType.Admin;
					
					Currency currency = new Currency();
					currency.LongName = "PLN";
					currency.MainCurrency = true;
					currency.Name = "PLN";
					currency.Ratio = 1.0m;
					
					item.Key = "CurrentSettlement";

					settlement.Balance = 0;
					settlement.From = DateTime.Now;
					settlement.UserBalances = new List<UserBalance>();
					settlement.Status = SettlementStatus.Closed;
					
					ses.Save(item);
					ses.Save(settlement);
					ses.Save(currency);
					ses.Save(user);
					ses.Save(cat);
					ses.Save(icat);
					
					Device device = new Device();
					device.Enabled = true;
					device.IpAddress = "127.0.0.1";
					device.Name = "POS1";
					device.Key = RandString(StringType.Other);
					
					ses.Save(device);
					
					tr.Commit();
				}
				
				using (ITransaction tr = ses.BeginTransaction())
				{
					item.Value = BitConverter.GetBytes(settlement.Id);
					
					ses.Update(item);
					ses.Save(item);
					tr.Commit();
				}
			}
		}
		
		public void GetConfig()
		{
			string basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			ConfigContext context = new ConfigContext();
			context.ReadFile(Path.Combine(basePath, "Config.xml"));
			ClientCount = int.Parse(context.Sections["DataCounts"].Properties["Client"]);
			DaysCount = int.Parse(context.Sections["DataCounts"].Properties["Time"]);
			DishCount = int.Parse(context.Sections["DataCounts"].Properties["Dish"]);
			DishCategoryCount = int.Parse(context.Sections["DataCounts"].Properties["DishCategory"]);
			DishGroupCount = int.Parse(context.Sections["DataCounts"].Properties["DishGroup"]);
			IngredientCount = int.Parse(context.Sections["DataCounts"].Properties["Ingredient"]);
			IngredientCategoryCount = int.Parse(context.Sections["DataCounts"].Properties["IngredientCategory"]);
			IngredientGroupCount = int.Parse(context.Sections["DataCounts"].Properties["IngredientGroup"]);
			UserCount = int.Parse(context.Sections["DataCounts"].Properties["User"]);
			ReservationsCount = int.Parse(context.Sections["DataCounts"].Properties["Reservation"]);
			AdminPassword = context.Sections["Passwords"].Properties["AdminPassword"];
			DefaultPassword = context.Sections["Passwords"].Properties["UsersPassword"];
			IsClientCompanyProbability = float.Parse(context.Sections["Probabilities"].Properties["CompanyFrecurency"]);
			IsIngredientOptionalProbability = float.Parse(context.Sections["Probabilities"].Properties["IngredientOptional"]);
			IsRebateAmountProbability = float.Parse(context.Sections["Probabilities"].Properties["RebateAmount"]);
			IsRebatePercentProbability = float.Parse(context.Sections["Probabilities"].Properties["RebatePercent"]);
			IsAnyDishModyficationProbability = float.Parse(context.Sections["Probabilities"].Properties["Modification"]);
			IsNextCategoryProbability = float.Parse(context.Sections["Probabilities"].Properties["Category"]);
			IsOrderCancelledProbablity = float.Parse(context.Sections["Probabilities"].Properties["CancelledProbability"]);
			
			m_Kitchens = new List<Kitchen>();
			
			foreach(var i in context.Sections["Printers"].Children)
			{
				Kitchen kitchen = new Kitchen();
				kitchen.Name = i.Key;
				kitchen.PrinterPort = i.Value.Properties["Port"];
				if(i.Value.Properties.ContainsKey("Speed"))
					kitchen.PrinterConfiguration = string.Format("baudrate={0}", i.Value.Properties["Speed"]);
				m_Kitchens.Add(kitchen);
			}
		}
		
		public void GetDataFromFile()
		{
			string basePath;
			string line;
			
			basePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			
			using(StreamReader sr = new StreamReader(Path.Combine(basePath, "Data/Cities.txt")))
			{
				m_Cities = new List<String>();
				while((line = sr.ReadLine()) != null)
					m_Cities.Add(line);
			}
			
			using(StreamReader sr = new StreamReader(Path.Combine(basePath, "Data/Names.txt")))
			{
				m_Names = new List<String>();
				while((line = sr.ReadLine()) != null)
					m_Names.Add(line);
			}
			
			using(StreamReader sr = new StreamReader(Path.Combine(basePath, "Data/Streets.txt")))
			{
				m_Streets = new List<String>();
				while((line = sr.ReadLine()) != null)
					m_Streets.Add(line);
			}
			
			using(StreamReader sr = new StreamReader(Path.Combine(basePath, "Data/Surnames.txt")))
			{
				m_Surnames = new List<String>();
				while((line = sr.ReadLine()) != null)
					m_Surnames.Add(line);
			}
			
			using(StreamReader sr = new StreamReader(Path.Combine(basePath, "Data/Menu.txt")))
			{
				while((line = sr.ReadLine()) != null)
				{
					if(line == "Dish")
						m_DishNames = sr.ReadLine().Replace(", ", ",").Split(',').ToList();
					
					if(line == "DishCategory")
						m_DishCategories = sr.ReadLine().Replace(", ", ",").Split(',').ToList();
					
					if(line == "DishGroup")
						m_DishGroups = sr.ReadLine().Replace(", ", ",").Split(',').ToList();
					
					if(line == "Ingredient")
						m_IngredientNames = sr.ReadLine().Replace(", ", ",").Split(',').ToList();
					
					if(line == "IngredientCategory")
						m_IngredientCategories = sr.ReadLine().Replace(", ", ",").Split(',').ToList();
					
					if(line == "IngredientGroup")
						m_IngredientGroups = sr.ReadLine().Replace(", ", ",").Split(',').ToList();
					
				}
			}
		}
		
		public void Generate()
		{
			
			using(ISession ses = m_Context.OpenSession())
			{
				for(int i=0; i<ClientCount; ++i)
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						foreach(Kitchen kitchen in m_Kitchens)
						{
							ses.Save(kitchen);	
						}
						
						Client client = new Client();
						client.Name = String.Format("{0} {1}", 
						                RandString(StringType.Name),
						                RandString(StringType.Surname));
						client.Address = RandString(StringType.Address);
						client.Company = RandDecision(IsClientCompanyProbability) ? true : false;
						client.Credit = m_Random.Next(1000);
						client.Description = RandString(StringType.Other);
						client.MaxCredit = m_Random.Next(1000, 2000);
						client.NIP = RandString(StringType.Nip);
						client.PhoneNumber = RandString(StringType.PhoneNumber);
						client.Points = m_Random.Next(10000);
						
						ses.Save(client);
						tr.Commit();
					}	
				}
			
				int roomsCount = m_Random.Next(4)+1;
				
				for (int i=0; i<roomsCount; ++i)
				{
					uint width = 500 + (uint)m_Random.Next(40)*10;
					uint height = 500 + (uint)m_Random.Next(40)*10;;
					
					Room room = new Room();
					
					using(ITransaction tr = ses.BeginTransaction())
					{
						room.Tables = new List<Table>();
						room.Name = RandString(9);
						room.Height = (uint)height;
						room.Width = (uint)width;
						
						ses.Save(room);
						tr.Commit();
					}
					
					uint tablecount=1;
					for(uint w=25; w<width - 150; w+=125)
					{
						for(uint h=25; h<height - 150; h+=125)
						{
							using(ITransaction tr = ses.BeginTransaction())
							{
								Table table = new Table();
								table.Width = 75;
								table.Height = 75;
								table.X = w;
								table.Y = h;
								table.Type = TableType.Rect;
								table.Parent = room;
								table.Number = tablecount;
								table.Name = RandString(StringType.Other);
								
								room.Tables.Add(table);
								
								ses.Save(table);
								tr.Commit();
								
								tablecount++;
							}
						}
					}
				}
				

				IList<Client> clients = ses.Query<Client>().ToList();
				IList<Table> tables = ses.Query<Table>().ToList();
				
				for(int i=0; i<ReservationsCount; ++i)
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						Reservation reservation = new Reservation();
						reservation.AdditionalNotes = 
							RandString(StringType.Other);
						reservation.Date = RandomDate();
						reservation.ParentClient = clients[m_Random
							.Next(clients.Count-1)];
						reservation.ParentTable = tables[m_Random
							.Next(tables.Count-1)];
						
							ses.Save(reservation);
						tr.Commit();
					}
				}
				
				Tax tax = new Tax();
				using(ITransaction tr = ses.BeginTransaction())
				{
					Tax vatA = new Tax();
					Tax vatB = new Tax();
					Tax vatC = new Tax();
					Tax vatD = new Tax();
					Tax vatG = new Tax();
					vatA.Name = "Vat A";
					vatA.Code = 'A';
					vatB.Name = "Vat B";
					vatB.Code = 'B';
					vatC.Name = "Vat C";
					vatC.Code = 'C';
					vatD.Name = "Vat D";
					vatD.Code = 'D';
					vatG.Name = "Vat G";
					vatG.Code = 'G';
					
					vatA.PercentValue = 23; 
					vatB.PercentValue = 8; 
					vatC.PercentValue = 0; 
					vatD.PercentValue = 5; 
					vatG.PercentValue = 0; 
					
					ses.Save(vatA);
					ses.Save(vatB);
					ses.Save(vatC);
					ses.Save(vatD);
					ses.Save(vatG);
					
					tr.Commit();
					
					tax = vatA;
				}
				
				for(int i=0; i<UserCount; ++i)
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						SHA1 hash = new SHA1CryptoServiceProvider();
						User user = new User();
						user.Name = String.Format("{0}{1}", RandString(StringType.Name), m_Random.Next(99));
						user.FirstName = RandString(StringType.Name);
						user.LastName = RandString(StringType.Surname);
						user.Pin = m_Random.Next(9999).ToString();
						user.Type = (UserType)m_Random.Next(2);
					
						byte[] hashpass = hash.ComputeHash(UTF8Encoding.Default.GetBytes(DefaultPassword));
						user.Password = BitConverter.ToString(hashpass).Replace("-", "").ToLower();
						
						ses.Save(user);
						tr.Commit();
					}	
				}
				
				IList<User> users = ses.Query<User>().ToList();
				
				int start = int.Parse(m_Context.Configuration
					                      .Sections["company"]
					                      .Properties["openhour"]);
				int end = int.Parse(m_Context.Configuration
					                    .Sections["company"]
					                    .Properties["closehour"]);
				
				DateTime now = DateTime.Now.AddDays(-DaysCount);
				now = new DateTime(now.Year, now.Month, now.Day, start, 0, 0);
				
				for(;DateTime.Compare(now, DateTime.Now)<0; now = now.AddDays(1))
				{
					DateTime starthour = new DateTime(now.Year, now.Month, now.Day, 
					                             now.Hour, now.Minute, now.Second);
					DateTime endhour = starthour.AddHours(end-start);
					
					for(;DateTime.Compare(starthour, endhour)<0;
					    starthour = starthour.AddMinutes(m_Random.Next(30) ))
					{
						using(ITransaction tr = ses.BeginTransaction())
						{
							LogonHistory lhs = new LogonHistory();
							lhs.Timestamp = starthour.AddMinutes(1);
							lhs.Device = ses.Query<Device>().First();
							lhs.User = users[m_Random.Next(users.Count-1)];
							lhs.Action = LogonAction.Logon;
							
							starthour = starthour.AddMinutes(m_Random.Next(30)+1);
														
							LogonHistory lhe = new LogonHistory();
							lhe.Timestamp = starthour.AddMinutes(1);
							lhe.Device = ses.Query<Device>().First();
							lhe.User = lhs.User;
							lhe.Action = LogonAction.Logout;
							
							ses.Save(lhs);
							ses.Save(lhe);
							tr.Commit();
						}
					}
					
				}
				
				
				using(ITransaction tr = ses.BeginTransaction())
				{
					AddIngredientCategory(IngredientCategoryCount,
					                      ses.Get<IngredientCategory>
					                      (IngredientCategory.RootCategory), ses);
					tr.Commit();
				}
				
				using(ITransaction tr = ses.BeginTransaction())
				{
					AddDishCategory(DishCategoryCount, 
					                ses.Get<DishCategory>
					                (DishCategory.RootCategory), ses);
					tr.Commit();
				}
				for(int i=0; i<IngredientGroupCount; ++i)
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						IngredientGroup grp = new IngredientGroup();
						grp.Name = RandString(StringType.IngredientGroup);
						
						ses.Save(grp);
						tr.Commit();
					}
				}
				
				for(int i=0; i<DishGroupCount; ++i)
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						DishGroup grp = new DishGroup();
						grp.Name = RandString(StringType.DishGroup);
						
						ses.Save(grp);
						tr.Commit();
					}
				}
				

				IList<IngredientCategory> categories = ses.Query<IngredientCategory>().ToList();
				IList<IngredientGroup> groups = ses.Query<IngredientGroup>().ToList();
				
				for(int i=0; i<IngredientCount; ++i)
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						Ingredient ingredient = new Ingredient();
						ingredient.Name = RandString(StringType.Ingredient);
						ingredient.Unit = (UnitOfMeasure)m_Random.Next(5);
						ingredient.Quantity = (decimal)(m_Random.NextDouble() 
						                              * m_Random.Next(100));
						ingredient.Price = Math.Round((decimal)(m_Random.NextDouble() 
						                           * m_Random.Next(10)), 2);
						ingredient.MinimumQuantity = (decimal)(m_Random.NextDouble() 
						                                     * m_Random.Next(10));
						
						int [] grnums = new int[m_Random.Next(groups.Count/3)];
						
						for(int j=0; j < grnums.Length; ++j)
						{
							if(groups[j].Ingredients == null)
								groups[j].Ingredients = new List<Ingredient>();
							
							groups[j].Ingredients.Add(ingredient);
							
							//if(ingredient.Groups == null)
								//ingredient.Groups = new List<IngredientGroup>();
							
							//ingredient.Groups.Add(groups[grnums[j]]);
						}
						
						m_Random.Next(groups.Count-1);
						int catnum = m_Random.Next(categories.Count-1);
						
						//ingredient.Category = categories[catnum];
						//categories[catnum].Ingredients.Add(ingredient);

						ses.Save(ingredient);
						tr.Commit();
					}
				}

				IList<Ingredient> ingredients = ses.Query<Ingredient>().ToList();
					
				for(int i=0; i<DishCount; ++i)
				{
					using(ITransaction tr = ses.BeginTransaction())
					{
						Dish dish = new Dish();
						Recipe recipe = new Recipe();
						dish.Color = "white"; //XXX
						dish.PositionNumber = i;
						dish.Description = RandString(StringType.Other);
						dish.Name = RandString(StringType.Dish);
						dish.IsPizza = dish.Name.Contains("pizza") ? true : false;
						dish.Categories = new List<DishCategory>();
						dish.Groups = new List<DishGroup>();
						dish.Tax = tax;
						dish.Points = m_Random.Next(100);
						dish.Kitchen = ses.Get<Kitchen>(1L);
						recipe.Protips = RandString(StringType.Other);
						recipe.Ingredients = new List<RecipeIngredient>();
						
						int ingcount = m_Random.Next(9)+1;
						
						decimal ingsum = 1.0m;
						
						for(int j=0; j<ingcount; ++j)
						{
							RecipeIngredientIngredient ingredient = 
								new RecipeIngredientIngredient();
							ingredient.Ingredient = 
								ingredients[m_Random
									.Next(ingredients.Count)];
							ingredient.Optional = RandDecision(IsIngredientOptionalProbability) 
								? true : false;
							ingredient.Amount = (decimal)(m_Random.NextDouble() 
								* m_Random.Next(10));
							ingsum += ingredient.Ingredient.Price 
								* ingredient.Amount;
							
							recipe.Ingredients.Add(ingredient);
						}
						
						IList<DishCategory> dcategories = ses.Query<DishCategory>().ToList();
						IList<DishGroup> dgroups = ses.Query<DishGroup>().ToList();
						
						dish.Recipe = recipe;
						recipe.Dish = dish;
						
						dish.Price1 = ingsum*3/2;
						dish.Price2 = ingsum*5/2;
						
						dish.RebatePercent = m_Random.Next(20);
						dish.RebateAmount = (decimal)m_Random.NextDouble()/2 * dish.Price1;
						
						int [] grnums = new int[m_Random.Next(dgroups.Count/3)];
						
						for(int j=0; j < grnums.Length; ++j)
						{
							//if(dgroups[j].Dishes == null)
							//	dgroups[j].Dishes = new List<Dish>();
							//dgroups[j].Dishes.Add(dish);
							dish.Groups.Add(dgroups[j]);
						}
						m_Random.Next(dgroups.Count-1);
						int catnum = m_Random.Next(dcategories.Count-1);
						
						dish.Categories.Add(dcategories[catnum]);
						
						ses.Save(dish);
						ses.Save(recipe);
							
						tr.Commit();
					}
				}


				IList<Kitchen> kitchens = ses.Query<Kitchen>().ToList();
				IList<Dish> dishes = ses.Query<Dish>().ToList();
				
				int activeorder=0;
				for(DateTime t=DateTime.Now.AddDays(-DaysCount); 
				    !(DateTime.Compare(t, DateTime.Now.AddDays(1))>0); t=t.AddDays(1))
				{
					using(ITransaction tr = ses.BeginTransaction())
					{	
						DateTime hour = new DateTime(t.Year, t.Month, 
						                             t.Day, start, 0, 0);
						
						OutgoingInvoice outcomingInvoice = new OutgoingInvoice();
						outcomingInvoice.Number = RandString(4);
						outcomingInvoice.BankAccount = RandString(StringType.Nip);
						outcomingInvoice.BankName = RandString(6);
						outcomingInvoice.BuyerAddress = RandString(StringType.Address);
						outcomingInvoice.BuyerNip = RandString(StringType.Nip);
						outcomingInvoice.BuyerName = String.Format("{0} {1}", RandString(StringType.Name),
						                                           RandString(StringType.Surname));
						outcomingInvoice.IssueTime = new DateTime(t.Ticks);
						outcomingInvoice.SellTime = new DateTime(t.Ticks);
						outcomingInvoice.Components = new List<OutgoingInvoiceComponent>();
						ses.Save(outcomingInvoice);
						int ingCompCount = m_Random.Next(10);
						for(int i=0; i<ingCompCount; ++i)
						{
							OutgoingInvoiceComponent component = new OutgoingInvoiceComponent();
							component.Amount = (decimal)(m_Random.NextDouble()*m_Random.Next(10));
							component.IdOnInvoice = i+1;
							component.Name = RandString(StringType.Ingredient);
							component.Pkwiu = String.Format("{0}.{1}.{2}.{3}",
							                                m_Random.Next(10, 99),
							                                m_Random.Next(10, 99),
							                                m_Random.Next(10, 99),
							                                m_Random.Next(10, 99));
							component.NetUnitPrice = (decimal)(m_Random.NextDouble()*m_Random.Next(10));
							component.UnitOfMeasure = (UnitOfMeasure)m_Random.Next(3);
							component.VatPercent = tax.PercentValue;
							outcomingInvoice.Components.Add(component);
						}
						
						ses.SaveOrUpdate(outcomingInvoice);
						
						IncomingInvoice incomingInvoice = new IncomingInvoice();
						incomingInvoice.BankAccount = String.Format("{0} {1} {2} {3} {4} {5}",
							m_Random.Next(10, 99), m_Random.Next(1000, 9999), m_Random.Next(1000, 9999),
							m_Random.Next(1000, 9999), m_Random.Next(1000, 9999),m_Random.Next(1000, 9999));
						incomingInvoice.BankName = RandString(7);
						incomingInvoice.IssueTime = t;
						incomingInvoice.PaymentTime = t.AddMinutes(5);
						incomingInvoice.SellerAddress = RandString(StringType.Address);
						incomingInvoice.SellerName = RandString(6);
						incomingInvoice.Number = String.Format("{0}-{1}", RandString(2), RandString(4));
						incomingInvoice.PaymentMethod = (PaymentMethod)m_Random.Next(5);
						incomingInvoice.SellerNip = RandString(StringType.Nip);
						incomingInvoice.SellerPersonName = String.Format("{0} {1}", RandString(StringType.Name),
						                                          RandString(StringType.Surname));
						incomingInvoice.SellTime = t;
						incomingInvoice.Components = new List<IncomingInvoiceComponent>();
						ses.Save(incomingInvoice);
						int ingInCompCount = m_Random.Next(10);
						for(int i=0; i<ingInCompCount; ++i)
						{
							IncomingInvoiceComponent component = 
								new IncomingInvoiceComponent();
							component.Amount = (decimal)(m_Random.NextDouble()*m_Random.Next(100));
							component.IdOnInvoice = i;
							component.Ingredient = ingredients[m_Random
									.Next(ingredients.Count)];
							component.Invoice = incomingInvoice;
							component.NetUnitPrice = component.Ingredient.Price;
							component.Pkwiu = String.Format("{0}.{1}.{2}.{3}",
							                                m_Random.Next(10, 99),
							                                m_Random.Next(10, 99),
							                                m_Random.Next(10, 99),
							                                m_Random.Next(10, 99));
							component.VatPercent = tax.PercentValue;
							component.UnitOfMeasure = component.Ingredient.Unit;
							//component.NetValue = //(decimal)(m_Random.NextDouble()*m_Random.Next(100));
							incomingInvoice.Components.Add (component);
						}
						
						ses.SaveOrUpdate(incomingInvoice);
						
						Settlement settlement = new Settlement();
						settlement.Owner = users[m_Random.Next(users.Count -1)];
						settlement.UserBalances = new List<UserBalance>();
						settlement.From = hour.AddMilliseconds(1);
						settlement.To = hour.AddHours(end - start);
						
						if(t.Day == DateTime.Now.Day &&
						   t.Month == DateTime.Now.Month &&
						   t.Year == DateTime.Now.Year)
						{
							settlement.Status = SettlementStatus.Opened;
							ses.Save(settlement);
							SessionItem sessionItem = ses.Get<SessionItem>("CurrentSettlement");
							sessionItem.Value = BitConverter.GetBytes(settlement.Id);
							ses.Update(sessionItem);
						}
						else
							settlement.Status = SettlementStatus.Closed;
						
						
						int ordercnt = m_Random.Next(100) + 80;
						List<Order> orders = new List<Order>();
						for(int o=0; o<ordercnt; ++o)
						{
							Order order = new Order();
							order.Client = clients[m_Random.Next(clients
							                                     .Count -1)];
							order.Description = RandString(StringType
							                               .Other);
							order.Time = (hour.AddMinutes(m_Random.Next(59)))
								.AddHours(end-start);
											
							order.Entries = new List<OrderDish>();
														
							int dest = m_Random.Next(3);
							
							switch(dest)
							{
								case 0:
									DestinationDelivery delivery = 
										new DestinationDelivery();
									delivery.Address = 
										RandString(StringType.Address);
									delivery.DeliveryTime = hour.AddHours(
										m_Random.Next(end-start));
									
									order.Destination = delivery;
									ses.Save(delivery);
									
								break;
								case 1:
									DestinationOnSite site = 
										new DestinationOnSite();
									site.Table = 
										tables[m_Random
									       .Next(tables.Count -1)];
									site.Description = 
										RandString(StringType.Other);
									
									order.Description = RandString(StringType.Other);
									ses.Save(site);
								break;
								case 2:
									order.Destination = 
										new DestinationNone();
								break;	
							}
							
							if(DateTime.Compare(order.Time, DateTime.Now.AddDays(-1) )>0 &&
							   activeorder < 20)
							{
								activeorder ++;
								order.Status = (OrderStatus)(m_Random.Next(3));
							}
							else
							{
								if(RandDecision(IsOrderCancelledProbablity))
									order.Status = OrderStatus.Canceled;
								else
									order.Status = OrderStatus.Completed;
							}
							
							int orderdishcnt = m_Random.Next(10) + 1;
							
							for(int i=0; i<orderdishcnt; ++i)
							{
								order.Entries.Add(
									NewOrderDish(kitchens, dishes, 
								             ingredients, ses));
							}
							
							order.RebateAmount = RandDecision(IsRebateAmountProbability) ? 
								m_Random.Next(30) : 0;
							order.RebatePercent = RandDecision(IsRebatePercentProbability) ? 
								m_Random.Next((int)(order.TotalValue/5)) : 0;
							
							if(order.Status == OrderStatus.Completed) {
								order.Payments = new List<OrderPayment>();
								order.Payments.Add(new OrderPayment() { Method = (PaymentMethod)(m_Random.Next(3)), Amount = order.TotalValue });
							}
		
							
							order.Waiter = users[m_Random.Next(users.Count -1)];
							
							orders.Add(order);
							ses.Save(order);
						}
						
						foreach(User user in users)
							settlement.UserBalances.Add(
								SaveWaiterSettlement( orders.Where(
									p => p.Waiter.Equals(user)),
								user, settlement, hour.AddHours(end-start))
								);
						
						ses.Save(settlement);
						tr.Commit();
					}
				}
			}
		}
		
		private UserBalance SaveWaiterSettlement(IEnumerable<Order> orders, User user, Settlement settlement, DateTime to)
		{
			UserBalance userBalance = new UserBalance();
			userBalance.Balance = orders.Sum(p => p.TotalValue);
			userBalance.Owner = user;
			userBalance.Time = to;
			userBalance.Settlement = settlement;
			userBalance.Status = SettlementStatus.Closed;

			return userBalance;
		}
		
		private OrderDish NewOrderDish(IList<Kitchen> kitchens, IList<Dish> dishes, 
		                               IList<Ingredient> ingredients, ISession ses)
		{
			OrderDish orderDish = new OrderDish();
			orderDish.Amount = m_Random.Next(4)+1;
			orderDish.Direction = kitchens.Count > 0 ? kitchens[m_Random.Next(kitchens.Count -1)] :
				null;
			orderDish.Dish = dishes[m_Random.Next(dishes.Count-1)];
			orderDish.Modifications = new List<OrderDishModification>();
			
			if(RandDecision(IsAnyDishModyficationProbability))
			{
				int count = m_Random.Next(4)+1;
				
				for(int j=0; j<count; ++j)
				{
					OrderDishModification odm = new OrderDishModification();
					odm.Amount = m_Random.Next(4)+1;

					odm.Ingredient = orderDish.Dish.Recipe
						.Ingredients[m_Random.Next(orderDish.Dish.Recipe
						                           .Ingredients.Count-1)];

					
					odm.PriceChange = m_Random.Next((int)odm.Ingredient.Component.Price);
					orderDish.Modifications.Add(odm);
					
					ses.Save(odm);
				}
			}
			
			return orderDish;
		}
		
		private void AddIngredientCategory(int count, IngredientCategory parent, ISession ses)
		{
			for (int i=0; i<count; ++i)
			{
				IngredientCategory cat = new IngredientCategory();
				cat.Name = RandString(StringType.IngredientCategory);
				cat.Ingredients = new List<Ingredient>();
				
				if(RandDecision(IsNextCategoryProbability))
				{
					int subcount = m_Random.Next(count-i/2);
					AddIngredientCategory(subcount, parent, ses);
					i+=subcount;
				}
				
				cat.Parent = parent;
				ses.Save(cat);
			}
		}
		
		private void AddDishCategory(int count, DishCategory parent, ISession ses)
		{
			for (int i=0; i<count; ++i)
			{
				DishCategory cat = new DishCategory();
				cat.Name = RandString(StringType.DishCategory);
				cat.Color = "white";
					
				if(RandDecision(IsNextCategoryProbability))
				{
					int subcount = m_Random.Next(count-i/2);
					AddDishCategory(subcount, parent, ses);
					i+=subcount;
				}
				else
					cat.Parent = parent;
				
				ses.Save(cat);
			}
		}
		
		private enum StringType {
			Name, Surname, Address, Other, Nip, PhoneNumber, Dish, DishCategory, 
			DishGroup, Ingredient, IngredientCategory, IngredientGroup
		}
		
		private string RandString(StringType type)
		{
			string result = "";
			switch(type)
			{
			case StringType.Name:
				result = m_Names[m_Random.Next(m_Names.Count-1)];
			break;
			case StringType.Address:
				result = String.Format("ul. {0}, {1}/{2}\n {3}-{4} {5}",
				                       m_Streets[m_Random.Next(m_Streets.Count-1)],
				                       m_Random.Next(99),
				                       m_Random.Next(200),
				                       m_Random.Next(99),
				                       m_Random.Next(999),
				                       m_Cities[m_Random.Next(m_Cities.Count-1)]);
			break;
			case StringType.Surname:
				result = m_Names[m_Random.Next(m_Surnames.Count-1)];
			break;
			case StringType.Dish:
				result = m_DishNames[m_Random.Next(m_DishNames.Count-1)];
				//m_DishNames.Remove(result);
			break;
			case StringType.DishCategory:
				result = m_DishCategories[m_Random.Next(m_DishCategories.Count-1)];;
				//m_DishCategories.Remove(result);
			break;
			case StringType.DishGroup:
				result = m_DishGroups[m_Random.Next(m_DishGroups.Count-1)];
				//m_DishGroups.Remove(result);
			break;
			case StringType.Ingredient:
				result = m_IngredientNames[m_Random.Next(m_IngredientNames.Count-1)];
				//m_IngredientNames.Remove(result);	
			break;
			case StringType.IngredientGroup:
				result = m_IngredientGroups[m_Random.Next(m_IngredientGroups.Count-1)];
				//m_IngredientGroups.Remove(result);
			break;
			case StringType.IngredientCategory:
				result = m_IngredientCategories[m_Random.Next(
					m_IngredientCategories.Count-1)];
				//m_IngredientCategories.Remove(result);
			break;
			case StringType.PhoneNumber:
				result = String.Format("{0}-{1}-{2}-{3}", m_Random.Next(99), 
					m_Random.Next(999), m_Random.Next(999), m_Random.Next(999));
			break;
			case StringType.Nip:
				result = RandNip();
			break;
			case StringType.Other:
				for(int i=0; i<m_Random.Next(100); ++i)
					result += " " + m_DishNames[m_Random.Next(m_DishNames.Count-1)];
			break;
			}
			
			if(result == "")
				result = RandString(m_Random.Next(15));
			
			return result;
		}
		
		private string RandString(int size)
		{
		        StringBuilder builder = new StringBuilder();
			char ch;
			for (int i = 0; i < size; i++)
			{
			    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 
				* m_Random.NextDouble() + 65)));                 
			    builder.Append(ch);
			}
			
			return builder.ToString();
		}
		
		private string RandNip()
		{
			int[] weights = new int[] { 6, 5, 7, 2, 3, 4, 5, 6, 7 };
			int[] header = new int[9];
			int sum=0;
			
			for (int i=0; i<9; ++i)
			{
				header[i] = m_Random.Next(9);
				sum += header[i]*weights[i];
			}
			
			return String.Join("", header) + (sum%11).ToString();
		}
		
		private bool RandDecision(float probability)
		{
			if(m_Random.NextDouble() <= probability)
				return true;
			else
				return false;
		}
		
		private DateTime RandomDate()
		{
			return new DateTime(DateTime.Now.Year, m_Random.Next(11) + 
			                         1, m_Random.Next(27)+1, m_Random.Next(23), 
			                         m_Random.Next(59), m_Random.Next(59));  
		}
		public string AdminPassword;
		public string DefaultPassword;
		
		public IList<Kitchen> m_Kitchens;
		
		public int ClientCount;
		public int TaxCount;
		public int UserCount;
		public int IngredientCount;
		public int ReservationsCount;
		public int IngredientCategoryCount;
		public int DishCategoryCount;
		public int IngredientGroupCount;
		public int DishGroupCount;
		public int DishCount;
		public int DaysCount;
		
		public float IsClientCompanyProbability;
		public float IsIngredientOptionalProbability;
		public float IsRebateAmountProbability;
		public float IsRebatePercentProbability;
		public float IsAnyDishModyficationProbability;
		public float IsNextCategoryProbability;
		public float IsOrderCancelledProbablity;
	
		private Context m_Context;
		
		private IList<String> m_DishNames;
		private IList<String> m_DishCategories;
		private IList<String> m_DishGroups;
		private IList<String> m_IngredientNames;
		private IList<String> m_IngredientCategories;
		private IList<String> m_IngredientGroups;
		private IList<String> m_Cities;
		private IList<String> m_Streets;
		private IList<String> m_Names;
		private IList<String> m_Surnames;
		
		private static Random m_Random = new Random((int)DateTime.Now.Ticks);
	}
}

