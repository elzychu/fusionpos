using System;
using System.Linq;
using FusionPOS;
using FusionPOS.Config;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;

namespace SystemConfig
{
	public class NetworkSetupForm: Form
	{
		public NetworkSetupForm(ConfigContext ctx): base(ctx)
		{
			Widgets.FormTable table = new Widgets.FormTable(8, 160);
			table.RowSpacing = 10;
		
			ConfigNode network = Config.Sections["network"];
			ConfigNode mainif = network.Children["interfaces"].Children[network.Properties["main-interface"]];
			ConfigNode dns = network.Children["dns-servers"];
			
			table.PackElement(
			    new Widgets.Label("Interfejs:"), 
			    new Widgets.Label(string.Format("<b>{0}</b>", 
			        network.Properties["main-interface"])), 0, false);
			
			m_IPAddress = new IpEntry(mainif.Properties["address"], delegate (string value) {
				mainif.Properties["address"] = value;	
			});
			
			table.PackElement(new Widgets.Label("Adres IP:"), m_IPAddress, 1, false);
			
			m_Netmask = new IpEntry(mainif.Properties["netmask"], delegate (string value) {
				mainif.Properties["netmask"] = value;
			});
			table.PackElement(new Widgets.Label("Maska:"), m_Netmask, 2, false);
			
			m_Gateway = new IpEntry(network.Properties["default-route"], delegate (string value) { 
				network.Properties["default-route"] = value;
			});
			table.PackElement(new Widgets.Label("Brama domyślna:"), m_Gateway, 3, false);
			
			m_EtherAddress = new TextBox(mainif.Properties["hardware-address"], true, delegate (string value) {
				mainif.Properties["hardware-address"] = value;
			});
			table.PackElement(new Widgets.Label("Adres MAC:"), m_EtherAddress, 4, false);
		
			m_DNS1 = new IpEntry(dns.Properties["first"], delegate (string value) {
				dns.Properties["first"] = value;
			});
			table.PackElement(new Widgets.Label("Serwer DNS 1:"), m_DNS1, 5, false);

			m_DNS2 = new IpEntry(dns.Properties["second"], delegate (string value) {
				dns.Properties["second"] = value;
			});
			table.PackElement(new Widgets.Label("Serwer DNS 2:"), m_DNS2, 6, false);
		
			table.AttachExpand(new Widgets.Button("Pobierz adres automatycznie"), 1, 2, 7, 8, true, false);
			
			Add(table);
			ShowAll();
		}
		
		private IpEntry m_IPAddress;
		private IpEntry m_Netmask;
		private IpEntry m_Gateway;
		private TextBox m_EtherAddress;
		private IpEntry m_DNS1;
		private IpEntry m_DNS2;	
	}
}

