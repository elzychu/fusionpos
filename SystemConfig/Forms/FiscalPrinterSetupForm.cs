using System;
using FusionPOS;
using System.Collections;
using System.Collections.Generic;
using FusionPOS.Config;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;
using FusionPOS.Drivers;
using System.IO.Ports;
using System.IO;

namespace SystemConfig
{
	public class FiscalPrinterSetupForm: Form
	{
		public FiscalPrinterSetupForm(ConfigContext ctx): base(ctx)
		{
			Widgets.FormTable table = new Widgets.FormTable(5, 160);
			table.RowSpacing = 10;
			Gtk.TreeIter iter;
			var portValues = new List<string>() { "ttyS0", "ttyS1", "ttyUSB0", "ttyUSB1" };
			m_Port = new ComboBox(portValues.ToArray(), delegate(string value) { 
				Config.Sections["fiscal"].Properties["port"] = String.Format("serial:/dev/{0}", value);
			});
			m_Port.Model.IterNthChild (out iter, portValues.IndexOf(
			    Config.Sections["fiscal"].Properties["port"].Substring(12)));
			m_Port.SetActiveIter(iter);
			
			table.PackElement(new Widgets.Label("Port drukarki:"), m_Port, 0, false);
			
			var driversValues = new List<string>() { "POSNET", "Novitus" };
			Dictionary<string, string> printers = new Dictionary<string, string>();
			printers.Add("POSNET", "FusionPOS.Drivers.PosnetFiscalPrinter");
			printers.Add("Novitus", "FusionPOS.Drivers.NovitusFiscalPrinter");
			
			m_DriverType = new ComboBox(driversValues.ToArray(), delegate(string value) { 
				Config.Sections["fiscal"].Properties["driver"] = printers[value];
			});
			
			string printerName = "";
			IEnumerator e = printers.GetEnumerator();
			while(e.MoveNext())
			{
				KeyValuePair<string, string> pair = (KeyValuePair<string, string>)e.Current;
				if( pair.Value == Config.Sections["fiscal"].Properties["driver"])
					printerName = pair.Key; 
			}
			    ;
			m_DriverType.Model.IterNthChild (out iter, driversValues.IndexOf(printerName));
			m_DriverType.SetActiveIter(iter);
			
			table.PackElement(new Widgets.Label("Sterownik:"), m_DriverType, 1, false);
			
			var speedValues = new List<string>() {"2400", "4800", "9600", "19200", 
			    "38400", "57600", "115200"};
			m_Speed = new ComboBox(speedValues.ToArray() , delegate(string value) { 
				Config.Sections["fiscal"].Children["config"].Properties["port-speed"] = value;
			});
                        m_Speed.Model.IterNthChild (out iter, speedValues.IndexOf(Config.Sections["fiscal"]
			    .Children["config"].Properties["port-speed"]));
			
                        m_Speed.SetActiveIter (iter);
			
			table.PackElement(new Widgets.Label("Prędkość portu:"), m_Speed, 2, false);
			
			m_ScreenText = new TextView(delegate(string value) { 
				Config.Sections["fiscal"].Children["config"].Properties["screen-text"] = value;
			});
			m_ScreenText.Text = Config.Sections["fiscal"].Children["config"].Properties["screen-text"];
			
			table.PackElement(new Widgets.Label("Tekst na wyświetlaczu:"), m_ScreenText, 3, false);
			Widgets.Button button = new Widgets.Button("Przetestuj połączenie");
			button.Clicked += (sender, f) => {
				Type printerType = Type.GetType(string.Format("{0}, Drivers", 
					Config.Sections["fiscal"].Properties["driver"]));
				IFiscalPrinterDriver fiscalPrinter = (IFiscalPrinterDriver)Activator.CreateInstance(printerType);
				
				SerialPort port = new SerialPort(Config.Sections["fiscal"].Properties["port"]);
				port.BaudRate = int.Parse(Config.Sections["fiscal"].Children["config"]
				    .Properties["port-speed"]);
				port.Handshake = Handshake.XOnXOff;
				port.ReadTimeout = 10000;
				
				try {
					port.Open();
					try {
						fiscalPrinter.Configure(new DriverConfiguration { Port = port });
						fiscalPrinter.Initialize();
						fiscalPrinter.DisplayText(Config.Sections["fiscal"].Properties["screen-text"], 1);
						MessageBox.Popup("FusionPOS", "Połączono pomyślnie z drukarką fiskalną");
					} catch (Exception g) {
						Gtk.Application.Invoke((sender_, args) => {
							MessageBox.Popup("Błąd", 
							    "Nie można połączyć się z drukarką fiskalną.\n");
						});
					}
				} catch(Exception g) {
					MessageBox.Popup("Błąd", "Drukarka nie podłączona");	
				}
			};
			
			table.AttachExpand(button, 1, 2, 4, 5, true, false);
			
			Add(table);
			ShowAll();
		}
		
		private Widgets.ComboBox m_Port;
		private Widgets.ComboBox m_DriverType;
		private Widgets.ComboBox m_Speed;
		private Widgets.TextView m_ScreenText;
	}
}

