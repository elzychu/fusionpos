using System;
using System.Security.Cryptography;
using System.Text;
using FusionPOS.Config;
using Widgets = FusionPOS.Widgets;
using FusionPOS;
using System.Linq;

namespace SystemConfig
{
	public class AdminPassword: Form
	{
		public AdminPassword (ConfigContext config): 
			base(config)
		{
			
			string pass = Config.Sections["system"].Properties["password"];
			Widgets.FormTable table = new Widgets.FormTable(5, 160);
			table.RowSpacing = 10;
			
			Widgets.Label password = new Widgets.Label("Aktualne hasło");
			Widgets.Label newPassword = new Widgets.Label("Nowe hasło");
			Widgets.Label repeatNewPass = new Widgets.Label("Powtórz nowe hasło");
			
			TextBox oldPassword = new TextBox("", false, delegate(string value) { });
			TextBox newPassBox = new TextBox("", false, delegate(string value) { });
			TextBox repeatNewPassBox = new TextBox("", false, delegate(string value) { 
				SHA1 csp = new SHA1CryptoServiceProvider();
				
				//string passA = u8e.GetString(csp.ComputeHash(
				    //u8e.GetBytes(oldPassword.Text)));
				byte[] hash = csp.ComputeHash(UTF8Encoding.Default.GetBytes(oldPassword.Text));
				string passA = string.Concat(hash.Select(i => i.ToString("x2")));
				string newPass = string.Concat(csp.ComputeHash(UTF8Encoding.Default
				    .GetBytes(newPassBox.Text)).Select(i => i.ToString("x2")));
				if(passA == pass)
				{
					if(newPassBox.Text == repeatNewPass.Text)
					{
						Config.Sections["system"].Properties["password"] = newPass;
					}
					else
					{
						MessageBox.Popup("FusionPOS", 
						    "Wpisane nowe hasło nie zgadza się z powtórzonym");
					}
				}
				else
				{
					MessageBox.Popup("FusionPOS",
					    "Niepoprawne stare hasło");
				}
				
			});
	
			table.PackElement(password, oldPassword, 0, false);
			table.PackElement(newPassword, newPassBox, 1, false);
			table.PackElement(repeatNewPass, repeatNewPassBox, 2, false);
			
			Add (table);
			ShowAll();
		}
	}
}

