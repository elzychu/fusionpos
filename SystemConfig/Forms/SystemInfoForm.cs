using System;
using FusionPOS;
using FusionPOS.Config;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;
using System.Collections.Generic;


namespace SystemConfig
{
	public class SystemInfoForm: Form
	{
		public SystemInfoForm(ConfigContext ctx): base(ctx)
		{
			Widgets.FormTable table = new Widgets.FormTable(2, 160);
			
			
			TextBox hostnameEntry = 
			    new TextBox(Config.Sections["system"].Properties["hostname"], true, delegate(string value) { 
			            Config.Sections["system"].Properties["hostname"] = value;
				});
			var languages = new List<string>(){ "PL", "GR", "UA" };
			
			ComboBox languageEntry = 
			    new ComboBox(languages.ToArray(), delegate(string value) {
				    Config.Sections["system"].Properties["lang"] = value;
				});
			Gtk.TreeIter i;
			languageEntry.Model.IterNthChild(out i,languages
			    .IndexOf(Config.Sections["system"].Properties["lang"]));
			languageEntry.SetActiveIter(i);
			table.PackElement(new Widgets.Label("Nazwa stanowiska:"), hostnameEntry, 0, false);
			table.PackElement(new Widgets.Label("Język:"), languageEntry, 1, false);
			
			Add(table);
			ShowAll();
		}
	}
}

