using System;
using FusionPOS;
using FusionPOS.Config;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;
using System.Net;

namespace SystemConfig
{
	public delegate void Save(string value);
	
	public interface ConfigWidget
	{
		void IfChangedSave();
	}
	
	public class TextBox: Widgets.TextBox, ConfigWidget 
	{
		public TextBox(string text, bool visibility, Save save): base(text, visibility)
		{ 
			m_Save = save;
		}
		
		protected override void OnChanged ()
		{
			base.OnChanged ();
			m_Changed = true;
		}
		
		public void IfChangedSave()
		{
			if(m_Changed)
				m_Save(Text);
		}
		
		private bool m_Changed = false;
		private Save m_Save;
	}
	
	public class ComboBox: Widgets.ComboBox, ConfigWidget
	{
		public ComboBox(String [] list, Save save): base(list)
		{
			m_Save = save;
		}
		
		protected override void OnChanged ()
		{
			base.OnChanged ();
			m_Changed = true;
		}
		
		public void IfChangedSave ()
		{			
			if(m_Changed)
			{
				Gtk.TreeIter i;
				GetActiveIter(out i);
				m_Save((string)Model.GetValue(i, 0));
			}
		}
		
		private bool m_Changed = false;
		private Save m_Save;
	}
	
	public class TextView: Widgets.TextView, ConfigWidget
	{
		public TextView(Save save)
		{
			m_Save = save;
		}
		
		protected override bool OnFocused (Gtk.DirectionType direction)
		{
			return base.OnFocused (direction);
			m_Changed = true;
		}
		
		public void IfChangedSave ()
		{
			if(m_Changed)
				m_Save(this.Text);
		}
		
		private bool m_Changed = false;
		private Save m_Save;
	}
	
	class IpEntry: Widgets.IpEntry, ConfigWidget {
		public IpEntry(string address, Save save): base(address)
		{
			m_Save = save;
			m_OctetA.Changed += OnChange;
			m_OctetB.Changed += OnChange;
			m_OctetC.Changed += OnChange;
			m_OctetD.Changed += OnChange;
		}
		
		protected void OnChange(object sender, EventArgs e) 
		{
			m_Changed = true;	
		}
		
		public void IfChangedSave ()
		{
			if(m_Changed)
			{
				IPAddress address = GetIpAddr();
				m_Save(address.ToString());
			}
		}
		
		private bool m_Changed = false;
		
		private Save m_Save;
	}
	
	public abstract class Form: BaseForm
	{
		public Form(ConfigContext config): base()
		{
			Config = config;	
		}
		
		protected ConfigContext Config
		{
			get; set;
		}
		
		public override void OnLeaving ()
		{
			base.OnLeaving ();
			CheckAndSaveContent(this);
		}
		
		private void CheckAndSaveContent(Gtk.Widget widget)
		{
			if(widget is ConfigWidget)
				((ConfigWidget)widget).IfChangedSave();
			
			if(widget is Gtk.Container)
			{
				foreach(Gtk.Widget current in (widget as Gtk.Container).AllChildren)
					CheckAndSaveContent(current);
			}		
		}
	}
}

