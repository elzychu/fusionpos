using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Gtk;
using FusionPOS;
using FusionPOS.Logging;
using FusionPOS.Config;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;

public class MainWindow: BaseWindow
{	
	public MainWindow(): base()
	{
		Gtk.HBox hbox = new Gtk.HBox();
		Gtk.TreeView treeView = new Gtk.TreeView();
		Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
		
		buttons.AddRightImageButton("Zapisz", "save", delegate(object o, EventArgs args) {
			m_CurrentForm.OnLeaving();
			
			m_Config.WriteFile("/Configuration/Application.xml", new string[] {
				"application", "database", "server", "fiscal", "services"	
			});
			
			m_Config.WriteFile("/Configuration/System.xml", new string[] {
				"system", "network", "storage", "graphics"	
			});
			
			MessageBox.Popup("Konfiguracja", "Zapisano konfigurację", false);
		});
		
		buttons.AddRightImageButton("Zakończ", "exit", delegate(object sender, EventArgs e) {
			Application.Quit();	
		});
		
		m_FormTitle = new Widgets.WindowLabel("", "Inverted,BigFont");
		m_FormTitle.SetSizeRequest(-1, 80);
		m_FormTitle.InverseColors();
		
		m_FormsStore = new Gtk.TreeStore(typeof(string), typeof(Type), typeof(string));
		treeView.AppendColumn(new Gtk.TreeViewColumn("Nazwa", new Gtk.CellRendererText(), "text", 0));
		treeView.Model = m_FormsStore;
		treeView.CursorChanged += FormSelected;
		treeView.SetSizeRequest(300, -1);
		
		hbox.PackStart(treeView, false, false, 0);
		hbox.PackStart(FormPlacement, true, true, 0);
		VBox.PackStart(m_FormTitle, false, false, 0);
		VBox.PackStart(hbox, true, true, 0);
		VBox.PackStart(new Gtk.Separator(Orientation.Horizontal), false, false, 0);
		VBox.PackStart(buttons, false, false, 0);
		SetDefaultSize(1024, 768);
		SetSizeRequest(1024, 768);
		Resizable = false;
		
		m_FormTitle.Text = "Informacje o systemie";		
		ShowAll();
		Initialize();
		ReadLayout();
		AddForm(new SystemConfig.SystemInfoForm(m_Config));
		Fullscreen();
	}
	
	private void Initialize()
	{
		/* Initialize logger */
		m_Logger = new LoggerContext();
		m_Logger.AddOutputStream(Console.Out);
		m_Logger.Log("SETTINGS", LogPriority.Info, "Loaded");
		
		/* Read XML configuration */
		m_Config = new ConfigContext();
		m_Config.Logger = new Logger(m_Logger, "CONF");
		m_Config.ReadFile("/Configuration/Application.xml");
		m_Config.ReadFile("/Configuration/System.xml");
	}

	private void FormSelected(object sender, EventArgs e)
	{
		Gtk.TreeView tv = sender as TreeView;
		Gtk.TreeModel model;
		Gtk.TreeIter iter;
		
		tv.Selection.GetSelected(out model, out iter);
		
		if (model.GetValue(iter, 1) == null)
		{
			string name = model.GetValue(iter, 2) as string;
			if(name == "Browser.exe")
				ProcessLauncher.Launch("Browser");
			//if(name == "Logs")
		
			//if(name == "Terminal")
			
			return;
		}
		
		m_FormTitle.Text = model.GetValue(iter, 0) as string;
		

		AddForm(Activator.CreateInstance(model.GetValue(iter, 1) as Type, m_Config) as BaseForm);
	}

	private void ReadLayout()
	{
		ConfigContext layout = new ConfigContext();
		layout.ReadFile("/home/pos/FusionPOS/SystemConfig/Configuration/Layout.xml");
		ReadLayoutSection(layout.Sections["layout"], null);
	}
	
	private void ReadLayoutSection(ConfigNode node, Gtk.TreeIter? iter)
	{
		foreach (KeyValuePair<string, ConfigNode> i in node.Children) {
			Gtk.TreeIter it = m_FormsStore.AppendValuesRef(iter, i.Key, null);
			ReadLayoutSection(i.Value, it);
		}
		
		foreach (KeyValuePair<string, string> i in node.Properties) {
			Type form = Assembly.GetEntryAssembly().GetType(i.Value);
			m_FormsStore.AppendValuesRef(iter, i.Key, form, i.Value);
		}
	}
	
	private LoggerContext m_Logger;
	private ConfigContext m_Config;
	private Widgets.WindowLabel m_FormTitle;
	private Gtk.TreeStore m_FormsStore;
}
