using System;
using Gtk;

namespace SystemConfig
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Gtk.CssProvider css = new Gtk.CssProvider();
			Application.Init();
			css.LoadFromPath("/home/pos/FusionPOS/Interface.rc");
			Gtk.StyleContext.AddProviderForScreen(Gdk.Screen.Default, css, 600);
			MainWindow mainWindow = new MainWindow();
			mainWindow.Show();
			Application.Run();
		}
	}
}
