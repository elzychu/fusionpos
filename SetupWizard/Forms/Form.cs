using System;
using FusionPOS;
using FusionPOS.Config;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;

namespace SetupWizard
{
	public abstract class Form: BaseForm
	{
		public Form(ConfigContext config): base()
		{
			Config = config;	
		}
		
		protected ConfigContext Config
		{
			get; set;
		}
	}
}

