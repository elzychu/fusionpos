using System;
using FusionPOS.Config;
using FusionPOS.Widgets;

namespace SetupWizard
{
	public class ServerSetupForm: Form
	{
		public ServerSetupForm(ConfigContext config): base(config)
		{
			Table table = new Table(3, 2, false) {
				BorderWidth = 20,
				ColumnSpacing = 10,
				RowSpacing = 10
			};
			
			Gtk.Alignment standaloneAlign = new Gtk.Alignment(0f, 0.5f, 0f, 0f);
			Gtk.Alignment externalAlign = new Gtk.Alignment(0f, 0.5f, 0f, 0f);
			
			m_Standalone = ImageButtonFactory.ImageToggleButton(string.Empty, "clients");
			m_Standalone.SetSizeRequest(180, -1);
			m_ExternalServer = ImageButtonFactory.ImageToggleButton(string.Empty, "configuration");
			m_ExternalServer.SetSizeRequest(180, -1);
			m_ExternalServer.Sensitive = false;
			
			table.AttachExpand(new Label("Wybierz rodzaj instalacji:"), 0, 2, 0, 1, true, true);
			table.AttachExpand(m_Standalone, 0, 1, 1, 2, false, true);
			table.AttachExpand(m_ExternalServer, 0, 1, 2, 3, false, true);
			
			standaloneAlign.Add(new Label(
			    "Pojedyncze stanowisko POS. Wybierz ten rodzaj instalacji, " +
			    "jeśli twoje piewsze i jedyne stanowisko FusionPOS w lokalu oraz " +
			    "nie posiadasz urządzenia FusionServer."));
			externalAlign.Add(new Label("Sieciowe stanowisko POS. Wybierz ten " +
			    "rodzaj instalacji, jeśli posiadasz już FusionServer."));
			
			table.AttachExpand(standaloneAlign, 1, 2, 1, 2, true, true);
			table.AttachExpand(externalAlign, 1, 2, 2, 3, true, true);
			Add(table);
			ShowAll();
		}

		private Gtk.ToggleButton m_Standalone;
		private Gtk.ToggleButton m_ExternalServer;	
	}
}

