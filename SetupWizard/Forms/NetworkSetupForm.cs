using System;
using FusionPOS.Config;
using FusionPOS.Widgets;

namespace SetupWizard
{
	public class NetworkSetupForm: Form
	{
		public NetworkSetupForm(ConfigContext config): base(config)
		{
			FormTable table = new FormTable(6, 160);
			table.RowSpacing = 10;
			
			table.AttachExpand(new Label("Podaj dane do konfiguracji sieci, etc"), 0, 2, 0, 1, true, true);

			m_IPAddress = new IpEntry();
			table.PackElement(new Label("Adres IP:"), m_IPAddress, 1, false);
			
			m_Netmask = new IpEntry();
			table.PackElement(new Label("Maska:"), m_Netmask, 2, false);
			
			m_Gateway = new IpEntry();
			table.PackElement(new Label("Brama domyślna:"), m_Gateway, 3, false);
			
			m_EtherAddress = new TextBox();
			table.PackElement(new Label("Adres MAC:"), m_EtherAddress, 4, false);
		
			m_DNS1 = new IpEntry();
			table.PackElement(new Label("Serwer DNS 1:"), m_DNS1, 5, false);

			m_DNS2 = new IpEntry();
			table.PackElement(new Label("Serwer DNS 2:"), m_DNS2, 6, false);
		
			Add(table);
			ShowAll();
		}
		
		private IpEntry m_IPAddress;
		private IpEntry m_Netmask;
		private IpEntry m_Gateway;
		private IpEntry m_DNS1;
		private IpEntry m_DNS2;
		private TextBox m_EtherAddress;
	}
}

