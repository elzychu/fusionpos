using System;
using FusionPOS.Config;
using FusionPOS.Widgets;

namespace SetupWizard
{
	public class CompanyDataForm: Form
	{
		public CompanyDataForm(ConfigContext config): base(config)
		{
			FormTable table = new FormTable(6, 160);
			table.RowSpacing = 10;
			
			table.AttachExpand(new Label("Podaj dane firmy, etc."), 0, 2, 0, 1, true, true);
			table.PackElement(new Label("Nazwa firmy:"), new TextBox(), 1, false);
			table.PackElement(new Label("Adres:"), new TextView(), 2, true);
			table.PackElement(new Label("NIP:"), new TextBox(), 3, false);
			table.PackElement(new Label("Telefon:"), new TextBox(), 4, false);
			table.PackElement(new Label("Godzina otwarcia:"), new TextBox(), 5, false);
			Add(table);
			ShowAll();
		}
	}
}

