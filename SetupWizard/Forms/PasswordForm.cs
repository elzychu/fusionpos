using System;
using FusionPOS.Config;
using FusionPOS.Widgets;

namespace SetupWizard
{
	public class PasswordForm: Form
	{
		public PasswordForm(ConfigContext config): base(config)
		{
			FormTable table = new FormTable(8, 160);

			m_AdminPassword = new TextBox();
			m_AdminPasswordRepeat = new TextBox();
			m_FirstLastNames = new TextBox();
			m_Username = new TextBox();
			m_PIN = new TextBox();
			m_PINRepeat = new TextBox();

			table.AttachExpand(new Label("Podaj hasło dostępu dla administratora oraz utwórz pierwsze konto kierownika zmiany"), 0, 2, 0, 1, true, true);

			table.PackElement(new Label("Hasło administratora:"), m_AdminPassword, 1, false);
			table.PackElement(new Label("Powtórz administratora:"), m_AdminPasswordRepeat, 2, false);
			table.AttachExpand(new Label("Dane pierwszego kierownika zmiany:"), 0, 2, 3, 4, true, true);
			table.PackElement(new Label("Imię i nazwisko:"), m_FirstLastNames, 4, false);
			table.PackElement(new Label("Nawa użytkownika :"), m_Username, 5, false);
			table.PackElement(new Label("PIN:"), m_PIN, 6, false);
			table.PackElement(new Label("Powtórz PIN :"), m_PINRepeat, 7, false);

			Add(table);
			ShowAll();
		}

		private TextBox m_AdminPassword;
		private TextBox m_AdminPasswordRepeat;
		private TextBox m_FirstLastNames;
		private TextBox m_Username;
		private TextBox m_PIN;
		private TextBox m_PINRepeat;
	}
}

