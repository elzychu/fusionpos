using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Gtk;
using FusionPOS;
using FusionPOS.Logging;
using FusionPOS.Config;
using FusionPOS.Widgets;
using Widgets = FusionPOS.Widgets;

namespace SetupWizard {
	public class MainWindow: BaseWindow
	{	
		public MainWindow(): base()
		{
			Widgets.ButtonsBar buttons = new Widgets.ButtonsBar();
			
			buttons.AddRightButton("Wstecz", delegate(object o, EventArgs args) {
				if (m_FormNumber == 1)
					return;

				SwitchTo(--m_FormNumber);
			});
			
			buttons.AddRightButton("Dalej", delegate(object o, EventArgs args) {
				if (m_FormNumber == m_FormCount)
					return;

				SwitchTo(++m_FormNumber);
			});
			
			m_FormTitle = new Widgets.WindowLabel("", "Inverted,BigFont");
			m_FormTitle.SetSizeRequest(-1, 80);
			m_FormTitle.InverseColors();
			
			VBox.PackFill(m_FormTitle);
			VBox.PackExpand(FormPlacement);
			VBox.PackFill(buttons);
			SetDefaultSize(1024, 768);
			SetSizeRequest(1024, 768);
			Resizable = false;
			
			m_FormTitle.Text = "Witaj w FusionPOS";		
			ShowAll();
			Initialize();
			Fullscreen();
		}
		
		private void Initialize()
		{
			/* Initialize logger */
			m_Logger = new LoggerContext();
			m_Logger.AddOutputStream(Console.Out);
			m_Logger.Log("SETTINGS", LogPriority.Info, "Loaded");
			
			/* Read XML configuration */
			m_Config = new ConfigContext();
			m_Config.Logger = new Logger(m_Logger, "CONF");
			m_Config.ReadFile("Workflow.xml");

			m_FormNumber = 1;
			m_FormCount = m_Config.Sections["workflow"].Children.Count;
			SwitchTo(m_FormNumber);
		}

		private void SwitchTo(int number)
		{
			ConfigNode node = m_Config.Sections["workflow"].Children[number.ToString()];
			Type formType = Assembly.GetEntryAssembly().GetType(node.Properties["form"]);
			AddForm(Activator.CreateInstance(formType, m_Config) as BaseForm);
			m_FormTitle.Text = node.Properties["title"];
		}
	
		private int m_FormNumber;
		private int m_FormCount;
		private LoggerContext m_Logger;
		private ConfigContext m_Config;
		private Widgets.WindowLabel m_FormTitle;
	}
}